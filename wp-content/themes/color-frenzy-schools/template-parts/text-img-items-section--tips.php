<?php
$textImageTitle = get_field('text-image__title');
$textImageItems = 'text-image__items';
$textImageTipsItems = 'text-image__tips-items';
?>
<section class="text-img-items">
    <div class="container">
        <div class="text-img-items__head">
            <h2>
                <?= $textImageTitle; ?>
            </h2>
        </div>
        <?php while(have_rows($textImageItems)): the_row();
            $bgSizeCond = get_sub_field('bg-cond');
            $textImageItemBg = get_sub_field('text_background');
            $textImageItemText = get_sub_field('text');
            $textImageItemImg = get_sub_field('image');

            if(!$bgSizeCond):
                ?>
                <div class=" text-img-item <?= $bgSizeCond ? ' text-img-item--message-bg-small ' : ' text-img-item--message-bg '; ?> ">
                    <div class="text-img-item__text">
                        <div class="text-img-item__text-inner">
                            <?= $textImageItemText; ?>
                        </div>
                        <div class="text-img-item__text-bg">
                            <?= wp_get_attachment_image($textImageItemBg['id'], 'full'); ?>
                        </div>
                    </div>
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($textImageItemImg['id'], 'full'); ?>
                    </div>
                </div>
            <?php else: ?>
                <div class=" text-img-item <?= $bgSizeCond ? ' text-img-item--message-bg-small ' : ' text-img-item--message-bg '; ?> ">
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($textImageItemImg['id'], 'full'); ?>
                    </div>
                    <div class="text-img-item__text">
                        <div class="text-img-item__text-inner">
                            <?= $textImageItemText; ?>
                        </div>
                        <div class="text-img-item__text-bg">
                            <?= wp_get_attachment_image($textImageItemBg['id'], 'full'); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
        <?php if(have_rows($textImageTipsItems)): ?>
            <div class="text-items slider-section__text-items text-items--colors-head">
                <?php while(have_rows($textImageTipsItems)): the_row();
                    $tipsItemText = get_sub_field('text');
                    ?>
                    <div class="text-items__item text-item">
                        <?= $tipsItemText; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>
