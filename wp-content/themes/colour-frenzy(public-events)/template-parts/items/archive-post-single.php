<?php
$postTitle = get_the_title();
$postLink = get_the_permalink();
$postCategory = get_the_category();
$postAuthor = get_the_author();
$postDate = get_the_date();
$postContent = get_the_content();
$postImage = get_the_post_thumbnail(null, array(500, 400));
?>
<div class="post-preview-item">
    <div class="post-preview-item__img">
        <a href="<?= $postLink; ?>"  class="btn">
            Continue reading
        </a>

        <?= $postImage; ?>

        <div class="post-preview-item__category">
            <?php
            $categoryLength = intval(count($postCategory));
            for($counter = 0; $counter < $categoryLength; $counter++):
                echo $counter == $categoryLength - 1 ? $postCategory[$counter]->name : $postCategory[$counter]->name . ', ';
            endfor;
            ?>
        </div>
    </div>
    <h4>
        <?= $postTitle; ?>
    </h4>
    <div class="post-preview-item__info">
        <div class="icon-text-info">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/person.svg" alt="person">
            Posted by <span><?= $postAuthor; ?></span>
        </div>
        <div class="icon-text-info">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/calendar.svg" alt="image">
            <?= $postDate; ?>
        </div>
    </div>
    <p>
        <?= wp_trim_words( $postContent, 30, '...' ); ?>
    </p>
</div>