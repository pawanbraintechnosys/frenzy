<?php
?>


<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label class="search-input blog__search-input">
        <input type="text" placeholder="Search Blog Post" value="<?= get_search_query(); ?>" name="s"/>
        <button type="submit">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/content-img/search.svg" alt="search">
        </button>
    </label>
</form>