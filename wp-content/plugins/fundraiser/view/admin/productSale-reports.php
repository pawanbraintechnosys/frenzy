<?php
global $wpdb;
$school_id = 7744;
?>
<div class="reports_detail">
    <div class="inbox">
        <p>Reports</p>
    </div>
    <div class="reports_EXL">
        <div class="pt-4">
            <div class="row">
                <div class="col-lg-11">
                    <form action="" post="">
                        <div class="row">
                            <div class="col-lg-3 col-lg-6">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Report</label>
                                    <select id="eventTyoe" class="form-select" name="event-id">
                                        <option value="">Select Events</option>
                                        <?php
                                        $Getevent = $wpdb->get_results("SELECT school_id,event_name FROM wp8t_school_details", ARRAY_A);
                                        foreach ($Getevent as $key => $Getevents) {
                                            if (!empty($Getevents)) {
                                                ?>
                                                <option value="<?php echo $Getevents['school_id']; ?>"><?php echo $Getevents['event_name']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-lg-6 m-pt-4">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Class Name</label>
                                    <?php
                                    $option='';
                                    $table_name=TABLE_PREFIX_MAIN;
                                    $result=$wpdb->get_results("SELECT DISTINCT class from {$table_name}child_details where school_id=$school_id",ARRAY_A);
                                    ?>
                                    <select id="getClass" class="form-select" name="class">
                                        <option>Select class</option>
                                        <?php
                                        foreach( $result as $key=>$val){
                                            $option.='<option data-class="'.$school_id.'" value="'.$val['class'].'">'.$val['class'].'</option>';
                                        }
                                        echo $option;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-lg-6 m-pt-4">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Individual</label>
                                    <select id="getchildren" class="form-select" name="children">
                                        <option>Select Individual</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-12 mt-2">
                <div class="skeltion-loader d-none">
                    <div class="prod--wrapper">
                        <div class="prod--col prod--details">
                            <div class="prod--row prod--name">
                                <span id="productName" class="prod--name-text skeleton-loader"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-report d-none" id="profile-report">
                    <?php get_template_part('include/school/reports/profile',null,array(
                        'data'=>[
                            'school_id'=>$school_id,
                            'user_id'=>get_current_user_id()
                        ]
                    )); ?>

                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script>

</script>
