<?php
/* Template Name: Two columns content */

get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-img-items-section');

get_footer();
