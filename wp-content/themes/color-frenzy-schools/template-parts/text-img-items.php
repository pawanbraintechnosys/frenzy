<?php

$items = 'text-img-items__items';

?>

<section class="text-img-items <?= $args['section-classes']; ?>" style="    background-color: #01a3d2;">

    <div class="container">

        <?php while(have_rows($items)): the_row();

        $itemReplace = get_sub_field('replace_image');

        $itemReverse = get_sub_field('reverse_items');

        $itemSliderCond = get_sub_field('slider-cond');

        $itemTitle = get_sub_field('title');

        $itemText = get_sub_field('text');

        $itemImg = get_sub_field('image');

        $itemBgImg = get_sub_field('background_image');

        $itemVideoLink = get_sub_field('video_link');

        $innerSlider = 'slider';

        ?>

        <?php if(!$itemReplace): ?>

        <div class="text-img-items__item <?= $args['items-classes']; ?>

            <?= $itemReverse ? ' text-img-item--reverse text-img-item--text-short ' : ' text-img-item--text-short-reverse '; ?>

            <?= $itemSliderCond ? ' text-img-item--bubble-top ' : ''; ?>

        ">

            <?php if($itemSliderCond): ?>

                <div class="text-img-item__slider">

                    <button class="slider-arrow slider-arrow--prev text-img-item__slider-arrow text-img-item__slider-arrow-prev">



                    </button>

                    <div class="text-img-item__slider-container swiper-container">

                        <div class="text-img-item__wrapper swiper-wrapper">

                            <?php while(have_rows($innerSlider)): the_row();

                                $innerImage = get_sub_field('image');

                                $innerText = get_sub_field('text');

                                ?>

                                <div class="swiper-slide text-img-item-slide">

                                    <div class="text-img-item-slide__img">

                                        <?= wp_get_attachment_image($innerImage['id'], 'full') ?>

                                    </div>

                                    <h4>

                                        <?= $innerText; ?>

                                    </h4>

                                </div>

                            <?php endwhile; ?>

                        </div>

                    </div>

                    <button class="slider-arrow slider-arrow--next text-img-item__slider-arrow text-img-item__slider-arrow-next">



                    </button>

                </div>

            <?php else: ?>

                <div class="text-img-item__img">

                    <?= wp_get_attachment_image($itemImg['id'], 'full') ?>

                </div>

            <?php endif; ?>

            <div class="text-img-item__text">

                <h2>

                    <?= $itemTitle; ?>

                </h2>

                <?= $itemText; ?>

            </div>

        </div>



        <?php else: ?>



        <div class="text-video-item text-video-item--align-center  text-video-item--brush

        <?= $itemReverse ? ' text-video-item--reverse ' : ''; ?>

        ">

            <div class="text-video-item__wr">

                <a href="#" data-video-src="<?= $itemVideoLink; ?>"

                   class="text-video-item__img">

                    <?= wp_get_attachment_image($itemImg['id'], 'full') ?>

                    <button class="play-video">

                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">

                    </button>

                </a>

                <div class="text-video-item__bg">

                    <?= wp_get_attachment_image($itemBgImg['id'], 'full') ?>

                </div>

            </div>

            <div class="text-video-item__text">

                <h2>

                    <?= $itemTitle; ?>

                </h2>

                <?= $itemText; ?>

            </div>

        </div>



        <?php endif; ?>

        <?php endwhile; ?>

    </div>

</section>

