<?php
$headerLogo = @get_field('header__logo', 'option');
$navRow = 'header__navigation';
$headerBtn = @get_field('header__button', 'option');
$page_id = get_queried_object_id();
$ID = get_the_ID();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--    <title>--><?php //wp_title(); ?><!--</title>-->
    <?php wp_head(); ?>
</head>
<body id="page-id-<?= $ID; ?>">

<header id="header" class="header">
    <div class="container">
        <div class="header__wrapper">
            <div class="header__fader">

            </div>
            <button class="header__btn">
                <span>

                </span>
            </button>
            <a href="/"  class="header__logo">
                <?= wp_get_attachment_image(@$headerLogo['id'], array(130, 115)); ?>
            </a>
            <div class="header__inner">
                <?php
                if( have_rows($navRow, 'option') ):
                ?>
                <nav class="header__menu menu">
                    <ul class="menu__list">
                <?php
                    while( have_rows($navRow, 'option') ) : the_row();
                    $navLink = get_sub_field('link');
                    $navPageId = url_to_postid($navLink['url']);
                    $hasSubMenu = get_sub_field('have-sub-menu');
                    $subMenu = 'sub_menu';
                ?>
                    <li class="menu__list-item" data-dropdown-status="close" data-working-range="0 1024">
                            <div class="menu__list-item-dropdown" data-dropdown-head>
                                <a href="<?= $navLink['url']; ?>"  class="menu__list-item-link <?= ($page_id === $navPageId) ? 'active' : '' ?> ">
                                    <?= $navLink['title']; ?>
                                </a>
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/shape.svg" alt="shape">
                            </div>
                            <?php if($hasSubMenu && have_rows($subMenu)): ?>
                                <ul class="menu__sub-list menu-sub-list" data-dropdown-body>
                                    <?php while (have_rows($subMenu)): the_row();
                                        $link = get_sub_field('link');
                                    ?>
                                        <li class="menu-sub-list__item">
                                            <a href="<?= $link['url']; ?>"  class="menu-sub-list__item-link"><?= $link['title']; ?></a>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                <?php

                    endwhile;
                ?>
                    </ul>
                </nav>
                <?php
                    endif;
                ?>
                <div class="header__pages-links pages-links">
                    <a href="/schools/"  class="pages-links__link">
                        SCHOOLS
                    </a>
                    <a href="/"  class="pages-links__link active">
                        PUBLIC
                    </a>
                    <div class="flap-bg"></div>
                </div>
                <?php if($headerBtn): ?>
                <a href="<?= $headerBtn['url']; ?>"  class="btn btn--without-border btn--animation">
                    <?= $headerBtn['title']; ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<main>
