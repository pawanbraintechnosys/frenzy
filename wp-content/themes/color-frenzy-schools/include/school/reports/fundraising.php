<?php
$schoolData=$args;
$school_id=$schoolData['data']['school_id'];
?>
<div class="report" id="printTable">
    <table class="table" id="dataTables">
        <thead>
        <tr>
            <!--th scope="col"><a type="button" id="delete_records"style="cursor: pointer;color:#1f1f1f;font-size: 1.8rem;"><i class="fa fa-trash"></i></a><input type="checkbox" id="select_all"></th-->
            <th scope="col">Title</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Class</th>
            <th scope="col">Start date</th>
            <th scope="col">Close date</th>
            <th scope="col">Goal</th>
            <th scope="col">Raised</th>
        </tr>
        </thead>
        <tbody id="serach_tables">
        <?php
        //$funddatas = getFundraisedData($school_id);
        /* $num_per_page = 2;
         if(isset($_GET['pages'])){
            $page = $_GET['pages'] ;
            //echo $page;
         }else {
            $page = 1;
         }
         $start_form = ($page-1)*$num_per_page;*/
        //echo $start_form;
        $funddatas = $wpdb->get_results("SELECT * FROM wp8t_child_details WHERE school_id= $school_id ", ARRAY_A);
        // pr($funddatas);

        foreach ($funddatas as $key => $funddata) {
            global $wpdb;
            $fundtitle_firstname = $funddata['firstname'];
            $fundtitle_lastname = substr($funddata['lastname'], 0, 1);
            $fundtitle_class = $funddata['class'];
            $fundtitle = $fundtitle_firstname . ' ' . $fundtitle_lastname . ' ' . $fundtitle_class;

            $campiagns = $wpdb->get_results("SELECT * FROM fundraise_posted_campiagn WHERE title = '$fundtitle'", ARRAY_A);
            foreach ($campiagns as $key => $campiagn) {
                ?>
                <tr id="<?php echo $campiagn['id']; ?>">
                    <!--td style="padding-left:16px;""><input type="checkbox" class="emp_checkbox" data-emp-id="<?php //echo $campiagn['id'];
                    ?>"></td-->
                    <td class="date-other"><?php echo $campiagn['title']; ?></td>
                    <td><?php echo $funddata['firstname']; ?></td>
                    <td><?php echo $funddata['lastname']; ?></td>
                    <td><?php echo $funddata['class']; ?></td>
                    <td class="date-other"><?php echo $campiagn['start-date']; ?></td>
                    <td class="date-other"><?php echo $campiagn['end-date']; ?></td>
                    <td class="date-other">$<?php echo $campiagn['goal-amount']; ?></td>
                    <td class="date-other">
                        <?php
                        $totalRaisedAmount = $wpdb->get_results("SELECT SUM(amount) as total_raise FROM fundraise_donation WHERE fundpage_id ={$campiagn['id']}", ARRAY_A)[0];
                        if (isset($totalRaisedAmount['total_raise']) && !empty($totalRaisedAmount['total_raise'])) {
                            echo DEFAULT_CURRENCY . number_format($totalRaisedAmount['total_raise'], 2);
                        } else {
                            echo DEFAULT_CURRENCY . "00.00";
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {
        // DataTable
        var table = $('#dataTables').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Fund Report'
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Fund Report'
                },{
                    extend: 'print',
                    text: 'Print',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ]
        });

    } );
</script>