$(document).ready(function () {

    /**
     * Form Sign Up
     *
     * @package colorFenzy
     * @version 1.0.0
     */
    var colorFenzyCreateFundraisingPage = {
        frm: null,
        init: function () {
            this.validate();
        },
        submit: function (form) {
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: $("#create-fundraiser").serialize(),
                success: function (data) {
                    $(".ajaxLoading").addClass("hide");
                    if (data.status == 'success') {
                        toastr.success(data.message);
                        setTimeout(() => {
                            window.location.href = data.url;
                        }, 1000);
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        },
        validate: function () {
            this.frm = $("#create-fundraiser").validate({
                errorClass: "error",
                onkeyup: false,
                onfocusout: false,
                rules: {
                    user_pass: {
                        required: true,
                    },
                    confirm_user_pass: {
                        minlength: 5,
                        equalTo: "#password"
                    }
                },
                messages: {
                    user_pass: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    confirm_user_pass: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long",
                        equalTo: "Please enter the same password as above"
                    }

                },
                submitHandler: function (from) {
                    colorFenzyCreateFundraisingPage.submit(from);
                }
            });
        },
    };
    colorFenzyCreateFundraisingPage.init();

});