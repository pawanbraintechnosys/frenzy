export function initImgsChange() {
  let items = document.querySelectorAll('[data-hover-src]')
  items.forEach((item) => {
    const itemImg = item.querySelector('.team-slider-item__img img')
    const imgStartSrc = itemImg.getAttribute('src')
    const dataHoverSrc = item.dataset.hoverSrc
    item.addEventListener('mouseover', () => {
      itemImg.src = dataHoverSrc
    })
    item.addEventListener('mouseout', () => {
      itemImg.src = imgStartSrc
    })
  })
}
