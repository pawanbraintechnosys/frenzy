<?php
$title = get_the_title();
$link = get_field('location-link');
$area = get_field('location-area');
$distance = get_field('location-distance');
?>
<div class="locations-items__item locations-item">
    <h3><?= $title; ?></h3>
    <p><?= $area; ?> - <?= $distance; ?></p>
    <?php if($link): ?>
        <a href="<?= $link['url']; ?>"  target="_blank" >
            <?= $link['title']; ?>
        </a>
    <?php endif; ?>
</div>