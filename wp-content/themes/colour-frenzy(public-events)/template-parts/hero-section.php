<?php
$heroTitle = get_field('hero__title');
$withTimer = isset($args['with-timer'])? $args['with-timer'] : false;
$searchQuery = isset($args['search-query'])? $args['search-query'] : false;
$isHomepage = isset($args['is_homepage'])? $args['is_homepage'] : false;
$catTitle = isset($args['cat-title'])? $args['cat-title'] : false;
$heroBg = isset($args['archive-bg']) ? get_field('hero__bg', 65) : get_field('hero__bg');
$heroClasses = isset($args['hero-classes'])? $args['hero-classes'] : false;
$archiveBg = isset($args['archive-bg'])? $args['archive-bg'] : false;
?>
<section class="hero-section <?= $heroClasses; ?> <?= $archiveBg ? ' hero-section--small hero-section--content-center ' : ''; ?> ">
    <div class="hero-section__bg">
        <?= wp_get_attachment_image($heroBg['id'], 'full'); ?>
    </div>
    <div class="container">
        <div class="hero-section__content">
            <h1>
                <?php
                if($heroTitle):
                    echo $heroTitle;
                elseif ($searchQuery):
                    echo 'Search result for: ' . $searchQuery;
                else:
                    echo $catTitle ? $catTitle : $heroTitle;
                endif; ?>
            </h1>
            <?php if($withTimer):
                $date = strtotime(get_field('data-start-event')) * 1000;
                $currentDate = strtotime(date('F j, Y')) * 1000;
            ?>
                <p class="date-event">
                    <?php if($date && $date > $currentDate): ?>
                        <?= get_field('data-start-event'); ?>
                    <?php else: ?>
                        Coming Soon
                    <?php endif; ?>
                </p>
                <?php if($date && $date > $currentDate): ?>
                    <div id="event-timer" data-event-date="<?= $date; ?>" class="hero-section__countdown countdown">
                    <div class="countdown-item countdown__item" id="days">
                        <svg viewBox="0 0 64 64" class="countdown-item__circle circle circle--yellow">
                            <circle class="circle__bg" r="50%" cx="50%" cy="50%"></circle>
                            <circle class="circle__line" r="50%" cx="50%" cy="50%" ></circle>
                        </svg>
                        <div class="countdown-item__wrapper">
                            <span class="countdown-item__num">
                                -
                            </span>
                            Days
                        </div>
                    </div>
                    <div class="countdown-item countdown__item" id="hours">
                        <svg viewBox="0 0 64 64" class="countdown-item__circle circle circle--blue">
                            <circle class="circle__bg" r="50%" cx="50%" cy="50%"></circle>
                            <circle class="circle__line" r="50%" cx="50%" cy="50%" ></circle>
                        </svg>
                        <div class="countdown-item__wrapper">
                            <span class="countdown-item__num">
                               -
                            </span>
                            Hours
                        </div>
                    </div>
                    <div class="countdown-item countdown__item" id="minutes">
                        <svg viewBox="0 0 64 64" class="countdown-item__circle circle circle--green">
                            <circle class="circle__bg" r="50%" cx="50%" cy="50%"></circle>
                            <circle class="circle__line" r="50%" cx="50%" cy="50%" ></circle>
                        </svg>
                        <div class="countdown-item__wrapper">
                            <span class="countdown-item__num">
                               -
                            </span>
                            Minutes
                        </div>
                    </div>
                    <div class="countdown-item countdown__item" id="seconds">
                        <svg viewBox="0 0 64 64" class="countdown-item__circle circle circle--violet">
                            <circle class="circle__bg" r="50%" cx="50%" cy="50%"></circle>
                            <circle  class="circle__line" r="50%" cx="50%" cy="50%" ></circle>
                        </svg>
                        <div class="countdown-item__wrapper">
                            <span class="countdown-item__num">
                               -
                            </span>
                            Seconds
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($isHomepage): ?>
                <button class="hero-section__scroll-down scroll-down">
                    <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/arrow-down.svg" alt="image">
                </button>
                <div class="hero-section__pages-links pages-links">
                    <a href="/schools/"  class="pages-links__link">
                        SCHOOLS
                    </a>
                    <a href="/"  class="pages-links__link active">
                        PUBLIC
                    </a>
                    <div class="flap-bg"></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
