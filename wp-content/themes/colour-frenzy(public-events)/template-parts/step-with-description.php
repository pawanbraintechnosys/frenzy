<?php
$items = 'steps-with-description__items';
$avatar = get_field('steps-with-description__avatar');
$titleMain = get_field('steps-with-description__title');
$btn = get_field('steps-with-description__button');
$descriptionMain = get_field('steps-with-description__description');
?>
<section class="one-row-items">
    <div class="container">
        <?php if(have_rows($items)): ?>
            <div class="one-row-items__inner">
                <?php while (have_rows($items)): the_row();
                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                ?>
                    <div class="one-row-items__item one-row-item">
                        <div class="one-row-item__img">
                            <?= wp_get_attachment_image($icon['id'], 'full'); ?>
                        </div>
                        <h3><?= $title; ?></h3>
                        <p><?= $description; ?></p>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <div class="one-row-items__description">
            <div class="one-row-items__description-img">
                <?= wp_get_attachment_image($avatar['id'], 'full'); ?>
            </div>
            <h2><?= $titleMain; ?></h2>
            <?php if($btn): ?>
                <a href="<?= $btn['url']; ?>"  class="btn">
                    <?= $btn['title']; ?>
                </a>
            <?php endif; ?>
            <p><?= $descriptionMain; ?></p>
        </div>
    </div>
</section>