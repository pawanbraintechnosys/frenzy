<?php
$ticketsTitle = get_field('prices__tickets-title');
$ticketsItems = 'prices__tickets-items';
$ticketsItemsTextAfter = get_field('prices__text-after');
$ticketsSecondTitle = get_field('prices__tickets-second-title');
$ticketsImg = get_field('prices__tickets-image');
$ticketsImgBtn = get_field('prices__tickets-image-button');

$titleEvents = get_field('popular-events__title');
$events = get_field('popular-events__events');
?>
<section class="three-col-items">
    <div class="container">
        <h2>
            <?= $ticketsTitle; ?>
        </h2>
        <?php if(have_rows($ticketsItems)): ?>
            <div class="three-col-items__inner">
                <div class=" ticket-items">
                    <?php while(have_rows($ticketsItems)): the_row();
                        $title = get_sub_field('title');
                        $text = get_sub_field('text');
                        $button = get_sub_field('button');
                        ?>
                        <div class="ticket-items__item ticket-item circle-mark-list">
                            <div class="ticket-item__bg">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/ticket-items/bg.png" alt="img">
                            </div>
                            <h3>
                                <?= $title; ?>
                            </h3>
                            <div class="ticket-item__text">
                                <?= $text; ?>
                            </div>
<!--                            <a href="--><?//= $button['url'] ?><!--" target="--><?//= $button['target'] ?><!--"  class="btn">-->
<!--                                --><?//= $button['title'] ?>
<!--                            </a>-->
                        </div>
                    <?php endwhile; ?>
                </div>
                <p>
                    <?= $ticketsItemsTextAfter; ?>
                </p>
            </div>
        <?php endif; ?>


        <section class="slider-section brush-bg-section--prev-sec">
            <div class="container">
                <div class="slider-section__head">
                    <h2><?= $titleEvents; ?></h2>
                </div>
                <div class="slider-section__body ">
                    <?php if($events): ?>
                        <div class="event-slider">
                            <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

                            </button>
                            <div class="slider-section__inner event-slider__container  swiper-container">
                                <div class="event-slider__wrapper swiper-wrapper">
                                    <?php foreach ($events as $event): setup_postdata($event);
                                        $title = get_the_title($event->ID);
                                        $date = strtotime(get_field('data-start-event', $event->ID));
                                        $date = date("jS \of F Y", $date);
                                        ?>
                                        <div class="event-slider__item event-slider-item swiper-slide">
                                            <h3><?= $title; ?></h3>
                                            <div class="event-slider-item__date">
                                                <?= $date; ?>
                                            </div>
                                            <a href="<?= get_the_permalink($event->ID); ?>#price"  class="btn btn--small">
                                                Buy now
                                            </a>
                                        </div>
                                    <?php endforeach;
                                    wp_reset_query();
                                    ?>
                                </div>
                            </div>
                            <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>
                            <div class="event-slider__pagination slider-pagination">

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>



        <h2>
            <?= $ticketsSecondTitle; ?>
        </h2>
        <div class="three-col-items__img">
            <?= wp_get_attachment_image($ticketsImg['id'], 'full'); ?>
            <a href="<?= $ticketsImgBtn['url'] ?>"  target="<?= $ticketsImgBtn['target'] ?>" class="btn">
                <?= $ticketsImgBtn['title'] ?>
            </a>
        </div>
    </div>
</section>
