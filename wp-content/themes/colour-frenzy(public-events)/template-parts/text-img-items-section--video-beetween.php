<?php
$itemsBeforeVideo = 'school__items-before-video';
$topLeftImage = get_field('video_section_top_left_image');
$topRightImage = get_field('video_section_top_right_image');
$botLeftImage = get_field('video_section_bottom_left_image');
$botRightImage = get_field('video_section_bottom_right_image');
$videoPreview = get_field('video_section_video_preview');
$videoLink = get_field('video_section_video_link');
$itemsAfterVideo = 'school__items-after-video';
$formTitle = get_field('form__title');
$formShortcode = get_field('form__shortcode');
?>
<?php if(have_rows($itemsBeforeVideo)): ?>
<section class="text-img-items">
    <div class="container">
        <?php while(have_rows($itemsBeforeVideo)): the_row();
            $itemReverse = get_sub_field('reverse');
            $itemImage = get_sub_field('image');
            $itemText = get_sub_field('text');
            $itemBtn = get_sub_field('button');
            ?>
            <div class=" text-img-item <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?> ">
                <div class="text-img-item__img">
                    <?= wp_get_attachment_image($itemImage['id'], 'full'); ?>
                </div>
                <div class="text-img-item__text">
                    <?= $itemText; ?>
                    <a href="<?= $itemBtn['url']; ?>"  target="<?= $itemBtn['target']; ?>" class="btn">
                        <?= $itemBtn['title']; ?>
                    </a>
                </div>
            </div>
        <?php endwhile; ?>
        <div class="text-img-items__video-imgs-items video-imgs-items">
            <div class="video-imgs-items__item  video-imgs-items__item--small video-imgs-item">
                <?= wp_get_attachment_image($topLeftImage['id'], 'full'); ?>
            </div>
            <div data-video-src="<?= $videoLink; ?>" class="video-imgs-items__item  video-imgs-items__item--big   video-imgs-item">
                <?= wp_get_attachment_image($videoPreview['id'], 'full'); ?>
                <button class="play-video">
                    <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                </button>
            </div>
            <a class="video-imgs-items__item  video-imgs-items__item--middle video-imgs-item">
                <?= wp_get_attachment_image($topRightImage['id'], 'full'); ?>
            </a>
            <div class="video-imgs-items__item  video-imgs-items__item--middle video-imgs-item">
                <?= wp_get_attachment_image($botLeftImage['id'], 'full'); ?>
            </div>
            <div class="video-imgs-items__item  video-imgs-items__item--small video-imgs-item">
                <?= wp_get_attachment_image($botRightImage['id'], 'full'); ?>
            </div>
        </div>
        <?php while(have_rows($itemsAfterVideo)): the_row();
            $itemReverse = get_sub_field('reverse');
            $itemImage = get_sub_field('image');
            $itemText = get_sub_field('text');
            $itemBtn = get_sub_field('button');
            ?>
            <div class=" text-img-item <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?> ">
                <div class="text-img-item__img">
                    <?= wp_get_attachment_image($itemImage['id'], 'full'); ?>
                </div>
                <div class="text-img-item__text">
                    <?= $itemText; ?>
                    <a href="<?= $itemBtn['url']; ?>"  target="<?= $itemBtn['target']; ?>" class="btn">
                        <?= $itemBtn['title']; ?>
                    </a>
                </div>
            </div>
        <?php endwhile; ?>
        <div class="text-img-items__form-wr">
            <h3>
                <?= $formTitle; ?>
            </h3>
            <?= do_shortcode($formShortcode); ?>
        </div>
    </div>
</section>
<?php endif; ?>