<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>

<?php
$schoolData = $args;
$school_id = $args['data']['school_id'];
?>
<div class="reports_detail">
    <div class="inbox">
        <p>Reports</p>
    </div>
    <div class="reports_EXL">
        <div class="pt-4">
            <div class="row">
                <div class="col-lg-11">
                    <form action="" post="">
                        <div class="row">
                            <!--div class="col-lg-3 col-lg-6">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Event Name</label>
                                    <select id="getevent" class="form-select">
                                        <option>Select</option>
                                        <?php
                                        $Getevent = $wpdb->get_results("SELECT school_id,event_name FROM wp8t_school_details", ARRAY_A);
                                        foreach ($Getevent as $key => $Getevents) {
                                            if (!empty($Getevents)) {
                                                //pr($Getevents);
                                                ?>
                                                <option value="<?php echo $Getevents['school_id']; ?>"><?php echo $Getevents['event_name']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div-->
                            <div class="col-lg-3 col-lg-6">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Report</label>
                                    <select id="report-type" class="form-select" name="report-type">
                                        <option>Select</option>
                                        <option value="PROFILE">Profile Report</option>
                                        <option value="FUNDRAISING">Fundraising Report</option>
                                        <option value="INCENTIVE">Incentive Report</option>
                                        <option value="MERCHANDISE">Merchandise Report</option>
                                        <option value="PRODUCT_SALE">Product sale Report</option>
                                        <option value="REGISTRATION">Registration Report</option>
                                        <option value="FULL_REPORT">Full Report</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-lg-6 m-pt-4">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Class Name</label>
                                    <?php
                                        $option='';
                                        $table_name=TABLE_PREFIX_MAIN;
                                        $result=$wpdb->get_results("SELECT DISTINCT class from {$table_name}child_details where school_id=$school_id",ARRAY_A);
                                    ?>
                                    <select id="getClass" class="form-select" name="class">
                                        <option>Select class</option>
                                        <?php
                                            foreach( $result as $key=>$val){
                                                $option.='<option data-class="'.$school_id.'" value="'.$val['class'].'">'.$val['class'].'</option>';
                                            }
                                            echo $option;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-lg-6 m-pt-4">
                                <div class="select_opt">
                                    <label for="inputState" class="form-label">Individual</label>
                                    <select id="getchildren" class="form-select" name="children">
                                        <option>Select Individual</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--div class="col-lg-12">
                    <div class="row pt-8">
                        <div class="col-lg-4">
                            <div class="row select_opt">
                                <div class="col-lg-4 col-md-6">
                                    <span class="expo">
                                        <img src="<?php echo get_template_directory_uri() . '/assets/images/share.png'; ?>">
                                        <select id="inputState" class="form-select">
                                            <option>Columns</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </span>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <span>
                                        <img src="<?php echo get_template_directory_uri() . '/assets/images/print.png'; ?>">
                                        <button onclick="printSection('printTable')">Print</button>
                                    </span>
                                </div>
                                <div class="col-lg-4 col-md-6 m-pt-4">
									<span class="expo">
										<img src="<?php echo get_template_directory_uri() . '/assets/images/share.png'; ?>">
										<select id="inputState" class="form-select">
											<option>Export</option>
											<option>Yes</option>
											<option>No</option>
										</select>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div-->
                <div class="col-lg-12 mt-2">
                    <div class="skeltion-loader d-none">
                        <div class="prod--wrapper">
                            <div class="prod--col prod--details">
                                <div class="prod--row prod--name">
                                    <span id="productName" class="prod--name-text skeleton-loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-report d-none" id="profile-report">
                        <?php get_template_part('include/school/reports/profile',null,array(
                                'data'=>[
                                        'school_id'=>$school_id,
                                        'user_id'=>get_current_user_id()
                                ]
                        )); ?>

                    </div>
                    <div class="fundraising-report" id="fundraising-report">
                        <?php get_template_part('include/school/reports/fundraising',null,array(
                                'data'=>[
                                        'school_id'=>$school_id,
                                        'user_id'=>get_current_user_id()
                                ]
                        )); ?>
                    </div>
                    <div class="Incentive-report d-none" id="Incentive-report">
                        <?php get_template_part('include/school/reports/incentive',null,array(
                                'data'=>[
                                        'school_id'=>$school_id,
                                        'user_id'=>get_current_user_id()
                                ]
                        )); ?>
                    </div>
                    <div class="Merchandise-report d-none" id="Merchandise-report">
                        <?php get_template_part('include/school/reports/merchandise',null,array(
                                'data'=>[
                                        'school_id'=>$school_id,
                                        'user_id'=>get_current_user_id()
                                ]
                        )); ?>
                    </div>
                    <div class="ProductSale-report d-none" id="ProductSale-report">
                        <?php get_template_part('include/school/reports/productSale',null,array(
                                'data'=>[
                                        'school_id'=>$school_id,
                                        'user_id'=>get_current_user_id()
                                ]
                        )); ?>
                    </div>
                    <div class="Registration-report d-none" id="Registration-report">
                        <?php get_template_part('include/school/reports/registration',null,array(
                                'data'=>[
                                        'school_id'=>$school_id,
                                        'user_id'=>get_current_user_id()
                                ]
                        )); ?>
                    </div>
                    <div class="Full-report d-none" id="Full-report">
                    <?php get_template_part('include/school/reports/full',null,array(
                            'data'=>[
                                    'school_id'=>$school_id,
                                    'user_id'=>get_current_user_id()
                            ]
                    )); ?>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $("#serach_table").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#serach_tables tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
    $(document).ready(function () {
        $(".dataExport").click(function () {
            var exportType = $(this).data('type');
            $('#dataTable').tableExport({
                type: exportType,
                escape: 'false',
                ignoreColumn: []
            });
        });
    });

    /* change Report */
    $(document).ready(function () {
        $(document).on('change', '#report-type', function () {
            let allReportType=['profile-report','fundraising-report','Incentive-report','Merchandise-report','ProductSale-report','Registration-report','Full-report']
           // let dataTableWrapper=['pro-report','dataTables','incentive-report','merchandise-report','productSale-report','registration-report','full-report']
            $(".skeltion-loader").show();
            $("#getClass").val('Select class');
            $("#getchildren option").remove();
            $("#getchildren").append('<option>Select Individiual</option>');
            allReportType.map((el)=>{
                $("."+el).addClass('d-none');
            })
            setTimeout(()=>{
                $(".skeltion-loader").hide();
                allReportType.map((el)=>{
                    $("."+el).addClass('d-none');
                })
                var reportType = $('#report-type option:selected').val();
                if(reportType==='PROFILE'){
                    $(".profile-report").removeClass('d-none');
                    eventTypeRecord();
                }else if(reportType==='FUNDRAISING'){
                    $(".fundraising-report").removeClass('d-none');
                    eventTypeRecord();
                }else if(reportType==='INCENTIVE'){
                    $(".Incentive-report").removeClass('d-none');
                    eventTypeRecord();
                }else if(reportType==='MERCHANDISE'){
                    $(".Merchandise-report").removeClass('d-none');
                    eventTypeRecord();
                }else if(reportType==='PRODUCT_SALE'){
                    $(".ProductSale-report").removeClass('d-none');
                    eventTypeRecord();
                }else if(reportType==='REGISTRATION'){
                    $(".Registration-report").removeClass('d-none');
                    eventTypeRecord();
                }else if(reportType==='FULL_REPORT'){
                    $(".Full-report").removeClass('d-none');
                    eventTypeRecord();
                }else{
                    $(".fundraising-report").removeClass('d-none');
                    eventTypeRecord();
                }

            },1000);
        });
        $(document).on('change', '#getchildren', function () {
            eventTypeRecord();
        });
        function eventTypeRecord(){
            let allReportType=['profile-report','fundraising-report','Incentive-report','Merchandise-report','ProductSale-report','Registration-report','Full-report']
            let dataTableWrapper=['pro-report','dataTables','incentive-report','merchandise-report','productSale-report','registration-report','full-report']
            $(".skeltion-loader").show();
            allReportType.map((el)=>{
                $("."+el).addClass('d-none');
            })
            setTimeout(()=>{
                $(".skeltion-loader").hide();
                var table;var ChildNameWithClass;
                var reportType = $('#report-type option:selected').val();
                var class_id = $('#getClass option:selected').val();
                var school_id = $('#getClass option:selected').data('class');
                var childName = $('#getchildren option:selected').text();
                if(reportType==='PROFILE'){
                    $(".profile-report").removeClass('d-none');
                    table = $('#pro-report').DataTable();
                    updateDataTable(table,class_id,childName);
                }else if(reportType==='FUNDRAISING'){
                    $(".fundraising-report").removeClass('d-none');
                    table = $('#dataTables').DataTable();
                    updateDataTable(table,class_id,childName);
                }else if(reportType==='INCENTIVE'){
                    $(".Incentive-report").removeClass('d-none');
                    table = $('#incentive-report').DataTable();
                    updateDataTable(table,class_id,childName);
                }else if(reportType==='MERCHANDISE'){
                    $(".Merchandise-report").removeClass('d-none');
                    table = $('#merchandise-report').DataTable();
                    updateDataTable(table,class_id,childName);
                }else if(reportType==='PRODUCT_SALE'){
                    $(".ProductSale-report").removeClass('d-none');
                    table = $('#productSale-report').DataTable();
                    updateDataTable(table,class_id,childName);
                }else if(reportType==='REGISTRATION'){
                    $(".Registration-report").removeClass('d-none');
                    table = $('#registration-report').DataTable();
                    updateDataTable(table,class_id,childName);
                }else if(reportType==='FULL_REPORT'){
                    $(".Full-report").removeClass('d-none');
                    table = $('#full-report').DataTable();
                    updateDataTable(table,class_id,childName);
                }else{
                    $(".fundraising-report").removeClass('d-none');
                    table = $('#dataTables').DataTable();
                    updateDataTable(table,class_id,childName);
                }

            },1000);
        }
        function updateDataTable(table,class_id,childName){
            if(class_id !=='' && childName !=='Select Class' && childName !=='Select Student' && childName !=='Loading..' && childName !=='Select Individiual'){
                var ChildNameWithClass=class_id+' '+childName;
                table.search(ChildNameWithClass).draw();
            }else if(childName !=='Select Class' && childName !=='Select Individiual' && childName !=='Loading..' && childName !=='Select Student'){
                table.search(childName).draw();
            }else if(class_id!=='' && class_id!=='Select class' ){
                table.search(class_id).draw();
            }else{
                table.search('').draw();
            }
        }
        $(document).on('change', '#getClass', function () {
            eventTypeRecord();
            $('#getchildren').find('option').remove().end().append('<option value="">Loading..</option>');
            var class_id = $('#getClass option:selected').val();
            var school_id = $('#getClass option:selected').data('class');
            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>' + '?action=get_school_children_by_class',
                data: {'class_id': class_id, 'school_id': school_id},
                success: function (data) {
                    $('#getchildren').find('option').remove().end().append(data);
                }
            });
        });
    });
    $(document).on('click', '#select_all', function () {
        $(".emp_checkbox").prop("checked", this.checked);
        $("#select_count").html($("input.emp_checkbox:checked").length + " Selected");
    });
    $(document).on('click', '.emp_checkbox', function () {
        if ($('.emp_checkbox:checked').length == $('.emp_checkbox').length) {
            $('#select_all').prop('checked', true);
        } else {
            $('#select_all').prop('checked', false);
        }
        $("#select_count").html($("input.emp_checkbox:checked").length + " Selected");
    });

    function printSection(el) {
        var getFullContent = document.body.innerHTML;
        var printsection = document.getElementById(el).innerHTML;
        document.body.innerHTML = printsection;
        window.print();
        document.body.innerHTML = getFullContent;
    }

</script>
