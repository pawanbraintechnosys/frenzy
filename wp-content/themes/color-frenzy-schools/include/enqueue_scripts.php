<?php

function my_load_scripts() {
    wp_deregister_script( 'jquery' );
   // wp_register_script( 'jquery', "https://code.jquery.com/jquery-2.1.1.min.js", array(), '2.1.1', true);
    wp_enqueue_script('jquery');
    wp_enqueue_style( 'main-css', get_template_directory_uri() . '/style.css', array(), null, false);
    wp_enqueue_script( 'main_js', get_template_directory_uri() . '/js/main.js', array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', 'my_load_scripts');