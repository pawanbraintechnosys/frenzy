<?php
/*
 * School Dashboard
 * @ver:1.0
 */

function school_dashboard_callback($atts)
{
    $atts = shortcode_atts(array(), $atts, 'school_dashboard');
    $schoolID=get_user_meta(get_current_user_id(),'school_id',true);
	$id = base64_decode($_GET['id']);
    //echo $schoolID;
    if (isset($schoolID)){        
     $school_fund_page = new Parent_model();        
     $table = "wp8t_school_details";        
     $where = " WHERE school_id=" . $schoolID;        
     $school_fund_page = $school_fund_page->get($table, $where);        
      if (!empty($school_fund_page['data'])){ 
        $child = new Parent_model();            
        $tabl="wp8t_child_details";
        $wher=" WHERE school_id=".$schoolID;
        $child=$child->get($tabl,$wher);
        if(!empty($child['data'])){
            $child=$child['data'][0];
            $user=get_userdata($child['parent_id']);
            /* Fund Detail */
            $child_fundpage=get_child_fundraising_page($child['parent_id'],$child['firstname']);
            $donation= get_all_fund_donation(@$child_fundpage['data']['id']);
            $fundpagee_id = $donation['latest_donation'][0]['fundpage_id'] ;
            //pr($donation);
        }
    }
}
    if (is_user_logged_in()):
        ?>
        <div class="main-dashboard school_dashboard">
            <div class="school_dashboard_1">
                <div class="container">
                    <div class="row school_dash">
                        <div class="goal">
                            <h3 class="heading"><?php echo get_school_webpage($schoolID)['data'][0]['event_name'];
                            ?></h3>
                            <p>Welcome to your School Dashboard</p>
                        </div>
                        <div class="goal_summary">
                            <div class="first_icon">
                                <img src="<?php echo home_url(); ?>/wp-content/uploads/sites/4/2021/06/header-logo.svg" />
                            </div>
                            <div class="fundraise_graph">
                                <span class="fund_left">Raised 
								<?php echo DEFAULT_CURRENCY.number_format($donation['total_fund_raised']  ['total_donation']); ?>
								</span>
                                <span class="fund_right">Goal 
								<?php
									$totalGoal = get_school_webpage($schoolID)['data'];
                                    //pr($totalGoal[0]['fundrasing_goal']);
										echo DEFAULT_CURRENCY.number_format($totalGoal[0]['fundrasing_goal']);
								?>
								</span>
                                <div class="school-light-grey" style="border-radius: 10px;">
								<?php $A = $totalGoal[0]['fundrasing_goal'];
									  $B = $donation['total_fund_raised']['total_donation'];
									  $per = round(($B/$A) * 100); 
                                      if($per>0){
								?>
                                    <div class="outdder">
									<div style="border-bottom-right-radius: <?php if($per == 100){ ?> 10px <?php } ?>;border-top-right-radius: <?php if($per == 100){ ?> 10px <?php } ?>;border-right:<?php if($per == 100){ ?> none <?php } ?>!important; height:25px;max-width:<?php echo $per?>%;border-right:solid 1px #000;border-top-left-radius: 10px;border-bottom-left-radius: 10px;background-color:#f83d96;">
									</div>
									</div>
                                <?php }else{ ?>
                                    <div class="outdder">
                                    <div style="height:25px;max-width:0%;background-color:#f83d96;">
                                    </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="fundrise_lv">
                                <img src="<?php echo home_url(); ?>/wp-content/uploads/sites/4/2021/06/header-logo.svg"/ >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<style type="text/CSS">

</style>
            <section class="funddash form-section">
                <?php get_template_part('include/school/donation-leaderboard',null,array(
                        'data'=>[
                                'school_id'=>$schoolID,
                                'user_id'=>get_current_user_id()
                        ]
                )); ?>
			</section>
			<section class="reports_message_detail">
                <?php get_template_part('include/school/fundraising-templates',null,array(
                        'data'=>[
                                'school_id'=>$schoolID,
                                'user_id'=>get_current_user_id()

                        ]
                )); ?>
            </section>
            <section class="reports">
                <?php get_template_part('include/school/fund-report',null,array(
                        'data'=>[
                                'school_id'=>$schoolID,
                                'user_id'=>get_current_user_id()
                        ]
                )); ?>
            </section>
			<section class="form-section">
                <?php get_template_part('include/school/manage-fundraising',null,array(
                        'data'=>[
                                'school_id'=>$schoolID,
                                'user_id'=>get_current_user_id(),
                                'page'=> $_GET['page']
                        ]
                )); ?>
			</section>
			<section class="section-help-guides">
                <?php get_template_part('include/school/help-guide',null,array(
                        'data'=>[
                                'school_id'=>$schoolID,
                                'user_id'=>get_current_user_id()
                        ]
                )); ?>
            </section>
		</div>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}
add_shortcode('school_dashboard', 'school_dashboard_callback');
?>
