<?php

/* Template Name: About us page */


get_header();


get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/about-section');

get_footer();
