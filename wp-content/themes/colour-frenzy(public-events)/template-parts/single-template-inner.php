<?php
$content = get_field('single-content-template');
?>
<?php if($content): ?>
    <section class="text-content-section">
        <div class="container">
            <div class="text-content-section__inner">
                <?= $content; ?>
            </div>
        </div>
    </section>
<?php endif; ?>