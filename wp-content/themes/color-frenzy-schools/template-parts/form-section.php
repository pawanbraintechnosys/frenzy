<?php

$title = get_field('form-section__title');

$textAfter = get_field('form-section__text-after');

$shortcode = get_field('form-section__shortcode');

$link = get_field('form-section__link');

$text = get_field('form-section__text');

$btn = get_field('form-section__btn');

?>

<section class="form-section">

    <div class="container">

        <div class="form-section__wrapper">

            <?php if($title): ?>

                <div class="<?= $args['form-title-classes'] ?>">

                    <h2>

                        <?= $title; ?>

                    </h2>

                </div>

            <?php endif; ?>



            <?php if($textAfter): ?>

                <p>

                    <?= $textAfter; ?>

                </p>

            <?php endif; ?>



<!--            <div class="main-form --><?//= $args['form-classes'] ?><!--">-->

                <?= do_shortcode($shortcode); ?>

<!--            </div>-->



            <?php if($text && $btn): ?>

                <div class="form-section__description">

                    <?php if($text): ?>

                        <h3>

                            <?= $text; ?>

                        </h3>

                    <?php endif; ?>



                    <?php if($btn): ?>

                        <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn btn--blue">

                            <?= $btn['title']; ?>

                        </a>

                    <?php endif; ?>

                </div>

            <?php endif; ?>

        </div>

    </div>

</section>

