<?php
/*
 * Parent Invoice
 * @Package : Colourfrenzy
 * @Version : 1.0.0
 */
function parent_invoice_callback($atts)
{

    if (is_user_logged_in()):
        $user_id=get_current_user_id();
        $user=get_user_data($user_id);
        $invoice=getParentInvoice($user_id);
        ?>
        <div class="main-dashboard parent-invoice">
            <section class="invoice-dashboard">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="invoice-title">
                                <h2>Invoice</h2><h3 class="pull-right">Order # <?= $invoice[0]['invoice_id'] ?></h3>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-4">
                                    <address>
                                        <strong>Billed To:</strong><br>
                                        Name : <?= $user['display_name'] ?></br>
                                        Email : <?= $user['user_email'] ?></br>
                                        Phone : <?= $user['phone'] ?></br>
                                    </address>
                                </div>
                                <div class="col-lg-4">
                                    <address>
                                        <strong>Payment Detail:</strong><br>
                                        Payment Mode : <?= ($invoice[0]['payment_mode'])?$invoice[0]['payment_mode']:'Not Exists' ?><br>
                                        Transaction ID : <?= ($invoice[0]['payment_id'])?$invoice[0]['payment_id']:'Not Exists' ?><br>
                                    </address>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <address>
                                        <strong>Order Date:</strong><br>
                                       <?php echo date('d-m-Y',strtotime($invoice[0]['created_date'])); ?><br><br>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title" style="text-align: center;"><strong>Order summary</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <td><strong>Child Name</strong></td>
                                                <td class="text-center"><strong>Class</strong></td>
                                                <td class="text-center"><strong>Hat</strong></td>
                                                <td class="text-center"><strong>Cloth</strong></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(!empty($invoice)) : ?>
                                            <?php foreach ($invoice as $key=>$val): ?>
                                            <tr>
                                                <td><?= $val['firstname'].' '.$val['lastname'] ?></td>
                                                <td><?= $val['class'] ?></td>
                                                <td class="text-center"><?= $val['hat_number'] ?></td>
                                                <td class="text-center"><?= $val['cloth_number'] ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td class="thick-line"></td>
                                                <td class="thick-line"></td>
                                                <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                                <td class="thick-line">$<?php echo $invoice[0]['total_amount'] ?></td>
                                            </tr>

                                            <?php else : ?>
                                            <tr>
                                                <td class='not-found' style="text-align: center;" colspan="4">No Invoice Found</td>
                                            </tr>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}
add_shortcode('parent_invoice', 'parent_invoice_callback');