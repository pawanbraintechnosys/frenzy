<?php
$title = get_field('three-col-link-items__title');
$items = 'three-col-link-items__items';
?>
<?php if(have_rows($items)): ?>
    <section class="brush-bg-section">
        <div class="brush-bg-section__top-brush">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/blot-bg/yellow-top-bg.png" alt="brush">
        </div>
        <div class="container">
            <h2><?= $title; ?></h2>
            <div class="brush-bg-section__items link-items">
                <?php while (have_rows($items)): the_row();
                    $img = get_sub_field('image');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $button = get_sub_field('button');
                ?>
                    <div class="link-items__item link-item">
                        <div class="link-item__img">
                            <?= wp_get_attachment_image($img['id'], 'full'); ?>
                        </div>
                        <h3><?= $title; ?></h3>
                        <p><?= $description; ?></p>
                        <?php if($button): ?>
                            <a href="<?= $button['url']; ?>"  class="btn btn--small btn--transparent">
                                <?= $button['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="brush-bg-section__bottom-brush">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/blot-bg/yellow-bottom-bg.png" alt="brush">
        </div>
    </section>
<?php endif; ?>