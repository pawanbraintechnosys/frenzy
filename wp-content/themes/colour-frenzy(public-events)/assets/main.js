import 'swiper/swiper.scss';
import "./scss/style.scss";
import { runAfterDomLoad } from './js';

document.addEventListener('DOMContentLoaded', runAfterDomLoad);
