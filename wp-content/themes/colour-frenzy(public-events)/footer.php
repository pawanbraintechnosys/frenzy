<?php
$curDate = date("Y");
$firstRow = 'footer__first-nav-column';
$secondRow = 'footer__second-nav-column';
$thirdRow = 'footer__third-nav-column';
$email = get_field('general__email', 'option');
$facebook = get_field('general__facebook', 'option');
$instagram = get_field('general__instagram', 'option');
?>
</main>

<footer class="footer">
    <div class="container">
        <div class="footer__wrapper">
            <?php if(have_rows($firstRow, 'option')): ?>
            <ul class="footer__list">
                <?php while(have_rows($firstRow, 'option')): the_row();
                $rowLink = get_sub_field('link');
                ?>
                    <?php if($rowLink): ?>
                        <li>
                            <a href="<?= $rowLink['url']; ?>" >
                                <?= $rowLink['title']; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endwhile; ?>
            </ul>
            <?php endif; ?>
            <?php if(have_rows($secondRow, 'option')): ?>
                <ul class="footer__list">
                    <?php while(have_rows($secondRow, 'option')): the_row();
                        $rowLink = get_sub_field('link');
                    ?>
                        <?php if($rowLink): ?>
                            <li>
                                <a href="<?= $rowLink['url']; ?>" >
                                    <?= $rowLink['title']; ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
            <?php if(have_rows($thirdRow, 'option')): ?>
                <ul class="footer__list">
                    <?php while(have_rows($thirdRow, 'option')): the_row();
                        $rowLink = get_sub_field('link');
                    ?>
                        <?php if($rowLink): ?>
                            <li>
                                <a href="<?= $rowLink['url']; ?>" >
                                    <?= $rowLink['title']; ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
            <div class="footer__contact">
                <a href="/contact-us/" class="contact-link">
                    CONTACT US
                </a>
                <a href="mailto:<?= $email; ?>">
                    <?= $email; ?>
                </a>
                <ul>
                    <li><a href="<?= $facebook; ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/facebook.svg" alt="icon"></a></li>
                    <li><a href="<?= $instagram; ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/instagram.svg" alt="icon"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="footer__copyright">
        <div class="container">
            <div class="footer__copyright-wr">
                <a href="javascript:void(0);" >
                    BODY WIZARDS PTY LTD ABN 38 619 014 907
                </a>
                <p>
                    © <?= $curDate; ?> Colour Frenzy. All Rights Reserved.
                </p>
                <a href="https://www.thinkroom.com/" target="_blank">
                    Created by
                    <img src="<?= get_template_directory_uri(); ?>/assets/img/logo/logo_svg_white.svg" alt="logo">
                </a>
            </div>
        </div>
    </div>
</footer>

<div id="modal-window" class="modal-window">
    <div class="modal-window__fader"></div>
    <div id="modal-video" class="modal-window__item modal-window__video modal-video-item">
        <button aria-label="close modal window" class="modal-video-item__close modal-window__close-icon">
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M27.4142 0.585786C28.1953 1.36683 28.1953 2.63317 27.4142 3.41421L3.41421 27.4142C2.63317 28.1953 1.36683 28.1953 0.585786 27.4142C-0.195262 26.6332 -0.195262 25.3668 0.585786 24.5858L24.5858 0.585786C25.3668 -0.195262 26.6332 -0.195262 27.4142 0.585786Z" fill="#fff"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0.585786 0.585786C1.36683 -0.195262 2.63317 -0.195262 3.41421 0.585786L27.4142 24.5858C28.1953 25.3668 28.1953 26.6332 27.4142 27.4142C26.6332 28.1953 25.3668 28.1953 24.5858 27.4142L0.585786 3.41421C-0.195262 2.63317 -0.195262 1.36683 0.585786 0.585786Z" fill="#fff"/>
            </svg>
        </button>
        <div class="modal-video-item__wr-iframe">
            <iframe src="" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
    </div>
    <div id="modal-images" class="modal-window__item modal-images">
        <div class="modal-window__fader"></div>

        <div class="modal-images__inner">
            <img src="" alt="Image" class="changing-img not-close">
            <button aria-label="close modal window" class="modal-window__close-icon">
                <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M27.4142 0.585786C28.1953 1.36683 28.1953 2.63317 27.4142 3.41421L3.41421 27.4142C2.63317 28.1953 1.36683 28.1953 0.585786 27.4142C-0.195262 26.6332 -0.195262 25.3668 0.585786 24.5858L24.5858 0.585786C25.3668 -0.195262 26.6332 -0.195262 27.4142 0.585786Z"
                          fill="#fff"/>
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M0.585786 0.585786C1.36683 -0.195262 2.63317 -0.195262 3.41421 0.585786L27.4142 24.5858C28.1953 25.3668 28.1953 26.6332 27.4142 27.4142C26.6332 28.1953 25.3668 28.1953 24.5858 27.4142L0.585786 3.41421C-0.195262 2.63317 -0.195262 1.36683 0.585786 0.585786Z"
                          fill="#fff"/>
                </svg>
            </button>
            <div class="modal-arrow prev-btn not-close">
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/arrow-left.svg" alt="Prev arrow">
            </div>

            <div class="modal-arrow next-btn not-close">
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/arrow-left.svg" alt="Next arrow">
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
