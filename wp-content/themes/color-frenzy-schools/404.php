<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package colour-frenzy
 */
get_header();
?>
    <section class="hero-section hero-section--small hero-section--content-center">
        <div class="hero-section__bg">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/hero-section/404.jpg" alt="Page Not Found">
        </div>
        <div class="container">
            <div class="hero-section__content">
                <h1>
                    404
                </h1>
            </div>
        </div>
    </section>

    <section class="section-page-not-found">
        <div class="container">
            <h2>The Page You Are Looking For Cannot Be Found</h2>
            <a href="/schools/" class="btn">Go Home</a>
        </div>
    </section>

<?php
get_footer();
