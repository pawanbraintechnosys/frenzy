<?php
define('TABLE_PREFIX_MAIN','wp8t_');
define('DEFAULT_CURRENCY','$');
define('DEV_EMAIL','pawan@braintechnosys.biz');
define('ALLOWED_CHILD',3);
/*
 * Paypal Details for colourfrenzy
 */
define('PAYPAL_ACCOUNT','sb-6fkl49021148@business.example.com');
define('PAYPAL_CLIENT_ID','AX8_DsRjEoELShR80MdCNylLXTmL9Ge5tahc_7eYGvMrwGdHvI0bq7YMom_NZjiN3E7TKZwAerSGTPq6');
define('PAYPAL_SECRET','EKT9c3Os5O97t4IdqKSzeP0rNemaGGlDqTLSIrnmhp9UypKJv1b12Fle2FUTzePCgj_7lVaGfYvaLwVv');
define('APPLICATION_NAME','ColourFenzy');

// Stripe Key //
define("STRIPE_KEY",'pk_test_51ILPxqHGcI1rewMhZKPkJ4IQfBr7ZsxzXnUeAHLhBNsCojuvNGUicpPyeMLJHax5v0mvltR5qLYIjy6oBqbvWZ6n00rAL0ETn5');
define("STRIPE_SECRET","sk_test_51ILPxqHGcI1rewMha2SJ71vp3bAOeZaCzjQscDrbGJYnUWDurPFjjgSoVMr5t5wBznYm7G3LICngHutHulNEgDx100gNxm4fiJ");


// PayPal API configuration
define('PAYPAL_API_USERNAME', 'sb-6fkl49021148_api1.business.example.com');
define('PAYPAL_API_PASSWORD', '3VX85LRZFT65J694');
define('PAYPAL_API_SIGNATURE', 'AIxZDQFevfFlzpB47dd3ouR93yHeAYMbPSH3W945clz-Hwv8jp-.ZxXi');
define('PAYPAL_SANDBOX', TRUE); //TRUE or FALSE

// Database configuration
define('DB_HOST', 'MySQL_Database_Host');
define('DB_USERNAME', 'MySQL_Database_Username');
define('DB_PASSWORD', 'MySQL_Database_Password');
define('DB_NAME', 'MySQL_Database_Name');


/*
 * Include all customize file
 */
if(!function_exists('pr')){
    function pr($data){
        echo "<pre>";
        print_r($data);
    }
}
/* Enqueque css */
function frenzy_scripts_styles(){
    // load custom theme js //
    wp_enqueue_script( 'creditCardValidator', get_template_directory_uri() . '/assets/js/jquery.creditCardValidator.js');
    wp_enqueue_script( 'school', get_template_directory_uri() . '/assets/js/school.js');
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/custom.js');
    wp_enqueue_script( 'student', get_template_directory_uri() . '/assets/js/student.js');
    wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
    wp_enqueue_script( 'jquery-validator', 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.1/jquery.validate.min.js');
    wp_enqueue_script( 'jquery-additional-validator', 'https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js');
    $prime_option = array(
        'ajaxURL' => admin_url('admin-ajax.php'),
        'childClass' =>child_class_detail(),
        'clothSize' =>cloth_size_detail(),
        'hatPrice' =>20,
        'schoolFee' =>100,
        'allowed_child'=>ALLOWED_CHILD,
        'clothPrice' =>10,
        'child_entry_fee' =>get_post_meta(base64_decode(@$_GET['school-id']),'child_entry_fee',true),
    );
    wp_localize_script('custom', 'COLORFENZY_OPTION', $prime_option);
    wp_localize_script('school', 'COLORFENZY_OPTION', $prime_option);
    wp_localize_script('student', 'COLORFENZY_OPTION', $prime_option);
}
add_action( 'wp_enqueue_scripts', 'frenzy_scripts_styles' );
add_action("wp_footer", function() {
    ?><div class="ajaxLoading hide"><div class="ajaxLoadingWheel"></div></div><?php
});
add_filter( 'wp_mail_content_type', 'set_html_content_type' );
function set_html_content_type()
{
    return 'text/html';
}
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

/* get post detail */
if(!function_exists('post_detail')){
    function post_detail($post_id=null){
        $post = get_post($post_id, ARRAY_A );
        return $post;
    }
}
/* get all child class detail */
if(!function_exists('child_class_detail')){
    function child_class_detail(){
       $args=array(
           'post_type'=>'child-class',
           'post_status'=>'publish',
           'numberposts'=>-1,
           'orderby'=>'ID',
           'order'=>'ASC'
       );
        $childClass = get_posts( $args );
        return $childClass;
   
        //$args[] = get_school_webpage(base64_decode($_GET['school-id']))['data'];
        //pr(array_values($args));
    }
}
/* get all cloth_size_detail detail */
if(!function_exists('cloth_size_detail')) {
    function cloth_size_detail()
    {
        $args = array(
                'post_type' => 'tshirt-size',
                'post_status' => 'publish',
                'numberposts' => -1,
                'orderby' => 'ID',
                'order' => 'ASC'
        );
        $clothSize = get_posts($args);
        return $clothSize;
    }
}


/* Add student/parent role */
function add_custom_student_parent_role()
{
    add_role('student_role', 'Student/Parent', array('read' => true, 'level_0' => true));
}
add_action('init', 'add_custom_student_parent_role');
/* rewrite URL */
require_once ('custom/rewrite-url.php');
/* rewrite URL */

require_once ('school/register-school.php');
require_once ('model/Parent_model.php');
require_once ('common/helper.php');
require_once ('shortcode/school-shortcode.php');
require_once ('shortcode/school-dashboard.php');
require_once ('shortcode/school-fundrising.php');

require_once ('shortcode/school-leaderboard.php');
require_once ('shortcode/student-parent-registration.php');
require_once ('shortcode/create-school-webpage.php');
require_once ('shortcode/forget-password.php');
require_once ('school/school-ajax.php');
require_once ('school/school-fundraising-detail.php');
require_once ('school/student-fundraising-details.php');
//require_once ('school/school-listing-state.php');
require_once ('studentParent/student-parent-ajax.php');
require_once ('studentParent/parent-dashboard.php');
require_once ('studentParent/invoice/parent-invoice.php');
require_once ('shortcode/student-fundraising_detail.php');
/** Products **/
require_once ('products/manage-products.php');