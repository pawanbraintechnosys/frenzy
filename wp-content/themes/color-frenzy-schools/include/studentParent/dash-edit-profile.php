<?php
/*
Edit Student Profile
@Package Package colourfrenzy
@version 1.0.0
*/
?>
<?php
if(is_user_logged_in()):?>
    <?php
    $childDetail=getChildDetailFromChildID(base64_decode($_GET['child-id']))['data'];
    ?>
    <div class="school_login school-login-form" style="Padding: 40px 0;background: #1b7eaa;">
        <div class="container">
            <h2 style="text-align: center;">Manage <?=  $childDetail['firstname'].' '.@$childDetail['lastname']  ?> Profile</h2>
            <div class="row myform">
                <div class="col-9 col-md-12 col-lg-9">
                    <div class="card">
                        <div class="card-body">
                            <p class=""></p>
                            <form class="form-post" id="update-profile" method="POST" action="">
                                <input type="hidden" name="action" value="update_student_page">
                                <input type="hidden" name="child_id" value="<?= @$_GET['child-id'] ?>">
                                <input type="hidden" name="parent_id" value="<?= get_current_user_id(); ?>">
                                <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'update-student-profile' ); ?>">
                                <div class="form-group">
                                    <label class="control-label" for="email">First Name</label>
                                    <input id="title" Placeholder="First Name" name="title" type="text"  class="form-control " required="" autocomplete="off" autofocus="">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-lg btn-block w-100" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <link href="<?php echo get_template_directory_uri() ?>/assets/css/private.css" rel="stylesheet">
    <div class="unauthorized" style="background: url('https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/become-sponser.jpeg')">
        <div class="four_zero_four_bg">
            <h1 class="text-center ">404</h1>
        </div>
        <div class="contant_box_404">
            <p>Unathorized Access.</p>
            <a href="<?php echo home_url(); ?>" class="link_404">Go to Home</a>
        </div>
    </div>
<?php endif ?>
