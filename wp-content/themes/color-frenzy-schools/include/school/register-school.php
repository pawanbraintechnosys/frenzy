<?php
/*
 * Register School
 * ver 1.0
 */

/* Add school role */
function add_custom_school_role()
{
    add_role('school_role', 'School', array('read' => true, 'level_0' => true));
}
add_action('init', 'add_custom_school_role');

function school_register_callback($post_id, $post, $update)
{

    if ($post->post_type == 'schools') {
       // pr($_POST); die;
        /* On new Add School */
        if (strpos(wp_get_raw_referer(), 'post-new') > 0) {
            $hashedPassword = wp_generate_password(12, true);
            if(get_field('school_contact_email')) {

                $user_name=  get_field('school_contact_name');
                $user_name=$user_name.rand(1111,9999);
                $schoolDetail = array(
                    'user_login' => $user_name,
                    'display_name' => get_field('school_contact_name'),
                    'user_email' => get_field('school_contact_email'),
                    'role' => 'school_role',
                    'user_pass' => $hashedPassword
                );
                $emailTemplateData=array(
                    'user_login' => get_field('school_contact_email'),
                    'display_name' => get_field('school_contact_name'),
                    'user_email' => get_field('school_contact_email'),
                    'fundraising_event' => get_field('fundraising_event'),
                    'campaign_name' => get_field('campaign_name'),
                    'url' => home_url('school-login'),
                    'user_pass' => $hashedPassword,
                );
                ob_start();
                get_template_part('include/email-template/register-school', null, $emailTemplateData);
                $output = ob_get_contents();

                /* create user as a scool */
                $user_id = wp_insert_user($schoolDetail);
                if (!is_wp_error($user_id)) {
                    update_user_meta($user_id, 'school_phone', @$_POST['school_contact_phone']);
                    update_user_meta($user_id, 'school_without_password', $hashedPassword);
                    update_user_meta($user_id,'school_id',@$_POST['post_ID']);
                }
                /* Send Email */
                $emailData = array(
                    'to' => get_field('school_contact_email'),
                    'message' => $output,
                    'subject' => 'School Registration',
                    'password' => $hashedPassword
                );
                $mail = send_mail($emailData);
                ob_end_clean();
            }
        }
        /* On update School */
        if (!empty($update) && ($_POST['action'] == 'editpost')) {

        }

    }
}

add_action('save_post', 'school_register_callback', 10, 3);

add_action( 'wp_mail_failed', 'onMailError', 10, 1 );
function onMailError( $wp_error ) {
    echo "<pre>";
    print_r($wp_error);
    echo "</pre>";
}
/* Delete School User */
add_action( 'before_delete_post', 'wpdocs_my_func', 99, 2 );
function wpdocs_my_func( $postid, $post ) {
    global $wpdb;
    $user_id=$wpdb->get_results('SELECT * from wp8t_usermeta WHERE meta_key="school_id" AND meta_value='.$postid.'',ARRAY_A)[0];
    if ( 'schools' == $post->post_type ) {
        $response = wp_delete_user($user_id['user_id']);
    }
}

/** Email Validation */