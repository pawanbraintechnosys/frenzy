<?php
$greenItems = 'green__items';
?>
<?php if(have_rows($greenItems)): ?>
<section class="text-video-items">
    <div class="text-video-items__container container">
        <div class="text-video-items__wrapper">
            <?php while(have_rows($greenItems)): the_row();
                $greenBgReverse = get_sub_field('reverse-cond');
                $greenBgImage = get_sub_field('background_image');
                $greenVideoPreview = get_sub_field('image');
                $greenVideoLink = get_sub_field('video_link');
                $greenText = get_sub_field('text');
                ?>
                <div class="text-video-item <?= $greenBgReverse ? ' text-video-item--reverse ' : ''; ?> text-video-item--pink-head text-video-item--align-center text-video-item--small <?= $greenBgImage ? ' text-video-item--brush ' : ''; ?> ">
                    <div class="text-video-item__wr">
                        <a href="#" data-video-src="<?= $greenVideoLink; ?>" class="text-video-item__img">
                            <?= wp_get_attachment_image($greenVideoPreview['id'], 'full') ?>
                            <button class="play-video">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                            </button>
                        </a>
                        <div class="text-video-item__bg">
                            <?= wp_get_attachment_image($greenBgImage['id'], 'full') ?>
                        </div>
                    </div>
                    <div class="text-video-item__text">
                        <?= $greenText; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>