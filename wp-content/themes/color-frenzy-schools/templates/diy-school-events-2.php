<?php

//Template Name: DIY School Events 2

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center'
));

get_template_part('template-parts/text-img-items', null, array(
    'section-classes' => 'text-img-items--green text-img-items--green-brush',
    'items-classes' => 'text-img-item'
));

get_template_part('template-parts/form-section--more-info');

get_footer();

