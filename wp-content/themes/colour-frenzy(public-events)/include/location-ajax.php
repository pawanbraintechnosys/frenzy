<?php
function ajax_location_scripts() {
    if(is_page_template('templates/page-courses.php')):
        $wp_query = new WP_Query(
            array(
                'post_type' => 'events',
                'posts_per_page' => 10,
                'post_status' => 'publish'
            )
        );

        wp_register_script( 'ajax-location', get_template_directory_uri() . '/js/location-ajax.js', array('jquery'));

        wp_localize_script('ajax-location', 'location_params', array(
            'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
            'posts' => json_encode( $wp_query->query_vars ),
            'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages,
        ));

        wp_enqueue_script('ajax-location');
    endif;
}

add_action( 'wp_enqueue_scripts', 'ajax_location_scripts' );

//Location Load More
function location_loadmore(){
    $params = json_decode( stripslashes( $_POST['query'] ), true );
    $params['paged'] = $_POST['page'] + 1;
    query_posts($params);

    while(have_posts()): the_post();
        get_template_part('template-parts/items/location-item');
    endwhile;

    die;
}
add_action('wp_ajax_location_loadmore', 'location_loadmore');
add_action('wp_ajax_nopriv_location_loadmore', 'location_loadmore');

//Location Search
function location_search(){
    $params = json_decode( stripslashes( $_POST['query'] ), true );
    $params['paged'] = 1;
    $params['posts_per_page'] = 20;
    $params['s'] = $_POST['search'];
    query_posts($params);

    while(have_posts()): the_post();
        get_template_part('template-parts/items/location-item');
    endwhile;

    die;
}
add_action('wp_ajax_location_search', 'location_search');
add_action('wp_ajax_nopriv_location_search', 'location_search');