const CURRENT_YEAR = new Date().getFullYear()
const CURRENT_MONTH = new Date().getMonth()

const SEC_IN_MIN = 60
const MIN_IN_HOUR = 60
const HOUR_IN_DAY = 24
const DAY_IN_MONTH = new Date(CURRENT_YEAR, CURRENT_MONTH + 1, 0).getDate()

const SECOND = 1000
const MINUTE = SECOND * SEC_IN_MIN
const HOUR = MINUTE * MIN_IN_HOUR
const DAY = HOUR * HOUR_IN_DAY

export function countdown() {
  if (document.querySelector('#event-timer')) {
    const eventBlock = document.getElementById('event-timer')
    const eventDate = eventBlock.dataset.eventDate

    const dateItems = {
      day: {
        element: document.getElementById('days'),
        getReminder(milliseconds) {
          return Math.floor(milliseconds / DAY)
        },
        getProgress(milliseconds) {
          return this.getReminder(milliseconds) / DAY_IN_MONTH
        }
      },
      hour: {
        element: document.getElementById('hours'),
        getReminder(milliseconds) {
          return Math.floor(milliseconds % DAY / HOUR)
        },
        getProgress(milliseconds) {
          return this.getReminder(milliseconds) / HOUR_IN_DAY
        }
      },
      minute: {
        element: document.getElementById('minutes'),
        getReminder(milliseconds) {
          return Math.floor(milliseconds % HOUR / MINUTE)
        },
        getProgress(milliseconds) {
          return this.getReminder(milliseconds) / MIN_IN_HOUR
        }
      },
      second: {
        element: document.getElementById('seconds'),
        getReminder(milliseconds) {
          return Math.floor(milliseconds % MINUTE / SECOND)
        },
        getProgress(milliseconds) {
          return this.getReminder(milliseconds) / SEC_IN_MIN
        }
      },
    }

    setInterval(() => {
      const eventOffset = eventDate - Date.now()
      Object.keys(dateItems).forEach(dateItem => {
        updateDateElement(dateItems[dateItem], eventOffset)
      })
    }, 1000)
  }

  function updateDateElement(dateItem, eventOffset) {
    const element = dateItem.element

    const numPlace = element.querySelector('.countdown-item__num')
    const circle = element.querySelector('.circle')
    const circleLine = element.querySelector('.circle__line')

    const {x, y, width, height} = circle.viewBox.baseVal;
    const circleLength = width * Math.PI

    numPlace.textContent = dateItem.getReminder(eventOffset)
    circleLine.style.strokeDashoffset =
        `${circleLength - circleLength * dateItem.getProgress(eventOffset)}px`
  }
}
