<?php

//Template Name: Parent information

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center hero-section--mob-bg-right'
));

get_template_part('template-parts/text-img-items--tips');

get_footer();