<?php
$id = base64_decode($_GET['id']);
$schoolID=get_user_meta(get_current_user_id(),'school_id',true);
    //echo $schoolID;
    if (isset($schoolID)){        
	 $school_fund_page = new Parent_model();        
	 $table = "wp8t_school_details";        
	 $where = " WHERE school_id=" . $schoolID;        
	 $school_fund_page = $school_fund_page->get($table, $where);		
      if (!empty($school_fund_page['data'])){ 
        $child = new Parent_model();            
        $tabl="wp8t_child_details";
        $wher=" WHERE school_id=".$schoolID;
        $child=$child->get($tabl,$wher);
        if(!empty($child['data'])){
            $child=$child['data'][0];
            $user=get_userdata($child['parent_id']);
            /* Fund Detail */
            $child_fundpage=get_child_fundraising_page($child['parent_id'],$child['firstname']);
            $donation= get_all_fund_donation(@$child_fundpage['data']['id']);
            $fundpagee_id = $donation['latest_donation'][0]['fundpage_id'] ;
            //pr($donation);
        }
    }
}
?>
<div class="latest_leader max_width_set">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
                <?php if(!empty($donation['latest_donation'])): ?>
                <h3 class="heading">RECENT SCHOOL DONATIONS</h3>
                <?php
                    
                        foreach ($donation['latest_donation'] as $key=>$top_donation):
                            if($key==5){
                                break;
                            }    
                ?>
                <div class="">
                    <div class="card">
                        <div class="card-title">
                            <p><?php echo DEFAULT_CURRENCY.number_format($top_donation['amount']) ; ?></p>
                        </div>
                        <div class="line"></div>
                        <div class="card-description">
                            <p><?php echo $top_donation['shipping_firstname'].' '.$top_donation['shipping_lastname'];  ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php else: ?>
                <h3 class="heading">RECENT SCHOOL DONATIONS</h3>
                <div class="">
                    <div class="card">
                        <div class="Blankcard-description">
                           <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card">
                        <div class="Blankcard-description">
                           <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card">
                        <div class="Blankcard-description">
                           <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card">
                        <div class="Blankcard-description">
                           <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card">
                        <div class="Blankcard-description">
                           <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
			 <div class="col-lg-6">
                <?php 
                    $leader_bord = $donation['top_donation'];
                    if(!empty($leader_bord)){
                 ?>
                <h3 class="heading">SCHOOL LEADER BOARD</h3>
                 <?php      
                    foreach($leader_bord as $key=>$leader_bords){
                       //pr($leader_bords);
                       // if($leader_bords['type'] == 'SCHOOL'){
                        if($key==5){
                            break;
                        }
                        
                ?>
                <div class="leader">
                    <div class="card">
                        <div class="card-title"><img
                            src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/imageedit_7_3907638477.png">
                        </div>
                        <div class="card-description progress-report">
                            <div class="name">
                                <p><?php echo $leader_bords['shipping_firstname'].' '.$leader_bords['shipping_lastname'];  ?></p>
                                <div class="school-light-grey">
                                    <div class="school-red" style="width:<?php echo (round($leader_bords['amount'])*100)/(round($donation['total_fund_raised']  ['total_donation'])); ?>%"></div>
                                </div>
                            </div>
                        </div>
                        <p>
                            <?php 
                                if(!empty($leader_bords['amount'])){
                                    echo DEFAULT_CURRENCY.number_format($leader_bords['amount']);
                                }else{
                                    echo DEFAULT_CURRENCY."0.00";
                                }
                            ?>
                        </p>
                    </div>
                </div>
             <?php }}else{ ?>
                <h3 class="heading">SCHOOL LEADER BOARD</h3>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <?php   }  ?>
            </div>
		</div>
	</div>
</div>