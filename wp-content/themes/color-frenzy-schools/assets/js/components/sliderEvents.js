import Swiper, {Navigation, Pagination} from 'swiper';

Swiper.use([Navigation, Pagination]);

export function initSliderEvents() {
  const oneCol = new Swiper('.event-slider__container', {
    spaceBetween: 35,
    watchOverflow: true,
    watchSlidesVisibility:true,
    navigation: {
      nextEl: '.event-slider__next',
      prevEl: '.event-slider__prev',
    },
    pagination: {
      el: '.event-slider__pagination',
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: "auto",
        spaceBetween: 14
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      1280: {
        slidesPerView: 4,
      }
    }
  });
}
