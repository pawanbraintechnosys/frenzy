<?php
$items = 'text-image-items__items';
$btnTitle = get_field('text-image-items__button-title');
$btn = get_field('text-image-items__button');
?>
<?php if(have_rows($items)): ?>
<section class="text-img-items <?= $args['section-classes']; ?>">
    <div class="container">
        <?php while(have_rows($items)): the_row();
            $itemReverse = get_sub_field('reverse');
            $itemImage = get_sub_field('image');
            $itemText = get_sub_field('text');
            $itemBtn = get_sub_field('button');
            $showSliderImages = get_sub_field('show-slider-with-image');
            $sliderImages = 'slider-images';
        ?>
            <div class="text-img-items__item text-img-item <?= $args['bg-title']? 'text-img-item--head-bg-green' : ''; ?> <?= $args['item-classes']; ?> <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?>">
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                    </div>
                    <div class="text-img-item__text">
                        <?= $itemText; ?>
                        <?php if($itemBtn): ?>
                        <a href="<?= $itemBtn['url'] ?>"  target="<?= $itemBtn['target'] ?>" class="btn">
                            <?= $itemBtn['title'] ?>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php if(have_rows($sliderImages) && $showSliderImages): ?>
                <div class="carousel-img">
                    <button class="slider-arrow slider-arrow--prev carousel-img__arrow carousel-img__prev"></button>
                    <div class="carousel-img__container swiper-container">
                        <div class="carousel-img__wrapper swiper-wrapper">
                            <?php while (have_rows($sliderImages)): the_row();
                                $img = get_sub_field('image');
                                ?>
                                <div class="carousel-img__item swiper-slide" data-modal-id="modal-images">
                                    <?= wp_get_attachment_image($img['id'], 'full'); ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <button class="slider-arrow slider-arrow--next carousel-img__arrow carousel-img__next"></button>
                    <div class="slider-pagination carousel-img__pagination"></div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
        <?php if($btnTitle || $btn): ?>
        <div class="text-img-items__description-bg">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/content-img/brush-violet-bg.png" alt="image">
            <?php if($btnTitle): ?>
            <h3>
                <?= $btnTitle; ?>
            </h3>
            <?php endif; ?>

            <?php if($btn): ?>
            <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn">
                <?= $btn['title']; ?>
            </a>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php endif; ?>
