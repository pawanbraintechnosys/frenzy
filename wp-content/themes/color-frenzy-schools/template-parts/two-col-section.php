<?php
$title = get_field('two-col-items__title');
$items = 'two-col-items__items';
?>
<section class="two-col-section <?= $args['section-classes']; ?>">
    <div class="container">
        <h2>
            <?= $title; ?>
        </h2>
        <div class=" link-items link-items--whiteBg link-items--two-col ">
            <?php while(have_rows($items)): the_row();
            $itemImage = get_sub_field('image');
            $itemTitle = get_sub_field('title');
            $itemLink = get_sub_field('btn');
            ?>
            <div class="link-items__item link-item">
                <div class="link-item__img">
                    <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                </div>
                <h3>
                    <?= $itemTitle; ?>
                </h3>
                <?php if($itemLink): ?>
                    <a href="<?= $itemLink['url']; ?>"  target="<?= $itemLink['target']; ?>" class="btn btn--small btn--transparent btn--border-pink btn--animation">
                        <?= $itemLink['title']; ?>
                    </a>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
