<?php
$items = 'items';
$tableItemsTitle = get_field('table_items_title');
$tableOptionTitle1 = get_field('table_option_title_1');
$tableOptionTitle2 = get_field('table_option_title_2');
$tableItems = 'table_items';
?>
<?php if(have_rows($items)): ?>
<section class="table-section">
    <div class="container">
        <?php while(have_rows($items)): the_row();
            $itemReverse = get_sub_field('reverse');
            $itemImage = get_sub_field('image');
            $itemText = get_sub_field('text');
            ?>
            <div class=" text-img-item <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?>">
                <div class="text-img-item__img">
                    <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                </div>
                <div class="text-img-item__text">
                    <?= $itemText; ?>
                </div>
            </div>
        <?php endwhile; ?>
        <div class="table-section__table table table--three-col">
            <div class="table__row">
                <div class="table__item">
                        <span>
                            <?= $tableItemsTitle; ?>
                        </span>
                </div>
                <div class="table__price">
                    <?= $tableOptionTitle1; ?>
                </div>
                <div class="table__price">
                    <?= $tableOptionTitle2; ?>
                </div>
            </div>
            <?php while(have_rows($tableItems)): the_row();
                $tableItemQuestion = get_sub_field('item_name');
                $tableItemFirstOption = get_sub_field('add_checked_to_first_option');
                $tableItemSecondOption = get_sub_field('add_checkbox_to_second_option');
                ?>
                <div class="table__row">
                    <div class="table__item">
                        <?= $tableItemQuestion; ?>
                    </div>
                    <div class="table__info">
                        <?= $tableItemFirstOption ? '<img src="' . get_template_directory_uri() . '/assets/img/icon/table-check.svg" alt="check">' : ''; ?>
                    </div>
                    <div class="table__info">
                        <?= $tableItemSecondOption ? '<img src="' . get_template_directory_uri() . '/assets/img/icon/table-check.svg" alt="check">' : ''; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>