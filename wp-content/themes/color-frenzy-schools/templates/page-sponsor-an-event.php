<?php
/* Template Name: Sponsor an event page */
get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-img-items-section', null,
    array(
        'section-classes' => ' text-img-items--green ',
        'item-classes' => ' text-img-item head-bg head-bg--blue ',
    ));

get_template_part('template-parts/table-section--four-items');

get_template_part('template-parts/form-section', null,
    array(
        'form-title-classes' => ' form-section__head-bg head-bg head-bg--center head-bg--green-large '
    ));

get_footer();
