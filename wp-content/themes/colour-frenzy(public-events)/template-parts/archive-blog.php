<?php
$allCategories = get_categories();

$popularPosts = get_field('blog__popular-posts', 65);


$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$page = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

if($args['cat-slug']):
    $post_args = array(
        'post_type'       => 'post',
        'posts_per_page'  => '10',
        'category_name' => $args['cat-slug'],
        'paged' => $paged
    );
elseif($args['search-query']):
    $post_args = array(
        'post_type'       => 'post',
        'posts_per_page'  => '10',
        's' => $args['search-query'],
        'paged' => $page
    );
else:
    $post_args = array(
        'post_type'       => 'post',
        'posts_per_page'  => '10',
        'paged' => $paged
    );
endif;

$query = new WP_Query( $post_args );

$current = $query->query_vars['paged'] > 1 ? $current = $query->query_vars['paged'] : $current = 1;
$mainBlogPageLink = get_permalink(65);
?>
<section class="blog">
    <div class="container">
        <div class="blog__inner">
            <div class="blog__post-previews-wr">
                <div class="blog__post-previews">
                    <?php
                    if ( $query->have_posts() ) :
                        while ( $query->have_posts() ) : $query->the_post();
                            get_template_part( 'template-parts/items/archive-post-single' );
                        endwhile;
                    else : ?>
                        <h2>No blog posts found</h2>
                    <?php endif; ?>
                </div>

                <div class="blog__pagination">
                    <?php
                    echo paginate_links( array(
                        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                        'total'        => $query->max_num_pages,
                        'current'      => $args['search-query'] ? $current : max( 1, get_query_var( 'paged' ) ),
                        'format'       => $args['search-query'] ? '?page=%#%' : '?paged=%#%',
                        'show_all'     => false,
                        'type'         => 'list',
                        'end_size'     => 2,
                        'mid_size'     => 1,
                        'prev_next'    => true,
                        'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
                        'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                        'add_args'     => false,
                        'add_fragment' => '',
                    ) );
                    ?>
                </div>

                <?php wp_reset_query(); ?>
            </div>

            <div class="blog__categories">
                <?= get_search_form(); ?>
                <h3>
                    Categories
                </h3>
                <ul>
                    <li><a href="<?= $mainBlogPageLink; ?>">All categories</a></li>
                    <?php
                    if( $allCategories ){
                        foreach( $allCategories as $cat ){
                            $curCatLink = get_category_link($cat->cat_ID);
                            echo '<li><a href="'. $curCatLink .'">'. $cat->name .'</a></li>';
                        }
                    }
                    ?>
                </ul>
            </div>

            <div class="blog__popular-posts popular-posts">
                <?php if($popularPosts): ?>
                    <div class="blog__popular-posts-wr popular-posts__wrapper">
                        <h3>
                            Popular posts
                        </h3>

                        <?php
                        foreach ($popularPosts as $post): setup_postdata($post);
                            get_template_part('template-parts/items/popular-item');
                        endforeach;
                        ?>
                    </div>
                <?php endif; ?>

<!--                <a href="#" class="blog__link">-->
<!--                    <img src="--><?//= get_template_directory_uri(); ?><!--/assets/img/popular-post/link.png" alt="image">-->
<!--                </a>-->
            </div>
        </div>
    </div>
</section>