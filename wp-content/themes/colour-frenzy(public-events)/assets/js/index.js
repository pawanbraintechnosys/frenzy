import {initAccordion} from './components/accordion';
import {initMenu} from './components/menu';
import {initSliderEvents} from './components/sliderEvents';
import {initSliderReviews} from './components/sliderReviews';
import {initSliderTeam} from './components/sliderTeam';
import {initCurrentPage} from './components/currentPage';
import {scrollDown} from './components/scrollDown';
import {countdown} from './components/countdown';
import {searchInputClear} from './components/searchInputClear';
import ModalWindow from './components/ModalWindow/ModalWindow'
import ModalContentWindow from './components/ModalWindow/ModalContentWindow'
import ModalVideoWindow from './components/ModalWindow/ModalVideoWindow'


export function runAfterDomLoad() {
    initAccordion()
    initMenu()
    initSliderEvents()
    initSliderReviews()
    initSliderTeam()
    initCurrentPage()
    scrollDown()
    countdown()
    searchInputClear()
    new ModalWindow(ModalContentWindow, ModalVideoWindow)
}
