<?php
$green__items = 'green__items';
?>
<?php if(have_rows($green__items)): ?>
<section class="text-img-items text-img-items--green-dark">
    <div class="container">
        <?php while (have_rows($green__items)): the_row();
            $image = get_sub_field('image');
            $videoLink = get_sub_field('video_link');
            $text = get_sub_field('text');
            $leftButton = get_sub_field('left_button');
            $rightButton = get_sub_field('right_button');
        ?>
            <div class=" text-video-item text-video-item--pink-head text-video-item--brush text-video-item--small text-video-item--mob-reverse">
                <div class="text-video-item__wr">
                    <a href="#" data-video-src="<?= $videoLink; ?>" class="text-video-item__img">
                        <?= wp_get_attachment_image($image['id'], 'full'); ?>
                        <button class="play-video">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                        </button>
                    </a>
                    <div class="text-video-item__bg">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/text-img-item/img-bg-1.png" alt="image">
                    </div>
                </div>
                <div class="text-video-item__text">
                    <?= $text; ?>
                    <div class="text-video-item__btns">
                        <?php if($leftButton): ?>
                            <a href="<?= $leftButton['url']; ?>" class="btn" >
                                <?= $leftButton['title']; ?>
                            </a>
                        <?php endif; ?>

                        <?php if($rightButton): ?>
                            <a href="<?= $rightButton['url']; ?>"  class="btn btn--transparent btn--border-pink">
                                <?= $rightButton['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</section>
<?php endif; ?>