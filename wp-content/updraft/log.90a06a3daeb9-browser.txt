0000.030 (R) [notice] Looking for db archive: file name: backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-db.gz
0000.030 (R) [notice] Archive is expected to be size: 10944.4 KB: OK
0000.031 (R) [notice] Looking for plugins archive: file name: backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-plugins.zip
0000.031 (R) [notice] Archive is expected to be size: 43911.6 KB: OK
0000.031 (R) [notice] Looking for themes archive: file name: backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-themes.zip
0000.031 (R) [notice] Archive is expected to be size: 138349.6 KB: OK
0000.031 (R) [notice] Looking for uploads archive: file name: backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-uploads.zip
0000.031 (R) [notice] Archive is expected to be size: 324651.5 KB: OK
0000.031 (R) [notice] Looking for others archive: file name: backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-others.zip
0000.031 (R) [notice] Archive is expected to be size: 277.3 KB: OK
0000.031 (R) [notice] Will not delete any archives after unpacking them, because there was no cloud storage for this backup
0000.032 (R) [notice] Unpacking backup... (backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-db.gz, 10.7 Mb)
0000.633 (R) [notice] Restoring the database (on a large site this can take a long time - if it times out (which can happen if your web hosting company has configured your hosting to limit resources) then you should use a different method, such as phpMyAdmin)...
0000.639 (R) [notice] Enabling Maintenance mode&#8230;
0000.641 (R) [notice] Backup of: https://colourfrenzy.com.au
0000.642 (R) [notice] Content URL: https://colourfrenzy.com.au/wp-content
0000.642 (R) [notice] Uploads URL: https://colourfrenzy.com.au/wp-content/uploads
0000.642 (R) [notice] Old table prefix: wpmh_
0000.642 (R) [notice] Site information: multisite = 1
0000.642 (R) [notice] Site information: sql_mode = ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
0000.642 (R) [notice] New table prefix: 1z_
0000.643 (R) [notice] Processing table (InnoDB):  wpmh_options - will restore as: 1z_options
0000.678 (R) [notice] Atomic restore: dropping original table (wp8t_options)
0000.678 (R) [notice] Atomic restore: renaming new table (1z_options) to final table name (wp8t_options)
0000.681 (R) [notice] Table prefix has changed: changing option table field(s) accordingly: OK
0000.682 (R) [notice] Processing table (InnoDB):  wpmh_site - will restore as: 1z_site
0000.686 (R) [notice] Atomic restore: dropping original table (wp8t_site)
0000.686 (R) [notice] Atomic restore: renaming new table (1z_site) to final table name (wp8t_site)
0000.687 (R) [notice] Processing table (InnoDB):  wpmh_blogs - will restore as: 1z_blogs
0000.691 (R) [notice] Atomic restore: dropping original table (wp8t_blogs)
0000.691 (R) [notice] Atomic restore: renaming new table (1z_blogs) to final table name (wp8t_blogs)
0000.693 (R) [notice] Processing table (InnoDB):  wpmh_users - will restore as: 1z_users
0000.698 (R) [notice] Atomic restore: dropping original table (wp8t_users)
0000.698 (R) [notice] Atomic restore: renaming new table (1z_users) to final table name (wp8t_users)
0000.699 (R) [notice] Processing table (InnoDB):  wpmh_usermeta - will restore as: 1z_usermeta
0000.706 (R) [notice] Atomic restore: dropping original table (wp8t_usermeta)
0000.706 (R) [notice] Atomic restore: renaming new table (1z_usermeta) to final table name (wp8t_usermeta)
0000.709 (R) [notice] Table prefix has changed: changing usermeta table field(s) accordingly: OK
0000.709 (R) [notice] Processing table (InnoDB):  wpmh_blogmeta - will restore as: 1z_blogmeta
0000.712 (R) [notice] Atomic restore: dropping original table (wp8t_blogmeta)
0000.712 (R) [notice] Atomic restore: renaming new table (1z_blogmeta) to final table name (wp8t_blogmeta)
0000.714 (R) [notice] Processing table (InnoDB):  wpmh_commentmeta - will restore as: 1z_commentmeta
0000.718 (R) [notice] Atomic restore: dropping original table (wp8t_commentmeta)
0000.718 (R) [notice] Atomic restore: renaming new table (1z_commentmeta) to final table name (wp8t_commentmeta)
0000.720 (R) [notice] Processing table (InnoDB):  wpmh_comments - will restore as: 1z_comments
0000.725 (R) [notice] Atomic restore: dropping original table (wp8t_comments)
0000.725 (R) [notice] Atomic restore: renaming new table (1z_comments) to final table name (wp8t_comments)
0000.727 (R) [notice] Processing table (InnoDB):  wpmh_links - will restore as: 1z_links
0000.731 (R) [notice] Atomic restore: dropping original table (wp8t_links)
0000.731 (R) [notice] Atomic restore: renaming new table (1z_links) to final table name (wp8t_links)
0000.732 (R) [notice] Processing table (InnoDB):  wpmh_postmeta - will restore as: 1z_postmeta
0000.901 (R) [notice] Database queries processed: 50 in 0.27 seconds
0000.902 (R) [notice] Atomic restore: dropping original table (wp8t_postmeta)
0000.902 (R) [notice] Atomic restore: renaming new table (1z_postmeta) to final table name (wp8t_postmeta)
0000.904 (R) [notice] Processing table (InnoDB):  wpmh_posts - will restore as: 1z_posts
0000.971 (R) [notice] Atomic restore: dropping original table (wp8t_posts)
0000.971 (R) [notice] Atomic restore: renaming new table (1z_posts) to final table name (wp8t_posts)
0000.973 (R) [notice] Processing table (InnoDB):  wpmh_registration_log - will restore as: 1z_registration_log
0000.977 (R) [notice] Atomic restore: dropping original table (wp8t_registration_log)
0000.977 (R) [notice] Atomic restore: renaming new table (1z_registration_log) to final table name (wp8t_registration_log)
0000.978 (R) [notice] Processing table (InnoDB):  wpmh_signups - will restore as: 1z_signups
0000.983 (R) [notice] Atomic restore: dropping original table (wp8t_signups)
0000.983 (R) [notice] Atomic restore: renaming new table (1z_signups) to final table name (wp8t_signups)
0000.985 (R) [notice] Processing table (InnoDB):  wpmh_sitemeta - will restore as: 1z_sitemeta
0001.300 (R) [notice] Database queries processed: 100 in 0.67 seconds
0001.882 (R) [notice] Database queries processed: 150 in 1.25 seconds
0002.467 (R) [notice] Database queries processed: 200 in 1.83 seconds
0003.046 (R) [notice] Database queries processed: 250 in 2.41 seconds
0003.380 (R) [notice] Atomic restore: dropping original table (wp8t_sitemeta)
0003.380 (R) [notice] Atomic restore: renaming new table (1z_sitemeta) to final table name (wp8t_sitemeta)
0003.387 (R) [notice] Processing table (InnoDB):  wpmh_term_relationships - will restore as: 1z_term_relationships
0003.392 (R) [notice] Atomic restore: dropping original table (wp8t_term_relationships)
0003.393 (R) [notice] Atomic restore: renaming new table (1z_term_relationships) to final table name (wp8t_term_relationships)
0003.394 (R) [notice] Processing table (InnoDB):  wpmh_term_taxonomy - will restore as: 1z_term_taxonomy
0003.398 (R) [notice] Atomic restore: dropping original table (wp8t_term_taxonomy)
0003.398 (R) [notice] Atomic restore: renaming new table (1z_term_taxonomy) to final table name (wp8t_term_taxonomy)
0003.400 (R) [notice] Processing table (InnoDB):  wpmh_termmeta - will restore as: 1z_termmeta
0003.405 (R) [notice] Atomic restore: dropping original table (wp8t_termmeta)
0003.405 (R) [notice] Atomic restore: renaming new table (1z_termmeta) to final table name (wp8t_termmeta)
0003.406 (R) [notice] Processing table (InnoDB):  wpmh_terms - will restore as: 1z_terms
0003.411 (R) [notice] Atomic restore: dropping original table (wp8t_terms)
0003.411 (R) [notice] Atomic restore: renaming new table (1z_terms) to final table name (wp8t_terms)
0003.412 (R) [notice] Processing table (InnoDB):  wpmh_4_actionscheduler_actions - will restore as: 1z_4_actionscheduler_actions
0003.418 (R) [notice] Atomic restore: dropping original table (wp8t_4_actionscheduler_actions)
0003.418 (R) [notice] Atomic restore: renaming new table (1z_4_actionscheduler_actions) to final table name (wp8t_4_actionscheduler_actions)
0003.420 (R) [notice] Processing table (InnoDB):  wpmh_4_actionscheduler_claims - will restore as: 1z_4_actionscheduler_claims
0003.423 (R) [notice] Atomic restore: dropping original table (wp8t_4_actionscheduler_claims)
0003.423 (R) [notice] Atomic restore: renaming new table (1z_4_actionscheduler_claims) to final table name (wp8t_4_actionscheduler_claims)
0003.425 (R) [notice] Processing table (InnoDB):  wpmh_4_actionscheduler_groups - will restore as: 1z_4_actionscheduler_groups
0003.429 (R) [notice] Atomic restore: dropping original table (wp8t_4_actionscheduler_groups)
0003.429 (R) [notice] Atomic restore: renaming new table (1z_4_actionscheduler_groups) to final table name (wp8t_4_actionscheduler_groups)
0003.430 (R) [notice] Processing table (InnoDB):  wpmh_4_actionscheduler_logs - will restore as: 1z_4_actionscheduler_logs
0003.434 (R) [notice] Atomic restore: dropping original table (wp8t_4_actionscheduler_logs)
0003.434 (R) [notice] Atomic restore: renaming new table (1z_4_actionscheduler_logs) to final table name (wp8t_4_actionscheduler_logs)
0003.435 (R) [notice] Processing table (InnoDB):  wpmh_4_commentmeta - will restore as: 1z_4_commentmeta
0003.439 (R) [notice] Atomic restore: dropping original table (wp8t_4_commentmeta)
0003.439 (R) [notice] Atomic restore: renaming new table (1z_4_commentmeta) to final table name (wp8t_4_commentmeta)
0003.441 (R) [notice] Processing table (InnoDB):  wpmh_4_comments - will restore as: 1z_4_comments
0003.446 (R) [notice] Atomic restore: dropping original table (wp8t_4_comments)
0003.446 (R) [notice] Atomic restore: renaming new table (1z_4_comments) to final table name (wp8t_4_comments)
0003.447 (R) [notice] Processing table (InnoDB):  wpmh_4_create_map - will restore as: 1z_4_create_map
0003.451 (R) [notice] Atomic restore: dropping original table (wp8t_4_create_map)
0003.451 (R) [notice] Atomic restore: renaming new table (1z_4_create_map) to final table name (wp8t_4_create_map)
0003.452 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_addon_feed - will restore as: 1z_4_gf_addon_feed
0003.457 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_addon_feed)
0003.457 (R) [notice] Atomic restore: renaming new table (1z_4_gf_addon_feed) to final table name (wp8t_4_gf_addon_feed)
0003.458 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_addon_payment_callback - will restore as: 1z_4_gf_addon_payment_callback
0003.464 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_addon_payment_callback)
0003.464 (R) [notice] Atomic restore: renaming new table (1z_4_gf_addon_payment_callback) to final table name (wp8t_4_gf_addon_payment_callback)
0003.465 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_addon_payment_transaction - will restore as: 1z_4_gf_addon_payment_transaction
0003.476 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_addon_payment_transaction)
0003.476 (R) [notice] Atomic restore: renaming new table (1z_4_gf_addon_payment_transaction) to final table name (wp8t_4_gf_addon_payment_transaction)
0003.478 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_draft_submissions - will restore as: 1z_4_gf_draft_submissions
0003.481 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_draft_submissions)
0003.481 (R) [notice] Atomic restore: renaming new table (1z_4_gf_draft_submissions) to final table name (wp8t_4_gf_draft_submissions)
0003.482 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_entry - will restore as: 1z_4_gf_entry
0003.592 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_entry)
0003.592 (R) [notice] Atomic restore: renaming new table (1z_4_gf_entry) to final table name (wp8t_4_gf_entry)
0003.593 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_entry_meta - will restore as: 1z_4_gf_entry_meta
0004.287 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_entry_meta)
0004.287 (R) [notice] Atomic restore: renaming new table (1z_4_gf_entry_meta) to final table name (wp8t_4_gf_entry_meta)
0004.289 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_entry_notes - will restore as: 1z_4_gf_entry_notes
0004.455 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_entry_notes)
0004.455 (R) [notice] Atomic restore: renaming new table (1z_4_gf_entry_notes) to final table name (wp8t_4_gf_entry_notes)
0004.457 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_form - will restore as: 1z_4_gf_form
0004.461 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_form)
0004.461 (R) [notice] Atomic restore: renaming new table (1z_4_gf_form) to final table name (wp8t_4_gf_form)
0004.462 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_form_meta - will restore as: 1z_4_gf_form_meta
0004.510 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_form_meta)
0004.510 (R) [notice] Atomic restore: renaming new table (1z_4_gf_form_meta) to final table name (wp8t_4_gf_form_meta)
0004.511 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_form_revisions - will restore as: 1z_4_gf_form_revisions
0004.515 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_form_revisions)
0004.515 (R) [notice] Atomic restore: renaming new table (1z_4_gf_form_revisions) to final table name (wp8t_4_gf_form_revisions)
0004.516 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_form_view - will restore as: 1z_4_gf_form_view
0004.534 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_form_view)
0004.534 (R) [notice] Atomic restore: renaming new table (1z_4_gf_form_view) to final table name (wp8t_4_gf_form_view)
0004.536 (R) [notice] Processing table (InnoDB):  wpmh_4_gf_rest_api_keys - will restore as: 1z_4_gf_rest_api_keys
0004.539 (R) [notice] Atomic restore: dropping original table (wp8t_4_gf_rest_api_keys)
0004.539 (R) [notice] Atomic restore: renaming new table (1z_4_gf_rest_api_keys) to final table name (wp8t_4_gf_rest_api_keys)
0004.541 (R) [notice] Processing table (InnoDB):  wpmh_4_group_map - will restore as: 1z_4_group_map
0004.543 (R) [notice] Atomic restore: dropping original table (wp8t_4_group_map)
0004.544 (R) [notice] Atomic restore: renaming new table (1z_4_group_map) to final table name (wp8t_4_group_map)
0004.545 (R) [notice] Processing table (InnoDB):  wpmh_4_links - will restore as: 1z_4_links
0004.548 (R) [notice] Atomic restore: dropping original table (wp8t_4_links)
0004.548 (R) [notice] Atomic restore: renaming new table (1z_4_links) to final table name (wp8t_4_links)
0004.549 (R) [notice] Processing table (InnoDB):  wpmh_4_map_locations - will restore as: 1z_4_map_locations
0004.552 (R) [notice] Atomic restore: dropping original table (wp8t_4_map_locations)
0004.552 (R) [notice] Atomic restore: renaming new table (1z_4_map_locations) to final table name (wp8t_4_map_locations)
0004.553 (R) [notice] Processing table (InnoDB):  wpmh_4_map_routes - will restore as: 1z_4_map_routes
0004.556 (R) [notice] Atomic restore: dropping original table (wp8t_4_map_routes)
0004.556 (R) [notice] Atomic restore: renaming new table (1z_4_map_routes) to final table name (wp8t_4_map_routes)
0004.557 (R) [notice] Processing table (InnoDB):  wpmh_4_options - will restore as: 1z_4_options
0004.585 (R) [notice] Atomic restore: dropping original table (wp8t_4_options)
0004.585 (R) [notice] Atomic restore: renaming new table (1z_4_options) to final table name (wp8t_4_options)
0004.587 (R) [notice] Table prefix has changed: changing option table field(s) accordingly: OK
0004.588 (R) [notice] Processing table (InnoDB):  wpmh_4_postmeta - will restore as: 1z_4_postmeta
0005.198 (R) [notice] Atomic restore: dropping original table (wp8t_4_postmeta)
0005.198 (R) [notice] Atomic restore: renaming new table (1z_4_postmeta) to final table name (wp8t_4_postmeta)
0005.200 (R) [notice] Processing table (InnoDB):  wpmh_4_posts - will restore as: 1z_4_posts
0005.396 (R) [notice] Atomic restore: dropping original table (wp8t_4_posts)
0005.396 (R) [notice] Atomic restore: renaming new table (1z_4_posts) to final table name (wp8t_4_posts)
0005.397 (R) [notice] Processing table (InnoDB):  wpmh_4_seopress_significant_keywords - will restore as: 1z_4_seopress_significant_keywords
0005.401 (R) [notice] Atomic restore: dropping original table (wp8t_4_seopress_significant_keywords)
0005.401 (R) [notice] Atomic restore: renaming new table (1z_4_seopress_significant_keywords) to final table name (wp8t_4_seopress_significant_keywords)
0005.402 (R) [notice] Processing table (InnoDB):  wpmh_4_sib_model_forms - will restore as: 1z_4_sib_model_forms
0005.406 (R) [notice] Atomic restore: dropping original table (wp8t_4_sib_model_forms)
0005.406 (R) [notice] Atomic restore: renaming new table (1z_4_sib_model_forms) to final table name (wp8t_4_sib_model_forms)
0005.407 (R) [notice] Processing table (InnoDB):  wpmh_4_sib_model_users - will restore as: 1z_4_sib_model_users
0005.410 (R) [notice] Atomic restore: dropping original table (wp8t_4_sib_model_users)
0005.410 (R) [notice] Atomic restore: renaming new table (1z_4_sib_model_users) to final table name (wp8t_4_sib_model_users)
0005.412 (R) [notice] Processing table (InnoDB):  wpmh_4_term_relationships - will restore as: 1z_4_term_relationships
0005.416 (R) [notice] Atomic restore: dropping original table (wp8t_4_term_relationships)
0005.416 (R) [notice] Atomic restore: renaming new table (1z_4_term_relationships) to final table name (wp8t_4_term_relationships)
0005.417 (R) [notice] Processing table (InnoDB):  wpmh_4_term_taxonomy - will restore as: 1z_4_term_taxonomy
0005.421 (R) [notice] Atomic restore: dropping original table (wp8t_4_term_taxonomy)
0005.421 (R) [notice] Atomic restore: renaming new table (1z_4_term_taxonomy) to final table name (wp8t_4_term_taxonomy)
0005.422 (R) [notice] Processing table (InnoDB):  wpmh_4_termmeta - will restore as: 1z_4_termmeta
0005.426 (R) [notice] Atomic restore: dropping original table (wp8t_4_termmeta)
0005.426 (R) [notice] Atomic restore: renaming new table (1z_4_termmeta) to final table name (wp8t_4_termmeta)
0005.427 (R) [notice] Processing table (InnoDB):  wpmh_4_terms - will restore as: 1z_4_terms
0005.431 (R) [notice] Atomic restore: dropping original table (wp8t_4_terms)
0005.431 (R) [notice] Atomic restore: renaming new table (1z_4_terms) to final table name (wp8t_4_terms)
0005.432 (R) [notice] Processing table (InnoDB):  wpmh_4_wpgmza - will restore as: 1z_4_wpgmza
0005.435 (R) [notice] Atomic restore: dropping original table (wp8t_4_wpgmza)
0005.435 (R) [notice] Atomic restore: renaming new table (1z_4_wpgmza) to final table name (wp8t_4_wpgmza)
0005.437 (R) [notice] Processing table (InnoDB):  wpmh_4_wpgmza_circles - will restore as: 1z_4_wpgmza_circles
0005.440 (R) [notice] Atomic restore: dropping original table (wp8t_4_wpgmza_circles)
0005.440 (R) [notice] Atomic restore: renaming new table (1z_4_wpgmza_circles) to final table name (wp8t_4_wpgmza_circles)
0005.441 (R) [notice] Processing table (InnoDB):  wpmh_4_wpgmza_maps - will restore as: 1z_4_wpgmza_maps
0005.444 (R) [notice] Atomic restore: dropping original table (wp8t_4_wpgmza_maps)
0005.444 (R) [notice] Atomic restore: renaming new table (1z_4_wpgmza_maps) to final table name (wp8t_4_wpgmza_maps)
0005.445 (R) [notice] Processing table (InnoDB):  wpmh_4_wpgmza_polygon - will restore as: 1z_4_wpgmza_polygon
0005.448 (R) [notice] Atomic restore: dropping original table (wp8t_4_wpgmza_polygon)
0005.449 (R) [notice] Atomic restore: renaming new table (1z_4_wpgmza_polygon) to final table name (wp8t_4_wpgmza_polygon)
0005.450 (R) [notice] Processing table (InnoDB):  wpmh_4_wpgmza_polylines - will restore as: 1z_4_wpgmza_polylines
0005.452 (R) [notice] Atomic restore: dropping original table (wp8t_4_wpgmza_polylines)
0005.453 (R) [notice] Atomic restore: renaming new table (1z_4_wpgmza_polylines) to final table name (wp8t_4_wpgmza_polylines)
0005.454 (R) [notice] Processing table (InnoDB):  wpmh_4_wpgmza_rectangles - will restore as: 1z_4_wpgmza_rectangles
0005.457 (R) [notice] Atomic restore: dropping original table (wp8t_4_wpgmza_rectangles)
0005.457 (R) [notice] Atomic restore: renaming new table (1z_4_wpgmza_rectangles) to final table name (wp8t_4_wpgmza_rectangles)
0005.458 (R) [notice] Processing table (InnoDB):  wpmh_actionscheduler_actions - will restore as: 1z_actionscheduler_actions
0005.464 (R) [notice] Atomic restore: dropping original table (wp8t_actionscheduler_actions)
0005.464 (R) [notice] Atomic restore: renaming new table (1z_actionscheduler_actions) to final table name (wp8t_actionscheduler_actions)
0005.466 (R) [notice] Processing table (InnoDB):  wpmh_actionscheduler_claims - will restore as: 1z_actionscheduler_claims
0005.469 (R) [notice] Atomic restore: dropping original table (wp8t_actionscheduler_claims)
0005.469 (R) [notice] Atomic restore: renaming new table (1z_actionscheduler_claims) to final table name (wp8t_actionscheduler_claims)
0005.470 (R) [notice] Processing table (InnoDB):  wpmh_actionscheduler_groups - will restore as: 1z_actionscheduler_groups
0005.473 (R) [notice] Atomic restore: dropping original table (wp8t_actionscheduler_groups)
0005.474 (R) [notice] Atomic restore: renaming new table (1z_actionscheduler_groups) to final table name (wp8t_actionscheduler_groups)
0005.475 (R) [notice] Processing table (InnoDB):  wpmh_actionscheduler_logs - will restore as: 1z_actionscheduler_logs
0005.478 (R) [notice] Atomic restore: dropping original table (wp8t_actionscheduler_logs)
0005.478 (R) [notice] Database queries processed: 500 in 4.84 seconds
0005.478 (R) [notice] Atomic restore: renaming new table (1z_actionscheduler_logs) to final table name (wp8t_actionscheduler_logs)
0005.479 (R) [notice] Processing table (InnoDB):  wpmh_bv_fw_requests - will restore as: 1z_bv_fw_requests
0005.482 (R) [notice] Atomic restore: dropping original table (wp8t_bv_fw_requests)
0005.483 (R) [notice] Atomic restore: renaming new table (1z_bv_fw_requests) to final table name (wp8t_bv_fw_requests)
0005.484 (R) [notice] Processing table (InnoDB):  wpmh_bv_ip_store - will restore as: 1z_bv_ip_store
0005.511 (R) [notice] Atomic restore: dropping original table (wp8t_bv_ip_store)
0005.511 (R) [notice] Atomic restore: renaming new table (1z_bv_ip_store) to final table name (wp8t_bv_ip_store)
0005.512 (R) [notice] Processing table (InnoDB):  wpmh_bv_lp_requests - will restore as: 1z_bv_lp_requests
0005.515 (R) [notice] Atomic restore: dropping original table (wp8t_bv_lp_requests)
0005.515 (R) [notice] Atomic restore: renaming new table (1z_bv_lp_requests) to final table name (wp8t_bv_lp_requests)
0005.516 (R) [notice] Processing table (InnoDB):  wpmh_create_map - will restore as: 1z_create_map
0005.520 (R) [notice] Atomic restore: dropping original table (wp8t_create_map)
0005.520 (R) [notice] Atomic restore: renaming new table (1z_create_map) to final table name (wp8t_create_map)
0005.521 (R) [notice] Processing table (InnoDB):  wpmh_gf_draft_submissions - will restore as: 1z_gf_draft_submissions
0005.524 (R) [notice] Atomic restore: dropping original table (wp8t_gf_draft_submissions)
0005.524 (R) [notice] Atomic restore: renaming new table (1z_gf_draft_submissions) to final table name (wp8t_gf_draft_submissions)
0005.525 (R) [notice] Processing table (InnoDB):  wpmh_gf_entry - will restore as: 1z_gf_entry
0005.539 (R) [notice] Atomic restore: dropping original table (wp8t_gf_entry)
0005.540 (R) [notice] Atomic restore: renaming new table (1z_gf_entry) to final table name (wp8t_gf_entry)
0005.541 (R) [notice] Processing table (InnoDB):  wpmh_gf_entry_meta - will restore as: 1z_gf_entry_meta
0005.590 (R) [notice] Atomic restore: dropping original table (wp8t_gf_entry_meta)
0005.590 (R) [notice] Atomic restore: renaming new table (1z_gf_entry_meta) to final table name (wp8t_gf_entry_meta)
0005.592 (R) [notice] Processing table (InnoDB):  wpmh_gf_entry_notes - will restore as: 1z_gf_entry_notes
0005.601 (R) [notice] Atomic restore: dropping original table (wp8t_gf_entry_notes)
0005.601 (R) [notice] Atomic restore: renaming new table (1z_gf_entry_notes) to final table name (wp8t_gf_entry_notes)
0005.603 (R) [notice] Processing table (InnoDB):  wpmh_gf_form - will restore as: 1z_gf_form
0005.607 (R) [notice] Atomic restore: dropping original table (wp8t_gf_form)
0005.607 (R) [notice] Atomic restore: renaming new table (1z_gf_form) to final table name (wp8t_gf_form)
0005.608 (R) [notice] Processing table (InnoDB):  wpmh_gf_form_meta - will restore as: 1z_gf_form_meta
0005.628 (R) [notice] Atomic restore: dropping original table (wp8t_gf_form_meta)
0005.628 (R) [notice] Atomic restore: renaming new table (1z_gf_form_meta) to final table name (wp8t_gf_form_meta)
0005.629 (R) [notice] Processing table (InnoDB):  wpmh_gf_form_revisions - will restore as: 1z_gf_form_revisions
0005.632 (R) [notice] Atomic restore: dropping original table (wp8t_gf_form_revisions)
0005.632 (R) [notice] Atomic restore: renaming new table (1z_gf_form_revisions) to final table name (wp8t_gf_form_revisions)
0005.634 (R) [notice] Processing table (InnoDB):  wpmh_gf_form_view - will restore as: 1z_gf_form_view
0005.645 (R) [notice] Atomic restore: dropping original table (wp8t_gf_form_view)
0005.645 (R) [notice] Atomic restore: renaming new table (1z_gf_form_view) to final table name (wp8t_gf_form_view)
0005.646 (R) [notice] Processing table (InnoDB):  wpmh_gf_rest_api_keys - will restore as: 1z_gf_rest_api_keys
0005.650 (R) [notice] Atomic restore: dropping original table (wp8t_gf_rest_api_keys)
0005.650 (R) [notice] Atomic restore: renaming new table (1z_gf_rest_api_keys) to final table name (wp8t_gf_rest_api_keys)
0005.652 (R) [notice] Processing table (InnoDB):  wpmh_group_map - will restore as: 1z_group_map
0005.655 (R) [notice] Atomic restore: dropping original table (wp8t_group_map)
0005.655 (R) [notice] Atomic restore: renaming new table (1z_group_map) to final table name (wp8t_group_map)
0005.657 (R) [notice] Processing table (InnoDB):  wpmh_mainwp_stream - will restore as: 1z_mainwp_stream
0006.018 (R) [notice] Atomic restore: dropping original table (wp8t_mainwp_stream)
0006.018 (R) [notice] Atomic restore: renaming new table (1z_mainwp_stream) to final table name (wp8t_mainwp_stream)
0006.020 (R) [notice] Processing table (InnoDB):  wpmh_mainwp_stream_meta - will restore as: 1z_mainwp_stream_meta
0007.547 (R) [notice] Atomic restore: dropping original table (wp8t_mainwp_stream_meta)
0007.547 (R) [notice] Atomic restore: renaming new table (1z_mainwp_stream_meta) to final table name (wp8t_mainwp_stream_meta)
0007.549 (R) [notice] Processing table (InnoDB):  wpmh_map_locations - will restore as: 1z_map_locations
0007.553 (R) [notice] Atomic restore: dropping original table (wp8t_map_locations)
0007.553 (R) [notice] Atomic restore: renaming new table (1z_map_locations) to final table name (wp8t_map_locations)
0007.555 (R) [notice] Processing table (InnoDB):  wpmh_map_routes - will restore as: 1z_map_routes
0007.558 (R) [notice] Atomic restore: dropping original table (wp8t_map_routes)
0007.558 (R) [notice] Atomic restore: renaming new table (1z_map_routes) to final table name (wp8t_map_routes)
0007.560 (R) [notice] Processing table (InnoDB):  wpmh_seopress_significant_keywords - will restore as: 1z_seopress_significant_keywords
0007.563 (R) [notice] Atomic restore: dropping original table (wp8t_seopress_significant_keywords)
0007.564 (R) [notice] Atomic restore: renaming new table (1z_seopress_significant_keywords) to final table name (wp8t_seopress_significant_keywords)
0007.565 (R) [notice] Processing table (InnoDB):  wpmh_sib_model_forms - will restore as: 1z_sib_model_forms
0007.569 (R) [notice] Atomic restore: dropping original table (wp8t_sib_model_forms)
0007.569 (R) [notice] Atomic restore: renaming new table (1z_sib_model_forms) to final table name (wp8t_sib_model_forms)
0007.570 (R) [notice] Processing table (InnoDB):  wpmh_sib_model_users - will restore as: 1z_sib_model_users
0007.573 (R) [notice] Atomic restore: dropping original table (wp8t_sib_model_users)
0007.573 (R) [notice] Atomic restore: renaming new table (1z_sib_model_users) to final table name (wp8t_sib_model_users)
0007.575 (R) [notice] Processing table (InnoDB):  wpmh_wpgmza - will restore as: 1z_wpgmza
0007.578 (R) [notice] Atomic restore: dropping original table (wp8t_wpgmza)
0007.578 (R) [notice] Atomic restore: renaming new table (1z_wpgmza) to final table name (wp8t_wpgmza)
0007.579 (R) [notice] Processing table (InnoDB):  wpmh_wpgmza_circles - will restore as: 1z_wpgmza_circles
0007.582 (R) [notice] Atomic restore: dropping original table (wp8t_wpgmza_circles)
0007.582 (R) [notice] Atomic restore: renaming new table (1z_wpgmza_circles) to final table name (wp8t_wpgmza_circles)
0007.584 (R) [notice] Processing table (InnoDB):  wpmh_wpgmza_maps - will restore as: 1z_wpgmza_maps
0007.587 (R) [notice] Atomic restore: dropping original table (wp8t_wpgmza_maps)
0007.587 (R) [notice] Atomic restore: renaming new table (1z_wpgmza_maps) to final table name (wp8t_wpgmza_maps)
0007.588 (R) [notice] Processing table (InnoDB):  wpmh_wpgmza_polygon - will restore as: 1z_wpgmza_polygon
0007.591 (R) [notice] Atomic restore: dropping original table (wp8t_wpgmza_polygon)
0007.591 (R) [notice] Atomic restore: renaming new table (1z_wpgmza_polygon) to final table name (wp8t_wpgmza_polygon)
0007.593 (R) [notice] Processing table (InnoDB):  wpmh_wpgmza_polylines - will restore as: 1z_wpgmza_polylines
0007.596 (R) [notice] Atomic restore: dropping original table (wp8t_wpgmza_polylines)
0007.596 (R) [notice] Atomic restore: renaming new table (1z_wpgmza_polylines) to final table name (wp8t_wpgmza_polylines)
0007.597 (R) [notice] Processing table (InnoDB):  wpmh_wpgmza_rectangles - will restore as: 1z_wpgmza_rectangles
0008.048 (R) [notice] Disabling Maintenance mode&#8230;
0008.048 (R) [notice] Atomic restore: dropping original table (wp8t_wpgmza_rectangles)
0008.048 (R) [notice] Atomic restore: renaming new table (1z_wpgmza_rectangles) to final table name (wp8t_wpgmza_rectangles)
0008.050 (R) [notice] Finished: lines processed: 615 in 7.42 seconds
0008.052 (R) [notice] Cleaning up rubbish...
0008.052 (R) [notice] This option was not selected.
0008.055 (R) [notice] Unpacking backup... (backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-plugins.zip, 42.9 MB)
0008.233 (R) [notice] Unzip progress: 2305 out of 11731 files (18 KB, plugins/post-smtp/Postman/Postman-Mail/mailchimp-mandrill-api-php-da3adc10042e/src/Mandrill/Senders.php)
0009.932 (R) [notice] Unzip progress: 3306 out of 11731 files (26.8 MB, plugins/post-smtp/Postman/Postman-Mail/Zend-1.12.10/Validate/Exception.php)
0010.388 (R) [notice] Unzip progress: 4307 out of 11731 files (30.6 MB, plugins/updraftplus/vendor/rackspace/php-opencloud/lib/OpenCloud/Common/Exceptions/NetworkError.php)
0012.463 (R) [notice] Unzip progress: 5308 out of 11731 files (52.3 MB, plugins/updraftplus/vendor/yahnis-elsts/plugin-update-checker/vendor/Parsedown.php)
0013.058 (R) [notice] Unzip progress: 6309 out of 11731 files (56.9 MB, plugins/wp-google-map-gold/assets/images/icons/skateboarding.png)
0013.810 (R) [notice] Unzip progress: 7310 out of 11731 files (64.3 MB, plugins/gravityformsstripe/includes/stripe/stripe-php/lib/Service/Reporting/ReportRunService.php)
0015.442 (R) [notice] Unzip progress: 8311 out of 11731 files (80.9 MB, plugins/w3-total-cache/Minify_Plugin.php)
0016.930 (R) [notice] Unzip progress: 9312 out of 11731 files (96.5 MB, plugins/w3-total-cache/vendor/aws/aws-sdk-php/src/data/cloudfront/2016-09-07/api-2.json.php)
0017.676 (R) [notice] Unzip progress: 10313 out of 11731 files (103 MB, plugins/w3-total-cache/lib/Azure/MicrosoftAzureStorage/Blob/Models/DeleteContainerOptions.php)
0019.235 (R) [notice] Unzip progress: 11314 out of 11731 files (119.2 MB, plugins/mailin/img/flags/ch.png)
0019.849 (R) [notice] Unzip progress: 11731 out of 11731 files (126.1 MB, plugins/w3-total-cache/lib/Azure/GuzzleHttp/Handler/CurlFactory.php)
0019.854 (R) [notice] Moving old data out of the way...
0019.880 (R) [notice] Moving unpacked backup into place...
0020.047 (R) [notice] Cleaning up rubbish...
0020.049 (R) [notice] Unpacking backup... (backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-themes.zip, 135.1 MB)
0028.236 (R) [notice] Unzip progress: 1002 out of 1395 files (98.5 MB, themes/twentynineteen/classes/class-twentynineteen-walker-comment.php)
0032.829 (R) [notice] Unzip progress: 1395 out of 1395 files (152.8 MB, themes/colour-frenzy(public-events)/assets/img/text-img-item/message-bg-1.png)
0032.830 (R) [notice] Moving old data out of the way...
0032.837 (R) [notice] Moving unpacked backup into place...
0032.854 (R) [notice] Cleaning up rubbish...
0032.856 (R) [notice] Unpacking backup... (backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-uploads.zip, 317 MB)
0041.294 (R) [notice] Unzip progress: 878 out of 2237 files (100.1 MB, uploads/2021/07/IMG_7586.jpg)
0049.776 (R) [notice] Unzip progress: 1411 out of 2237 files (200.2 MB, uploads/sites/4/2021/07/544-1-of-1_1-1154x2048.jpg)
0058.263 (R) [notice] Unzip progress: 2107 out of 2237 files (300.3 MB, uploads/sites/4/2021/06/school-home-1536x560.jpg)
0059.901 (R) [notice] Unzip progress: 2237 out of 2237 files (319.7 MB, uploads/sites/4/2021/09/Colour-Frenzy-Student-Take-Home-Slip-SCCC-Kippa-Ring-1-pdf.jpg)
0059.902 (R) [notice] Moving old data out of the way...
0059.903 (R) [notice] Moving unpacked backup into place...
0059.928 (R) [notice] Cleaning up rubbish...
0059.930 (R) [notice] Unpacking backup... (backup_2021-11-23-0704_Colorfrenzy_a5cf4fe1fa2d-others.zip, 0.3 MB)
0060.193 (R) [notice] Unzip progress: 10 out of 10 files (3.4 MB, advanced-cache.php)
0060.194 (R) [notice] Cleaning up rubbish...
0060.282 (R) [notice] Restore successful!
