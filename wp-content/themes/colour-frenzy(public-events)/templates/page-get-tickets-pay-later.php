<?php

/* Template Name: Get tickets pay later page */


get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-image-section--icon-list');

get_footer();
