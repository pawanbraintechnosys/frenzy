<?php

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="post-content__comments-form-area">

	<?php
	if ( have_comments() ) :
		?>
		<h3>
			<?php
			$colour_frenzy_comment_count = get_comments_number();

			echo 'COMMENTS (' . $colour_frenzy_comment_count . ')';
			?>
		</h3>

		<?php the_comments_navigation(); ?>

		<ul class="post-content__comments">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ul',
					'short_ping' => true,
                    'max_depth' => '3',
                    'callback' => 'custom_comments'
				)
			);
			?>
		</ul>

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'colour-frenzy' ); ?></p>
			<?php
		endif;

	endif;


    $args = array(
        'fields'               => [
            'author' => '
            <div class="field main-form__field ">
                <label>
                    Name *
                    <input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" aria-required="true" required="required"/>
                </label>
            </div>',
            'email'  => '
            <div class="field main-form__field ">
                <label>
                    Email *
                    <input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" aria-required="true" required="required"/>
                </label>
            </div>',
            'url'  => '
            <div class="field main-form__field ">
                <label>
                    Website
                    <input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" aria-required="true" required="required" />
                </label>
            </div>',
            'cookies' => ''
        ],
        'comment_field'        => '
        <div class="field main-form__field ">
            <label>' . _x( 'Comment', 'noun' ) . '
                <textarea id="comment" name="comment" aria-required="true" required="required"></textarea>
            </label>
	    </div>',
//        'logged_in_as'         => '',
        'comment_notes_after' => '',
        'comment_notes_before' => '',
        'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s btn" value="%4$s" />',
        'submit_field'         => '<div class="form-submit">%1$s %2$s</div>',
        'title_reply' => '<h3>Leave a Comment</h3><p>Your email address will not be published. Required fields are marked *</p>',
        'title_reply_to'  => '<h3>Leave a Reply to %s</h3>',
        'cancel_reply_link'    => 'Cancel reply',
        'class_container' => 'post-content__main-form-wr',
        'class_form' => 'post-content__main-form main-form main-form--violet '
    );




	comment_form($args);
	?>

</div>
