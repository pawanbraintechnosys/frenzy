<?php
//Template Name: Homepage
get_header();

get_template_part('template-parts/hero-section', null, array('is_homepage' => true));

get_template_part('template-parts/text-img-items-section', null, array('class-inner-item' => true));

get_template_part('template-parts/popular-public-events');

get_template_part('template-parts/brush-bg-link-item');

get_template_part('template-parts/site-info-three-col');

get_template_part('template-parts/text-img-items-with-subscribe');

get_footer();
?>