export function initCurrentPage() {
  const fullUrl = window.location.href.toString();
  const page = fullUrl.substr(fullUrl.substr(0, (fullUrl + '?').indexOf('?')).lastIndexOf('/') + 1);
  const url = page.split('?')[0]

  const menuLinks = [
    '.menu__list-item-link',
    '.menu-sub-list__item-link',
  ]

  menuLinks.forEach(link => {
    document.querySelectorAll(link).forEach(el => {
      const href = el.getAttribute('href')
      if (url === href && el.classList.contains('menu__list-item-link')) {
        el.classList.add('active')
      } else if (url === href && el.classList.contains('menu-sub-list__item-link')) {
        const elParent = findParent(el, 'menu__list-item')
        const elChild = findChild(elParent, 'menu__list-item-dropdown')
        findChild(elChild, 'menu__list-item-link').classList.add('active')
      }
    })
  })
}


function findParent(el, className) {
  while (!el.classList.contains(className)) {
    el = el.parentElement;
    if (!el) {
      break;
    }
  }
  return el
}

function findChild(el, className) {
  for (let i = 0; i < el.childNodes.length; i++) {
    if (el.childNodes[i].className === className) {
      el = el.childNodes[i];
      break;
    }
  }
  return el;
}
