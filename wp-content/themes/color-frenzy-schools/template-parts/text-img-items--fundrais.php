<?php
$title = get_field('text-img-items__title');
$text = get_field('text-img-items__text');
$img = get_field('text-img-items__img');
$btn1 = get_field('text-img-items__btn1');
$btn2 = get_field('text-img-items__btn2');
$centeredBtnTitle = get_field('text-img-items__cntrd-btn-title');
$centeredBtn = get_field('text-img-items__cntrd-btn');
?>
<section class="text-img-items ">
    <div class="container">
        <div class="text-img-items__item  text-img-item text-img-item--reverse paw-mark-list head-bg head-bg--blue-gradient">
            <div class="text-img-item__img">
                <?= wp_get_attachment_image($img['id'], 'full') ?>
            </div>
            <div class="text-img-item__text">
                <?php if($title): ?>
                <h2>
                    <?= $title; ?>
                </h2>
                <?php endif; ?>
                <?= $text; ?>
                <?php if($btn1 || $btn2): ?>
                <div class="text-img-item__inner">
                    <?php if($btn1): ?>
                    <a href="<?= $btn1['url']; ?>"  target="<?= $btn1['target']; ?>" class="btn">
                        <?= $btn1['title']; ?>
                    </a>
                    <?php endif; ?>
                    <?php if($btn2): ?>
                        <a href="<?= $btn2['url']; ?>"  target="<?= $btn2['target']; ?>" class="btn">
                            <?= $btn2['title']; ?>
                        </a>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="text-img-items__inner">
            <p>
                <?= $centeredBtnTitle; ?>
            </p>
            <?php if($centeredBtn): ?>
                <a href="<?= $centeredBtn['url']; ?>"  target="<?= $centeredBtn['target']; ?>" class="btn btn--transparent"><?= $centeredBtn['title']; ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>
