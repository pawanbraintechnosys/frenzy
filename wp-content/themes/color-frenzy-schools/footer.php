<?php
$footerFirstList = 'footer__first-links-list';
$footerSecondList = 'footer__second-links-list';
$footerThirdList = 'footer__third-links-list';
$contactInfo = 'footer__contact-info';
$contactSocial = 'footer__contact-social';
$copyrightLeft = get_field('footer__copyright-left', 'option');
$copyrightCenter = get_field('footer__copyright-center', 'option');
$curYear = date("Y");
?>

</main>
<?php if(!is_page_template('templates/template-single-without-header.php')): ?>
    <footer class="footer">
        <div class="container">
            <div class="footer__wrapper">
                <ul class="footer__list">
                    <?php while(have_rows($footerFirstList, 'option')): the_row();
                    $itemLink = get_sub_field('link');
                ?>
                    <li>
                        <?php if($itemLink): ?>
                            <a href="<?= $itemLink['url']; ?>"  target="<?= $itemLink['target']; ?>">
                                <?= $itemLink['title']; ?>
                            </a>
                        <?php endif; ?>
                    </li>
                    <?php endwhile; ?>
                </ul>
                <ul class="footer__list">
                    <?php while(have_rows($footerSecondList, 'option')): the_row();
                        $itemLink = get_sub_field('link');
                    ?>
                        <?php if($itemLink): ?>
                            <li>
                                <a href="<?= $itemLink['url']; ?>"  target="<?= $itemLink['target']; ?>">
                                    <?= $itemLink['title']; ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </ul>
                <ul class="footer__list">
                    <?php while(have_rows($footerThirdList, 'option')): the_row();
                        $itemLink = get_sub_field('link');
                        ?>
                        <?php if($itemLink): ?>
                            <li>
                                <a href="<?= $itemLink['url']; ?>"  target="<?= $itemLink['target']; ?>">
                                    <?= $itemLink['title']; ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </ul>
                <div class="footer__contact">
                    <a href="/schools/contact-us/" class="contact-link">
                        CONTACT US
                    </a>
                    <?php while(have_rows($contactInfo, 'option')): the_row();
                        $itemLink = get_sub_field('link');
                    ?>
                        <?php if($itemLink): ?>
                            <a href="<?= $itemLink['url']; ?>" >
                                <?= $itemLink['title']; ?>
                            </a>
                        <?php endif; ?>
                    <?php endwhile; ?>
                    <ul>
                        <?php while(have_rows($contactSocial, 'option')): the_row();
                            $itemLink = get_sub_field('link');
                            $itemIcon = get_sub_field('icon');
                        ?>
                            <?php if($itemLink): ?>
                                <li><a href="<?= $itemLink; ?>" target="_blank"><img src="<?= $itemIcon['url']; ?>" alt="icon"></a></li>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="footer__copyright">
            <div class="container">
                <div class="footer__copyright-wr">
                    <a href="<?= $copyrightLeft['url']; ?>" target="<?= $copyrightLeft['target']; ?>">
                        <?= $copyrightLeft['title']; ?>
                    </a>
                    <p>
                        © <?= $curYear+1; ?> <?= $copyrightCenter; ?>
                    </p>
                    <a href="https://www.thinkroom.com/" target="_blank">
                        Created by <span class="foot">Body Wizards</span>
<!--                        <img src="--><?//= get_template_directory_uri(); ?><!--/assets/img/logo/logo_svg_white.svg" alt="logo">-->
                    </a>
                </div>
            </div>
        </div>
    </footer>


    <div id="modal-window" class="modal-window">
        <div class="modal-window__fader"></div>
        <!-- .modal-window__item--active -->
        <div id="modal-video" class="modal-window__item modal-window__video modal-video-item">
            <button aria-label="close modal window" class="modal-video-item__close modal-window__close-icon">
                <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M27.4142 0.585786C28.1953 1.36683 28.1953 2.63317 27.4142 3.41421L3.41421 27.4142C2.63317 28.1953 1.36683 28.1953 0.585786 27.4142C-0.195262 26.6332 -0.195262 25.3668 0.585786 24.5858L24.5858 0.585786C25.3668 -0.195262 26.6332 -0.195262 27.4142 0.585786Z" fill="#fff"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.585786 0.585786C1.36683 -0.195262 2.63317 -0.195262 3.41421 0.585786L27.4142 24.5858C28.1953 25.3668 28.1953 26.6332 27.4142 27.4142C26.6332 28.1953 25.3668 28.1953 24.5858 27.4142L0.585786 3.41421C-0.195262 2.63317 -0.195262 1.36683 0.585786 0.585786Z" fill="#fff"/>
                </svg>
            </button>
            <div class="modal-video-item__wr-iframe">
                <iframe src="" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_footer(); ?>



</body>
</html>
