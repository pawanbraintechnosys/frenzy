<?php
/*
 * School Dashboard
 * @ver:1.0
 */
function parent_dashboard_callback($atts)
{
    $atts = shortcode_atts(array(), $atts, 'parent_dashboard');
    if (is_user_logged_in()):
        $user_id=get_current_user_id();
        //echo $user_id;
        $user=get_userdata($user_id);
        $userMeta=get_user_meta($user->ID );
        $parent=new Parent_model();
        $table=TABLE_PREFIX_MAIN."child_details";
        $where=" WHERE parent_id=".$user_id;
        $childData=$parent->get($table,$where);
        //pr($childData['data']);
        //pr($childData['data']['nickname']);
        ?>
        <div class="main-dashboard parent-dsh">
			<section class="reports_message_detail parent_login">
				<div class="login_link_detail" style="max-width: 1250px;">
					<h3 class="heading">Parent Dashboard</h3>
					<div class="row">
						<div class="col-lg-6 parent-register">
							<div class="row pb-1">
								<div class="col-lg-8">
									<div class="copy_link">
										<label>Parent First Name</label>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<input type="text" readonly class="form-control" value="<?php echo $userMeta['first_name'][0]; ?>">
									</div>
								</div>
							</div>
							<div class="row pb-1">
								<div class="col-lg-8">
									<div class="copy_link">
										<label>Parent Last Name</label>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<input type="text" class="form-control" readonly value="<?php echo $userMeta['last_name'][0]; ?>">
									</div>
								</div>
							</div>
							<div class="row pb-1">
								<div class="col-lg-8">
									<div class="copy_link">
										<label>Parent Email</label>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<input type="email" class="form-control" readonly value="<?php echo $user->data->user_email; ?>" >
									</div>
								</div>
							</div>
							<div class="row pb-1">
								<div class="col-lg-8">
									<div class="copy_link">
										<label>Parent Phone</label>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<input type="text" class="form-control" readonly value="<?php echo $userMeta['phone'][0]; ?>">
									</div>
								</div>
							</div>
							<div class="row pb-1">
								<div class="col-lg-8">
									<div class="copy_link">
										<label>Event Name</label>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<input type="text" class="form-control" readonly value="<?php echo $childData[data][0][event_name]; ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="parent_login_link">
								<div class="row pb-1">
									<div class="col-lg-8 col-md-6">
										<div class="form-group for-button">
											<label>View Invoice</label>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="form-group for-button">
											<a href="<?php echo home_url().'/invoice'; ?>" class="btn btn-warning">Invoice</a>
										</div>
									</div>
								</div>
								<!--div class="row pb-1">
									<div class="col-lg-8 col-md-6">
										<div class="form-group for-button">
											<label>Message Change Requests</label>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="form-group for-button">
											<a href="javascript:void(0);" id="change_request" class="btn btn-warning change_request">Message</a>
                                            <?php //get_template_part('include/studentParent/change-request-modal'); ?>
										</div>
									</div>
								</div-->
								<div class="row pb-1">
									<div class="col-lg-8 col-md-6">
										<div class="form-group for-button">
											<label>Frequently Asked Questions</label>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="form-group for-button">
											<a href="<?php echo home_url(); ?>/faqs" class="btn btn-warning">FAQS</a>
										</div>
									</div>
								</div>
								<div class="row pb-1">
									<div class="col-lg-8 col-md-6">
										<div class="form-group for-button">
											<label>Parent Take Home Letter</label>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="form-group for-button">
                                            <?php
                                               $school_id=getChildSchoolId($user_id);
                                               $parentTakeHomeLetter=get_field('parent_take_home_letter',$school_id['school_id']);
                                            ?>
											<a target="_blank" href="<?php echo $parentTakeHomeLetter['url']; ?>" class="btn btn-warning">Letter</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			  
				</div>
			</section>
			<section class="funddash form-section for-mobile" >
				
                  <div class="child_register" style="max-width: 1250px;">
                  	<?php if(!empty($childData['data'])){ ?>
					<h3 class="heading" style="    margin-bottom: 0px;">Children registered</h3>
					<?php
				    $totalChild=count($childData['data']);
				    //echo $totalChild;
				    $i=0;
				    $j=1;
				foreach ( ($childData['data']) as $key=>$child): //pr($child);
					
					$bName = $child['nickname'].' '.$child['firstname'];
					//echo $bName;
					$text = in_array($child['nickname'], $bName);
					echo $text;
					
					/*if($child['nickname']){
						  $child_name = $child['nickname'];
					}
					if(empty($child['nickname'])){
						  $child_name = $child['firstname'];
					}*/
                    $child_fundpage=get_child_fundraising_page($child['parent_id'],$child_name); 
                    $donation= get_all_fund_donation(@$child_fundpage['data']['id']);
                    //pr($donation);
					?>
					<h3 style="margin-bottom: 0px;font-size: 30px;">Child <?php echo $j; ?></h3>
					<div class="row">
						<div class="col-lg-8">
							<div class="user-detail">
								<div class="user-n-ab">
                                    <a  href="<?php echo home_url().'/student-fundraising-page/?child-id='.base64_encode($child['id']); ?>"><img style="max-width: 100%;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/imageedit_7_3907638477.png"></a>
									<div class="name_abc">
										<h3><?php echo $child['firstname'].' '.$child['lastname'] ; ?></h3>
										<div class="row m-0">
											<div class="col-lg-6">
												<div class="row align-items-center">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>First Name</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<input type="text" class="form-control" readonly value="<?php echo $child['firstname'];  ?>" >
														</div>
													</div>
												</div>
												<div class="row px-2 align-items-center">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>Last Name</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<input type="text" class="form-control" readonly value="<?php echo $child['lastname'];  ?>">
														</div>
													</div>
												</div>
												<div class="row align-items-center">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>Class 1</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<input type="text" class="form-control" readonly value="<?php echo $child['class'];  ?>">
														</div>
													</div>
												</div>
												<div class="row align-items-center" style="padding: 7px 0;">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>Class 2</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<input type="text" class="form-control" readonly value="<?php echo $child['childClasss'];  ?>">
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="row align-items-center">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>T-Shirt</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<input type="text" class="form-control" readonly value="<?php echo $child ['cloth_size']; ?>">
														</div>
													</div>
												</div>
												<div class="row px-2 align-items-center">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>Bucket Hat</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<input type="text" class="form-control" readonly value="<?php echo (!empty($child['hat_number']) && (isset($child['hat_number']))&& (($child['hat_number']))!==' ')?'YES':'NO';  ?>">
														</div>
													</div>
												</div>
												<div class="row align-items-center">
													<div class="col-lg-6">
														<div class="copy_link">
															<label>Merchandise</label>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group for-button">
															<a href="#" class="btn btn-warning">Order</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="goal_summary">
									<div class="fundraise_graph">
										<div class="fund">
											<span class="fund_left lft">Progress</span>
											<span class="fund_right cent">Funds Raised</span>
											<span class="fund_right rght">Goal</span>
										</div>
										<div class="school-light-grey">
											<div class="school-red" style="height:24px;max-width:<?php echo (round($donation['total_fund_raised']['total_donation'])*100)/(round($child_fundpage['data']['goal-amount'])); ?>%"></div>
										</div>
										<div class="fund">
											<span class="fund_left lft">$<?php echo round($donation['total_fund_raised']['total_donation']); ?></span>
											<!--span class="fund_right cent">Funds Raised</span-->
											<span class="fund_right rght">$<?php echo $child_fundpage['data']['goal-amount']; ?></span>
										</div>
									</div>
								</div>
								<div class="fundrase_profile">
									<div class="row">
										<div class="col-lg-6">
											<div class="classes">
                                                <?php
                                                    $school_id=getChildSchoolId($user_id);
                                                    $schoolFundEndDate=get_field('incentive_end_date',$school_id['school_id']);
                                                    $currentDate=date('F j, Y');
                                                    $newcurrentDate = date("d-m-Y", strtotime($currentDate));
                                                    $date1Curent = strtotime($newcurrentDate);
                                                    $newendDate = date("d-m-Y", strtotime($schoolFundEndDate));
                                                    $date1END = strtotime($newendDate);
                                                    if($date1END>=$date1Curent):
                                                ?>
												<h3>Fundrasing closes</h3>
												<div class="d_h_m_s">
													<div class="d"><span id="day-<?php echo $key; ?>" class="day"></span><p>Days</p></div>
													<div class="d"><span id="hour-<?php echo $key; ?>" class="hour"></span><p>Hours</p></div>
													<div class="d"><span id="minute-<?php echo $key; ?>" class="minute"></span><p>Minute</p></div>
													<div class="d"><span id="second-<?php echo $key; ?>" class="second"></span><p>Second</p></div>
												</div>
                                                <?php else: ?>
                                                	<h3>Fundrasing Closed</h3>
                                                    <h3><?php echo $schoolFundEndDate; ?></h3>
                                                <?php endif; ?>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="profile">
												<h3>Share Profile</h3>
												<div class="f_en_cop">
                                                    <?php
                                                        $socialData=array(
                                                                'link'=>home_url().'/student-fundraising-page/?child-id='.base64_encode($child['id']),
                                                                'title'=>$child['firstname'].' '.$child['lastname'],
                                                                'email'=>$user->data->user_email,
                                                        );
                                                        echo social_share($socialData);
                                                    ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="prize" style="margin-top: 10px;">
								<div>
									<div class="st_prize">
										<h3>Star Prizes</h3>
										<p>Student are rewarded for there fundraising efforts with amazing prize!</p>
                                            <p>As each Student reaches new fundraising goals they unlock stars which earn them bigger prizes.</p>
                                            <p>Help them achieve their ultimate goal while helping our school fundraise by donating to a student now.</p>
									</div>
									<div class="st_star">
										<span class="timer">
											<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
											<img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
										</span>
									</div>
									<?php 
									/*$star = $donation['total_fund_raised']['total_donation']; 
										if($star/100 >= 1){
										*/	
									 ?>
									<!--div class="Stars" style="--rating:<?php //echo $star/100; ?>" aria-label="100">
                                    </div>
                                <?php //}else{ ?>
                                	<div class="Stars" style="--rating:0" aria-label="100">
                                    </div-->
                                <?php //} ?>
								</div>
								<div class="prize_select">
									<div class="row">
										<div class="col-lg-6">
											<h3>Prize Selection</h3>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
										</div>
										<div class="col-lg-6 pr">
											<div class="row">
												<div class="col-lg-6">
													<img src="<?php echo get_template_directory_uri().'/assets/images/prize.png'; ?>">
												</div>
												<div class="col-lg-6 car">
													<a href="#" class="btn btn-warning">Prize</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="donat" style="margin-top:-28px;">
								<h3>RECENT SCHOOL DONATION</h3>
                                <?php
                                $child_fundpage=get_child_fundraising_page($child['parent_id'],$child['firstname']);
                                get_template_part('include/studentParent/fundraiser/latest-donation', null, array(
                                    'data' => array(
                                            'child_id' => @$child_fundpage['data']['id'],
                                            'parent_id' =>$child['parent_id'],
                                            'fundraising_page_id' =>@$child_fundpage['data']['id'],
                                            'user_id' =>get_current_user_id(),
                                            'latest_donation' =>$donation['latest_donation'],
                                    ))
                                );
                                ?>
							</div>
							<div class="Badges_earnt">
								<div><h3>Badges Earnt</h3></div>
                                <?php
                                get_template_part('include/studentParent/fundraiser/child-badges', null, array(
                                    'data' => array(
                                            'child_id' => $child['id'],
                                            'total_fund_raised' => $donation['total_fund_raised'],
                                            'number_of_badge' => 10,
                                            'child_school_name' => strtolower(str_replace(' ', '',$child['firstname'].''.$child['event_name']))
                                    ))
                                );
                                ?>
							</div>
						</div>
					</div>
					<?php ++$j !== $totalChild; ?>
                <?php if(++$i !== $totalChild): ?>
                <div class="divider"></div>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php } else{ ?>
                <span class="no_child">No child registered.</span>
                <?php } ?>
				</div>
				

			</section>
		</div>
        <?php
        if(!empty($childData['data'])){
            $school_id=getChildSchoolId($user_id);
            $schoolFundEndDate=get_field('incentive_end_date',$school_id['school_id']);
            $res = date('M d, Y  h:i:s', strtotime($schoolFundEndDate));
            foreach ( ($childData['data']) as $key=>$child){
                ?>
                <script>
                    function childTimer<?php echo $key; ?>(){
                        var x = setInterval(function() {
                            var countDownDate = new Date("<?php echo $res;  ?>").getTime();
                          //  var countDownDate = new Date("Jan 15, 2022 15:37:25").getTime();
                            var now = new Date().getTime();
                            var distance = countDownDate-now ;
                            console.log(distance)
                            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                            document.querySelector("#day-<?php echo $key; ?>").innerHTML = days;
                            document.querySelector("#hour-<?php echo $key; ?>").innerHTML = hours;
                            document.querySelector("#minute-<?php echo $key; ?>").innerHTML = minutes;
                            document.querySelector("#second-<?php echo $key; ?>").innerHTML =seconds;
                            if (distance < 0) {
                                document.querySelector("#day-<?php echo $key; ?>").innerHTML = '00';
                                document.querySelector("#hour-<?php echo $key; ?>").innerHTML = '00';
                                document.querySelector("#minute-<?php echo $key; ?>").innerHTML = '00';
                                document.querySelector("#second-<?php echo $key; ?>").innerHTML ='00';
                                clearInterval(x);
                            }
                        }, 1000);
                    }
                    childTimer<?php echo $key; ?>();
                </script>
            <?php }  } ?>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}
add_shortcode('parent_dashboard', 'parent_dashboard_callback');
?>

