<?php

/* Template Name: Animal rescue goal page */


get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-img-items-section', null,
    array(
            'section-classes' => ' text-img-items--violet ',
            'item-classes' => ' text-img-item ',
    ));

get_footer();
