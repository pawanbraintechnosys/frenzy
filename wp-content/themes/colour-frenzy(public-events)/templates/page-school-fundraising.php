<?php

/* Template Name: School fundraising page */


$formText = get_field('form_text');
$formShortcode = get_field('form_shortcode');

get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-img-items-section', null,
    array(
            'section-classes' => ' text-img-items--brush-bg-violet text-img-items--green-head text-img-items--violet ',
            'item-classes' => ' text-img-item ',
    ));

get_template_part('template-parts/text-video-items-section');

get_template_part('template-parts/form-section');

get_footer();
