<?php
/*
 * Common Helper
 */

/*
 * Social share
 * Version @1.0
 * Package colourfrenzy
 */

function social_share($data=null){
    $link=$data['link'];
    $caption=$data['title'];
    $email=$data['email'];
    $output ='<div class="f face"><a target="_blank" href="http://www.facebook.com/sharer.php?quote='.$caption.'&u='.$link.'"><img src="'.get_template_directory_uri().'/assets/images/facebook-sym.png'.'"></a></div>';
    $output .='<div class="f env"><a href="mailto:'.$email.'?subject='.$caption.' Fund Page&body=This is the fundpage url :'.$link.'"><img src="'.get_template_directory_uri().'/assets/images/envelope.png'.'"></a></div>';
    $output .='<div class="f cop"><a target="_blank" href="#"><img src="'.get_template_directory_uri().'/assets/images/copy.png'.'"></a></div>';
    return $output;
}
function funcdraiseCounter($child_id=""){

    $start_date= date( 'Y-m-d H:i:s');
    $end_date= date( 'Y-m-d H:i:s',strtotime('+3 days'));
    return $end_date;

}
/** Get State
 * detail by state name
 */

function get_school_by_state($state_name=""){
    $resonse=[];
    $args = array(
        'numberposts' => -1,
        'post_type' => 'schools',
        'meta_query' => array(
            array(
                'key'       => 'school_state',
                'value'     => "$state_name",
            )
        )
    );
    $posts = new WP_Query($args);
   if($posts->have_posts()){
       while($posts->have_posts()): $posts->the_post();
           $resonse['data'][]=array(
             'ID'=>get_the_ID(),
             'title'=>get_the_title(get_the_ID()),
             'link'=>get_the_permalink(get_the_ID()),
             'logo'=> get_the_post_thumbnail_url(get_the_ID(),'full'),
             'school_logo'=>@get_field('school_logo',get_the_ID())['url']
       );
       $resonse['status']=true;
       endwhile;
   }else{
       $resonse['status']=0;
       $resonse['data']=[];
   }
   return $resonse;

}

/*
 * Get state name and its respective images
 */
function get_state_url($state_name=""){
    $assets_url=get_template_directory_uri().'/assets/images/states';
    $response=[];
    switch ($state_name) {
        case "south-aus":
            $response=array(
                'url'=>$assets_url.'/SA_1.png',
                'name'=>'South Australia'
            );
            break;
        case "west-aus":
            $response=array(
                'url'=>$assets_url.'/WA_1.png',
                'name'=>'Western Australia'
            );
            break;
        case "north-territory":
            $response=array(
                'url'=>$assets_url.'/NT_1.png',
                'name'=>'Northern Territory'
            );
            break;
        case "queenslands":
            $response=array(
                'url'=>$assets_url.'/QLD_1.png',
                'name'=>'Queensland'
            );
            break;
        case "new-south-wales":
            $response=array(
                'url'=>$assets_url.'/NSW_1.png',
                'name'=>'New South Wales'
            );
            break;
        case "victoria":
            $response=array(
                'url'=>$assets_url.'/VIC_1.png',
                'name'=>'Victoria'
            );
            break;
        case "tasmania":
            $response=array(
                'url'=>$assets_url.'/HOB.png',
                'name'=>'Tasmania'
            );
            break;
        default:
            $response=array(
                'url'=>'',
                'name'=>''
            );
            break;
    }
    return $response;
}
function generate_fundraising_page($table_name=null,$data=null){
    global $wpdb;
    $id=$wpdb->insert($table_name,$data);
    if($wpdb->insert_id){
        return true;
    }else{
        return false;
    }
}
/*
 * get fundraising page details by user_id
 */
function get_fundraising_page_detail($user_id=null){
    global $wpdb;
    $response=array();
    $where =" WHERE user_id=".$user_id." AND status=1";
    $sql="SELECT * From fundraise_posted_campiagn".$where;
    $result=$wpdb->get_results($sql,ARRAY_A);
    return
        $response=array(
          'data'=>$result,
          'sql'=>$wpdb->last_query
        );

}
/*
 * School web page data
 */
function get_school_webpage($school_id=null){
    global $wpdb;
    $response=array();
    $where =" WHERE school_id=".$school_id;
    $sql="SELECT * From wp8t_school_details".$where;
    $result=$wpdb->get_results($sql,ARRAY_A);
    return
        $response=array(
            'data'=>$result,
            'sql'=>$wpdb->last_query
        );

}
/*
 * User All profile data
 */
function get_user_data($user_id=null){
     $users=get_user_by('id',$user_id);
     $users=(array)$users->data;
     $user_meta=get_user_meta($users['ID']);
     $users['phone']=$user_meta['phone'][0];

     return $users;

}
/* Get child fundraising page */
function get_child_fundraising_page($parent_id=null,$firstname=null){
    global $wpdb;
    $where =" WHERE user_id=".$parent_id." AND title LIKE '%".trim($firstname)."%'";
    $sql="SELECT * From fundraise_posted_campiagn".$where;
    $result=$wpdb->get_row($sql,ARRAY_A);
    return
        $response=array(
            'data'=>$result,
            'sql'=>$wpdb->last_query
        );

}

/* Get School from from user_id */
function getChildSchoolId($user_id=null){
    global $wpdb;
    $table_name=TABLE_PREFIX_MAIN;
    $where =" WHERE parent_id=".$user_id;
    $sql="SELECT school_id From {$table_name}child_details ".$where;
    $result=$wpdb->get_row($sql,ARRAY_A);
    return $result;

}
/*
 * Get child detail and fundpage by child ID
 * @Package Colourfrenzy
 * @Version 1.0.0
 */
function getChildDetailFromChildID($child_id=null){
    global $wpdb;
    $table_name=TABLE_PREFIX_MAIN;
    $sql= "Select fnd.*,chld.firstname as firstname,chld.lastname as lastname from {$table_name}child_details as chld
            INNER JOIN 
            fundraise_posted_campiagn as fnd
            ON chld.parent_id =fnd.user_id
            WHERE fnd.id={$child_id} AND fnd.title LIKE CONCAT('%',firstname, '%')";
    $result=$wpdb->get_row($sql,ARRAY_A);
    return array(
        'data'=>$result,
        'sql'=>$wpdb->last_query
    );

}
/* Get child and fund campign details by child id for student fundraised details*/
function GetchildAndFundriaseDetails($child_id=null){
	global $wpdb;
	$sqldetails = "Select * from wp8t_child_details
            INNER JOIN 
            fundraise_posted_campiagn
            ON wp8t_child_details.parent_id =fundraise_posted_campiagn.user_id
            WHERE fundraise_posted_campiagn.id={$child_id} AND fundraise_posted_campiagn.title LIKE CONCAT('%',firstname, '%')";
			$result=$wpdb->get_row($sqldetails,ARRAY_A);
			return $result;
}

/* Get all donation by fundpage ID */

function get_all_fund_donation($fundpage_id=null){
    global $wpdb;
	$table_name=TABLE_PREFIX_MAIN;
    /**   Latest Donation  */
	$where =" WHERE fundpage_id=".$fundpage_id." ORDER BY created_date desc";
	//$where =" ORDER BY created_date desc LIMIT 10";
    $sql="SELECT * From fundraise_donation".$where;
    $latest_donation=$wpdb->get_results($sql,ARRAY_A);

    /** Top donation */
    $where2 =" WHERE fundpage_id=".$fundpage_id." ORDER BY shipping_firstname ASC";
    $sql2="SELECT * From fundraise_donation".$where2;
    $top_donation=$wpdb->get_results($sql2,ARRAY_A);

	
    /** Total fundraised */
    $where3 =" WHERE fundpage_id=".$fundpage_id;
    $sql3="SELECT SUM(amount) as total_donation  From fundraise_donation".$where3;
    $total_donation=$wpdb->get_row($sql3,ARRAY_A);

	
    return
        $response=array(
            'latest_donation'=>$latest_donation,
            'top_donation'=>$top_donation,
            'total_fund_raised'=>$total_donation,
            'sql'=>$wpdb->last_query
        );
}
/** SEND MAIL */
if(!function_exists('send_mail')){
    function send_mail($data=null,$mail_template=null){
        $to = $data['to'];
        $subject =$data['subject'];
        $body = $data['message'];
        $headers = array('Content-Type: text/html; charset=UTF-8','From: ColorFenzy<admin@onlineprojectprogress.com>');
        $mail=wp_mail( $to, $subject, $body, $headers );
        if($mail){
            return true;
        }else{
            return false;
        }
    }
}
/*
 * GET PARENT INVOICE
 * @Package ColourFrenzy
 * @Version 1.0.0
 */
function getParentInvoice($user_id=null){
    global $wpdb;
    $table_prefix=TABLE_PREFIX_MAIN;
    $sql="
        SELECT payment.id as invoice_id, payment.payment_id as payment_id,payment.type as payment_mode,payment.amount as total_amount, child.* FROM {$table_prefix}payment_transaction as payment 
        INNER JOIN {$table_prefix}child_details as child 
        ON payment.user_id=child.parent_id
        WHERE payment.user_id={$user_id}";
    $invoice=$wpdb->get_results($sql,ARRAY_A);
    return $invoice;
}
/* Manage Fundraised with Two table*/
function getFundraisedData($school_id= null){
    global $wpdb;
    $table_prefix=TABLE_PREFIX_MAIN;
    //$sqlt="SELECT * FROM wp8t_child_details,  fundraise_posted_campiagn WHERE  school_id = $school_id";
    $sqlt= "SELECT * FROM wp8t_child_details WHERE school_id= $school_id";
    $fundraise =$wpdb->get_results($sqlt,ARRAY_A);
    return $fundraise;
}

function SearchAllStudentAndClass($school_id=null){
    global $wpdb;
    $sqlsearch = "Select firstname,class from wp8t_child_details
            WHERE school_id = $school_id  LIKE CONCAT('%',firstname,'%' AND '%',class,'%')";
            $result=$wpdb->get_row($sqlsearch,ARRAY_A);
            return $result;
}