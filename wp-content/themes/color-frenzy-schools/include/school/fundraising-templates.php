<?php

    $schoolData=$args;
    $school_id=$args['data']['school_id'];
    //echo $school_id;
    //pr(get_school_webpage(8238));
    $string = get_school_webpage($school_id)['data'][0] ['event_name'];

    $strings = strtolower(str_replace(' ', '-', $string));
    //pr(get_field('resource_2',$school_id)['url']);
?>

<div class="templates_resource max_width_set">

    <div>

        <h3>Fundraising Resources and Templates</h3>
        <div class="container">

        <div class="row mt--3">

            <div class="col-lg-6">

                <div class="resource">

                    <label for="inputState" class="form-label">Edit Fundrasing Website</label>

                    <div class="form-group for-button f-ed-pag">

                        <a href="<?php echo home_url().'/create-website-page/' ?>" class="btn btn-warning">Edit</a>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="resource">

                    <label for="inputState" class="form-label">Fundrasing Page</label>

                    <div class="form-group for-button f-ed-pag">

                        <a href="<?php echo home_url().'/school-fundraising-page/?event-name='.$strings.'&id='.base64_encode($school_id); ?>" class="btn btn-warning">Page</a>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="resource">

                    <label for="inputState" class="form-label">Parent Take Home Letter</label>

                    <div class="form-group for-button">

                        <a href="<?php echo get_field('parent_take_home_letter',$school_id)['url'] ; ?>" class="btn btn-warning">Download</a>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="resource">

                    <label for="inputState" class="form-label">School Poster 1</label>

                    <div class="form-group for-button">

                        <a href="<?php echo get_field('school_booklet',$school_id)['url']; ?>" class="btn btn-warning">Download</a>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="resource">

                    <label for="inputState" class="form-label">Parent FAQ's</label>

                    <div class="form-group for-button">

                        <a href="<?php echo get_field('resource_1',$school_id)['url']; ?>" class="btn btn-warning" >Download</a>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="resource">

                    <label for="inputState" class="form-label">School Poster 2</label>

                    <div class="form-group for-button">

                        <a href="<?php echo get_field('resource_2',$school_id)['url'];?>" class="btn btn-warning">Download </a>

                    </div>

                </div>

            </div>

        </div>

    </div>
    </div>

    

</div>

