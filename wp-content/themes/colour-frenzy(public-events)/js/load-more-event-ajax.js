jQuery(function($){
    $('#load-more-event').on('click', function(e){
        e.preventDefault();

        var data = {
            'action': 'event_loadmore',
            'query': event_params.posts,
            'page': event_params.current_page
        };

        $.ajax({
            url : event_params.ajaxurl,
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {

            },
            success : function( data ){
                if( data ) {
                    event_params.current_page++
                    $('.location-section__events-wr').append(data)
                    if(event_params.max_page == event_params.current_page){
                        $('#load-more-event').addClass('hidden')
                    }
                }
            }
        });
    });

    $('#search-location .search-input input').on('keyup', function () {
        let searchLocation = $(this).val()
        $('div.wpgmp_search_form input.wpgmp_search_input').val(searchLocation)
        $('div.wpgmp_search_form input.wpgmp_search_input').trigger('focus').trigger('keyup')
        $(this).trigger('focus')
    })

    $('.search-input__reset').on('click', function (e) {
        e.preventDefault()
        $('#search-location .search-input input').val('')
        $('#search-location .search-input input').trigger('keyup')
    })


    $('.carousel-img__item').on('click', function (e) {
        e.preventDefault()
        $('.carousel-img__item').removeClass('active')
        $(this).addClass('active')
        const imgSrc = $(this).find('img').attr('src')
        $('.modal-images__inner .changing-img').attr('src', imgSrc)
    })

    function lightbox(arrow) {
        let el
        const lengthCarousel = $('.carousel-img__item').length
        let currentIndex = $('.carousel-img__item.active').index() + 1
        if(arrow === 'next'){
            if(lengthCarousel === currentIndex){
                currentIndex = 0
            }
            el = $('.carousel-img__item').eq(currentIndex)
            el.trigger('click')
        }
        else{
            if(currentIndex === 1){
                currentIndex = lengthCarousel + 1
            }
            el = $('.carousel-img__item').eq(currentIndex - 2)
            el.trigger('click')
        }
    }

    $('.modal-arrow').on('click', function () {
        if($(this).hasClass('next-btn')){
            lightbox('next')
        }
        else{
            lightbox('prev')
        }
    })

    $('.changing-img').on('click', function () {
        lightbox('next')
    })

    $(document).mouseup(function (e){
        var div = $(".not-close");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            $('#modal-images').removeClass('modal-window__item--active')
            $('#modal-window').removeClass('modal-window--active')
        }
    });
});
