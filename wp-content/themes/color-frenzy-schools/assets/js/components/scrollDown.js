export function scrollDown() {
  const arrow = document.querySelector('.scroll-down')
  const $header = document.getElementById('header')
  if (arrow) {
    arrow.addEventListener('click', (e) => {
      const nextSectionOffsetTop =  e.target.closest('section').nextElementSibling.getBoundingClientRect().top
      const headerHeight = $header.offsetHeight

      scrollBy({
        top: nextSectionOffsetTop - headerHeight,
        left: 0,
        behaviour: "smooth"
      })
    })
  }
}
