<?php
$title = get_the_title();
$dateS = strtotime(get_field('data-start-event'));
$date = date("jS \of F Y", $dateS);
$dateS = strtotime(get_field('data-start-event')) * 1000;
$currentDate = strtotime(date('F j, Y')) * 1000;
?>
<div class="event-item location-section__event <?= $args['slider-class']; ?>">
    <h3><?= $title; ?></h3>
    <p>
        <?php if($dateS && $dateS > $currentDate): ?>
            <?= $date; ?>
        <?php else: ?>
            Coming Soon
        <?php endif; ?>
    </p>
    <a href="<?= get_the_permalink(); ?>"  class="btn btn--transparent btn--border-pink">
        Learn more
    </a>
</div>