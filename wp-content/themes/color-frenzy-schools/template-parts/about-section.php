<?php
$aboveItems = 'about-us__above-slider';
$sliderTitle = get_field('about-us__slider-title');
$teamSlider = 'about-us__team-slider';
$belowItems = 'about-us__below-slider';
$btnTitle = get_field('about-us__btn-title');
$btn = get_field('about-us__btn');
?>
<section class="about-section">
    <div class="container">
        <?php while(have_rows($aboveItems)): the_row();
        $itemReverse = get_sub_field('reverse');
        $itemAlignTop = get_sub_field('align-top');
        $itemTitle = get_sub_field('title');
        $itemText = get_sub_field('text');
        $itemImg = get_sub_field('image');
        ?>
        <div class="about-section__text-img-item text-img-item
        <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?>
        <?= $itemAlignTop ? ' text-img-item--align-top ' : ''; ?>
        ">
            <div class="text-img-item__img">
                <?= wp_get_attachment_image($itemImg['id'], 'full') ?>
            </div>
            <div class="text-img-item__text">
                <?php if($itemTitle): ?>
                <h2>
                    <?= $itemTitle; ?>
                </h2>
                <?php endif; ?>
                <?= $itemText; ?>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <div class="container about-section__slider-wr">
        <div class="team-slider">
            <h2>
                <?= $sliderTitle; ?>
            </h2>
            <div class="team-slider__container swiper-container">
                <div class="team-slider__wrapper swiper-wrapper">
                    <?php while(have_rows($teamSlider)): the_row();
                    $itemPhoto = get_sub_field('photo');
                    $itemPhotoHover = get_sub_field('hover_photo');
                    $itemName = get_sub_field('name');
                    $itemPost = get_sub_field('post');
                    ?>
                    <div class="team-slider__item team-slider-item swiper-slide " data-hover-src="<?= $itemPhotoHover['url']; ?>">
                        <h3>
                            <?= $itemName; ?>
                        </h3>
                        <h4>
                            <?= $itemPost; ?>
                        </h4>
                        <div class="team-slider-item__img">
                            <img src="<?= $itemPhoto['url']; ?>" alt="img">
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="team-slider__arrows">
                <button class="team-slider__arrow team-slider__arrow-prev">
                    <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/team-slider-arrow.svg" alt="arrow">
                </button>
                <button class="team-slider__arrow team-slider__arrow-next">
                    <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/team-slider-arrow.svg" alt="arrow">
                </button>
            </div>
        </div>
    </div>
    <div class="container">
        <?php while(have_rows($belowItems)): the_row();
        $itemReverse = get_sub_field('reverse');
        $itemAlignTop = get_sub_field('align-top');
        $itemTitle = get_sub_field('title');
        $itemText = get_sub_field('text');
        $itemImg = get_sub_field('image');
        ?>
        <div class="about-section__text-img-item text-img-item
        <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?>
        <?= $itemAlignTop ? ' text-img-item--align-top ' : ''; ?>
        ">
                <div class="text-img-item__img">
                    <?= wp_get_attachment_image($itemImg['id'], 'full') ?>
                </div>
                <div class="text-img-item__text">
                    <?php if($itemTitle): ?>
                        <h2>
                            <?= $itemTitle; ?>
                        </h2>
                    <?php endif; ?>
                    <?= $itemText; ?>
                </div>
        </div>
        <?php endwhile; ?>
        <div class="about-section__description">
            <h3>
                <?= $btnTitle; ?>
            </h3>
            <?php if($btn): ?>
                <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn">
                    <?= $btn['title']; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
</section>
