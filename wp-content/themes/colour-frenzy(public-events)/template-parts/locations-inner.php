<?php
$centerContent = get_field('locations__center-content');
$titleEvents = get_field('locations__title-events');
$titleMap = get_field('locations__title-map');
$events = get_field('locations__events');
$titleForm = get_field('locations__title-form');
$descriptionForm = get_field('locations__description-form');
$formShortcode = get_field('locations__form-shortcode');
?>
<section class="location-section">
    <div class="container">
        <?php if($centerContent): ?>
            <div class="location-section__head">
                <?= $centerContent; ?>
            </div>
        <?php endif; ?>

        <div class="location-section__slider-wr">
            <h2><?= $titleEvents; ?></h2>
            <?php if($events): ?>
                <div class="event-slider">
                    <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

                    </button>

                    <div class="location-section__events-wr swiper-container">
                        <div class="event-slider__wrapper swiper-wrapper">
                            <?php foreach ($events as $post): setup_postdata($post);
                                get_template_part('template-parts/items/event-location-item', null, array('slider-class' => 'swiper-slide'));
                            endforeach;
                            wp_reset_query();
                            ?>
                        </div>
                    </div>

                    <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>

                    <div class="event-slider__pagination slider-pagination"></div>
                </div>
            <?php endif; ?>

<!--            --><?php //if($maxPagePagination > 1): ?>
<!--                <a href="#" id="load-more-event"  class="btn location-section__btn">-->
<!--                    load more-->
<!--                </a>-->
<!--            --><?php //endif; ?>
        </div>

        <div class="location-section__map-wr">
            <h2><?= $titleMap; ?></h2>
            <form id="search-location" class="location-section__map-form">
                <p>
                    Search location
                </p>
                <label class="search-input location-section__search-input">
                    <input type="text" placeholder="Enter a Location">
                    <button type="submit">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/search.svg" alt="search">
                    </button>
                    <button type="reset" class="search-input__reset"></button>
                </label>
            </form>
            <?= do_shortcode('[put_wpgm id=1]'); ?>
        </div>

        <?php if($formShortcode): ?>
            <div class="location-section__form-wr">
                <h2><?= $titleForm; ?></h2>
                <p><?= $descriptionForm; ?></p>
                <?= do_shortcode($formShortcode); ?>
            </div>
        <?php endif; ?>
    </div>
</section>