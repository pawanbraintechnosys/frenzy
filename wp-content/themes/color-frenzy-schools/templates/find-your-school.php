<?php

//Template Name: Find your school

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center'
));

get_template_part('template-parts/text-img-items', null, array(
    'section-classes' => 'text-img-items--violet text-img-items--violet-brush',
    'items-classes' => 'text-img-item text-img-item--head-green'
));

get_template_part('template-parts/search-section', null, array('type' => 'school'));

get_footer();
?>
