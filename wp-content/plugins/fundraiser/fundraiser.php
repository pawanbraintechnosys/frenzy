<?php
/*
Plugin name: Fundraising
Description: Student and parent fundraising functionality
Author:Braintechnosys
Author URI:http://Braintechnosys.in
Version:1.0
*/
/* setup path */
define('DIR_NAME', basename(FUNDRAISER_DIR));
$base_url = home_url();
require(ABSPATH.'wp-config.php' );
define('FUNDRAISER_DIR',plugin_dir_path(__FILE__));
define('FUNDRAISER_URL',plugin_dir_url("/")."fundraiser");
$site_url = get_option('siteurl');
define($site_url.'/wp-content/plugins/'.FUNDRAISER_DIR ,plugin_dir_url(__FILE__));
define('TABLE_PREFIX','fundraise_');
show_admin_bar( false );
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
require_once("libraries/common/Constants.php");
require_once("libraries/MainController.php");
require_once("libraries/ajax/fundraiser-ajax.php");

register_activation_hook(__FILE__,'fundraiser_active');
register_deactivation_hook(__FILE__,'fundraiser_deactive');
register_uninstall_hook( __FILE__,'fundraiser_uninstall' );

/* Plugin Active */
function fundraiser_active(){

    /* Create Table for ad campaign */
    global $wpdb;
    $wpdb->query("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."posted_campiagn (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `user_id` int(11) NOT NULL,
          `status` tinyint(4) NOT NULL COMMENT '1=>active',
          `accepted_at` datetime NOT NULL,
          `modified_at` datetime NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci
    ");

    /* Create pages for ad compaign page */
        $fundraiser = array(
            'post_title'    => 'Add fundraiser page',
            'post_content'  => '[add_fundraiser_campaign]',
            'post_status'   => 'publish',
        );
        wp_insert_post( $fundraiser );
    /* Create pages for ad compaign page */

}
/* Uninstall Plugin */
function fundraiser_deactive(){

}
/* Uninstall Plugin */
function fundraiser_uninstall(){

}
add_action('admin_menu','admin_menu_actions');
function admin_menu_actions(){
    add_menu_page('Reports',"Reports","administrator","dashboard","dashboard_fundraiser","dashicons-chart-bar");
    add_submenu_page("dashboard","Profile Report","Profile Report",'8','profile-reports','profile_reports');
    add_submenu_page("dashboard","Fundraising Report","Fundraising Report",'8','fundraising-reports','fundraising_reports');
    add_submenu_page("dashboard","Incentive Report","Incentive Report",'8','incentive-reports','incentive_reports');
    add_submenu_page("dashboard","Merchandise Report","Merchandise Report",'8','merchandise-reports','merchandise_reports');
    add_submenu_page("dashboard","Product Sales Report","Product Sales Report",'8','product-sales-reports','product_sales_reports');
    add_submenu_page("dashboard","Registration Report","Registration Report",'8','registration-reports','registration_reports');
    add_submenu_page("dashboard","Full Report","Full Report",'8','full-reports','full_reports');
}
function dashboard_fundraiser(){
    require_once("view/admin/dashboard.php");
}
function profile_reports(){
    require_once("view/admin/profile-reports.php");
}
function fundraising_reports(){
    require_once("view/admin/fundraising-reports.php");
}
function incentive_reports(){
    require_once("view/admin/incentive-reports.php");
}
function merchandise_reports(){
    require_once("view/admin/merchandise-reports.php");
}
function product_sales_reports(){
    require_once("view/admin/productSale-reports.php");
}
function registration_reports(){
    require_once("view/admin/registration-reports.php");
}
function full_reports(){
    require_once("view/admin/full-reports.php");
}
/* Include Assets admin */
add_action( 'admin_enqueue_scripts', 'load_admin_styles' );
function load_admin_styles() {

    $current_page = get_current_screen()->base;
   if( ($current_page=='reports_page_profile-reports') ||
       ($current_page=='reports_page_merchandise-reports') ||
       ($current_page=='reports_page_product-sales-reports') ||
       ($current_page=='reports_page_fundraising-reports') ||
       ($current_page=='reports_page_incentive-reports') ||
       ($current_page=='reports_page_registration-reports') ||
       ($current_page=='reports_page_full-reports')
   ){
       wp_enqueue_style( 'jquery_dataTables','https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css');
       wp_enqueue_style( 'buttons_dataTables','https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css');
       wp_enqueue_style( 'buttons_dataTables','https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css');
       wp_enqueue_style( 'admin_custom', FUNDRAISER_URL . '/assets/admin/custom.css', false, '1.0.0' );
       wp_enqueue_script( 'jquery-main', 'https://code.jquery.com/jquery-3.1.1.min.js?ver=3.1.1');
       wp_enqueue_script( 'jquery-dataTables', 'https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js');
       wp_enqueue_script( 'dataTables-buttons', 'https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js');
       wp_enqueue_script( 'jszip-min', 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js');
       wp_enqueue_script( 'pdfmake-min', 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js');
       wp_enqueue_script( 'vfs_fonts', 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js');
       wp_enqueue_script( 'buttons-html5-minn', 'https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js');
       wp_enqueue_script( 'buttons-print', 'https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js');
   }


}
/* Include css to frontend */
function wpdocs_theme_name_scripts() {
    $prime_option = array(
        'ajaxURL' => admin_url('admin-ajax.php'),
    );
    wp_localize_script('custom-fund', 'COLORFENZY_OPTION', $prime_option);
    wp_enqueue_style( 'custom-fund', FUNDRAISER_URL. '/assets/css/custom.css', false, '1.0.0');
    wp_enqueue_script( 'bootstrap.min', FUNDRAISER_URL. '/assets/js/bootstrap.min.js', false, '1.0.0');
    wp_enqueue_script( 'jquery-validator', 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js');
    wp_enqueue_style( 'bootstrap-datetimepicker', FUNDRAISER_URL. '/assets/css/bootstrap-datepicker.min.css', false, '1.0.0');
    wp_enqueue_script( 'moment.min', FUNDRAISER_URL. '/assets/js/moment.min.js', false, '1.0.0');
    wp_enqueue_script( 'bootstrap-datetimepicker', FUNDRAISER_URL. '/assets/js/bootstrap-datepicker.min.js', false, '1.0.0');
    wp_enqueue_script( 'custom-fun', FUNDRAISER_URL. '/assets/js/custom.js', false, '1.0.0');

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
