export function searchInputClear() {
  if(document.querySelector('.search-input')) {
    let input = document.querySelector('.search-input')
    let reset = document.querySelector('.search-input__reset')
    input.addEventListener('input', () => {
      input.classList.add('active')
    })
    reset.addEventListener('click', () => {
      input.classList.remove('active')

    })
  }
}

