<?php
/*
 * Change Request Modal
 * @Package Colourfrenzy
 * @Version 1.0.0
 */
?>
<!-- TSHirt -->
<div id="change-request-modal" style="z-index: 99999;" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <div class="modal-title">
            <h3 style="text-align: center;">Request Admin to Change</h3>
        </div>
        <div class="modal-body">
            <div class="container">
                <form id="student-profile_change" enctype="multipart/form-data" type="POST">
                    <div class="form-group group first">
                        <input type="hidden" name="parent_id" value="<?php echo base64_encode(get_current_user_id()); ?>">
                        <input type="hidden" name="action" value="student_change_request">
                        <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'change-profile-request' ); ?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea rows="10" cols="20" name="message" id="message" placeholder="Enter the message" class="form-control"></textarea>
                            </div>
                            <div class="error" style="margin: 0 auto;" id="message-error"></div>
                        </div>
                    </div>
                    <div class="form-group group" style="margin-top: 10px;">
                        <input type="submit" style="margin: 0 auto;" name="submit" value="Send" class="btn">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
