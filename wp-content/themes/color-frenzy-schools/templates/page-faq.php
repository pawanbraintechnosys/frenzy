<?php
/* Template Name: FAQ page */
get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/faq-section');

get_footer();
