<?php
$itemsBeforeSlider = 'about__items-before-slider';
$sliderTitle = get_field('about__slider-title');
$sliderItems = 'about__slider-items';
$itemsAfterSlider = 'about__items-after-slider';
$btnTitle = get_field('about__btn-title');
$btn = get_field('about__button');
?>
<?php if(have_rows($itemsBeforeSlider)): ?>
    <section class="about-section">
        <div class="container">
            <?php while(have_rows($itemsBeforeSlider)): the_row();
                $reverseItem = get_sub_field('reverse');
                $itemText = get_sub_field('text');
                $beforeItemImage = get_sub_field('imageBefore');
                $useVideo = get_sub_field('use-video');
                $videoUrl = get_sub_field('video-url');
                ?>
                <div class="about-section__text-img-item text-img-item <?= $reverseItem ? ' text-img-item--reverse ' : ''; ?>  text-img-item--align-top">
                    <?php if($useVideo): ?>
                        <a href="#" data-video-src="<?= $videoUrl; ?>" class="text-video-item__img">
                            <?= wp_get_attachment_image($beforeItemImage['id'], 'full') ?>
                            <button class="play-video">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                            </button>
                        </a>
                    <?php  else: ?>
                        <div class="text-img-item__img">
                            <?= wp_get_attachment_image($beforeItemImage['id'], 'full') ?>
                        </div>
                    <?php endif; ?>

                    <div class="text-img-item__text">
                        <?= $itemText; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <div class="container about-section__slider-wr">
            <div class="team-slider">
                <h2>
                    <?= $sliderTitle; ?>
                </h2>
                <div class="team-slider__container swiper-container">
                    <div class="team-slider__wrapper swiper-wrapper">
                        <?php while(have_rows($sliderItems)): the_row();
                            $photo = get_sub_field('photo');
                            $hoverPhoto = get_sub_field('hover-photo');
                            $name = get_sub_field('name');
                            $post = get_sub_field('post');
                            ?>
                            <div class="team-slider__item team-slider-item swiper-slide" data-hover-src="<?= $hoverPhoto['url']; ?>">
                                <h3>
                                    <?= $name; ?>
                                </h3>
                                <h4>
                                    <?= $post; ?>
                                </h4>
                                <div class="team-slider-item__img">
                                    <img src="<?= $photo['url']; ?>" alt="img">
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <div class="team-slider__arrows">
                    <button class="team-slider__arrow team-slider__arrow-prev">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/team-slider-arrow.svg" alt="arrow">
                    </button>
                    <button class="team-slider__arrow team-slider__arrow-next">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/team-slider-arrow.svg" alt="arrow">
                    </button>
                </div>
            </div>
        </div>
        <div class="container">
            <?php while(have_rows($itemsAfterSlider)): the_row();
                $reverseItem = get_sub_field('reverse');
                $itemText = get_sub_field('text');
                $afterItemImage = get_sub_field('image');
                $useVideo = get_sub_field('use-video');
                $videoUrl = get_sub_field('video-url');
            ?>


                <div class="about-section__text-img-item text-img-item <?= $reverseItem ? ' text-img-item--reverse ' : ''; ?>  text-img-item--align-top">
                    <div class="text-img-item__img">
                        <?php if($useVideo): ?>
                            <a href="#" data-video-src="<?= $videoUrl; ?>" class="text-video-item__img">
                                <?= wp_get_attachment_image($afterItemImage['id'], 'full') ?>
                                <button class="play-video">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                                </button>
                            </a>
                        <?php  else: ?>
                            <?= wp_get_attachment_image($afterItemImage['id'], 'full') ?>
                        <?php endif; ?>
                    </div>
                    <div class="text-img-item__text">
                        <?= $itemText; ?>
                    </div>
                </div>
            <?php endwhile; ?>

            <div class="about-section__description">
                <h3>
                    <?= $btnTitle; ?>
                </h3>
                <a href="<?= $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn">
                    <?= $btn['title']; ?>
                </a>
            </div>
        </div>
    </section>
<?php endif; ?>
