<?php
$title = get_field('logotypes__title');
$logos = 'logotypes';
?>
<?php if(have_rows($logos)): ?>
<section class="logo-section">
    <div class="container">
        <div class="logo-section__wrapper">
            <h2><?= $title; ?></h2>
            <div class="logo-section__inner">
                <?php while (have_rows($logos)): the_row();
                    $logo = get_sub_field('logo');
                    $url = get_sub_field('url');
                ?>
                    <a href="<?= $url; ?>"  class="logo-section__item logo-item">
                        <?= wp_get_attachment_image($logo['id'], 'full'); ?>
                    </a>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>