<?php
$items = 'text-image-items__items';
$titleReviews = get_field('reviews__title');
$titleThreeColumn = get_field('three-col-items__title');
$descriptionThreeColumn = get_field('three-col-items__description');
$reviewsItems = 'reviews__items';
$threeColItems = 'three-col-items__items';
$videoPreview = get_field('video_section_video_preview');
$videoLink = get_field('video_section_video_link');

?>
<section class="text-img-items">
    <div class="container">
        <div class="text-img-items__wrapper">
            <?php if(have_rows($items)): ?>
                <?php while (have_rows($items)): the_row();
                    $reverse = get_sub_field('reverse');
                    $img = get_sub_field('image');
                    $text = get_sub_field('text');
                    $button = get_sub_field('button');
                    $showTicketsImage = get_sub_field('show-tickets-image');
                    $showSliderImages = get_sub_field('show-slider-with-image');
                    $videoUrl = get_sub_field('video-url');
                    $videoImage = get_sub_field('video-image');
                    $sliderImages = 'slider-images';
                ?>
                    <div <?= $showTicketsImage ? 'id="price"' : ''; ?> class="text-img-items__item <?= $videoLink && $videoPreview ? 'flex-start' : ''; ?>  text-img-item check-mark-list <?= $reverse? 'text-img-item--reverse' : ''; ?>">
                        <div class="text-img-item__img <?= $videoLink && $videoPreview && $showTicketsImage? 'with-video' : ''; ?>">
                            <?php if($videoLink && $videoPreview && $showTicketsImage): ?>
                                <div data-video-src="<?= $videoLink; ?>" class="video-imgs-items__item  video-imgs-items__item--big  video-imgs-item">
                                    <?= wp_get_attachment_image($videoPreview['id'], 'full'); ?>
                                    <button class="play-video">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                                    </button>
                                </div>
                            <?php endif; ?>
                            <?= wp_get_attachment_image($img['id'], 'full'); ?>
                        </div>
                        <div class="text-img-item__text">
                            <?= $text; ?>
                            <?php if($button): ?>
                                <a href="<?= $button['url']; ?>" target="<?= $button['target']; ?>" class="btn" >
                                    <?= $button['title']; ?>
                                </a>

                                <?php if($showTicketsImage): ?>
                                    <img src="<?= get_template_directory_uri(); ?>/assets/img/logo/ticketebo-white-trans_poweredby.png" alt="Tickets Image" class="tickets-img">
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php if(have_rows($sliderImages) && $showSliderImages): ?>
                        <div class="carousel-img">
                            <button class="slider-arrow slider-arrow--prev carousel-img__arrow carousel-img__prev"></button>
                            <div class="carousel-img__container swiper-container">
                                <div class="carousel-img__wrapper swiper-wrapper">
                                    <?php while (have_rows($sliderImages)): the_row();
                                        $img = get_sub_field('image');
                                        ?>
                                        <div class="carousel-img__item swiper-slide" data-modal-id="modal-images">
                                            <?= wp_get_attachment_image($img['id'], 'full'); ?>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                            <button class="slider-arrow slider-arrow--next carousel-img__arrow carousel-img__next"></button>
                            <div class="slider-pagination carousel-img__pagination"></div>
                        </div>
                    <?php endif; ?>

                <?php endwhile; ?>
            <?php endif; ?>

            <?php if(have_rows($reviewsItems)): ?>
                <div class="text-img-items__slider-inner">
                    <h2><?= $titleReviews; ?></h2>
                    <div class="slider-wrapper event-slider">
                        <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

                        </button>

                        <div class="text-img-items__slider reviews-slider swiper-container">
                            <div class="reviews-slider__wrapper swiper-wrapper">
                                <?php while (have_rows($reviewsItems)): the_row();
                                    $avatar = get_sub_field('avatar');
                                    $name = get_sub_field('name');
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                ?>
                                    <div class="reviews-slider__item reviews-slider-item swiper-slide">
                                        <div class="reviews-slider-item__info">
                                            <div class="reviews-slider-item__img">
                                                <?= wp_get_attachment_image($avatar['id'], 'full'); ?>
                                            </div>
                                            <div class="reviews-slider-item__text">
                                                <h3><?= $name; ?></h3>
                                                <i>
                                                    <?= $title; ?>
                                                </i>
                                            </div>
                                        </div>
                                        <ul class="reviews-slider-item__mark">
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                        </ul>
                                        <p><?= $description; ?></p>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>

                        <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>

                        <div class="slider-pagination reviews-slider__pagination"></div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<section class="brush-bg-section brush-bg-section--pink-bg">
    <div class="container">
        <?php if(have_rows($threeColItems)): ?>
            <div class="brush-bg-section__text-item-wr">
                <div class="title-three-column">
                    <h2><?= $titleThreeColumn; ?></h2>
                    <?= $descriptionThreeColumn? $descriptionThreeColumn : ''; ?>
                </div>

                <div class="text-items slider-section__text-items">
                    <?php while (have_rows($threeColItems)): the_row();
                        $title = get_sub_field('item_title');
                        $text = get_sub_field('item_text');
                    ?>
                        <div class="text-items__item text-item">
                            <h3><?= $title; ?></h3>
                            <p><?= $text; ?></p>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php
            $topLeft = get_field('video_section_top_left_image');
            $topRight = get_field('video_section_top_right_image');
            $bottomLeft = get_field('video_section_bottom_left_image');
            $bottomRight = get_field('video_section_bottom_right_image');
            $videoPreview = get_field('video_section_video_preview');
            $videoLink = get_field('video_section_video_link');
        ?>

        <?php if($videoLink): ?>
<!--            <div class="brush-bg-section__video-imgs-items video-imgs-items">-->
<!--                <div class="video-imgs-items__item  video-imgs-items__item--small video-imgs-item">-->
<!--                    --><?//= wp_get_attachment_image($topLeft['id'], 'full'); ?>
<!--                </div>-->
<!---->
<!--                <div data-video-src="--><?//= $videoLink; ?><!--" class="video-imgs-items__item  video-imgs-items__item--big  video-imgs-item">-->
<!--                    --><?//= wp_get_attachment_image($videoPreview['id'], 'full'); ?>
<!--                    <button class="play-video">-->
<!--                        <img src="--><?//= get_template_directory_uri(); ?><!--/assets/img/icon/ic-video.svg" alt="image">-->
<!--                    </button>-->
<!--                </div>-->
<!---->
<!--                <a class="video-imgs-items__item  video-imgs-items__item--middle video-imgs-item">-->
<!--                    --><?//= wp_get_attachment_image($topRight['id'], 'full'); ?>
<!--                </a>-->
<!--                <div class="video-imgs-items__item  video-imgs-items__item--middle video-imgs-item">-->
<!--                    --><?//= wp_get_attachment_image($bottomLeft['id'], 'full'); ?>
<!--                </div>-->
<!--                <div class="video-imgs-items__item  video-imgs-items__item--small video-imgs-item">-->
<!--                    --><?//= wp_get_attachment_image($bottomRight['id'], 'full'); ?>
<!--                </div>-->
<!--            </div>-->
        <?php endif; ?>

        <?php $centerContent = get_field('center-content-events'); ?>

        <?php if($centerContent): ?>
            <div class="center-content" id="racepack-collection-block">
                <?= $centerContent; ?>
            </div>
        <?php endif; ?>

        <?php
            $itemsThreeLinks = 'three-col-link-items__items';
            $titleThreeLinks = get_field('three-col-link-items__title');
        ?>
        <?php if(have_rows($itemsThreeLinks)): ?>
            <div class="brush-bg-section__link-items-wr">
                <h2><?= $titleThreeLinks; ?></h2>
                <div class=" link-items link-items--shadow">
                    <?php while (have_rows($itemsThreeLinks)): the_row();
                        $img = get_sub_field('image');
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $btn = get_sub_field('button');
                    ?>
                        <div class="link-items__item link-item">
                            <div class="link-item__img">
                                <?= wp_get_attachment_image($img['id'], 'full'); ?>
                            </div>
                            <h3><?= $title; ?></h3>
                            <p><?= $description; ?></p>
                            <?php if($btn): ?>
                                <a href="<?= $btn['url']; ?>" target="<?= $btn['target']; ?>" class="btn btn--small btn--transparent">
                                    <?= $btn['title']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="brush-bg-section__form">
            <h4>
                Join Our VIP Colour Frenzy Newsletter
            </h4>

            <?= do_shortcode('[sibwp_form id=1]'); ?>
        </div>
    </div>
</section>