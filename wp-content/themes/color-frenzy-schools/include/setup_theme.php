<?php
if ( ! function_exists( 'rhona_setup' ) ) :
    function rhona_setup() {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'parent_post_rel_link', 10);
        remove_action('wp_head', 'start_post_rel_link', 10);
        remove_action('wp_head', 'adjacent_posts_rel_link', 10);
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'wp_oembed_add_discovery_links');
        remove_action('wp_head', 'rest_output_link_wp_head');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
    }
endif;
add_action( 'after_setup_theme', 'rhona_setup' );
//add_filter('show_admin_bar', '__return_false');
add_action('admin_head', 'my_custom_width_gutenberg');
function my_custom_width_gutenberg() {
    echo '<style>
    .wp-block{
        max-width: 100% !important;
    }
  </style>';
}
add_filter( 'upload_mimes', 'svg_upload_allow' );

function svg_upload_allow( $mimes ) {
    $mimes['svg']  = 'image/svg+xml';

    return $mimes;
}

add_filter( 'wpcf7_autop_or_not', '__return_false' );

add_filter( 'wpcf7_form_elements', 'mycustom_wpcf7_form_elements' );

function mycustom_wpcf7_form_elements( $form ) {
    $form = do_shortcode( $form );

    return $form;
}

add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);
function prefix_disable_gutenberg($current_status, $post_type)
{
    if ($post_type === 'post' || $post_type === 'private-tours' || $post_type === 'boats') return false;
    return $current_status;
}

add_filter( 'paginate_links', 'my_search_pagination_mod', 1 );

function my_search_pagination_mod( $link )
{

    if ( is_search() ) {

        $pattern = '/page\/([0-9]+)\//';

        if ( preg_match( $pattern, $link, $matches ) ) {
            $number = $matches[ 1 ];

            $link = remove_query_arg( 'paged' );

            $link = add_query_arg( 'page', $number );

        } else {

            $link = str_replace( 'paged', 'page', $link );

        }

    }

    return $link;
}

function my_post_queries( $query ) {
    if (!is_admin() && $query->is_main_query()){
        if(is_category()){
            $query->set('posts_per_page', 1);
        }
    }
}

add_action( 'pre_get_posts', 'my_post_queries' );

add_theme_support( 'menus' );



