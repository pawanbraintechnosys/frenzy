<?php
/*
* Email Template
* Desc : Send welcome email to parent
* @ver 1.0
*/

$parentEmailData=$args;

?>
<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        @media screen {
            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 400;
                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 400;
                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
            font-family: "Have Heart Two";
        }

        table td p{}

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;"> We're thrilled to have you here! Get ready to dive into your new account. </div>
<div style="background: #fafafa;padding: 30px 33px;max-width: 600px;align-items: center;display: flex;width: 100%;margin: 0 auto;}">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" ><img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/12/email-image.png" style="width:100%;" alt="color"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="" >
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding-bottom:12px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                            <p style="font-size: 37px; font-weight: 600;line-height: 42px;letter-spacing: 1px; margin:0px;padding: 10px 0;"><strong>Welcome</strong></p>
                            <p style="font-size: 26px;font-weight: 400;line-height: 26px;letter-spacing: 1px;background: #1e8ec3;margin:0; padding:25px 5px;color:#fff;"><strong>Colourfrenzy</strong></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;text-align: center;">
                            <p style="margin: 0;">Thank you for registering on colourfrenzy.</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;text-align: center;">
                            <p style="margin-top: 10px;"><strong>These are the below details.</strong></p>
                            <?php
                             $parentDetail=get_user_data($parentEmailData[0]['parent_id']);
                            ?>
                            <p style="margin: 0;"><strong>School Name : </strong><?php echo post_detail($parentEmailData[0]['school_id'])['post_title']; ?></p>
                            <p style="margin: 0;"><strong>Parent Name : </strong><?php echo $parentDetail['display_name']; ?></p>
                            <p style="margin: 0;"><strong>Parent Email : </strong> <?php echo  $parentDetail['user_email']; ?></p>
                            <p style="margin: 0;"><strong>Parent Phone : </strong> <?php  echo $parentDetail['phone']; ?></p>
                            <p style="margin: 0;"><strong>Total Charge: </strong><?php echo $parentEmailData[0]['total_sub_total']; ?></p>

                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;text-align: center;">
                            <p style="margin: 0;">Below are the children details.</p>
                        </td>
                    </tr>
                    <?php foreach ($parentEmailData as $key=>$val): ?>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px 30px 15px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;text-align: center;">
                            <p style="margin-top:10px;"><strong></strong></p>
                            <p style="margin: 0;"><strong>Name: </strong><?php echo $val['firstname'].' '.$val['lastname']; ?></p>
                            <p style="margin: 0;"><strong>Event Name : </strong><?php echo $val['event_name']; ?></p>

                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding-bottom:20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                            <p style="font-size: 26px;font-weight: 400;line-height: 26px;letter-spacing: 1px;background: #1e8ec3;margin:0; padding:30px 5px;color:#fff;"><strong>Thank You.</strong></p>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
</div>
</body>

</html>