<?php

//Template Name: Homepage

get_header();

get_template_part('template-parts/hero-section', null, array('is_homepage' => true, 'section-classes' => 'hero-section--content-center'));

get_template_part('template-parts/text-video-items', null, array(
    'section-classes' => 'text-video-items--blue',
    'items-classes' => 'text-video-item--align-center text-video-item--text-small'
));

get_template_part('template-parts/one-row-slider', null, array(
    'section-classes' => 'one-row-slider--yellow-brush',
));

get_template_part('template-parts/two-col-section', null, array(
    'section-classes' => 'brush-bg-section--pink-bg',
));

get_template_part('template-parts/three-col-imgs');

get_template_part('template-parts/reviews-slider-section');

get_template_part('template-parts/video-section', null, array(
    'item-classes' => 'text-video-item text-video-item--without-text text-video-item--bubbles'
));

get_template_part('template-parts/form-section');

get_footer();
