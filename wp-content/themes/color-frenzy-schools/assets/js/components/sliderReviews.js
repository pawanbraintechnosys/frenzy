import Swiper, {Pagination} from 'swiper';

Swiper.use([Pagination]);

export function initSliderReviews() {
  const oneCol = new Swiper('.reviews-slider', {
    spaceBetween: 30,
    watchOverflow: true,
    pagination: {
      el: '.reviews-slider__pagination',
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 15
      },
      1024: {
        spaceBetween: 30
      },
      1280: {
        slidesPerView: 3,
      }
    }
  });

}
