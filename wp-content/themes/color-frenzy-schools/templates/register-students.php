<?php

//Template Name: Register students

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center hero-section--mob-bg-right'
));

get_template_part('template-parts/text-img-items', null, array(
    'section-classes' => 'text-img-items--violet',
    'items-classes' => 'text-img-item text-img-item--head-green',
));

get_template_part('template-parts/search-section', null, array('type' => 'register'));

get_footer();

