<?php

//Template Name: School Fundraising Goal

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center'
));

get_template_part('template-parts/text-img-items', null, array(
    'section-classes' => 'text-img-items--violet text-img-items--violet-brush',
    'items-classes' => 'text-img-item text-img-item--head-green'
));

get_template_part('template-parts/text-video-items', null, array(
    'items-classes' => 'text-video-item--pink-head text-video-item--align-center text-video-item--text-small text-video-item--brush'
));

get_template_part('template-parts/form-section', null, array(
    'form-classes' => 'main-form--btn-center'
));

get_footer();

