<?php

/* Template Name: Fundrasing events page */



get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-image-section', null,
    array(
            'text-classes' => ' paw-mark-list head-bg head-bg--violet ',
            'section-classes' => ' text-img-section--violet-bg ',
    )
);

get_footer();
