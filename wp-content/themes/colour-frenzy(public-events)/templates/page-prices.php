<?php

/* Template Name: Prices page */


get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small '));

get_template_part('template-parts/three-col-imgs-section--tickets');

get_template_part('template-parts/text-img-items-section', null,
    array(
            'section-classes' => ' text-img-items--green text-img-items--green-brush ',
            'item-classes' => ' text-img-item text-img-item--list-small check-mark-list ',
    )
);

get_footer();
