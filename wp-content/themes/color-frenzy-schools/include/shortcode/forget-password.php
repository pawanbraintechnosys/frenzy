<?php
/*
 * Forget Password
 *
 */
function forget_password_callback($atts)
{
    $atts = shortcode_atts(array(), $atts, 'forget_password');
    if(!is_user_logged_in()):
        ?>
        <div class="school_login school-login-form">
            <div class="container">
                <div class="row myform">
                    <div class="col-lg-4">
                        <h2 class="text-center">Forgot Password</h2>
                        <div class="card">
                            <div class="card-body">
                                <p class="">Please enter your registered email Address.</p>
                                <form class="form-post" id="forget_pasword" method="POST" action="">
                                    <input type="hidden" name="action" value="forget_password">
                                    <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'forget-password' ); ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Email address</label>
                                        <div class="">
                                            <input id="email" Placeholder="Email Address" name="user_email" type="email" class="form-control " name="email" value="" required="" autocomplete="email" autofocus="">
                                        </div>
                                    </div>
                                    <div class="form-group signin">
                                        <input type="submit" class="btn btn-warning btn-lg btn-block w-100" value="RESET"></input>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <?php echo wp_safe_redirect(home_url()); ?>
    <?php endif ?>
    <?php
}
add_shortcode('forget_password', 'forget_password_callback');

/* Reset Password */
function reset_password_callback($atts)
{
    global $wpdb;
    $atts = shortcode_atts(array(), $atts, 'reset-passowrd');
    $token= @$_GET['token'];
    $result = $wpdb->get_results("SELECT * from wp8t_users_token where token='$token'", ARRAY_A)[0];
    if(!is_user_logged_in() && isset($result) ):
        ?>
        <div class="school_login school-login-form">
            <div class="container">
                <div class="row myform">
                    <div class="col-6 col-md-4 col-lg-4">
                        <h2 class="text-center">Reset Password</h2>
                        <div class="card">
                            <div class="card-body">
                                <p class=""></p>
                                <form class="form-post" id="reset_password" method="POST" action="">
                                    <input type="hidden" name="action" value="reset_password">
                                    <input type="hidden" name="token" value="<?php echo $token; ?>">
                                    <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'reset-password' ); ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Password</label>
                                            <input id="password" Placeholder="Password" name="user_pass" type="password" class="form-control " name="user_pass" value="" required="" autocomplete="user_pass" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="email">Confirm Password</label>
                                        <input id="confirm_password" Placeholder="Password" name="confirm_user_pass" type="password" class="form-control "  value="" required="" autocomplete="user_pass" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-warning btn-lg btn-block w-100" value="Submit"></input>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <link href="<?php echo get_template_directory_uri() ?>/assets/css/private.css" rel="stylesheet">
        <div class="unauthorized" style="background: url('https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/become-sponser.jpeg')">
            <div class="four_zero_four_bg">
                <h1 class="text-center ">404</h1>
            </div>
            <div class="contant_box_404">
                <p>Token has been Expired.</p>
                <a href="<?php echo home_url(); ?>" class="link_404">Go to Home</a>
            </div>
        </div>
    <?php endif ?>
    <?php
}
add_shortcode('reset_password', 'reset_password_callback');
?>
