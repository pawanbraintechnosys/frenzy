<?php
/*
 * Student and parent Login
 */
?>
<div class="col-lg-4">
    <h2 class="text-center">PARENT/STUDENT <span>LOGIN</span></h2>
    <div class="card">
        <div class="card-body">
            <p class="">Sign in to your account.</p>
            <form class="form-post" method="POST" action="" id="student_parent">
                <input type="hidden" name="action" value="studen_parent_login">
                <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('student-parent-registration'); ?>">
                <div class="form-group group">
                    <label class="control-label" for="email">Email address</label>
                    <div class="">
                        <input id="email1" Placeholder="Email Address" type="email" class="form-control " name="user_email" value="" required="" autocomplete="email" autofocus="">
                    </div>
                </div>
                <div class="form-group group">
                    <label class="control-label" for="password" >Password</label>
                    <div class="">
                        <input id="password2" Placeholder="Password" type="password" class="form-control " name="user_pass" required="" autocomplete="current-password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <a href="<?php echo home_url('/forget-password'); ?>" class="forgot-pass">Forgot your Password?</a>
                    </div>
                </div>
                <div class="form-group signin col-md-6">
                    <input type="submit" class="btn btn-warning" value="log in"></input>
                </div>
                <div class="form-group signup col-md-6">
                    <a type="submit" class="btn btn-warning btn-lg btn-block w-100" href="<?php echo home_url().'/find-your-school-student/' ?>">SIGNUP</a>
                </div>
            </form>
        </div>
    </div>
</div>
