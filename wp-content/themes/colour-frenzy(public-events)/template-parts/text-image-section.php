<?php

$title = get_field('text-image__title');
$text = get_field('text-image__text');
$btn = get_field('text-image__btn');
$img = get_field('text-image__img');
?>
<section class="text-img-section <?= $args['section-classes']; ?> ">
    <div class="container">
        <div class="text-img-section__wrapper">
            <div class="text-img-section__text <?= $args['text-classes']; ?> ">
                <?php if($title): ?>
                    <h2><?= $title; ?></h2>
                <?php endif; ?>
                <?= $text; ?>
                <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn">
                    <?= $btn['title']; ?>
                </a>
            </div>
            <div class="text-img-section__img">
                <?= wp_get_attachment_image($img['id'], 'full'); ?>
            </div>
        </div>
    </div>
</section>
