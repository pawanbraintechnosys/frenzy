<?php
/*
 * SCHOOL REGISTRAION
 * @package ColorFenzy
 * @ver 1.0
 */

add_action('wp_ajax_nopriv_school_login', 'school_login');
add_action('wp_ajax_school_login', 'school_login');
function school_login()
{
    global $wpdb;
    $html = '';
    $errors = new WP_Error();
    $response = array();
    $flag = false;
    $cred = array(
        'user_login' => $_POST['user_email'],
        'user_password' => $_POST['user_pass'],
        'remember' => true
    );
    $user = wp_signon($cred, false);
    if (is_wp_error($user)) {
        $errors->add("register_issues", __('Invalid details', 'fanclub'));
    } else {
		$user_id = $user->data->ID;
        $user_meta = get_userdata($user_id);
        $user_roles = $user_meta->roles;
        if (in_array('school_role', $user_roles)) {	
			$school_id = get_user_meta($user_id, 'school_id', true);
			$tableName=TABLE_PREFIX_MAIN;
			$schoolExists=$wpdb->get_results("SELECT id from {$tableName}school_details where school_id=$school_id",ARRAY_A);
			if(!empty($schoolExists)){
				$url = home_url("/school-dashboard/?id=") . base64_encode($school_id);
			}else{
				$url = home_url("/create-website-page/?id=") . base64_encode($school_id);
			}
            $flag = true;
            $html .= "Logged in successfull.";
            $response['url'] = $url;
        } else {
            $errors->add("register_issues", __('You are not authorized to login here.', 'fanclub'));
            wp_logout();
            $flag = false;
        }
    }
    if ($flag) {
        $response['status'] = "success";
    } else {
        $response['status'] = "failed";
    }
    $error_messages = $errors->get_error_messages();
    if (count($error_messages) > 0) {
        foreach ($error_messages as $k => $message) {
            $html .= $message;
        }
    }
    $response['message'] = $html;
    wp_send_json($response);
    die;
}

/* Forget Password */


add_action('wp_ajax_nopriv_forget_password', 'forget_password');
add_action('wp_ajax_forget_password', 'forget_password');
function forget_password()
{
    $html = '';
    $errors = new WP_Error();
    $response = array();
    $flag = false;
    global $wpdb;
    $email = @$_POST['user_email'];
    if (email_exists($email)) {
        $user = get_user_by('email', $email);
        $user_id = $user->data->ID;
        $expiry_date = date('Y-m-d H:i:s', strtotime('+1 day', current_time('timestamp')));
        $token = openssl_random_pseudo_bytes(16);
        $token = bin2hex($token);

        /* Delete Expired token */
        $expiredID = array();
        $expiredToken = $wpdb->get_results("SELECT * FROM wp8t_users_token WHERE expiry <= CURRENT_TIMESTAMP", ARRAY_A);
        if (isset($expiredToken) && !empty($expiredToken)) {
            foreach ($expiredToken as $key => $expiredToken) {
                $expiredID[] = $expiredToken['id'];
            }
        }
        $expired_ids = implode(",", $expiredID);
        if (!empty($expired_ids)) {
            $wpdb->query("DELETE FROM wp8t_users_token WHERE id IN ($expired_ids)");
        }
        $result = $wpdb->get_results("SELECT * from wp8t_users_token where user_id= $user_id", ARRAY_A)[0];
        if (isset($result) && !empty($result)) {
            $id = $result['id'];
            $wpdb->update('wp8t_users_token',
                array(
                    'user_id' => $user_id,
                    'token' => $token,
                    'expiry' => $expiry_date
                ),
                array('id' => $id)
            );
            $flag = true;
        } else {
            $wpdb->insert('wp8t_users_token', array(
                    'user_id' => $user_id,
                    'token' => $token,
                    'expiry' => $expiry_date
                )
            );
            $tokenID = $wpdb->insert_id;
            $flag = true;
        }
        $forgetPasswordEmail =
            array(
                'user_id' => $user_id,
                'token' => $token,
                'expiry' => $expiry_date,
                'activationLink' => home_url('/reset-password/?token=') . $token
            );
        ob_start();
        get_template_part('include/email-template/forget-password', null, $forgetPasswordEmail);
        $output = ob_get_contents();
        $emailData = array(
            'to' => $email,
            'message' => $output,
            'subject' => 'Forget Password Email',

        );
        $mail = send_mail($emailData);
        ob_end_clean();

        $html .= "Verification link has been sent to your registered email ID.";
    } else {
        $flag = false;
        $html .= "Email does not exists. Please enter valid email.";
    }
    if ($flag) {
        $response['status'] = "success";
    } else {
        $response['status'] = "failed";
    }
    $error_messages = $errors->get_error_messages();
    if (count($error_messages) > 0) {
        foreach ($error_messages as $k => $message) {
            $html .= $message;
        }
    }
    $response['message'] = $html;
    wp_send_json($response);
    die;
}

/* Reset Password */

add_action('wp_ajax_nopriv_reset_password', 'reset_password_callback_ajax');
add_action('wp_ajax_reset_password', 'reset_password_callback_ajax');
function reset_password_callback_ajax()
{
    $html = '';
    $errors = new WP_Error();
    $response = array();
    $flag = false;
    global $wpdb;
    $user_pass = @$_POST['user_pass'];
    $token = @$_POST['token'];

    if ($token) {
        $user = $wpdb->get_results("SELECT * from wp8t_users_token where token='$token'", ARRAY_A)[0];
        $user_id = $user['user_id'];
        /* set password */
        wp_set_password($user_pass, $user_id);
        /* delete the verification */
        $wpdb->delete('wp8t_users_token',
            array(
                'user_id' => $user_id
            ),
        );
        $flag = true;
        $html .= "Password changed successfully.";
        $response['url'] = home_url() . '/school-login';
    } else {
        $flag = false;
        $html .= "Unable to change the password.";
    }

    if ($flag) {
        $response['status'] = "success";
    } else {
        $response['status'] = "failed";
    }
    $error_messages = $errors->get_error_messages();
    if (count($error_messages) > 0) {
        foreach ($error_messages as $k => $message) {
            $html .= $message;
        }
    }
    $response['message'] = $html;
    wp_send_json($response);
    die;
}

/* Create School webpage */

add_action('wp_ajax_nopriv_create_school_webpage', 'create_school_webpage_ajax');
add_action('wp_ajax_create_school_webpage', 'create_school_webpage_ajax');
function create_school_webpage_ajax()
{

    $html = '';
    $errors = new WP_Error();
    $response = array();
    $flag = false;
    global $wpdb;
    $schoolData = array(
        'school_id' => @$_POST['school_id'],
        'event_name' => @$_POST['event_name'],
        //'about_event' => @$_POST['about_event'],
        'fundrasing_goal' => @$_POST['fundrasing-goal'],
        
        'school_state' => @$_POST['school_state'],
        'class_detail' => @$_POST['class_detail'],
        'class_detail_2' => @$_POST['class_detail_2'],
        'rto_number' => ($_POST['rto_number'])?$_POST['rto_number']:' ',
        'crios_code' => ($_POST['crios_code'])?$_POST['crios_code']:' ',
        'created_date' => date('Y-m-d H:i:s', current_time('timestamp', 1)),
        'updated_date' => date('Y-m-d H:i:s', current_time('timestamp', 1)),
    );
    /** Update Post Meta */
    foreach ($schoolData as $key=>$school){
        if($key !=='school_id'){
            update_post_meta($_POST['school_id'],$key,$school);
        }
    }
    /*
     * Upload file
     */
    if (!empty($_FILES['school_logo']['name'])) {
        $files = $_FILES['school_logo'];
        if ($files['name']) {
            $file = array(
                'name' => $files['name'],
                'type' => $files['type'],
                'tmp_name' => $files['tmp_name'],
                'error' => $files['error'],
                'size' => $files['size']
            );
            $uploaded_file_type = $files['type'];
            $allowed_file_types = array('image/jpg', 'image/jpeg', 'image/png');
            if (in_array($uploaded_file_type, $allowed_file_types)) {
                $uploadedfile = $file;
                $upload_overrides = array(
                    'test_form' => false
                );
                $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                $filename=$movefile['file'];
                /* Insert Attachment */
                $filetype  = wp_check_filetype(basename( $movefile['file'] ), null );
                $wp_upload_dir = wp_upload_dir();
                // Prepare an array of post data for the attachment.
                $attachment = array(
                    'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
                    'post_mime_type' => $filetype['type'],
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );
                $attach_id = wp_insert_attachment( $attachment, $filename,$_POST['school_id']);
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );
                update_post_meta($_POST['school_id'],'school_logo',$attach_id);
                // Prepare an array of post data for the attachment.
                if ( $movefile && ! isset( $movefile['error'] ) ) {
                    $school_logo=$movefile['url'];
                    $schoolData['school_logo']=$school_logo;
                    $flag=true;
                } else {
                    echo $movefile['error'];
                    $flag=false;
                }
            } else {
                $html .= "Invalid File Type. <br />";
                $flag=false;
            }
        }
    }
    if (!empty($_FILES['hero_image']['name'])) {
        $files = $_FILES['hero_image'];
        if ($files['name']) {
            $file = array(
                'name' => $files['name'],
                'type' => $files['type'],
                'tmp_name' => $files['tmp_name'],
                'error' => $files['error'],
                'size' => $files['size']
            );
            $uploaded_file_type = $files['type'];
            $allowed_file_types = array('image/jpg', 'image/jpeg', 'image/png');
            if (in_array($uploaded_file_type, $allowed_file_types)) {
                $uploadedfile = $file;
                $upload_overrides = array(
                    'test_form' => false
                );
                $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                $filename=$movefile['file'];
                /* Insert Attachment */
                $filetype  = wp_check_filetype(basename( $movefile['file'] ), null );
                $wp_upload_dir = wp_upload_dir();
                // Prepare an array of post data for the attachment.
                $attachment = array(
                    'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
                    'post_mime_type' => $filetype['type'],
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );
                $attach_id = wp_insert_attachment( $attachment, $filename,$_POST['school_id']);
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );
                set_post_thumbnail($_POST['school_id'], $attach_id );
                // Prepare an array of post data for the attachment.
                if ( $movefile && ! isset( $movefile['error'] ) ) {
                   $heroImage=$movefile['url'];
                    $schoolData['hero_image']=$heroImage;
                    $flag=true;
                } else {
                    $flag=false;
                }
            } else {
                $html .= "Invalid File Type. <br />";
                $flag=false;
            }
        }
    }
    /* Insert Into Table */
    $school_id=@$_POST['school_id'];
    $result=$wpdb->get_results("SELECT id from wp8t_school_details where school_id=$school_id",ARRAY_A);
    if(!empty($result)){
        foreach ( $schoolData as $key=>$val){
            if( $key=='school_id'){
                unset($schoolData[$key]);
            }
        }
        $wpdb->update('wp8t_school_details',
            $schoolData,
            array('school_id'=>$school_id)
        );
        $flag=true;
    }else{
        $wpdb->insert('wp8t_school_details',
            $schoolData
        );
        $flag=true;
    }
    if ($flag) {
        $html .= "School website created successfully";
        $response['status'] = "success";
        $response['url'] = home_url().'/school-dashboard';
    } else {
        $response['status'] = "failed";
        $html .= "Unable to create school webpage.";
    }
    $error_messages = $errors->get_error_messages();
    if (count($error_messages) > 0) {
        foreach ($error_messages as $k => $message) {
            $html .= $message;
        }
    }
    $response['message'] = $html;
    wp_send_json($response);
    die;
}

/* Donate School Fundraising page */

add_action('wp_ajax_nopriv_donate_payment', 'donate_payment_for_school');
add_action('wp_ajax_donate_payment', 'donate_payment_for_school');
function donate_payment_for_school()
{

    $html = '';
    $errors = new WP_Error();
    $response = array();
    $flag = false;
    global $wpdb;
    $donateTransaction=array(
        'fundpage_id'=>($_POST['fundpage_id'])?$_POST['fundpage_id']:' ',
        'user_id'=>($_POST['user_id'])?$_POST['user_id']:' ',
        'child_id'=>($_POST['child_id'])?$_POST['child_id']:' ',
        'shipping_firstname'=>($_POST['first_name'])?$_POST['first_name']:' ',
        'shipping_lastname'=>($_POST['last_name'])?$_POST['last_name']:' ',
        'shipping_email'=>($_POST['email'])?$_POST['email']:' ',
        'amount'=>($_POST['paypal']['purchase_units'][0]['amount']['value'])?$_POST['paypal']['purchase_units'][0]['amount']['value']:' ',
        'status'=>($_POST['paypal']['status'])?$_POST['paypal']['status']:' ',
        'payer_id'=>($_POST['paypal']['payer']['payer_id'])?$_POST['paypal']['payer']['payer_id']:' ',
        'invoice_id'=>($_POST['paypal']['purchase_units'][0]['invoice_id'])?$_POST['paypal']['purchase_units'][0]['invoice_id']:' ',
        'merchant_id'=>($_POST['paypal']['purchase_units'][0]['payee']['merchant_id'])?$_POST['paypal']['purchase_units'][0]['payee']['merchant_id']:' ',
        'mode'=>'PAYPAL',
        'type'=>'SCHOOL',
        'created_date'=>current_time('mysql'),
        'updated_date'=>current_time('mysql'),
    );
    /* Insert Into Table */
    $wpdb->insert('fundraise_donation',
        $donateTransaction
    );
    $id=$wpdb->insert_id;
    if($id){
        $flag=true;
        //send_donation_email();
    }else{
        $flag=false;
    }
    if ($flag) {
        $html .= "Donation successful";
        $response['status'] = "success";
        $response['url'] = home_url().'/thank-you/?id='.$donateTransaction['payer_id'];
    } else {
        $response['status'] = "failed";
        $html .= "Unable to make the donation.";
    }
    $error_messages = $errors->get_error_messages();
    if (count($error_messages) > 0) {
        foreach ($error_messages as $k => $message) {
            $html .= $message;
        }
    }
    $response['message'] = $html;
    wp_send_json($response);
    die;
}
if(!function_exists('send_donation_email')){
    function send_donation_email($data=null){

    }
}
/* get School class */
add_action('wp_ajax_nopriv_get_school_class', 'get_school_class_callback');
add_action('wp_ajax_get_school_class', 'get_school_class_callback');
function get_school_class_callback(){

    global $wpdb;
    $html='';
    $option='';
    $flag=false;$errors=false;
    $school_id=@$_POST['school_id'];
    $table_name=TABLE_PREFIX_MAIN;
    $result=$wpdb->get_results("SELECT DISTINCT class from {$table_name}child_details where school_id=$school_id",ARRAY_A);
    if(!empty($result)){
        $option.='<option  value="">Select Class</option>';
       foreach( $result as $key=>$val){
           $option.='<option data-class="'.$school_id.'" value="'.$val['class'].'">'.$val['class'].'</option>';
       }
    }else {
        $option.='<option value="">No record Foud</option>';
    }
    echo $option;
    die;
}
/* get School class */
add_action('wp_ajax_nopriv_get_school_children_by_class', 'get_school_children_by_class');
add_action('wp_ajax_get_school_children_by_class', 'get_school_children_by_class');
function get_school_children_by_class(){

    global $wpdb;
    $html='';
    $option='';
    $flag=false;$errors=false;
    $school_id=@$_POST['school_id'];
    $class_id=@$_POST['class_id'];
    $table_name=TABLE_PREFIX_MAIN;
    $result=$wpdb->get_results("SELECT id,firstname,lastname,parent_id from {$table_name}child_details where school_id=$school_id AND class='".$class_id."'",ARRAY_A);
    if(!empty($result)){
        $option.='<option  value="">Select Student</option>';
        foreach( $result as $key=>$val){
            $name=$val['firstname'].' '.$val['lastname'];
            $option.='<option data-parent_id="'.$val['parent_id'].'" value="'.$val['id'].'">'.$name.'</option>';
        }
    }else {
        $option.='<option value="">No record Foud</option>';
    }
    echo $option;
    die;
}

