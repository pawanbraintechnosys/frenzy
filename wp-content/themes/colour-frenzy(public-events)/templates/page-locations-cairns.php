<?php
//Template Name: Locations cairns
get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => 'hero-section--small hero-section--mob-space-between'));

get_template_part('template-parts/locations-cairns-inner');

get_footer();
?>