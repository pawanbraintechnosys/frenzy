<?php
$faqBeforeTitle = get_field('faq__before-title');
$faqBeforeText = get_field('faq__before-text');
$faqItems = 'faq__items';
$faqAfterTitle = get_field('faq__after-title');
$faqAfterText = get_field('faq__after-text');
?>
<?php if(have_rows($faqItems)): ?>
    <section class="faq-section">
        <div class="container">
            <div class="faq-section__head">
                <h2>
                    <?= $faqBeforeTitle; ?>
                </h2>
                <?= $faqBeforeText; ?>
            </div>
            <div class="faq-section__accordion accordion">
                <?php while(have_rows($faqItems)): the_row();
                    $itemTitle = get_sub_field('title');
                    $itemInnerText = get_sub_field('inner_text');
                    ?>
                    <div class="accordion__item accordion-item " data-dropdown-status="close" data-working-range="0 5000">
                        <div class="accordion-item__head" data-dropdown-head>
                            <span>
                                <?= $itemTitle ?>
                            </span>
                        </div>
                        <div class="accordion-item__body" data-dropdown-body>
                            <?= $itemInnerText; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="faq-section__description">
                <h3>
                    <?= $faqAfterTitle; ?>
                </h3>
                <?= $faqAfterText; ?>
            </div>
        </div>
    </section>
<?php endif; ?>