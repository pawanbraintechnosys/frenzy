<?php

//Template Name: School fun prizes

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center'
));

get_template_part('template-parts/three-col-imgs', null, array(
    'section-classes' => 'three-col-imgs--head-yellow three-col-imgs--violet'
));

get_template_part('template-parts/one-row-slider', null, array(
    'section-classes' => 'one-row-slider--violet',
));

get_footer();
