<?php
/* Template Name: Common Template */
$ID = get_the_ID();
if($ID == 7636 || $ID == 7437){
    get_header('new');
    //get_header();
}else{
    get_header();
}
get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));
?>
    <link href="<?php echo get_template_directory_uri() ?>/assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/assets/css/developer.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/assets/css/private.css" rel="stylesheet">    <link href="<?php echo get_template_directory_uri() ?>/assets/css/media.css" rel="stylesheet">

<?php
if (have_posts()):
    wp_reset_query();
    while (have_posts()) : the_post();
        the_content();
    endwhile;
    wp_reset_query();
endif;
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<?php
get_footer();
