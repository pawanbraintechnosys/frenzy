<?php
/* color Fenzy */
require_once ('include/setup_theme.php');
require_once ('include/enqueue_scripts.php');
require_once ('include/acf.php');
require_once ('include/custom_post_type.php');
require_once ('include/load-more-event.php');
require_once ('include/location-ajax.php');

require ('template-parts/items/custom-comments.php');
