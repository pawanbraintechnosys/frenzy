<?php


$tableTitle = get_field('table__title');
$tableEventsTop1 = get_field('table_events_top_info_1');
$tableEventsTop2 = get_field('table_events_top_info_2');
$tableEventsTop3 = get_field('table_events_top_info_3');
$tableEventsTop4 = get_field('table_events_top_info_4');
$tableEventsBot1 = get_field('table_events_bottom_info_1');
$tableEventsBot2 = get_field('table_events_bottom_info_2');
$tableEventsBot3 = get_field('table_events_bottom_info_3');
$tableEventsBot4 = get_field('table_events_bottom_info_4');
$tableEventsInfoItems = 'table__events-info';
?>
<section class="table-section">
    <div class="container">
        <div class="table-section__head">
            <div class="table-section__head-img">
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/pows.png" alt="image">
            </div>
            <h2>
                <?= $tableTitle; ?>
            </h2>
        </div>
        <div class="table-section__table table">
            <div class="table__row">
                <div class="table__item">

                </div>
                <div class="table__price">
                    <?= $tableEventsTop1; ?>
                </div>
                <div class="table__price">
                    <?= $tableEventsTop2; ?>
                </div>
                <div class="table__price">
                    <?= $tableEventsTop3; ?>
                </div>
                <div class="table__price">
                    <?= $tableEventsTop4; ?>
                </div>
            </div>
            <?php
            $templateDirectoryUri = get_template_directory_uri();
            while(have_rows($tableEventsInfoItems)): the_row();
                $tableEventQuestion = get_sub_field('question');
                $tableEventPlusMinusCond = get_sub_field('change_plusminus_for_items');
                $tableEvent1 = get_sub_field('event_1');
                $tableEvent2 = get_sub_field('event_2');
                $tableEvent3 = get_sub_field('event_3');
                $tableEvent4 = get_sub_field('event_4');
                $tableEventText1 = get_sub_field('text_event_1');
                $tableEventText2 = get_sub_field('text_event_2');
                $tableEventText3 = get_sub_field('text_event_3');
                $tableEventText4 = get_sub_field('text_event_4');
                ?>
                <div class="table__row">
                    <div class="table__item">
                        <?= $tableEventQuestion; ?>
                    </div>
                    <?php if($tableEventPlusMinusCond): ?>
                        <div class="table__info">
                            <?= $tableEventText1; ?>
                        </div>
                        <div class="table__info">
                            <?= $tableEventText2; ?>
                        </div>
                        <div class="table__info">
                            <?= $tableEventText3; ?>
                        </div>
                        <div class="table__info">
                            <?= $tableEventText4; ?>
                        </div>
                    <?php else: ?>
                        <div class="table__info">
                            <?= $tableEvent1 ? '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-check.svg" alt="check">' : '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-minus.svg" alt="check">'; ?>
                        </div>
                        <div class="table__info">
                            <?= $tableEvent2 ? '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-check.svg" alt="check">' : '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-minus.svg" alt="check">'; ?>
                        </div>
                        <div class="table__info">
                            <?= $tableEvent3 ? '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-check.svg" alt="check">' : '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-minus.svg" alt="check">'; ?>
                        </div>
                        <div class="table__info">
                            <?= $tableEvent4 ? '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-check.svg" alt="check">' : '<img src="' . $templateDirectoryUri . '/assets/img/icon/table-minus.svg" alt="check">'; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
            <div class="table__row ">
                <div class="table__item">
                    <span>(Subject to availablity)</span>
                </div>
            </div>
            <div class="table__row ">
                <div class="table__item">
                </div>
                <div class="table__value">
                    <?= $tableEventsBot1; ?>
                </div>
                <div class="table__value">
                    <?= $tableEventsBot2; ?>
                </div>
                <div class="table__value">
                    <?= $tableEventsBot3; ?>
                </div>
                <div class="table__value">
                    <?= $tableEventsBot4; ?>
                </div>
            </div>
        </div>
        <div class="table-section__img">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/pows-blue.png" alt="image">
        </div>
    </div>
</section>
