$(document).ready(function () {
    /*
    Card validation
     */
    var hatPrice=parseInt(COLORFENZY_OPTION.hatPrice);
    var clothPrice=parseInt(COLORFENZY_OPTION.clothPrice);
    $('.frm-step3 input[type=text]').on('keyup',function(){
        cardFormValidate();
    });
    var childClass=COLORFENZY_OPTION.childClass.map(function(val){
        '<option value="'+val.post_title+'">'+val.post_title+'<option>';
    });
    console.log(childClass);

    /**
     * Form Sign Up
     *
     * @package colorFenzy
     * @version 1.0.0
     */

    var colorFenzyLoginForm = {
        frm: null,
        init: function () {
            this.validate();
        },
        submit: function (form) {
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: $("#school_login").serialize(),
                success: function (data) {

                    $(".ajaxLoading").addClass("hide");
                    if (data.status == 'success') {
                        toastr.success(data.message);
                        setTimeout(() => {
                            window.location.href = data.url;
                        }, 1000);
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        },
        validate: function () {
            this.frm = $("#school_login").validate({
                errorClass: "error",
                onkeyup: false,
                onfocusout: false,
                rules: {
                    user_email: {
                        required: true,
                        minlength: 2
                    },
                    user_pass: {
                        required: true,
                        minlength: 5
                    },
                },
                messages: {
                    user_email: {
                        required: "Please fill the valid email Id",
                    },
                    user_pass: {
                        required: "Please provide a password",
                    },
                },
                submitHandler: function (from) {
                    colorFenzyLoginForm.submit(from);

                }
            });
        },
    };
    colorFenzyLoginForm.init();
    /**
     * Form Sign Up
     *
     * @package colorFenzy
     * @version 1.0.0
     */

    var colorFenzyParentLoginForm = {
        frm: null,
        init: function () {
            this.validate();
        },
        submit: function (form) {
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: $("#student_parent").serialize(),
                success: function (data) {
                    $(".ajaxLoading").addClass("hide");
                    if (data.status == 'success') {
                        toastr.success(data.message);
                        setTimeout(() => {
                            window.location.href = data.url;
                        }, 1000);
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        },
        validate: function () {
            this.frm = $("#student_parent").validate({
                errorClass: "error",
                onkeyup: false,
                onfocusout: false,
                rules: {
                    user_email: {
                        required: true,
                        minlength: 2
                    },
                    user_pass: {
                        required: true,
                        minlength: 5
                    },
                },
                messages: {
                    user_email: {
                        required: "Please fill the valid email Id",
                    },
                    user_pass: {
                        required: "Please provide a password",
                    },
                },
                submitHandler: function (from) {
                    colorFenzyParentLoginForm.submit(from);

                }
            });
        },
    };
    colorFenzyParentLoginForm.init();
    /**
     * Forget Password
     * @package colorFenzy
     * @version 1.0.0
     */

    var colorFenzyForgetPassword = {
        frm: null,
        init: function () {
            this.validate();
        },
        submit: function (form) {
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: $("#forget_pasword").serialize(),
                success: function (data) {
                    $(".ajaxLoading").addClass("hide");
                    if (data.status == 'success') {
                        toastr.success(data.message);
                        $("#forget_pasword")[0].reset();
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        },
        validate: function () {
            this.frm = $("#forget_pasword").validate({
                errorClass: "error",
                onkeyup: false,
                onfocusout: false,
                rules: {
                    user_email: {
                        required: true,
                    },

                },
                messages: {
                    user_email: {
                        required: "Please fill the valid email Id",
                    },
                },
                submitHandler: function (from) {
                    colorFenzyForgetPassword.submit(from);

                }
            });
        },
    };
    colorFenzyForgetPassword.init();


    /**
     * Reset Password
     * @package colorFenzy
     * @version 1.0.0
     */

    var colorFenzyResetPassword = {
        frm: null,
        init: function () {
            this.validate();
        },
        submit: function (form) {
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: $("#reset_password").serialize(),
                success: function (data) {
                    $(".ajaxLoading").addClass("hide");
                    if (data.status == 'success') {
                        toastr.success(data.message);
                        setTimeout(() => {
                            window.location.href = data.url;
                        }, 1000);
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        },
        validate: function () {
            this.frm = $("#reset_password").validate({
                errorClass: "error",
                onkeyup: false,
                onfocusout: false,
                rules: {
                    user_pass: {
                        required: true,
                    },
                    confirm_user_pass: {
                        minlength: 5,
                        equalTo: "#password"
                    }
                },
                messages: {
                    user_pass: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    confirm_user_pass: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long",
                        equalTo: "Please enter the same password as above"
                    }

                },
                submitHandler: function (from) {
                    colorFenzyResetPassword.submit(from);

                }
            });
        },
    };
    colorFenzyResetPassword.init();

    /**
     * create School Webpages
     * @package colorFenzy
     * @version 1.0.0
     */





    var colorFenzyCreateWebpage = {

        frm: null,
        init: function () {
            this.validate();
        },
        submit: function (form) {
            var formData = new FormData($('#create_website')[0]);
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function (data) {
                    $(".ajaxLoading").addClass("hide");
                    if (data.status == 'success') {
                        toastr.success(data.message);
                        setTimeout(() => {
                            window.location.href = data.url;
                        }, 1000);
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        },
        validate: function () {
            this.frm = $("#create_website").validate({
                errorClass: "error",
                onkeyup: false,
                onfocusout: false,
                rules: {
                    event_name:{required: true},

                    // school_logo: {required: true},
                    //hero_image: {required: true},
                    about_event: {required: true},
                    class_detail: {required: true},
                    school_state: {required: true},
                    funrising_agree: {required: true},
                },
                errorPlacement:function(error, element) {
                    if (element.attr("type") == "checkbox") {
                        $(element).next().after(error);
                    }else{
                        error.insertAfter($(element));
                    }
                },
                submitHandler: function (from) {
                    colorFenzyCreateWebpage.submit(from);

                }
            });
        },
    };
    colorFenzyCreateWebpage.init();


    /**
     *
     * School parent Registration
     * @package colorfenzy
     * @version 1.0.0
     */
    $(document).ready(function() {

        /* Additional Password rule check */
        $.validator.addMethod('mypassword', function(value, element) {
                return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
            },
            'Password must contain at least one number');

        $(document).on('click','.btnAddSchoolNext',function (e) {
            e.preventDefault();
            var current_step, next_step;
            var form = $("#addStudentForm");
            form.validate({
                //ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.form-group').addClass("has-error");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.form-group').removeClass("has-error");
                },
                rules: {
                    Name: "required",
                    user_password : {
                        minlength : 5,
                        mypassword: true
                    },
                    confm_user_pass : {
                        minlength : 5,
                        equalTo : '[name="user_password"]'
                    }
                },
                submitHandler:  function(form) {

                    if(cardFormValidate()) {
                        $(".ajaxLoading").removeClass("hide");
                        stripePay();
                        setTimeout(function (){
                            $(".ajaxLoading").removeClass("hide");
                            $.ajax({
                                type: 'POST',
                                url: COLORFENZY_OPTION.ajaxURL,
                                data: $('#addStudentForm').serialize(),
                                success: function (data) {
                                    $(".ajaxLoading").addClass("hide");
                                    if (data.status == 'success') {
                                        toastr.success(data.message);
                                        setTimeout(() => {
                                            window.location.href = data.url;
                                        }, 1000);
                                    } else {
                                        toastr.error(data.message);
                                    }
                                },
                            });

                        },2000);

                    }
                }
            });

            if (form.valid() === true) {
                /* Scroll to Top */


                /* Scroll to Top */
                if ($('.frm-step1').is(":visible")) {
                    current_step = 1;
                    next_step = 2;
                    current_fs = $('.frm-step1');
                    next_fs = $('.frm-step2');
                    current_nav = $('.item-step2');
                    prev_nav = $('.item-step1');
                    set_school_fee();
                } else if ($('.frm-step2').is(":visible")) {
                    preview_formData();
                    totalPrice();
                    current_step = 2;
                    next_step = 3;
                    current_fs = $('.frm-step2');
                    next_fs = $('.frm-step3');
                    current_nav = $('.item-step3');
                    prev_nav = $('.item-step2');
                } else if ($('.frm-step3').is(":visible")) {
                    current_step = 3;
                    next_step = 4;
                    current_nav = $('.item-step3');
                    prev_nav = $('.item-step2');
                }
                next_fs.show();
                current_fs.hide();
                $(".step_number").text(next_step);
                current_nav.addClass('active');
                prev_nav.removeClass('active');
            }
        });
    });
    function set_school_fee(){

    }
    function totalPrice(){
        var sum=0;

        $('.price').each(function(){
            var item_val=parseFloat($(this).data('total_price'));
            if(isNaN(item_val)){
                item_val=0;
            }
            sum+=item_val;
        });
        for(var m=0;m<=4;m++){
            var child_fee=COLORFENZY_OPTION.child_entry_fee
            var selctTor="#child-register-price-"+m;
            var SelectClass=$(selctTor)
            if(SelectClass.length>0){
                sum+=parseInt(child_fee);
            }

        }

        $('#total_price').val(sum.toFixed(2));
        $('#total_price_detail').text('Total : $'+ sum.toFixed(2));
        $('#sub_total_price').val(sum.toFixed(2));
        $('#sub_total_price_detail').text('Total : $'+ sum.toFixed(2));
        $('#sub_total_price_step3').val(sum.toFixed(2));
        $('#sub_total_price_detail_step3').text(' $'+ sum.toFixed(2));
    }
    function preview_formData(){
        let i=1;var arr = [];
        var firstName; var lastName;var childClass;var childsecondClasss;var nickName;var childSize;var hatSize;var cloth;var clothqty;var hatqty;var nickname_checkbox;
        for(i=1;i<=parseInt(COLORFENZY_OPTION.allowed_child);i++){
            firstName=$("#FirstName-"+i).val();
            lastName=$("#lastName-"+i).val();
            childClass=$("#childClass-"+i).find(":selected").val();
            childsecondClasss=$("#childsecondClasss-"+i).find(":selected").val();
            nickName=$("#nickName-"+i).val();
            //console.log(nickName);
            if(nickName){
                firstName = $("#nickName-"+i).val();
            }
            nickname_checkbox=$(".nickname_checkbox-"+i+":checked").val();
            childSize=$("#childSize-"+i).find(":selected").val();
            clothqty=$("#clothqty-"+i).val();
            hatqty=$("#hatqty-"+i).val();
            hatSize=$("#hatSize-"+i).find(":selected").val();
            if(firstName){
                arr.push({
                    ['FirstName_'+i]:firstName,
                    ['lastName_'+i]:lastName,
                    ['childClass_'+i]:childClass,
                    ['childsecondClasss_'+i]:childsecondClasss,
                    ['nickName_'+i]:nickName,
                    ['nickname_checkbox_'+i]:nickname_checkbox,
                    ['childSize_'+i]:childSize,
                    ['clothqty_'+i]:clothqty,
                    ['hatqty_'+i]:hatqty,
                    ['hatSize_'+i]:hatSize,
                });
            }

        }
        sessionStorage.setItem("childDetail",JSON.stringify(arr));
        show_childDetailPreview();
    }
    $(document).ready(function(){
        if(sessionStorage.getItem("childDetail")){

        }
    });
    function show_childDetailPreview(){
        var clothCheck;var hatcheck;var nicknamecheck;
        var ParentFirstName=$("#ParentFirstName").val();
        var ParentLastName=$("#ParentLastName").val();
        var parentName=ParentFirstName+' '+ParentLastName;
        $("#parentNamePreview").val(parentName);

        var ParentEmail=$("#ParentEmail").val();
        $("#ParentEmailPreview").val(ParentEmail);

        var parentPhone=$("#parentPhone").val();
        $("#ParentPhonePreview").val(parentPhone);

        var childDetail=JSON.parse(sessionStorage.getItem("childDetail"));
        var smallDiv;
        var j=0;
        childDetail.map(function(val,key){

            j++;
            if(val[`clothqty_${j}`] !=='0') {
                clothCheck=val[`childSize_${j}`];
            }else{
                clothCheck="No";
            }
            if(val[`hatqty_${j}`] !=='0') {
                hatcheck="Yes";
            }else{
                hatcheck="No";
            }
            if(val[`nickname_checkbox_${j}`] !=='0') {
                nicknamecheck="style='display:block'";
            }else{
                nicknamecheck="style='display:none'";
            }


            childDiv = ' <div class="row child-detail">\n' +
                '                <div class="col-lg-6">\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1 " class="fund_label">Child ' + j + ' Name</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="text" class="form-control"  readonly id="FirstName_-' + j + '" value="'+val[`FirstName_${j}`]+' '+val[`lastName_${j}`]+'">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-6">\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1" class="fund_label">Child ' + j + ' Class</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="text" class="form-control" readonly id="childClass_-' + j + '"  value="'+val[`childClass_${j}`]+'" aria-describedby="emailHelp">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-6">\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1" class="fund_label">Child ' + j + ' Class 2</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="text" class="form-control" readonly id="childsecondClasss_-' + j + '"  value="'+val[`childsecondClasss_${j}`]+'" aria-describedby="emailHelp">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-6">\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1" class="fund_label">Fundraising Title</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="text" class="form-control"  readonly name="fundraising_title-' + j + '"  value="'+val[`FirstName_${j}`]+' '+ (val[`lastName_${j}`].substr(0, 1))+' '+val[`childClass_${j}`]+''+val[`childsecondClasss_${j}`]+'" aria-describedby="emailHelp">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-6" '+nicknamecheck+'>\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1" class="fund_label">Child ' + j + ' Nickname</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="text" class="form-control"  readonly id="nickName_-' + j + '" value="'+val[`nickName_${j}`]+'" aria-describedby="emailHelp">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-6">\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1" class="fund_label">Child ' + j + ' T-shirt</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="hidden" class="form-control" id="clothqty-' + j + '" readonly id="lastName-1" value="'+val[`clothqty_${j}`]+'" aria-describedby="emailHelp">\n' +
                '                            <input type="text" class="form-control" id="clothqty-' + j + '" readonly id="lastName-1" value="'+clothCheck +'" aria-describedby="emailHelp">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-6">\n' +
                '                    <div class="row mb-5" style="align-items: center;">\n' +
                '                        <div class="col-lg-4">\n' +
                '                            <label for="exampleInputEmail1" class="fund_label">Child ' + j + ' Bucker Hat</label>\n' +
                '                        </div>\n' +
                '                        <div class="col-lg-8">\n' +
                '                            <input type="hidden" class="form-control" id="hatqty-' + j + '" readonly  value="'+val[`hatqty_${j}`]+'" aria-describedby="emailHelp">\n' +
                '                            <input type="text" class="form-control" id="hatdfqty-' + j + '" readonly  value="'+hatcheck+'" aria-describedby="emailHelp">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '           ';
            $('.child_preview').append(childDiv);
        });

    }
    $('.btnAddSchoolPrev').click(function () {
        var prev_step, next_step;
        if ($('.frm-step2').is(":visible")) {
            prev_step = 1;
            next_step = 2;
            current_fs = $('.frm-step2');
            next_fs = $('.frm-step1');
            current_nav = $('.item-step1');
            prev_nav = $('.item-step2');
        } else if ($('.frm-step3').is(":visible")) {
            $(".child-detail").remove();
            prev_step = 2;
            next_step = 3;
            current_fs = $('.frm-step3');
            next_fs = $('.frm-step2');
            current_nav = $('.item-step2');
            prev_nav = $('.item-step3');
        }
        next_fs.show();
        current_fs.hide();
        $(".step_number").text(prev_step);
        current_nav.addClass('active')
        prev_nav.removeClass('active')
    });
    /* set child */
    $(".total_child").on('change', function () {
        var total_child;
        var total = 4;
        var last;
        var hide;
        for (var j = 1; j <= total; j++) {
            $(".child_" + j).hide();
            $(".child_merchandise_" + j).hide();

        }
        total_child = $(this).find('option:selected').val();
        if (total_child) {
            for (var i = 1; i <= total_child; i++) {
                $(".child_" + i).show();
                $(".child_merchandise_" + i).show();
            }
        }
    });
    /** Add more child **/
    var child = 1;
    var j = 1;
    $(".add_more").on('click', function (e) {
        j++;
        $('<div class="child_' + j + '">\n' +'        <input type="hidden" id="child-register-price-' + j + '" value="'+COLORFENZY_OPTION.child_entry_fee+'">\n'+
            '<h3 class="heading">Enter your Childs Details for your Fundraising Page</h3>\n'+
            '        <div class="form-group group first">\n' +
            '            <div class="detail">\n' +
            '                <label for="exampleInputEmail1">Child ' + j + ' <small>(Required)</small></label>\n' +
            '            </div>\n' +
            '            <div class="row">\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <input type="text" class="first_name form-control netEmp" id="FirstName-' + j + '" name="FirstName-' + j + '">\n' +
            '                    <label for="exampleInputEmail1">First</label>\n' +
            '                </div>\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <input type="text" class="form-control last_name" name="lastName-' + j + '" id="lastName-' + j + '" aria-describedby="emailHelp" >\n' +
            '                    <label for="exampleInputEmail1">Last</label>\n' +
            '                </div>\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <div class="form-group group">\n' +
            '                        <label for="exampleInputEmail1">Child ' + j + ' Class</label>\n' +
            '                        <div class="">\n' +
            '                            <select  id="childClass-' + j + '" Placeholder="Junior or Senior" name="childClass-' + j + '"\n' +
            '                                    class="form-control netEmp">\n' +
            '                                <option value="">Choose</option>\n' +COLORFENZY_OPTION.childClass.map(function(val){
                return '<option value="'+val.post_title+'">'+val.post_title+'</option>';
            })+
            '                            </select>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <div class="form-group group">\n' +
            '                        <label for="exampleInputEmail1">Child ' + j + ' Class 2</label>\n' +
            '                        <div class="">\n' +
            '                            <select  id="childsecondClasss-' + j + '" Placeholder="Junior or Senior" name="childsecondClasss-' + j + '"\n' +
            '                                    class="form-control ">\n' +
            '                                <option value="">Choose</option>\n' +COLORFENZY_OPTION.childClass.map(function(val){
                return '<option value="'+val.post_title+'">'+val.post_title+'</option>';
            })+
            '                            </select>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="form-group group">\n' +
            '            <div class="row">\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <label for="exampleInputEmail1"  style="display: inline-flex;">Nickname Required?</label>\n'+
            '<div class="size-ch" style="background: #fff;"><a id="myBtn" href="javascript:void(0);">i</a></div>\n'+
            '                    <div class="form-check checkbox-flex nickname-check-'+j+'">\n' +
            '                         <input class="form-check-input nickname_checkbox-' + j + '" name="nickname_checkbox-'+j+'" type="radio" value="0" id="flexCheckDefault nickname_checkbox" checked>\n' +
            '                        <label class="form-check-label check-label" for="flexCheckDefault">\n' +
            '                            No\n' +
            '                        </label>\n' +
            '                        <input class="form-check-input  nickname_checkbox-' + j + '" name="nickname_checkbox-'+j+'" type="radio" value="1" id="flexCheckDefault nickname_checkbox" >\n' +
            '                        <label class="form-check-label check-label" for="flexCheckChecked">\n' +
            '                            Yes\n' +
            '                        </label>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div style="display: none;" class="col-6 col-lg-6 nickname_field-'+j+'">\n' +
            '                    <label for="exampleInputEmail1">Profile Nickname Only use when required for Media Release (Required)</label>\n' +
            '                    <input type="text" class="form-control"  id="nickName-'+j+'" name="nickName-'+j+'" aria-describedby="emailHelp">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>' +
            '<div class="child_merchandise_' + j + '">\n' +
            '        <div class="form-group group">\n' +'<h3 class="heading">Optional Extras to wear at your school\'s Colour Frenzy</h3>\n'+
            '            <div class="row">\n' +
            '                <div class="col-lg-4">\n' +
            '                    <div class="sizing">\n' +
            '                        <div class="count">\n' +
            '                            <p>Colour Frenzy TShirt <span data-type="CLOTH" data-price="0" data-total_price="0" class="d-block price">$0</span></p>\n' +
            '                                <p style="font-size: 10px;text-align: start;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>\n'+
            '                            <div class="size-chart">\n' +
            '                                <p>Size chart (Required)</p>\n' +
            '                                <div class="size-ch"><a id="sizechartBtn" href="javascript:void(0);">i</a></div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="col-lg-8 increase">\n' +
            '                    <div class="shirt">\n' +
            '                        <img class="wooco-img" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-12T172412.753.png">\n' +
            '                                <div class="size-ch"><a id="TshirtBtn" href="javascript:void(0);">i</a></div>\n' +
            '                    </div>\n' +
            '                    <div class="up-down">\n' +
            '<p>\n' +
            '                                    <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/12/minus.png" id="minus1" width="40" height="20" class="minus" />\n' +
            '                                    <input id="clothqty-' + j + '" type="text" readonly value="0" class="qty" name="cloth-' + j + '"/>\n' +
            '                                    <img id="add1" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/12/plus.png" width="40" height="20" class="add" />\n' +
            '                                </p>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="row">\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <label for="exampleInputEmail1">Child ' + j + '</label>\n' +
            '                    <div class="">\n' +
            '                        <select id="childSize-' + j + '" Placeholder="Junior or Senior" name="childSize-' + j + '"\n' +
            '                                class="form-control" >\n' +
            '                                <option value="">Choose</option>\n' +COLORFENZY_OPTION.clothSize.map(function(val){
                return '<option value="'+val.post_title+'">'+val.post_title+'</option>';
            })+
            '                        </select>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="form-group group">\n' +
            '            <div class="for-back-color">\n' +
            '                <div class="row ">\n' +
            '                    <div class="col-lg-4">\n' +
            '                        <div class="sizing">\n' +
            '                            <div class="count">\n' +
            '                                <p>Bucket Mat <span data-type="HAT" data-price="0" data-total_price="0" class="d-block price">$0</span></p>\n' +
            '                                <p style="font-size: 10px;text-align: start;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>\n'+
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <div class="col-lg-8 increase">\n' +
            '                        <div class="hat">\n' +
            '                            <img class="wooco-img" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/hat-cf.png">\n' +
            '                        </div>\n' +
            '<div class="size-ch popup">\n' +
            '<a id="hatBtn" href="javascript:void(0);">i</a>\n' +
            '</div>\n'+
            '                        <div class="up-down">\n' +'  <p>\n' +
            '                                    <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/12/minus.png" id="minus1" width="40" height="20" class="minus" />\n' +
            '                                    <input id="hatqty-' + j + '" type="text" value="0" class="qty" readonly name="hat-' + j + '"/>\n' +
            '                                    <img id="add1" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/12/plus.png" width="40" height="20" class="add" />\n' +
            '                                </p>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="row d-none">\n' +
            '                <div class="col-6 col-lg-6">\n' +
            '                    <label for="exampleInputEmail1">Child ' + j + '</label>\n' +
            '                    <div class="">\n' +
            '                        <select id="hatSize-' + j + '" Placeholder="Junior or Senior" name="hatSize-' + j + '"\n' +
            '                                class="form-control">\n' +
            '                            <option>Choose</option>\n' +
            '                            <option value="2">2</option>\n' +
            '                            <option value="3">3</option>\n' +
            '                            <option value="4">4</option>\n' +
            '                        </select>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +'<div class="liner"></div>\n'+
            ' </div>\n').insertAfter(".child_" + child);
        child++;
        if(child > 3){ 
        $("#button_disable").addClass("intro");
            //pointer-events: none;
        }
        totalPrice();
        
        $('.netEmp').each(function () {
            $(this).rules("add", {
                required: true
            });
        });

    });
    /*
     *  Nickname checkboz
     */

    for( var k=0;k <= 3;k++){
        let selecTor=".nickname_checkbox-"+k;
        $(document).on('change',selecTor, function (e) {
            const val = $(this).val() == 0 ? false : true;
            const el = $(this).parent().parent().siblings();
            if (val) {
                el.show();
            } else {
                el.hide();
            }
        });
    }

});
/** Update the qunatity **/
$(function() {
    var hatPrice=parseInt(COLORFENZY_OPTION.hatPrice);
    var clothPrice=parseInt(COLORFENZY_OPTION.clothPrice);
    $(document).on('click','.minus,.add', function() {
        var $qty = $(this).closest('p').find('.qty'),
            currentVal = parseInt($qty.val()),
            isAdd = $(this).hasClass('add');
            var tempVal=currentVal;
            if(!tempVal){
               if(isAdd){
                   tempVal= ++tempVal;
               }
            }else{
                if(!isAdd) {
                    tempVal = --tempVal;
                }
            }
        $qty.val(tempVal);
       
          //  return;
         //   !isNaN(currentVal) && (currentVal > 0) && $qty.val(isAdd ? ++currentVal : (currentVal >= 0 ? --currentVal : --currentVal));

        var qnty = parseInt($(this).closest('p').find('.qty').val());
        var Type;var price;
        Type=$(this).parent().parent().parent().parent().find('.price').data('type');
        if(Type=='HAT'){
            price=hatPrice;
        }else{
            price=clothPrice;
        }
        var total_price=price * qnty;
        $(this).parent().parent().parent().parent().find('.price').html('$ '+total_price);
        $(this).parent().parent().parent().parent().find('.price').data('total_price',total_price);
        for(var m=0;m<=4;m++){
           var childSeclector="#childSize-"+m;
           var childClassExists=$(this).parent().parent().parent().parent().siblings().find(childSeclector);
           if(childClassExists.length>0){
               if(qnty){
                   childClassExists.prop('required',true);
               }else{
                   childClassExists.prop('required',false);
               }
           }else{
               childClassExists.prop('required',false);
           }
        }



        /* Update total price */
        /*
       *  Set child price on DOM READY
       */
        var sum=0;
        for(var m=0;m<=4;m++){
            var child_fee=COLORFENZY_OPTION.child_entry_fee
            var selctTor="#child-register-price-"+m;
            var SelectClass=$(selctTor)
            if(SelectClass.length>0){
                sum+=parseInt(child_fee);
            }
           // sum+=parseInt(child_fee);
        }
        $('.price').each(function(){
            var item_val=parseFloat($(this).data('total_price'));
            sum+=item_val;
            $('#total_price').val(sum.toFixed(2));
            $('#total_price_detail').text('Total : $'+ sum.toFixed(2));
        });
    });


    var sum=0;
    var school_fee=parseInt(COLORFENZY_OPTION.child_entry_fee);
    sum+=school_fee;
    $('.price').each(function(){
        var item_val=parseFloat($(this).data('total_price'));
        var clothQty=parseInt($("#clothqty-1").closest('p').find('.qty').val());
        var hatQty=parseInt($("#hatqty-1").closest('p').find('.qty').val());
        if(isNaN(item_val)){
            item_val=0;
        }else{
            item_val=(clothQty*item_val)+ (hatQty*item_val);
        }
        sum+=item_val;
        $('#total_price').val(sum.toFixed(2));
        $('#total_price_detail').text('Total : $'+ sum.toFixed(2));
    });
});
$(document).ready(function(){
    $(".select-arrow").click(function(){
        $(this).parent().toggleClass("show");
    });
});

$(document).ready(function(){

    $(document).on('click','#myBtn',function(e){
        $("#myModal").show();
    });
    $(document).on('click','#sizechartBtn',function(e){
        $("#sizechart").show();
    });
    $(document).on('click','#TshirtBtn',function(e){
        $("#tshirt").show();
    });
    $(document).on('click','#hatBtn',function(e){
        $("#hatModal").show();
    });


    $("#tshirt .close").click(function(){ $("#tshirt").hide(); })
    $("#hatModal .close").click(function(){ $("#hatModal").hide(); })
    $("#myModal .close").click(function(){ $("#myModal").hide(); })
    /** SiZE chart **/
    $("#sizechart .close").click(function(){ $("#sizechart").hide(); })
    /** SiZE chart **/


});
function cardFormValidate(){
    var cardValid = 0;

    // Card number validation
    $('#card_number').validateCreditCard(function(result) {
        var cardType = (result.card_type == null)?'':result.card_type.name;

        if(cardType == 'Visa'){
            var backPosition = result.valid?'2px -163px, 260px -87px':'2px -163px, 260px -61px';
        }else if(cardType == 'MasterCard'){
            var backPosition = result.valid?'2px -247px, 260px -87px':'2px -247px, 260px -61px';
        }else if(cardType == 'Maestro'){
            var backPosition = result.valid?'2px -289px, 260px -87px':'2px -289px, 260px -61px';
        }else if(cardType == 'Discover'){
            var backPosition = result.valid?'2px -331px, 260px -87px':'2px -331px, 260px -61px';
        }else if(cardType == 'Amex'){
            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
        }else{
            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
        }
        $('#card_number').css("background-position", backPosition);
        if(result.valid){
            $("#card_type").val(cardType);
            $("#card_number").removeClass('required');
            cardValid = 1;

        }else{
            $("#card_type").val('');
            $("#card_number").addClass('required');
            cardValid = 0;
        }
    });

    // Card details validation
    var cardName = $("#name_on_card").val();
    var expMonth = $("#expiry_month").val();
    var expYear = $("#expiry_year").val();
    var cvv = $("#cvv").val();
    var regName = /^[a-z ,.'-]+$/i;
    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
    var regYear = /^2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
    var regCVV = /^[0-9]{3,3}$/;
    if(cardValid == 0){
        $("#card_number").addClass('required');
        $("#card_number").focus();
        return false;
    }else if(!regMonth.test(expMonth)){
        $("#card_number").removeClass('required');
        $("#expiry_month").addClass('required');
        $("#expiry_month").focus();
        return false;
    }else if(!regYear.test(expYear)){
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").addClass('required');
        $("#expiry_year").focus();
        return false;
    }else if(!regCVV.test(cvv)){
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").addClass('required');
        $("#cvv").focus();
        return false;
    }else if(!regName.test(cardName)){
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").addClass('required');
        $("#name_on_card").focus();
        return false;
    }else{
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").removeClass('required');
        $('#cardSubmitBtn').prop('disabled', false);
        return true;
    }
}

function stripePay() {
    Stripe.setPublishableKey('pk_test_51ILPxqHGcI1rewMhZKPkJ4IQfBr7ZsxzXnUeAHLhBNsCojuvNGUicpPyeMLJHax5v0mvltR5qLYIjy6oBqbvWZ6n00rAL0ETn5');
    Stripe.createToken({
        number:$('#card_number').val(),
        cvc:$('#cvv').val(),
        exp_month : $('#expiry_month').val(),
        exp_year : $('#expiry_year').val()
    }, stripeResponseHandler);
    return true;
}


function stripeResponseHandler(status, response) {
    if(response.error) {
        $('#message').html(response.error.message).show();
    } else {
        var stripeToken = response['id'];
        $('#addStudentForm').append("<input type='hidden' name='stripeToken' value='" + stripeToken + "' />");
    }
    return response;
}

/* Product page*/

$(function() {
    $('.quantityplus').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('field');
        var qty = $(this).closest('.cart_item').find('.quantity');
        var currentVal = parseInt(qty.val());
        if (!isNaN(currentVal)) {
            qty.val(currentVal + 1);
        } else {
            qty.val(0);
        }
        qty.trigger('change');
    });

    $(".quantityminus").click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('field');
        var qty = $(this).closest('.cart_item').find('.quantity');
        var currentVal = parseInt(qty.val());
        if (!isNaN(currentVal) && currentVal > 0) {
            qty.val(currentVal - 1);
        } else {
            qty.val(0);
        }
        qty.trigger('change');
    });

    $(".quantity").change(function() {
        var sum = 0;
        var total = 0;
        $('.actual_price').each(function () {
            var price = $(this);
            var count = price.closest('.cart_item').find('.quantity');
            sum = (price.html() * count.val());
            total = total + sum;
            price.closest('.cart_item').find('.item_price').html(sum + "₴");
        });
        $('.total_pric').html("<h3>Total: $ " + total + "</h3>");

    }).change();
});
