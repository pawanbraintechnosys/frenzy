<link href="<?php echo get_template_directory_uri() ?>/assets/css/private.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() ?>/assets/css/top-header.css" rel="stylesheet">
<div class="unauthorized" style="background: url('https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/become-sponser.jpeg')">
    <div class="four_zero_four_bg">
        <h1 class="text-center ">404</h1>
    </div>
    <div class="contant_box_404">
        <p>Direct Access Not Allowed.</p>
        <a href="<?php echo home_url(); ?>" class="link_404">Go to Home</a>
    </div>
</div>