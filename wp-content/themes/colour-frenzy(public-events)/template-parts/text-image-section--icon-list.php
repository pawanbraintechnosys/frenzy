<?php

$itemText = get_field('text-image__item-text');
$listTitle = get_field('text-image__list-title');
$listItems = 'text-image__list-items';
$imgBg = get_field('text-image__img-bg');
$img = get_field('text-image__img');
$descTitle = get_field('text-image__description-title');
$descBtn = get_field('text-image__description-button');
$descText = get_field('text-image__description-text');
?>
<section class="text-img-items">
    <div class="container">
        <div class="text-img-items__wrapper">
            <div class="text-img-items__item text-img-item text-img-item--img-large">
                <div class="text-img-item__img-wr">
                    <div class="text-img-item__img-bg">
                        <?= wp_get_attachment_image($imgBg['id'], 'full'); ?>
                    </div>
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($img['id'], 'full'); ?>
                    </div>
                </div>
                <div class="text-img-item__text">
                    <?= $itemText; ?>
                    <h2>
                        <?= $listTitle ?>
                    </h2>
                    <?php if(have_rows($listItems)): ?>
                        <ul class="text-img-item__mark-img-list mark-img-list">
                            <?php while(have_rows($listItems)): the_row();
                                $listItemIcon = get_sub_field('icon');
                                $listItemTitle = get_sub_field('title');
                                $listItemTitleColor = get_sub_field('title_color');
                                $listItemText = get_sub_field('text');
                                ?>
                                <li class="mark-img-list__item mark-img-list__item--<?= $listItemTitleColor ?>">
                                    <div class="mark-img-list__item-img">
                                        <?= wp_get_attachment_image($listItemIcon['id'], 'full'); ?>
                                    </div>
                                    <div class="mark-img-list__item-text">
                                        <div class="h3">
                                            <?= $listItemTitle; ?>
                                        </div>
                                        <?= $listItemText; ?>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
            <div class="text-img-items__description">
                <h3>
                    <?= $descTitle; ?>
                </h3>
                <a href="<?= $descBtn['url']; ?>"  target="<?= $descBtn['target']; ?>" class="btn">
                    <?= $descBtn['title']; ?>
                </a>
                <?= $descText; ?>
            </div>
        </div>
    </div>
</section>
