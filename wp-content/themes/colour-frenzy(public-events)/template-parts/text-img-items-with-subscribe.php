<?php
$items = 'subscribe__text-image-items';
$title = get_field('subscribe__title-form');
$hideForm = get_field('subscribe__hide-subscribe-form');
$counter = 0;
?>
<?php if(have_rows($items)): ?>
    <section class="text-img-items">
        <div class="container">
            <div class="text-img-items__wrapper">
                <?php while (have_rows($items)): the_row();
                    $reverse = get_sub_field('reverse');
                    $image = get_sub_field('image');
                    $text = get_sub_field('text');
                    $button = get_sub_field('button');
                    $showButtons = get_sub_field('show_buttons');
                    $buttons = 'buttons';
                ?>
                    <div class="text-img-items__item <?= $args['bg-title']? 'text-img-item--head-bg-violet' : ''; ?> text-img-item <?= $reverse? 'text-img-item--reverse' : ''; ?>">
                        <div class="text-img-item__img">
                            <?= wp_get_attachment_image($image['id'], 'full'); ?>
                        </div>
                        <div class="text-img-item__text">
                            <?= $text; ?>
                            <?php if(!$showButtons): ?>
                                <?php if($button): ?>
                                    <a href="<?= $button['url']; ?>"  class="btn btn--animation">
                                        <?= $button['title']; ?>
                                    </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="text-img-item__btns">
                                    <?php while (have_rows($buttons)): the_row();
                                        $button = get_sub_field('button');
                                        $counter++;
                                    ?>
                                        <a href="<?= $button['url']; ?>"  class="btn <?= $counter == 2? 'btn--transparent btn--border-pink' : ''; ?>">
                                            <?= $button['title']; ?>
                                        </a>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>

                <?php if(!$hideForm): ?>
                    <div class="text-img-items__single-input-form">
                        <h2><?= $title; ?></h2>
                        <?= do_shortcode('[sibwp_form id=1]'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>