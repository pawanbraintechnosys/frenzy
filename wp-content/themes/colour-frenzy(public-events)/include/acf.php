<?php


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));

}


add_filter('block_categories', function ($categories, $post) {
    $arr = array_merge(
        array(
            array(
                'slug' => 'blocks',
                'title' => 'BLocks',
            ),
        ),
        $categories
    );
    return $arr;
}, 10, 2);
if (function_exists('acf_register_block_type')) {
    add_action('acf/init', 'register_acf_block_types');
}
function register_acf_block_types() {

    //Blocks
    acf_register_block_type(array(
        'name' => 'hero-section',
        'title' => __('Hero section'),
        'render_template' => 'template-parts/hero-section.php',
        'category' => 'blocks',
        'icon' => 'admin-comments',
        'keywords' => array('Hero section'),
    ));

    acf_register_block_type(array(
        'name' => 'three-col-items-section',
        'title' => __('Three column items section'),
        'render_template' => 'template-parts/three-col-items-section.php',
        'category' => 'blocks',
        'icon' => 'admin-comments',
        'keywords' => array('Three column items section'),
    ));

    acf_register_block_type(array(
        'name' => 'text-img-items-section',
        'title' => __('Text image items section'),
        'render_template' => 'template-parts/text-img-items-section.php',
        'category' => 'blocks',
        'icon' => 'admin-comments',
        'keywords' => array('Text image items section'),
    ));

}


















