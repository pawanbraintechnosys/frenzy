<?php
//Template Name: Single Template
get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => 'hero-section--small hero-section--content-center'));

get_template_part('template-parts/single-template-inner');

get_footer();


?>