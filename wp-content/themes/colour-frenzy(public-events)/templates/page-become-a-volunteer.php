<?php

/* Template Name: Become a volunteer page */


get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-img-items-section', null,
    array(
            'section-classes' => ' text-img-items--green ',
            'item-classes' => ' text-img-item text-img-item--align-top ',
    )
);

get_template_part('template-parts/form-section', null,
    array(
        'form-title-classes' => ' form-section__head-bg-small head-bg head-bg--violet-small ',
    )
);

get_footer();
