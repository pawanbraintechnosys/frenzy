<?php

get_header();

$searchQuery = get_search_query();

get_template_part('template-parts/hero-section', null,
    array(
        'archive-blog' => true,
        'search-query' => $searchQuery,
        'archive-bg' => true
    ));

get_template_part('template-parts/archive-blog', null,
    array(
        'search-query' => $searchQuery
    ));


get_footer();