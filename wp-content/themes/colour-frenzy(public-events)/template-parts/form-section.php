<?php
$formTitle = get_field('form__title');
$formText = get_field('form__text');
$formShortcode = get_field('form__shortcode');
$formTextAfter = get_field('form__text-after');
$formBtnAfter = get_field('form__btn-after');
?>
<section class="form-section">
    <div class="container">
        <div class="form-section__wrapper">
            <?php if($formTitle): ?>
            <div class="<?= $args['form-title-classes'] ?>">
                <h2>
                    <?= $formTitle; ?>
                </h2>
            </div>
            <?php endif; ?>

            <?= $formText; ?>

            <?= do_shortcode($formShortcode); ?>

            <?php if($formTextAfter && $formBtnAfter): ?>
                <div class="form-section__description">
                <?php if($formTextAfter): ?>
                    <h3>
                        <?= $formTextAfter; ?>
                    </h3>
                <?php endif; ?>

                <?php if($formBtnAfter): ?>
                    <a href="<?= $formBtnAfter['url']; ?>"  target="<?= $formBtnAfter['target']; ?>" class="btn btn--blue">
                        <?= $formBtnAfter['title']; ?>
                    </a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
