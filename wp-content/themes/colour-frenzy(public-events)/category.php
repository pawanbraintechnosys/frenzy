<?php
get_header();
$categoryObject = get_queried_object();
$pageTitle = $categoryObject->name;
$curCatSlug = $categoryObject->slug;

get_template_part('template-parts/hero-section', null,
    array(
        'archive-blog' => true,
        'cat-title' => $pageTitle,
        'archive-bg' => true
    ));

get_template_part('template-parts/archive-blog', null,
    array(
        'cat-slug' => $curCatSlug
    ));

get_footer();