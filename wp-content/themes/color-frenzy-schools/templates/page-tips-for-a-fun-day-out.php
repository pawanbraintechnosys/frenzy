<?php
/* Template Name: Tips for a fun day out page */

get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-img-items-section--tips');

get_template_part('template-parts/brush-bg-section');

get_footer();
