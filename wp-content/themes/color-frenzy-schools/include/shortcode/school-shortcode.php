<?php
/*
 * School Login Shortcode
 * And parent Login
 *
 */
function school_login_callback($atts)
{
    $atts = shortcode_atts(array(), $atts, 'school_login');
    if(!is_user_logged_in()):
        
    ?>
    <div class="school_login school-login-form">
	   <div class="container">
		  <div class="row myform">
			 <?php get_template_part('include/studentParent/student-login') ?>
			 <?php get_template_part('include/school/school-login') ?>
		  </div>
	   </div>
	</div>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif ?>
    <?php
}
add_shortcode('school_login', 'school_login_callback');


/*
 * Thank you
 * Package Colourfenzy
 * version 1.0
 */
function school_fundraising_thank_you($atts)
{
    $id=@$_GET['id'];
    if(!empty($id)):
        ?>
        <div class="school_login school-login-form">
            <div class="container">
                <div class="row thankyou">
                    <?php get_template_part('include/school/thank-you') ?>
                </div>
            </div>
        </div>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif ?>
    <?php
}
add_shortcode('thank_you', 'school_fundraising_thank_you');
?>
