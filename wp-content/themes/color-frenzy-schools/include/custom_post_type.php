<?php
 /* Custom post type for school*/
function create_schools_type(){
    register_post_type( 'schools', [
        'label'               => 'Schools',
        'labels'              => array(
            'name'          => 'Schools',
            'singular_name' => 'Schools',
            'menu_name'     => 'Schools',
            'all_items'     => 'All schools',
            'add_new'       => 'Add school',
            'add_new_item'  => 'Add new school',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit school',
            'new_item'      => 'New school',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
//        'rewrite'             => array( 'slug'=>'faq/%faqcat%', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
//        'has_archive'         => true,
        'query_var'           => true,
        'supports'            => array( 'title','editor','author', 'thumbnail'),
        'menu_icon'           => 'dashicons-admin-home'
    ] );
     /* Create size for Tshirt */
     register_post_type( 'tshirt-size', [
        'label'               => 'Tshirt Size',
        'labels'              => array(
            'name'          => 'Tshirt Size',
            'singular_name' => 'Tshirt Size',
            'menu_name'     => 'Tshirt Size',
            'all_items'     => 'All Tshirt Size',
            'add_new'       => 'Add Tshirt Size',
            'add_new_item'  => 'Add new Tshirt Size',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit Tshirt Size',
            'new_item'      => 'New Tshirt Size',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
        'query_var'           => true,
        'supports'            => array( 'title', 'thumbnail','author','editor','excerpt'),
        'menu_icon'           => 'dashicons-welcome-learn-more'
    ] );

    /* Create Student Class post type */
    register_post_type( 'child-class', [
        'label'               => 'Child Class',
        'labels'              => array(
            'name'          => 'Child Class',
            'singular_name' => 'Child Class',
            'menu_name'     => 'Child Class',
            'all_items'     => 'All Child Class',
            'add_new'       => 'Add Child Class',
            'add_new_item'  => 'Add new Child Class',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit Child Class',
            'new_item'      => 'New Child Class',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
        'query_var'           => true,
        'supports'            => array( 'title', 'thumbnail','author','editor','excerpt'),
        'menu_icon'           => 'dashicons-welcome-learn-more'
    ] );

    /*** Create Badges **/
    register_post_type( 'badges', [
        'label'               => 'Badges',
        'labels'              => array(
            'name'          => 'Badges',
            'singular_name' => 'Badges',
            'menu_name'     => 'Badges',
            'all_items'     => 'All Badges',
            'add_new'       => 'Add Badges',
            'add_new_item'  => 'Add new Badges',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit Badges',
            'new_item'      => 'New Badges',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
        'query_var'           => true,
        'supports'            => array( 'title', 'thumbnail','excerpt'),
        'menu_icon'           => 'dashicons-welcome-widgets-menus'
    ] );
}
add_action( 'init', 'create_schools_type' );


/* Custom post type for Products*/

function create_product(){
    register_post_type( 'productitem', [
        'label'               => 'Products',
        'labels'              => array(
            'name'          => 'Products',
            'singular_name' => 'Products',
            'menu_name'     => 'Product item',
            'all_items'     => 'All products',
            'add_new'       => 'Add products',
            'add_new_item'  => 'Add new products',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit products',
            'new_item'      => 'New products',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
//        'rewrite'             => array( 'slug'=>'faq/%faqcat%', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
//        'has_archive'         => true,
        'query_var'           => true,
        'supports'            => array( 'title','editor','author', 'thumbnail'),
        'menu_icon'           => 'dashicons-admin-home'
    ] );

   
}
add_action( 'init', 'create_product' );


/* Custom post type for Sales Products*/

function create_sales(){
    register_post_type( 'sales', [
        'label'               => 'sales',
        'labels'              => array(
            'name'          => 'Sales',
            'singular_name' => 'Sales',
            'menu_name'     => 'Sales',
            'all_items'     => 'All sales',
            'add_new'       => 'Add sales',
            'add_new_item'  => 'Add new sales',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit sales',
            'new_item'      => 'New sales',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
//        'rewrite'             => array( 'slug'=>'faq/%faqcat%', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
//        'has_archive'         => true,
        'query_var'           => true,
        'supports'            => array( 'title','editor','author', 'thumbnail'),
        'menu_icon'           => 'dashicons-admin-home'
    ] );

    
}
add_action( 'init', 'create_sales' );