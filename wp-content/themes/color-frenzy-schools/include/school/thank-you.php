<link href="<?php echo get_template_directory_uri() ?>/assets/css/top-header.css" rel="stylesheet">
<div style="margin: 0 auto;">
    <div class="four_zero_four_bg">
        <h1 class="text-center ">Thank You</h1>
    </div>
    <div class="contant_box_404">
        <p>Payment Successful.</p>
        <a href="<?php echo home_url(); ?>" class="link_404">Go to Home</a>
    </div>
</div>