<?php
$imgAndContent = 'collection-field__image-and-content';
$contentMain = get_field('collection-field__content');
$btnMain = get_field('collection-field__button');
$collectionBlock = 'collection-blocks';
?>
<section class="collection-section">
    <div class="container">
        <?php if(have_rows($imgAndContent)): ?>
            <?php while (have_rows($imgAndContent)): the_row();
                $image = get_sub_field('image');
                $content = get_sub_field('content');
                $btn = get_sub_field('button');
            ?>
                <div class="collection-section__text-img-item text-img-item text-img-item--reverse">
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($image['id'], 'full'); ?>
                    </div>
                    <div class="text-img-item__text">
                        <?= $content; ?>

                        <?php if($btn): ?>
                            <a href="<?= $btn['url']; ?>" class="btn">
                                <?= $btn['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>

        <?php if(have_rows($collectionBlock)): ?>
            <?php while (have_rows($collectionBlock)): the_row();
                $title = get_sub_field('title');
                $content = get_sub_field('content');
                $events = get_sub_field('events');
            ?>
                <?php if($events): ?>
                    <div class="collection-section__slider">
                        <div class="collection-section__slider-head">
                            <h2><?= $title; ?></h2>
                            <?= $content; ?>
                        </div>

                        <div class="event-slider">
                            <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

                            </button>

                            <div class="collection-section__events-wr location-section__events-wr swiper-container event-items-wr event-items-wr--green">
                                <div class="event-slider__wrapper swiper-wrapper">
                                    <?php foreach ($events as $post):
                                        setup_postdata($post);
                                        $title = get_the_title();
                                        $link = get_the_permalink();
                                        $date = strtotime(get_field('data-start-event'));
                                        $date = date("jS \of F Y", $date);
                                    ?>
                                        <div class="event-item swiper-slide">
                                            <h3><?= $title; ?></h3>
                                            <p><?= $date; ?></p>
                                            <a href="<?= $link; ?>#racepack-collection" class="btn btn--transparent btn--border-pink">
                                                Learn more
                                            </a>
                                        </div>
                                    <?php endforeach;
                                        wp_reset_query();
                                    ?>
                                </div>
                            </div>
                            <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>

                            <div class="event-slider__pagination slider-pagination"></div>
                        </div>

                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>

        <?php if($contentMain): ?>
            <div class="collection-section__description">
                <?= $contentMain; ?>
                <?php if($btnMain): ?>
                    <a href="<?= $btnMain['url']; ?>" class="btn">
                        <?= $btnMain['title']; ?>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</section>