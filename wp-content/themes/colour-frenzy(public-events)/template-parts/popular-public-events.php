<?php
$title = get_field('popular-events__title');
$events = get_field('popular-events__events');
$subtitle = get_field('popular-events__subtitle');
$items = 'popular-events__items';

$args = array(
    'post_type' => 'events',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);

$query = new WP_Query($args);
$postId = $query->posts[0]->ID;
$firstPostId = $query->posts[0]->ID;
$currentDate = strtotime(date('F j, Y')) * 1000;
$dateFirstPost = strtotime(get_field('data-start-event', $firstPostId)) * 1000;
$date = $dateFirstPost;
while ($query->have_posts($query)): $query->the_post();
    $postDate = strtotime(get_field('data-start-event')) * 1000;
    if($dateFirstPost > $postDate && $currentDate < $postDate){
        $date = $postDate;
        $postId = get_the_ID();
    }
endwhile;
$secondsDate = $date / 1000;
$postDate = date("l jS \of F", $secondsDate);
//echo $postId;

?>
<section class="slider-section brush-bg-section--prev-sec">
    <div class="container">
        <div class="slider-section__head">
            <h2><?= $title; ?></h2>
            <?php if($postId > 0): ?>
                <h3><?= get_the_title($postId); ?>, you're next! <?= $postDate; ?></h3>
            <?php endif; ?>
        </div>
        <div class="slider-section__body ">
            <?php if($events): ?>
                <div class="event-slider">
                    <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

                    </button>
                    <div class="slider-section__inner event-slider__container  swiper-container">
                        <div class="event-slider__wrapper swiper-wrapper">
                            <?php foreach ($events as $post): setup_postdata($post);
                                $title = get_the_title();
                                $dateS = strtotime(get_field('data-start-event'));
                                $date = date("jS \of F Y", $dateS);
                                $dateS = strtotime(get_field('data-start-event')) * 1000;
                                $currentDate = strtotime(date('F j, Y')) * 1000;
                            ?>
                                <div class="event-slider__item event-slider-item swiper-slide">
                                    <h3><?= $title; ?></h3>
                                    <div class="event-slider-item__date">
                                        <?php if($dateS && $dateS > $currentDate): ?>
                                            <?= $date; ?>
                                        <?php else: ?>
                                            Coming Soon
                                        <?php endif; ?>
                                    </div>
                                    <a href="<?= get_the_permalink(); ?>"  class="btn btn--small btn--location btn--animation">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/location.svg" alt="image">
                                        Learn More
                                    </a>
                                </div>
                            <?php endforeach;
                                wp_reset_query();
                            ?>
                        </div>
                    </div>
                    <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>
                    <div class="event-slider__pagination slider-pagination">

                    </div>
                </div>
            <?php endif; ?>

            <?php if(have_rows($items)): ?>
                <div class="text-items slider-section__text-items">
                    <?php while (have_rows($items)): the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                    ?>
                        <div class="text-items__item text-item">
                            <h3><?= $title; ?></h3>
                            <?= $description; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>