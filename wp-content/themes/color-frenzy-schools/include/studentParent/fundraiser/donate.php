<?php
    global $wpdb;
    $school=$args;
    $user_id=$school['data']['user_id'];
    $parent_id=$school['data']['parent_id'];
    $fundpage_id=$school['data']['fundraising_page_id'];
    $table_prefix='';
    $child_id=$school['data']['child_id'];
    $result=$wpdb->get_results("SELECT * FROM {$table_prefix}fundraise_posted_campiagn WHERE id={$fundpage_id}",ARRAY_A)[0];
    ?>
    <div class="donat user-donat">
        <div class="donat-step">
            <div class="stepss">
                <span class="active" id="student-main-step-1">1</span>
                <span id="student-main-step-2">2</span>
                <span id="student-main-step-3">3</span>
            </div>
        </div>
        <div class="donation_content">
            <form class="donate-form" id="donate-student-fund" method="post">
                <input type="hidden" name="action" value="donate_student_payment">
                <div class="donate_step-1" id="donate-student-step-1">
                    <p>Donate to<span id="product_name"><?php echo $result['title']; ?></span></p>
                    <div class="amt">
                        <a class="donate_amount_student price_donat"data-donate="10" href="javascript:void(0)">$10</a>
                        <a class="donate_amount_student price_donat" data-donate="25" href="javascript:void(0)">$25</a>
                        <a class="donate_amount_student price_donat" data-donate="50" href="javascript:void(0)">$50</a>
                        <a class="donate_amount_student price_donat" data-donate="100" href="javascript:void(0)">$100</a>
                        <a class="donate_amount_student price_donat" data-donate="200" href="javascript:void(0)">$200</a>
                    </div>
                    <div class="input">
                        <input type="hidden" class="dnt-student-amnt" name="donate_priced">
                        <div class="in">
                            <input type="number" class="form-control donate_student_other_amount" name="donate_price" placeholder="$ Other">
                        </div>
                    </div>
                    <div class="terms"><a href="#">Plus Bank Processing Fee of $0.30 & 3%</a><!--img
                                src="<?php //echo get_template_directory_uri() . '/assets/images/down-arrow.png'; ?>"-->
                    </div>
                    <div class="car">
                        <button  class="btn btn-warning btnDonateStudentNext" <?php //echo ($user_id == $result['user_id'])?'disabled':'' ?> data-current="step1" data-next="step2">Next</button>
                    </div>
                </div>
                <div class="donate_step-2 d-none show" id="donate-student-step-2">
                    <p>Your Details</p>
                    <div class="col-lg-12 ">
                        <div class="form-group profile_validation">
                            <label for="first_name">First Name</label>
                            <span class="astrict">*</span>
                            <input type="text" style="padding: 9px 0px 0px 9px;" class="form-control" id="first_name" name="first_name" required>
                        </div>
                        <div class="form-group profile_validation">
                            <label for="last_name">Last Name</label>
                            <span class="astrict">*</span>
                            <input type="text" style="padding: 9px 0px 0px 9px;" class="form-control" id="last_name" name="last_name">
                        </div>
                        <div class="form-group profile_validation">
                            <label for="email">Email</label>
                            <span class="astrict">*</span>
                            <input type="email" style="padding: 9px 0px 0px 9px;" class="form-control" id="email" name="email"  required>
                        </div>
                        <div class="form-group keep_checkbox">
                            <input type="checkbox" class="form-control keep_check" name="keep_safe" value="1" placeholder="Email" >
                            <label for="exampleInputEmail1" class="keep_safe">Keep my donation anonymous</label>
                        </div>
                    </div>
                    <div class="next-donate">
                        <a class="btnDonatePrev btn btn-primary" style='color: #2848e5;' id="btnstddonatePrev" href="javascript:void(0);" data-current="step2" data-prev="step1"><?php _e(" < ", "colorfenzy"); ?></a>
                        <button data-current="step2" data-next="step3" class=" btn btn-primary btnDonateStudentNext">Next <span class="naext"> > </span></button>
                    </div>
                </div>
                <div class="donate_step-3 d-none" id="donate-student-step-3">
                    <p>Your Payment Detail</p>
                    <div class="color-checkout">
                        <div class="paypal">
                            <div id="paypal-button-container"></div>
                        </div>
                        <div class="commonwealth">
                            <!--     <h3>Common Wealth Bank</h3>-->
                        </div>
                    </div>
                    <div class="final-donate">
                        <a class="btnDonatePrev btn btn-primary" id="btnstddonatePrev" href="javascript:void(0);" data-current="step3" data-prev="step2"><?php _e("<", "colorfenzy"); ?></a>
                        <input type="submit" disabled class="btn btn-primary final-payment-student" value="Donate $0.00"></input>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://www.paypal.com/sdk/js?client-id=<?php echo PAYPAL_CLIENT_ID; ?>&currency=USD"></script>
    <script>
        paypal.Buttons({
            createOrder: function(data, actions) {
                // This function sets up the details of the transaction, including the amount and line item details.
                return actions.order.create({
                    intent: "CAPTURE",
                    application_context: {
                        brand_name: '<?php echo APPLICATION_NAME; ?>',
                        user_action: "PAY_NOW",
                        shipping_preference: "NO_SHIPPING",
                        //return_url:"http://localhost/return.php",
                        cancel_url:'<?php echo home_url(); ?>/payment-cancelled',
                        payment_method: {
                            payer_selected: "PAYPAL",
                            payee_preferred: "IMMEDIATE_PAYMENT_REQUIRED"
                        }
                    },
                    payer: {
                        name: {
                            given_name:$("#first_name").val()+' '+$("#last_name").val(),
                        },
                        email_address: $("#email").val(),
                        phone: {
                            phone_number: {
                                national_number: "7485744854"
                            }
                        }
                    },
                    purchase_units: [{
                        invoice_id: "<?php echo rand(11111111,99999999); ?>",
                        amount: {
                            currency_code: 'USD',
                            value:  $(".dnt-student-amnt").val(),
                        },
                        item: [{
                            name:$("#product_name").text(),
                            description: $("#product_name").text(),
                            //sku: "sku01",
                            unit_amount: {
                                currency_code: "USD",
                                value: $(".dnt-student-amnt").val(),
                            },
                            quantity: "1",
                            // category: "Product Category"
                        }]
                    }],
                    description: "transaction Description"
                });
            },
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    console.log(details);
                    if(details.purchase_units[0].payments.captures[0].status) {
                        $.ajax({
                            type: "POST",
                            url: '<?php echo admin_url('admin-ajax.php') ?>/?action=donate_student_payment',
                            data: {
                                'paypal':details,
                                'first_name':$("#first_name").val(),
                                'last_name':$("#last_name").val(),
                                'email':$("#email").val(),
                                'user_id':"<?php echo @$user_id; ?>",
                                'fundpage_id':"<?php echo @$fundpage_id; ?>"
                            },
                            dataType: "json",
                            complete: function(response, textStatus) {
                                if(response.responseJSON.status=='success'){
                                    toastr.success(response.responseJSON.message);
                                    setTimeout(() => {
                                        window.location.href =response.responseJSON.url;
                                    }, 1000);
                                }else{
                                    toastr.error(response.responseJSON.message);
                                }
                            }

                        });

                    }
                }).then(function(res) {
                    //return res.json();
                }).then(function(captureData) {
                });
            }
        }).render('#paypal-button-container');

$(".price_donat").click(function(){
  $(".show").show();
  $("#donate-student-step-1").hide();
})
    </script>

