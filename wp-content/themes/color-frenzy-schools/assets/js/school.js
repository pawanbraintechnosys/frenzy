$(document).ready(function () {
    /**
     * School Fundraising Payment
     *
     * @package colorFenzy
     * @version 1.0.0
     */

    $(".donate_amount").click(function(e){
        $(".donate_amount").removeClass('active');
        let donate_price=$(this).data('donate');
        $(this).addClass('active');
        $(".donate_other_amount").val(donate_price);
        $(".dnt-amnt").val(donate_price);
        $(".final-payment").val("DONATE $"+donate_price);
    });
    $(".donate_other_amount").on('change',function(e){
        let other_amount=$(this).val();
        if ($(".donate_amount").hasClass("active")) {
            $(".donate_amount").removeClass("active")
        }
        $(".dnt-amnt").val(other_amount);
        $(".final-payment").val("DONATE $"+donate_price);
    });

    $(".btnDonateNext").click(function(e){
        e.preventDefault();
        var form = $("#donate-fund");
        form.validate({
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass("has-error");
            },
            rules: {
                donate_price:{
                    required: true,
                },
                first_name: {
                    required: true,
                },
                email: {
                    required: true,
                },
                 card_name: {
                    required: true,
                },

            },
            messages: {
                username: {
                    required: "Username required",
                },
            },
            submitHandler: function(form) {
                console.log('submitted');
                $(".ajaxLoading").removeClass("hide");
                $.ajax({
                    type: 'POST',
                    url: COLORFENZY_OPTION.ajaxURL,
                    data: $('#donate-fund').serialize(),
                    success: function (data) {
                        $(".ajaxLoading").addClass("hide");
                        console.log(data);
                        if (data.status == 'success') {
                        } else {

                        }
                    },
                });
            }
        });
        if (form.valid() === true){
            if ($('#donate-step-1').is(":visible")){
                current_fs = $('#donate-step-1');
                next_fs = $('#donate-step-2');

                /* step Number */
                current_step = $('#main-step-1').removeClass('active');
                next_step = $('#main-step-2').addClass('active');


            }else if($('#donate-step-2').is(":visible")){
                current_fs = $('#donate-step-2');
                next_fs = $('#donate-step-3');

                /* step Number */
                current_step = $('#main-step-2').removeClass('active');
                next_step = $('#main-step-3').addClass('active');

            }

            next_fs.show();
            current_fs.hide();


        }
    });

    $(document).on('click','#btndonatePrev',function(){
        console.log('sd');
        if($('#donate-step-2').is(":visible")){
            current_fs = $('#donate-step-2');
            next_fs = $('#donate-step-1');
            /* step Number */
            current_step = $('#main-step-2').removeClass('active');
            next_step = $('#main-step-1').addClass('active');

        }else if ($('#donate-step-3').is(":visible")){
            current_fs = $('#donate-step-3');
            next_fs = $('#donate-step-2');
            /* step Number */
           current_step = $('#main-step-3').removeClass('active');
            next_step = $('#main-step-2').addClass('active');
        }
        next_fs.show();
        current_fs.hide();


    });


});