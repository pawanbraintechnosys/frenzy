<div class="t-shirts steps-form d-none  frm-step frm-step3" id="t-shirts frmSchoolStep3" >
    <div class="tshirts-content">
        <h3>Confirm Details for your Fundraising Pages</h3>
    </div>
    <div class="child_merchandise_1">
        <div class="form-group group">
            <div class="row">
                <div class="col-lg-6">
                    <div class="row mb-5" style="align-items: center;">
                        <div class="col-lg-4">
                            <label for="exampleInputEmail1 " class="fund_label">Event Name</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="event-name" readonly id="eventNamePreview" value="<?php echo get_post_meta(base64_decode(@$_GET['school-id']),'campaign_name',true); ?>" aria-describedby="emailHelp">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row mb-5" style="align-items: center;">
                        <div class="col-lg-4">
                           <label for="exampleInputEmail1" class="fund_label">Parent Name</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="parent-name" readonly id="parentNamePreview" id="parentNamePreview" name="lastName-1" aria-describedby="emailHelp">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row mb-5" style="align-items: center;">
                        <div class="col-lg-4">
                             <label for="exampleInputEmail1" class="fund_label">Parent Email</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="parent-email" readonly id="ParentEmailPreview" aria-describedby="emailHelp">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row mb-5" style="align-items: center;">
                        <div class="col-lg-4">
                             <label for="exampleInputEmail1" class="fund_label">Parent Phone</label>
                        </div>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" readonly id="ParentPhonePreview" name="parent-phone"  aria-describedby="emailHelp">
                        </div>
                    </div>
                </div>
            </div>
            <div class="child_preview" id="">

            </div>
            <div class="pointer"></div>
            <div class="card_detail">
                <h3>Lets Start fundraising !</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                    , when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                    , when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                    , when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="form-check">
                    <label class="form-check-label check-label" for="flexCheckChecked">Terms Required</label>
                    <div class="terms-cond">
                        <input class="form-check-input" type="checkbox" value="1" name="term-check" id="flexCheckChecked" required>
                        <p>I accept color Fenzy T&C and give my child permission to join the fundraiser event.</p>
                    </div>
                </div>
                <div class="payment_detail">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-check">
                                <label class="form-check-label check-label" for="flexCheckChecked">Payment Method</label>
                                <span class="crd" style="display: inline-flex;">
                                    <input class="form-check-input" name="card-check" type="radio" value="1" id="flexCheckChecked"  checked >
                                      <p>Credit Card </p>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="credit-card-detail">
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckChecked" style="margin-bottom: 8px;">Credit Card</label>
                            <div class="vertical" style="border-radius: 10px;">
                                <input type="text" class="form-control" maxlength="20" id="card_number" name="card_number" aria-describedby="emailHelp" required>
                                <div class="card_cvv">
                                    <input type="text"  class="form-control" placeholder="MM" maxlength="2" id="expiry_month" name="expiry_month">
                                    <input type="text"  class="form-control" placeholder="YYYY" maxlength="4" id="expiry_year" name="expiry_year">
                                    <input type="text" class="form-control" placeholder="CVV" maxlength="3" id="cvv" name="cvv">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cardholder-detail">
                        <div class="form-check">
                            <label  class="form-check-label" >Card Details</label>
                            <input type="text" id="name_on_card" class="form-control" name="name_on_card">
                            <label  class="form-check-label" >Cardholder Name</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="total">
        <input type="hidden" name="total_price" id="sub_total_price" value="85" />
    </div>
    <div class="" style="float: right;">
        <div class="form-check tott">
            <label class="form-check-label" for="flexCheckChecked">Total </label>
            <div class="total" >
                <input type="hidden" name="total_sub_total" id="sub_total_price_step3" value="50.00">
                <label for="exampleInputPassword1" class="subtotal" id="sub_total_price_detail_step3">$50.00</label>
            </div>
        </div>
    </div>
    <div class="next">
        <a class="btn_border btn btn-primary float-left btnAddSchoolPrev" href="javascript:;" data-current="step3" data-prev="step2"><?php _e("back", "school"); ?></a>
        <input type="submit" class="btn btn-primary float-right step_3" value="submit"></input>
    </div>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</div>
