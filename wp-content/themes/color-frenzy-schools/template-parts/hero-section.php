<?php

$bg = get_field('hero__background-image');

$title = get_field('hero__title');
$ID = get_the_ID();
if($ID != 7618 && $ID != 7636  ){
?>

<section class="hero-section <?= @$args['section-classes']; ?> <?= @$args['hero-classes']; ?>">

    <div class="hero-section__bg">

        <?= wp_get_attachment_image($bg['id'], 'full') ?>

    </div>

    <div class="container">

        <div class="hero-section__content">

            <h1>

                <?= $title; ?>

            </h1>

            <?php if(@$args['is_homepage']): ?>

            <button class="hero-section__scroll-down scroll-down">

                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/arrow-down.svg" alt="image">

            </button>

            <div class="hero-section__pages-links pages-links school-active">

                <a href="/schools/"  class="pages-links__link active">

                    SCHOOLS

                </a>

                <a href="/"  class="pages-links__link">

                    PUBLIC

                </a>

                <div class="flap-bg"></div>

            </div>

            <?php endif; ?>

        </div>

    </div>

</section>

<?php } ?>