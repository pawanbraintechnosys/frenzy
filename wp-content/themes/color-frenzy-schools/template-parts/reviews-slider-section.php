<?php
$title = get_field('reviews-slider-section__title');
$items = 'reviews-slider-section__items';
?>
<section class="reviews-slider-section">
    <div class="container">
        <h2>
            <?= $title; ?>
        </h2>
        <div class="slider-wrapper event-slider">
            <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

            </button>

                <div class=" reviews-slider swiper-container">
                    <div class="reviews-slider__wrapper swiper-wrapper">
                        <?php while(have_rows($items)): the_row();
                        $itemPhoto = get_sub_field('author_photo');
                        $itemName = get_sub_field('author_name');
                        $itemPost = get_sub_field('author_post');
                        $itemText = get_sub_field('text');
                        ?>
                        <div class="reviews-slider__item reviews-slider-item swiper-slide">
                            <div class="reviews-slider-item__info">
                                <div class="reviews-slider-item__img">
                                    <?= wp_get_attachment_image($itemPhoto['id'], 'full') ?>
                                </div>
                                <div class="reviews-slider-item__text">
                                    <h3>
                                        <?= $itemName; ?>
                                    </h3>
                                    <i>
                                        <?= $itemPost; ?>
                                    </i>
                                </div>
                            </div>
                            <ul class="reviews-slider-item__mark">
                                <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                            </ul>
                            <?= $itemText; ?>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>

            <div class="slider-pagination reviews-slider__pagination"></div>

        </div>
    </div>
</section>
