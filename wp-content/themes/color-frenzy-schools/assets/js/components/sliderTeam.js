import Swiper, {Navigation} from 'swiper';

Swiper.use([Navigation]);

export function initSliderTeam() {
  const oneCol = new Swiper('.team-slider__container', {
    slidesPerView: "auto",
    navigation: {
      nextEl: '.team-slider__arrow-next',
      prevEl: '.team-slider__arrow-prev',
    },
    breakpoints: {
      320: {
        spaceBetween: 11
      },
      576: {
        spaceBetween: 30
      },
    }
  });

}
