<?php
/*
 * Child Detail Page
 * @ver:1.0
 */

function student_fundraising_detail_callback($atts)
{
    $child_id=base64_decode(@$_GET['child-id']);
    echo $child_id;
    if(isset($child_id)):
        $child=new Parent_model();
        $table="wp8t_child_details";
        $where=" WHERE id=".$child_id;
        $child=$child->get($table,$where);
        if(!empty($child['data'])):
            $child=$child['data'][0];
            $user=get_userdata($child['parent_id']);
            /* Fund Detail */
            $child_fundpage=get_child_fundraising_page($child['parent_id'],$child['firstname']);
            $donation= get_all_fund_donation(@$child_fundpage['data']['id']);
            //pr($donation);

            ?>
            <div class="main-dashboard child_detail">
                <?php
                if(is_user_logged_in() && (get_current_user_id()==$child['parent_id'])){
                    ?>
                <div class="user-dashboard">
                    <nav class="user-menu">
                        <?php
                      /*  $tabs = array(
                                'edit-fundpage' => __('Manage Fund Page', 'colourfrenzy'),
//                                'edit-profile' => __('Manage Profile', 'colourfrenzy'),
                                'fund-detail' => __('View Detail', 'colourfrenzy'),
                                'dashboard' => __('Dashboard', 'colourfrenzy'),
                        );*/
                        ?>
                        <ul class="user-item">
                        <?php
                            $current_url =home_url().'/student-fundraising-page/';
                            $flag = true;
                            foreach ($tabs as $tabKey => $tabVal):
                                $style = '';
                                if (!empty($_GET['dtab'])) {
                                    if ($tabKey == $_GET['dtab']) {
                                        $style = 'active';
                                        $flag = false;
                                    }
                                } elseif ($tabKey == 'fundpage') {
                                    if ($flag) {
                                        $style = 'active';
                                        $flag = false;
                                    }
                                }
                                $new_url = add_query_arg( array(
                                        'dtab' =>$tabKey,
                                        'child-id' =>@$_GET['child-id'],
                                ), $current_url );

                        ?>
                        <li class="item-tab"><a href="<?php echo $new_url; ?>" class="tab-<?php echo $tabKey; ?> <?php echo $style; ?>"><?php echo $tabVal; ?></a></li>
                        <?php endforeach;   ?>
                        </ul>
                    </nav>
                </div>
                <?php } ?>
                <?php
                if(is_user_logged_in() && (get_current_user_id()==$child['parent_id']) && (!empty($_GET['dtab']) && ($_GET['dtab'] !=='fund-detail') && ($_GET['dtab'] !=='dashboard') )) {
                    $dtab = (!empty($_GET['dtab'])) ? $_GET['dtab'] : '';
                    get_template_part("include/studentParent/dash", $dtab);
                }elseif ($_GET['dtab'] == 'dashboard'){
                    wp_safe_redirect(home_url().'/parent-dashboard');
                } else{ ?>
                <section class="funddash form-section for-mobile student_fund_set" style="margin-top:-0.8rem;margin-bottom: -40px;">
                    <!--h3 class="heading">Children registered</h3-->
                    <div class="child_register max_width_set">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="user-detail donte details_usr">
                                    <div class="user-n-ab">
                                        <div class="user-img">
                                            <img style="width: 50%;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/imageedit_7_3907638477.png">
                                        </div>
                                        <div class="name_abcd">
                                            <h3><?php echo $child['firstname'].' '.$child['lastname']; ?></h3>
                                            <h3><?php //$title = get_field('hero__title'); echo $title; ?></h3>
                                            <p style="width: 70%;"><?php echo $child_fundpage['data']['description']; ?></p>
                                        </div>
                                        <?php  
                                        $school_id=getChildSchoolId($child['parent_id']);
                                        $schoo_id_logo = $school_id['school_id'];
                                        //echo $schoo_id_logo;
                                        $string = get_school_webpage($schoo_id_logo)['data'][0] ['event_name'];
                                        $strings = strtolower(str_replace(' ', '-', $string));
                                        $logo = get_school_webpage($schoo_id_logo)['data']; 
                                        foreach($logo as $key=>$logos){
                                        }
                                        ?>  
                                        <div class="fund_money_section">
                                            <p class="fund_money">Fundraising Money For</p>
                                            <h3 class="school_name"><?php echo $logos['event_name'];?></h3>
                                        <div class="user-img school_logo">
                                        <!--img class="logo_img" src="<?php //echo get_the_post_thumbnail_url($schoo_id_logo);?>"-->
                                        <img class="logo_img" src="<?php echo get_field('school_logo',$schoo_id_logo);?>">
                                        <div class="main_heading">
                                            <p class="heading_pag">School Main Page</p>
                                            <a href="<?php echo home_url().'/school-fundraising-page/?event-name='.$strings.'&id='.base64_encode($schoo_id_logo); ?>" style="margin-top: 15px;" class="btn btn-warning go_to_page">Page</a>
                                        </div>
                                        </div>

                                        </div>

                                    </div> 
                                    
                                    <div class="goal_summary">
                                        <div class="fundraise_graph">
                                            <div class="fund">
                                                <span class="fund_left lft">Progress</span>
                                                <span class="fund_right cent">Funds Raised</span>
                                                <span class="fund_right rght">Goal</span>
                                            </div>
                                            <div class="school-light-grey">
                                                <div class="school-red" style="height:24px;max-width:<?php echo (round($donation['total_fund_raised']['total_donation'],0)*100)/(round($child_fundpage['data']['goal-amount'],0)); ?>%"></div>
                                            </div>
                                            <div class="fund">
                                                <span class="fund_left lft">$<?php echo round($donation['total_fund_raised']['total_donation'],0); ?></span>
                                                <span class="fund_right rght">$<?php echo $child_fundpage['data']['goal-amount']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fundrase_profile">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="classes">
                                                    <?php
                                                    $school_id=getChildSchoolId($child['parent_id']);
                                                    $schoolFundEndDate=get_field('incentive_end_date',$school_id['school_id']);
                                                    $currentDate=date('F j, Y');
                                                    $new_current_date = date('Y-m-d', strtotime($currentDate));
                                                    
                                                    $new_end_date = date('Y-m-d', strtotime($schoolFundEndDate));
                                                    //echo $new_end_date;

                                                    if($new_end_date>$new_current_date):
                                                        ?>
                                                        <h3>Fundrasing closes</h3>
                                                        <div class="d_h_m_s">
                                                            <div class="d"><span id="day" class="day"></span><p>Days</p></div>
                                                            <div class="d"><span id="hour" class="hour"></span><p>Hours</p></div>
                                                            <div class="d"><span id="minute" class="minute "></span><p>Minute</p></div>
                                                            <div class="d"><span id="second" class="second"></span><p>Second</p></div>
                                                        </div>
                                                    <?php else: ?>
                                                        <h3>Fundrasing Closed</h3>
                                                        <h3><?php echo $schoolFundEndDate; ?></h3>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="profile">
                                                    <h3>Share Profile</h3>
                                                    <div class="f_en_cop">
                                                        <?php
                                                        $socialData=array(
                                                                'link'=>home_url().'/student-fundraising-page/?child-id='.base64_encode($child['id']),
                                                                'title'=>$child['firstname'].' '.$child['lastname'],
                                                                'email'=>$user->data->user_email,
                                                        );
                                                        echo social_share($socialData); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prize">
                                    <div>
                                        <div class="st_prize">
                                            <h3>Star Prizes</h3>
                                            <p>Student are rewarded for there fundraising efforts with amazing prize!</p>
                                            <p>As each Student reaches new fundraising goals they unlock stars which earn them bigger prizes.</p>
                                            <p>Help them achieve their ultimate goal while helping our school fundraise by donating to a student now.</p>
                                        </div>
                                        <div class="st_star">
											<span class="timer">
                                            <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                            <img style="opacity: .7;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
                                        </span>
                                        </div>
                                        <?php //$star = $donation['total_fund_raised']['total_donation']; 
                                               // if(!empty($star)){
                                        ?>

                                        <!--div class="Stars" style="--rating:<?php //echo $star/100; ?>" aria-label="100">
                                        </div-->
                                    <?php //}else{ ?>
                                        <!--div class="Stars" style="--rating:0" aria-label="100">
                                        </div-->
                                    <?php //} ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 ttt">
                                <?php
                                //if($schoolFundEndDate>=$currentDate) {
                                    get_template_part('include/studentParent/fundraiser/donate', null, array(
                                        'data' => array(
                                                'child_id' => @$child_fundpage['data']['id'],
                                                'parent_id' =>$child['parent_id'],
                                                'fundraising_page_id' =>@$child_fundpage['data']['id'],
                                                'user_id' =>get_current_user_id(),
                                        ))
                                    );
                               // }
                                ?>
                                <div class="Badges_earnt mt-2">
                                    <div>
                                        <h3>Badges Earnt</h3>
                                    </div>
                                    <?php
                                    get_template_part('include/studentParent/fundraiser/child-badges', null, array(
                                        'data' => array(
                                                'total_fund_raised' =>$donation['total_fund_raised'],
                                                'number_of_badge' => 6,
                                        ))
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <section class="">
                            <div class="latest_leader donate_board">
                                <div class="container p0-0">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h3 class="heading">LATEST DONATION</h3>
                                            <?php

                                                if(!empty($donation['latest_donation'])):
                                                    foreach ($donation['latest_donation'] as $key=>$top_donation):
                                                        //pr($top_donation);
                                                        if($key==5){
                                                            break;
                                                        }
                                            ?>
                                            
                                            <div class="">
                                                <div class="card">
                                                    <div class="card-title">
                                                        <p>$<?php echo $top_donation['amount']  ?></p>
                                                    </div>
                                                    <div class="line"></div>
                                                    <div class="card-description">
                                                        <p><?php echo $top_donation['shipping_firstname'].' '.$top_donation['shipping_lastname']  ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                          
                <div class="">
                    <div class="card" style="padding-top: 50px;">
                        <div class="card-title">
                            <p></p>
                        </div>
                        <div class="card-description">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card" style="padding-top: 50px;">
                        <div class="card-title">
                            <p></p>
                        </div>
                        <div class="card-description">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card" style="padding-top: 50px;">
                        <div class="card-title">
                            <p></p>
                        </div>
                        <div class="card-description">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card" style="padding-top: 50px;">
                        <div class="card-title">
                            <p></p>
                        </div>
                        <div class="card-description">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card" style="padding-top: 50px;">
                        <div class="card-title">
                            <p></p>
                        </div>
                        <div class="card-description">
                            <p></p>
                        </div>
                    </div>
                </div>
                                            <?php endif; ?>

                                        </div>
                                        <div class="col-lg-6">
                                           <h3 class="heading">LEADER BOARD</h3>
                                            <?php
                                                $donation= get_all_fund_donation(@$child_fundpage['data']['id']);
                                                if(!empty($donation['latest_donation'])):
                                                    foreach ($donation['latest_donation'] as $key=>$top_donation):
                                                        if($key==5){
                                                            break;
                                                        }
                                            ?>
                                             
                                            <div class="leader">
                                                <div class="card">
                                                    <div class="card-title"><img src="<?php echo get_template_directory_uri().'/assets/images/user.png'; ?>"></div>
                                                    <div class="card-description progress-report">
                                                        <div class="name">
                                                            <p><?= $top_donation['shipping_firstname'].' '.$top_donation['shipping_lastname']  ?></p>
                                                            <div class="school-light-grey">
                                                                <div class="school-red" style="background-color: #f4e64c;max-width:<?php echo (round($top_donation['amount'],0)*100)/(round($child_fundpage['data']['goal-amount'])); ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>$<?= $top_donation['amount']  ?></p>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                              
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="child_register">
                        <div class="row">
                        </div>
                    </div>
                </section>
                <section class="the-big-prize" style="padding: 3% 0;">
                    <div  class="t-bg-prz_main">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="text-img-item__slider">
                                    <button class="slider-arrow slider-arrow--prev text-img-item__slider-arrow text-img-item__slider-arrow-prev">
                                    </button>
                                    <div class="text-img-item__slider-container swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                        <div class="text-img-item__wrapper swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(-249.667px, 0px, 0px);">
                                            <div class="swiper-slide text-img-item-slide" style="width: 220.667px; margin-right: 29px;">
                                                <div class="text-img-item-slide__img">
                                                    <img width="289" height="289" src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/06/img-2.png" data-src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/06/img-2.png" class="attachment-full size-full lazy loaded" alt="image" loading="lazy" data-srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/06/img-2.png 289w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/06/img-2-150x150.png 150w" data-sizes="(max-width: 289px) 100vw, 289px" sizes="(max-width: 289px) 100vw, 289px" srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/06/img-2.png 289w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/06/img-2-150x150.png 150w" data-was-processed="true">
                                                </div>
                                                <h4>Instax Camera </h4>
                                            </div>
                                            <div class="swiper-slide text-img-item-slide swiper-slide-prev" style="width: 220.667px; margin-right: 29px;">
                                                <div class="text-img-item-slide__img">
                                                    <img width="500" height="500" src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484.png" data-src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484.png" class="attachment-full size-full lazy loaded" alt="" loading="lazy" data-srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px" sizes="(max-width: 500px) 100vw, 500px" srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484-150x150.png 150w" data-was-processed="true">
                                                </div>
                                                <h4>Electric Scooter</h4>
                                            </div>
                                            <div class="swiper-slide text-img-item-slide swiper-slide-active" style="width: 220.667px; margin-right: 29px;">
                                                <div class="text-img-item-slide__img">
                                                    <img width="500" height="500" src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png" data-src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png" class="attachment-full size-full lazy loaded" alt="" loading="lazy" data-srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px" sizes="(max-width: 500px) 100vw, 500px" srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-150x150.png 150w" data-was-processed="true">
                                                </div>
                                                <h4>NRL Football</h4>
                                            </div>
                                            <div class="swiper-slide text-img-item-slide swiper-slide-next" style="width: 220.667px; margin-right: 29px;">
                                                <div class="text-img-item-slide__img">
                                                    <img width="500" height="500" src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668.png" data-src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668.png" class="attachment-full size-full lazy loaded" alt="" loading="lazy" data-srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px" sizes="(max-width: 500px) 100vw, 500px" srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668-150x150.png 150w" data-was-processed="true">
                                                </div>
                                                <h4>Rainbow Salon</h4>
                                            </div>
                                            <div class="swiper-slide text-img-item-slide" style="width: 220.667px; margin-right: 29px;">
                                                <div class="text-img-item-slide__img">
                                                    <img width="500" height="500" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20500%20500'%3E%3C/svg%3E" data-src="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616.png" class="attachment-full size-full lazy" alt="" loading="lazy" data-srcset="https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616.png 500w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616-300x300.png 300w, https://colourfrenzy.com.au/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px">
                                                </div>
                                                <h4>Ride On ATV 12v</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="slider-arrow slider-arrow--next text-img-item__slider-arrow text-img-item__slider-arrow-next">
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="big_pri">
                                    <h3 class="heading">More Stars the bigger the prize!</h3>
                                    <p>0 to 12 stars, the more a student raises the bigger the prize they earn! Its our way of thanking them for all their hard work.</p>
                                    <span>
									<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
									<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
									<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
									<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
									<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/CF_Star.png">
								</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="the-donat-login" style="padding: 2% 0;">
                    <div  class="t-bg-prz_main">
                        <div class="">
                            <h3 class="heading">Help by Donating</h3>
                            <div class="donatlogin">
                                <a href="#" class="btn btn-warning">Donate</a>
                                <!--a href="https://colourfrenzy.onlineprojectprogress.com/schools/school-login" class="btn btn-warning log_in">login</a-->
                            </div>
                        </div>
                    </div>
                </section>
                <?php
                $school_id=getChildSchoolId($child['parent_id']);
                $schoolFundEndDate=get_field('incentive_end_date',$school_id['school_id']);
                $res = date('M d, Y  h:i:s', strtotime($schoolFundEndDate));
                //echo $res;

                ?>
                <script>
                    function childDetailTimer(){
                        var x = setInterval(function() {
                            var countDownDate = new Date("<?php echo $res; ?>").getTime();
                            var now = new Date().getTime();
                            var distance = countDownDate - now;
                            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                            document.querySelector("#day").innerHTML = days;
                            document.querySelector("#hour").innerHTML = hours;
                            document.querySelector("#minute").innerHTML = minutes;
                            document.querySelector("#second").innerHTML = seconds;
                            /*if (distance < 0) {
                            document.querySelector("#day").innerHTML = '00';                            
                            document.querySelector("#hour").innerHTML = '00';                            
                            document.querySelector("#minute").innerHTML = '00';                            
                            document.querySelector("#second").innerHTML = '00'; 
                            clearInterval(x);
                            }*/
                        }, 1000);
                    }
                    childDetailTimer();
                </script>
                <?php } ?>
            </div>
        <?php else: ?>
            <?php echo get_template_part('template-parts/unauthorized'); ?>
        <?php endif; ?>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}
add_shortcode('student_fundraising_detail', 'student_fundraising_detail_callback');
?>