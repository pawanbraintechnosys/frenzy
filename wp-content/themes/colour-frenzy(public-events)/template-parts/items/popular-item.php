<?php
$title = get_the_title();
$date = get_the_date();
$image = get_the_post_thumbnail(null, array(300, 300));
$link = get_the_permalink();
?>
<a href="<?= $link; ?>"  class="popular-posts__item popular-post">
    <div class="popular-post__img">
        <?= $image; ?>
    </div>
    <div class="popular-post__text">
        <h5>
            <?= $title; ?>
        </h5>
        <div class="icon-text-info">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/calendar.svg" alt="calendar">
            <?= $date; ?>
        </div>
    </div>
</a>