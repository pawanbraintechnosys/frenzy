<?php
/*
 * School Dashboard
 * @ver:1.0
 */

function school_fundrising_callback($atts)
{
    $atts = shortcode_atts(array(), $atts, 'school_fundrising');
    $schoolID=base64_decode(@$_GET['id']);
    if (is_user_logged_in() && !empty($schoolID)):
        ?>
        <div class="school_login">
            <div class="container">
                <div class="row h-100vh justify-content-center align-items-center myform">
                    <h3 class="h2">Look like you're lost</h3>
                </div>
            </div>
        </div>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}
add_shortcode('school_fundrising', 'school_fundrising_callback');
?>