<?php
$moreInfoTitle = get_field('form-section__more-info-title');
$moreInfoItems = 'form-section__more-info-items';
$formTitle = get_field('form-section__form-title');
$formShortcode = get_field('form-section__form-shortcode');
?>
<section class="form-section">
    <div class="container">
        <div class="form-section__wrapper">
            <h2>
                <?= $moreInfoTitle; ?>
            </h2>
            <?php if(have_rows($moreInfoItems)): ?>
                <div class="form-section__link-items link-items link-items--whiteBg link-items--two-col ">
                    <?php while(have_rows($moreInfoItems)): the_row();
                    $itemImage = get_sub_field('image');
                    $itemTitle = get_sub_field('title');
                    $itemLink = get_sub_field('link');
                    ?>
                    <div class="link-items__item link-item">
                        <div class="link-item__img">
                            <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                        </div>
                        <h3>
                            <?= $itemTitle; ?>
                        </h3>
                        <?php if($itemLink): ?>
                            <a href="<?= $itemLink['url']; ?>"  target="<?= $itemLink['target']; ?>" class="btn btn--small btn--transparent btn--border-pink btn--animation">
                                <?= $itemLink['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
            <h3>
                <?= $formTitle; ?>
            </h3>
<!--            <div class=" main-form main-form--btn-center ">-->
                <?= do_shortcode($formShortcode); ?>
<!--            </div>-->
        </div>
    </div>
</section>
