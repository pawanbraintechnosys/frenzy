<?php

//Template Name: Fundraise and help

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center'
));

get_template_part('template-parts/text-img-items--fundrais');

get_footer();
