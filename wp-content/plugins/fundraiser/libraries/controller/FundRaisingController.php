<?php
/*
 * Fundraising Controller
 * Package @colorfenzy
 * Version @1.0.0
 */
class FundRaisingController{

    private $var = 'fundraise';
    public function __construct()
    {
        add_filter( 'get_fundraiser_instance', [ $this, 'get_instance' ] );
    }
    public function get_instance()
    {
        return $this; // return the object
    }
    public function create(){
        if(is_user_logged_in()):
            ?>
            <div class="school_login school-login-form">
                <div class="container">
                    <div class="row myform">
                        <div class="col-9 col-md-12 col-lg-9">
                            <div class="card">
                                <div class="card-body">
                                    <p class=""></p>
                                    <form class="form-post" id="create-fundraiser" method="POST" action="">
                                        <input type="hidden" name="action" value="create_fundraising_page">
                                        <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'create-fundraising-page' ); ?>">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Title</label>
                                            <input id="title" Placeholder="Fundraising Page title" name="title" type="text" class="form-control " required="" autocomplete="off" autofocus="">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="email">Description</label>
                                            <?php
                                                $content   = '';
                                                $editor_id = 'description';
                                                wp_editor( $content, $editor_id );
                                            ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="email">Start Date</label>
                                                    <input id="event_start_date datepicker" Placeholder="Fundraising Start Date" name="start-date" type="text" class="form-control datepicker" required="" autocomplete="off" autofocus="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="email">End Date</label>
                                                    <input id="event_end_date" Placeholder="Fundraising End Date" name="end-date" type="text" class="form-control " required="" autocomplete="off" autofocus="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="email">Minimum Amount</label>
                                                    <input id="min-amount" Placeholder="Enter Minimum Amount" name="min-amount" type="number" class="form-control " required="" autocomplete="off" autofocus="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="email">Maximun Amount</label>
                                                    <input id="max-amount" Placeholder="Enter Maximum Amount" name="max-amount" type="number" class="form-control " required="" autocomplete="off" autofocus="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="email">Fundraising Goal</label>
                                                    <input id="goal-amount" Placeholder="Enter Goal Amount" name="goal-amount" type="number" class="form-control " required="" autocomplete="off" autofocus="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="predefined-amount">Predefined Pledge Amount</label>
                                                    <input id="predefined-amount" Placeholder="Enter Predefined Amount" name="predefined-amount" type="text" class="form-control " required="" autocomplete="off" autofocus="">
                                                    <p style="font-size: 9px;line-height: 11px;font-style: italic;">Predefined amount allow you to place the amount in donate box by click, price should separated by comma (,), example: 10,20,30,40</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-block w-100" value="Submit"></input>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <link href="<?php echo get_template_directory_uri() ?>/assets/css/private.css" rel="stylesheet">
            <div class="unauthorized" style="background: url('https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/become-sponser.jpeg')">
                <div class="four_zero_four_bg">
                    <h1 class="text-center ">404</h1>
                </div>
                <div class="contant_box_404">
                    <p>Unathorized Access.</p>
                    <a href="<?php echo home_url(); ?>" class="link_404">Go to Home</a>
                </div>
            </div>
        <?php endif ?>
    <?php
    }
}
add_shortcode( 'create_fundraise_page', [ new FundRaisingController, 'create' ] );