<?php
$postLink = get_the_permalink();
$httpsUrl = preg_replace("/^http:/i", "https:", $postLink);
$postTitle = get_the_title();
$author_id = get_post_field( 'post_author', get_the_ID() );
$postAuthor = get_the_author_meta( 'display_name', $author_id );
$postDate = get_the_date();
$postImgUrl = get_the_post_thumbnail_url();
$popularPosts = get_field('blog__popular-posts', 65);
?>

<section class="single-post">
    <div class="container">
        <div class="single-post__wrapper">
            <div class="single-post__content post-content">
                <h1 class="title-post"><?= $postTitle; ?></h1>
                <div class="single-post__info">
                    <div class="single-post__icon-text-info icon-text-info">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/person.svg" alt="person">
                        Posted by <span><?= $postAuthor; ?></span>
                    </div>
                    <div class="icon-text-info">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/today.svg" alt="image">
                        <?= $postDate; ?>
                    </div>
                </div>

                <?php the_content(); ?>

                <div class="post-content__share">
                    <p>
                        Share this post
                    </p>
                    <ul>
                        <li>
                            <a href="#"
                               target="popup"
                               onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= $postLink; ?>&t=Test','popup','width=500,height=400'); return false;">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/post-facebook.svg" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#"
                               class="twitter-share-button"
                               target="popup"
                               onclick="window.open('https://twitter.com/intent/tweet?text=<?= $postTitle; ?>&url=<?= $postLink; ?>','popup','width=500,height=400'); return false;">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/post-twitter.svg" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#"
                               target="popup"
                               onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=<?= $httpsUrl; ?>&title=<?= $postTitle; ?>','popup','width=500,height=400'); return false;">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/post-linkedin.svg" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#"
                               target="popup"
                               onclick="window.open('//pinterest.com/pin/create/link/?url=<?= $postLink; ?>&media=<?= $postImgUrl; ?>&description=<?= $postTitle; ?>','popup','width=500,height=400'); return false;">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/post-pinterest.svg" alt="icon">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="post-content__nav">
                    <?php
                    wp_reset_query();
                    $currentID = get_the_ID();

                    $prevPostGen = get_previous_post();

                    !$prevPostGen ? $prevPost = get_boundary_post( false, '', false )[0] : $prevPost = get_previous_post() ;
                    $prevPostID = $prevPost->ID;
                    $prevPostTitle = get_the_title($prevPostID);
                    $prevPostLink = get_the_permalink($prevPostID);
                    $prevPostImg = get_the_post_thumbnail($prevPostID, array(175, 130));
                    ?>
                    <a href="<?= $prevPostLink; ?>"  class="post-content__prev-post nav-post nav-post--prev ">
                        <div class="nav-post__arrow">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/nav-post-arrow.svg" alt="arrow">
                        </div>
                        <div class="nav-post__wrapper ">
                            <div class="nav-post__img">
                                <?= $prevPostImg; ?>
                            </div>
                            <h3><?= $prevPostTitle; ?></h3>
                        </div>
                    </a>
                    <?php
                    $nextPostGen = get_next_post();
                    !$nextPostGen ? $nextPost = get_boundary_post()[0] : $nextPost = get_next_post() ;
                    $nextPostID = $nextPost->ID;
                    $nextPostTitle = get_the_title($nextPostID);
                    $nextPostLink = get_the_permalink($nextPostID);
                    $nextPostImg = get_the_post_thumbnail($nextPostID, array(175, 130));
                    ?>
                    <a href="<?= $nextPostLink; ?>" class="post-content__prev-post nav-post nav-post--next ">
                        <div class="nav-post__arrow">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/nav-post-arrow.svg" alt="arrow">
                        </div>
                        <div class="nav-post__wrapper ">
                            <div class="nav-post__img">
                                <?= $nextPostImg; ?>
                            </div>
                            <h3><?= $nextPostTitle; ?></h3>
                        </div>
                    </a>
                </div>
                <?php comments_template(); ?>
            </div>

            <div class="blog__popular-posts popular-posts">
                <?php if($popularPosts): ?>
                    <div class="blog__popular-posts-wr popular-posts__wrapper">
                        <h3>
                            Popular posts
                        </h3>

                        <?php
                        foreach ($popularPosts as $post): setup_postdata($post);
                            get_template_part('template-parts/items/popular-item');
                        endforeach;
                        ?>
                    </div>
                <?php endif; ?>
<!--                <a href="#" class="blog__link">-->
<!--                    <img src="--><?//= get_template_directory_uri(); ?><!--/assets/img/popular-post/link.png" alt="image">-->
<!--                </a>-->
            </div>
        </div>
    </div>

</section>