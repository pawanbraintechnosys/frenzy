<?php
//Template Name: Racepack Collection
get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => 'hero-section--small hero-section--content-center'));

get_template_part('template-parts/collection-field');

get_footer();
?>