<?php

$imagesTitle = get_field('images__title');
$imagesText = get_field('images__text');
$imagesItems = 'images__items';
$formTitle = get_field('form__title');
$formText = get_field('form__text');
$formShortcode = get_field('form__shortcode');
?>
<section class="three-col-imgs">
    <div class="container">
        <div class="three-col-imgs__head">
            <h2>
                <?= $imagesTitle; ?>
            </h2>
            <?= $imagesText; ?>
        </div>
        <div class="three-col-imgs__inner">
            <?php while(have_rows($imagesItems)): the_row();
                $itemImage = get_sub_field('image');
                ?>
                <div class="three-col-imgs__img">
                    <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                </div>
            <?php endwhile; ?>
        </div>
        <div class="three-col-imgs__form-wr">
            <h3>
                <?= $formTitle; ?>
            </h3>
            <?= $formText; ?>

            <?= do_shortcode($formShortcode); ?>
        </div>
    </div>
</section>
