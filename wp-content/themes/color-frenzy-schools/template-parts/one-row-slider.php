<?php
$sliderTitle = get_field('one-row-slider__title');
$items = 'one-row-slider__items';
$btnTitle = get_field('one-row-slider__btn-title');
$btn = get_field('one-row-slider__btn');
?>
<section class="one-row-slider <?= $args['section-classes'] ?>">
    <div class="container">
        <h2>
            <?= $sliderTitle; ?>
        </h2>
        <div class="one-row-slider__slider">
            <button class="slider-arrow slider-arrow--pink slider-arrow--prev one-row-slider__slider-arrow one-row-slider__slider-arrow-prev">

            </button>
            <div class="one-row-slider__slider-container swiper-container">

                <div class="swiper-wrapper">
                    <?php while(have_rows($items)): the_row();
                    $itemImage = get_sub_field('image');
                    $itemText = get_sub_field('text');
                    ?>
                    <div class="one-row-slider__item one-row-slider-item swiper-slide">
                        <div class="one-row-slider-item__img">
                            <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                        </div>
                        <h3>
                            <?= $itemText; ?>
                        </h3>
                    </div>
                    <?php endwhile; ?>
                </div>

            </div>
            <button class="slider-arrow slider-arrow--pink slider-arrow--next one-row-slider__slider-arrow one-row-slider__slider-arrow-next">

            </button>
        </div>
        <?php if($btnTitle || $btn): ?>
        <div class="one-row-slider__inner">
            <p>
                <?= $btnTitle; ?>
            </p>
            <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn">
                <?= $btn['title']; ?>
            </a>
        </div>
        <?php endif; ?>
    </div>
</section>
