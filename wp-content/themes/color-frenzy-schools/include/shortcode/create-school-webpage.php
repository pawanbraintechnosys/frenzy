<?php
/*
 * Student Parent Registration
 * @ver:1.0
 */

function create_school_webpage($atts)
{
    $atts = shortcode_atts(array(), $atts, 'school_fundrising');
    $schoolID = get_user_meta(get_current_user_id(), 'school_id', true);

    if (is_user_logged_in() && !empty($schoolID)):
        $fundraisingPageDetail = get_school_webpage($schoolID);
        if (!empty($fundraisingPageDetail['data'])) {
            $fundraisingPageDetail = $fundraisingPageDetail['data'][0];
            //pr($fundraisingPageDetail);
        }
        ?>
        <form class="form-post" method="POST" id="create_website" enctype="multipart/form-data">
            <section class="school_login form-section school_webpage m-form-section">
                <div class="container padding">
                    <div class="row">
                        <div class="heading">
                            <h1 class="school-heading"
                                style="text-align: center;"><?php echo strtoupper(post_detail($schoolID)['post_title']); ?></h1>
                            <p>Please complete the page below to create your fundraising page. You will need the following:</p>
                            <p>• Your school Logo in 200px x 200px size on transparent background.</p>
                            <p>• A hero image for the school main page 300px x 300px.</p>
                            <p>• Short write up about the school and what you are fundraising for (up to 100 words).</p>
                            <p>• Schools Class list or lists for the school (Can create two lists if needed, example 1 for yr level and one for tutor. If only need 1 leave Class 2 blank).</p>
                            <p>• RTO Number if needed and your Crios Code if needed. If not needed leave blank.</p>
                        </div>
                        <div class="school-page">
                            <input type="hidden" name="school_id" value="<?php echo $schoolID; ?>">
                            <input type="hidden" name="fundpage_id" value="<?php echo @$fundraisingPageDetail['id']; ?>">
                            <input type="hidden" name="action" value="create_school_webpage">
                            <input type="hidden" name="nounce"
                                   value="<?php echo wp_create_nonce('create_school_website'); ?>">
                            <div class="group m-group">
                                <label class="control-label school-label" for="email">Event Name</label>
                                <div class="file-require">
                                    <input id="text" Placeholder="Enter compaign Name" name="event_name" type="text"
                                           class="form-control"
                                           value="<?php echo @$fundraisingPageDetail['event_name']; ?>">
                                </div>
                            </div>

                            <div class="group m-group">
                                <label class="control-label school-label" for="email">Fundrasing Goal</label>

                                <div class="input-group mb-3" style="color: #fff;border: 1px solid #E4E4E4;display: flex;   border-radius: 10px;">
                              <span class="input-group-text" style="padding-top: 12px;padding-left: 6px;background: #018bb3;
                                    border-bottom-left-radius: 10px;border-top-left-radius: 10px;font-size: 15px;">$</span>
                              <input Placeholder="Enter number goal Eg $15000" style="border: none;padding-left: 3px;" type="number" required name="fundrasing-goal" class="form-control"  value="<?php echo @$fundraisingPageDetail['fundrasing_goal'];?>">
                            </div>

                            <!--div class="input-group">
                              <div class="input-group-area">
                                <input Placeholder="Enter number goal Eg $15000" type="text" required name="fundrasing-goal" class="form-control"  value="<?php //echo @$fundraisingPageDetail['fundrasing_goal'];?>"></div>
                            </div>
                            </div-->
                            <div class="group m-group">
                                <label for="exampleFormControlTextarea1" class="control-label school-label">About Your
                                    Event</label>
                                <div class="file-require">
                                        <textarea readonly class="form-control" name="about_event" id="" rows="6"  Placeholder="Enter write up about your compaign Upto 300 words"> <?php echo get_field('event_information',$schoolID,false);?></textarea>
                                </div>
                            </div>
                            <div class="form-group group Event-text-area m-group">
                                <div class="class-detail">
                                    <label for="exampleFormControlTextarea1" class="control-label school-label">Class 1
                                        Details</label>
                                </div>
                                <div class="file-require">
                                    <input type class="form-control" name="class_detail" id="exampleFormControlTextarea1"
                                    Placeholder="Enter each Class with comma between Eg 1A, 2A, 2B, etc" value="<?php echo @$fundraisingPageDetail['class_detail']; ?>">
                                </div>
                            </div>
                            <div class="form-group group Event-text-area m-group">
                                <div class="class-detail">
                                    <label for="exampleFormControlTextarea1" class="control-label school-label">Class 2
                                        Details</label>
                                </div>
                                <div class="file-require">
                                        <input type class="form-control" name="class_detail_2" id="exampleFormControlTextarea1"Placeholder="Enter each Class with comma between Eg 1A, 2A, 2B, etc"
                                                   value="<?php echo @$fundraisingPageDetail['class_detail_2']; ?>">
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-lg-6">
                                    <div class="group">
                                        <div class="upload_file">
                                            <label class="control-label school-label" for="email">School Logo</label>
                                        </div>
                                        <div class="school_logo_upload">
                                            <div class="school_logo">
                                                <img id="image_showing_logo" src="<?= $fundraisingPageDetail['school_logo']; ?>" width="120px" />
                                            </div>
                                            <div class="upload_file">
                                               <input type="file" name="school_logo" class="custom-file-input" id="school_logo_show">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="group">
                                        <div class="upload_file">
                                            <label class="control-label school-label" for="email">Hero Image</label>
                                        </div>
                                             <div class= "hero_image_upload ">
                                            <div class="school_logo">
                                                <img id="image_showing_hero" src="<?= $fundraisingPageDetail['hero_image'] ?>" width="120px" />
                                            </div>
                                            <div class="upload_file">
                                                <input type="file" class="custom-file-input" name="hero_image" id="hero_image_show">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 m-group">
                                    <div class="group">
                                        <label class="control-label school-label" for="email">RTO Number</label>
                                        <div class="">
                                            <input id="text" Placeholder="Enetr RTO (if applicable)" type="text"
                                                   class="form-control" name="rto_number"
                                                   value="<?php echo @$fundraisingPageDetail['rto_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 m-group">
                                    <div class="group">
                                        <label class="control-label school-label" for="email">Crios Code</label>
                                        <div class="file-require">
                                            <input id="text" Placeholder="if applicable" type="text"
                                                   class="form-control" name="crios_code"
                                                   value="<?php echo @$fundraisingPageDetail['crios_code']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 m-group">
                                    <div class="group">
                                        <label class="control-label school-label" for="email">School Street Address</label>
                                        <div class="">
                                            <input id="text" Placeholder="if applicable" type="text"
                                                   class="form-control" name="school_address"
                                                   value="<?php echo @$fundraisingPageDetail['school_address']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 m-group">
                                    <div class="form-group group">
                                        <label class="control-label school-label" for="email">School State</label>
                                        <div class="">
                                            <select id="text" Placeholder="Choose State" name="school_state"
                                                    class="form-control ">
                                                <option>Select the state</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Qld') ? 'selected' : ''; ?>value="Qld">Qld</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Nsw') ? 'selected' : ''; ?>value="Nsw">Nsw</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Vic') ? 'selected' : ''; ?>value="Vic">Vic</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Sa') ? 'selected' : ''; ?>value="Sa">Sa</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Nt') ? 'selected' : ''; ?>value="Nt">Nt</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Wa') ? 'selected' : ''; ?>value="Wa">Wa</option>
                                                <option <?php echo ($fundraisingPageDetail['school_state'] == 'Tas') ? 'selected' : ''; ?>value="Tas">Tas</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="school-page">
                <div class="update">
                    <div class="create-update">
                        <div class="form-check">
                            <input class="form-check-input" name="funrising_agree" type="checkbox">
                            <label class="form-check-label" for="flexCheckDefault">I agree to DIY
                                Fundrasings<span>Website term and conditions.</span></label>
                        </div>
                        <div class="form-group for-button">
                            <input style="margin-left: 27px;" type="submit" class="btn btn-warning" value="Create/Update"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
<script>
    school_logo_show.onchange = evt => {
        const [file] = school_logo_show.files
            if (file) {
             image_showing_logo.src = URL.createObjectURL(file)
            }
    }

    hero_image_show.onchange = evt => {
      const [file] = hero_image_show.files
      if (file) {
        image_showing_hero.src = URL.createObjectURL(file)
      }
    }
    </script>
     <?php
}
add_shortcode('create_school_webpage', 'create_school_webpage');
?>