<?php

$latest_donation=$args;

if (!empty($latest_donation['data']['latest_donation'])):

foreach ($latest_donation['data']['latest_donation'] as $key => $latest_donation):
    if ($key == 5) {
            break;
    }

?>

<div class="">

    <div class="card">

        <div class="card-title">

            <p>$<?= $latest_donation['amount']  ?></p>

        </div>

        <div class="line"></div>

        <div class="card-description">

            <p><?= $latest_donation['shipping_firstname'].' '.$latest_donation['shipping_lastname']  ?></p>

        </div>

    </div>

</div>

<?php endforeach; ?>

<?php else: ?>

    <div class="leader">
       <div class="card">
            <div class="Blankcard-description">
               <p style="font-size: 18px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
            </div>
        </div>
    </div>
    <div class="leader">
       <div class="card">
            <div class="Blankcard-description">
               <p style="font-size: 18px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
            </div>
        </div>
    </div>
    <div class="leader">
       <div class="card">
            <div class="Blankcard-description">
               <p style="font-size: 18px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
            </div>
        </div>
    </div>
    <div class="leader">
       <div class="card">
            <div class="Blankcard-description">
               <p style="font-size: 18px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
            </div>
        </div>
    </div>
    <div class="leader">
       <div class="card">
            <div class="Blankcard-description">
               <p style="font-size: 18px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
            </div>
        </div>
    </div>

<?php endif; ?>