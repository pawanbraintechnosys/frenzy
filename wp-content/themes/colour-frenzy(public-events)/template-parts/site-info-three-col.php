<?php
$infoBlocks = 'site-info__info-blocks';
$titleReviews = get_field('reviews__title');
$reviewsItems = 'reviews__items';
$twoColBlocks = 'site-info__two_columns_block';
?>
<section class="number-items brush-bg-section--second-sec">
    <div class="container">

        <div class="number-items__wrapper">
            <?php if(have_rows($infoBlocks)): ?>
                <div class="number-items__inner">
                    <?php while (have_rows($infoBlocks)): the_row();
                        $icon = get_sub_field('icon');
                        $number = get_sub_field('number');
                        $text = get_sub_field('text');
                    ?>
                        <div class="number-items__item number-item">
                            <div class="number-item__icon">
                                <?= wp_get_attachment_image($icon['id'], 'full'); ?>
                            </div>
                            <div class="number-item__number">
                                <?= $number; ?>
                            </div>
                            <div class="number-item__text">
                                <?= $text; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

<!--            <div class="number-items__link-items link-items link-items--whiteBg link-items--two-col ">-->
<!--                <div class="link-items__brush-bg">-->
<!--                    <img src="--><?//= get_template_directory_uri(); ?><!--/assets/img/content-img/brush-violet.png" alt="image">-->
<!--                </div>-->
<!--                --><?php //while (have_rows($twoColBlocks)): the_row();
//                    $image = get_sub_field('image');
//                    $title = get_sub_field('title');
//                    $button = get_sub_field('button');
//                ?>
<!--                    <div class="link-items__item link-item">-->
<!--                        <div class="link-item__img">-->
<!--                            --><?//= wp_get_attachment_image($image['id'], 'full'); ?>
<!--                        </div>-->
<!--                        <h3>-->
<!--                            --><?//= $title; ?>
<!--                        </h3>-->
<!--                        --><?php //if($button): ?>
<!--                            <a href="--><?//= $button['url']; ?><!--"  class="btn btn--small btn--transparent btn--border-pink btn--animation">-->
<!--                                --><?//= $button['title']; ?>
<!--                            </a>-->
<!--                        --><?php //endif; ?>
<!--                    </div>-->
<!--                --><?php //endwhile; ?>
<!--            </div>-->

            <?php if(have_rows($reviewsItems)): ?>
                <div class="text-img-items__slider-inner">
                    <h2><?= $titleReviews; ?></h2>
                    <div class="slider-wrapper event-slider">
                        <button class="slider-arrow slider-arrow--prev event-slider__arrow event-slider__prev">

                        </button>

                        <div class="text-img-items__slider reviews-slider swiper-container">
                            <div class="reviews-slider__wrapper swiper-wrapper">
                                <?php while (have_rows($reviewsItems)): the_row();
                                    $avatar = get_sub_field('avatar');
                                    $name = get_sub_field('name');
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    ?>
                                    <div class="reviews-slider__item reviews-slider-item swiper-slide">
                                        <div class="reviews-slider-item__info">
                                            <div class="reviews-slider-item__img">
                                                <?= wp_get_attachment_image($avatar['id'], 'full'); ?>
                                            </div>
                                            <div class="reviews-slider-item__text">
                                                <h3><?= $name; ?></h3>
                                                <i>
                                                    <?= $title; ?>
                                                </i>
                                            </div>
                                        </div>
                                        <ul class="reviews-slider-item__mark">
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                            <li><img src="<?= get_template_directory_uri(); ?>/assets/img/icon/star.svg" alt="image"></li>
                                        </ul>
                                        <p><?= $description; ?></p>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>

                        <button class="slider-arrow slider-arrow--next event-slider__arrow event-slider__next"></button>

                        <div class="slider-pagination reviews-slider__pagination"></div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>