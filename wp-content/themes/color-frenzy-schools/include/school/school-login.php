<?php
/*
 * School Login
 */
?>
<div class="col-lg-4 mt-4">
    <h2 class="">SCHOOL/CLUB <span>LOGIN</span></h2>
    <div class="card">
        <div class="card-body">
            <p>Sign in to your account.</p>
            <form class="form-post" id="school_login" method="POST" action="">
                <input type="hidden" name="action" value="school_login">
                <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'school_login' ); ?>">
                <div class="form-group group">
                    <label class="control-label" for="email">Email address</label>
                    <div class="">
                        <input id="email" Placeholder="Email Address" name="user_email" type="email" class="form-control " name="email" value="" required="" autocomplete="email" autofocus="">
                    </div>
                </div>
                <div class="form-group group">
                    <label class="control-label" for="password" >Password</label>
                    <div class="">
                        <input id="password1" Placeholder="Password" name="user_pass" type="password" class="form-control " name="password" required="" autocomplete="current-password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <a href="<?php echo home_url('/forget-password'); ?>" class="forgot-pass">Forgot your Password?</a>
                    </div>
                </div>
                <div class="form-group signin">
                    <input type="submit" class="btn btn-warning btn-lg btn-block w-100" value="Log in"></input>
                </div>
            </form>
        </div>
    </div>
</div>
