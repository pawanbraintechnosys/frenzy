<?php
    $ID = get_the_ID();
    $headerLogo = get_field('header__logo', 'option');
    $headerNav = 'new_header_navigation';
    $headerBtn = get_field('header__tickets-btn', 'option');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo get_template_directory_uri() ?>/assets/css/main.css" rel="stylesheet">

    <!--    <title>--><?php //wp_title(); ?><!--</title>-->
    <?php wp_head(); ?>
</head>
<body id="page-id-<?= $ID; ?>">
<?php if(!is_page_template('templates/template-single-without-header.php')): ?>
    <header id="header" class="header header--school-event header--violet" style="background-color: #000;">
        <div class="container">
            <div class="header__wrapper">
                <div class="header__fader">
                </div>
                <a href="/schools/"  class="header__logo">
                    <img src="<?= $headerLogo['url']; ?>" alt="logo">
                </a>
                <div class="header__inner">
                    <!--                <div class="header__btns">-->
                    <div class="header__pages-links pages-links school-active d-none">
                        <a href="/schools/"  class="pages-links__link active">
                            SCHOOLS
                        </a>
                        <a href="/"  class="pages-links__link">
                            PUBLIC
                        </a>
                        <div class="flap-bg"></div>
                    </div>

                     <?php    if($headerBtn): ?>
                        <a href="<?= $headerBtn['url']; ?>"  target="<?= $headerBtn['target']; ?>" class="btn btn--without-border">
                            <?= $headerBtn['title']; ?>
                        </a>
                    <?php endif; ?>
                    <!--                </div>-->
                    <nav class="header__menu menu">
                        <ul class="menu__list">
                            <?php while(have_rows($headerNav, 'option')): the_row();
                            $navLink = get_sub_field('links');
                            $navSubmenuCond = get_sub_field('submenus');
                            $submenuItems = 'sub_item_menu';
                            ?>
                            <?php if($navSubmenuCond): ?>
                                <li class="menu__list-item" data-dropdown-status="close" data-working-range="0 1024">
                                <div class="menu__list-item-dropdown" data-dropdown-head>
                                    <a href="<?= $navLink['url']; ?>"  target="<?= $navLink['target']; ?>" class="menu__list-item-link">
                                        <?= $navLink['title']; ?>
                                    </a>
                                    <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/shape.svg" alt="shape">
                                </div>
                                <ul class="menu__sub-list menu-sub-list" data-dropdown-body>
                                    <?php while(have_rows($submenuItems)): the_row();
                                    $submenuLink = get_sub_field('sub_menus_links');
                                    ?>
                                    <li class="menu-sub-list__item">
                                        <a href="<?= $submenuLink['url']; ?>"  target="<?= $submenuLink['target']; ?>" class="menu-sub-list__item-link">
                                            <?= $submenuLink['title']; ?>
                                        </a>
                                    </li>
                                    <?php endwhile; ?>
                                </ul>
                            </li>
                            <?php else: ?>
                                <li class="menu__list-item" >
                                <div class="menu__list-item-dropdown" >
                                    <a href="<?= $navLink['url']; ?>"  target="<?= $navLink['target']; ?>" class="menu__list-item-link">
                                        <?= $navLink['title']; ?>
                                    </a>
                                </div>
                            </li>
                            <?php endif; ?>
                            <?php endwhile; ?>
                        </ul>
                    </nav>

                    <div class="header__pages-links myaccount">
                        <?php //if(!is_user_logged_in()): ?>
                            <!--a href="/schools/find-your-school/"  class="school_btn">
                                SIGNUP
                            </a>
                            <a href="/schools/school-login"  class="school_btn" style="margin-left: 10px;">
                                LOGIN
                            </a-->
                        <?php //else: ?>
                            <a href="https://colourfrenzy.onlineprojectprogress.com/schools/find-your-school"  class="btn btn--form donate_1">
                                Donate
                            </a>
                        <?php //endif; ?>
                    </div>

                </div>

                <button class="header__burger">
                    <span>

                    </span>
                </button>
            </div>
        </div>
    </header>
<?php endif; ?>
<main class="<?= is_page_template('templates/template-single-without-header.php')? 'without-margin' : ''; ?>">
