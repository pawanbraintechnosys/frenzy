<?php
$videoBlock = 'course__two-columns-with-video';
$title = get_field('course__title-courses');
$args = array(
    'post_type' => 'events',
    'posts_per_page' => 10,
    'post_status' => 'publish'
);
$query = new WP_Query($args);
$maxPage = $query->max_num_pages;
?>
<section class="course-section">
    <div class="container">
        <?php if(have_rows($videoBlock)): ?>
            <?php while (have_rows($videoBlock)): the_row();
                $image = get_sub_field('image');
                $videoUrl = get_sub_field('video_url');
                $content = get_sub_field('content');
            ?>
                <div class="course-section__text-video-item text-video-item text-video-item--brush text-video-item--small">
                    <div class="text-video-item__wr">
                        <a href="#" data-video-src="<?= $videoUrl; ?>" class="text-video-item__img">
                            <?= wp_get_attachment_image($image['id'], 'full'); ?>
                            <button class="play-video">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                            </button>
                        </a>
                        <div class="text-video-item__bg">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/text-img-item/img-bg-1.png" alt="image">
                        </div>
                    </div>
                    <div class="text-video-item__text">
                        <?= $content; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>

        <?php if($query->have_posts()): ?>
            <div class="course-section__head-bg head-bg head-bg--green">
                <h2><?= $title; ?></h2>
            </div>

            <form id="search-location" class="course-section__single-input-form single-input-form">
                <input placeholder="Search Location" type="text">
                <button class="btn btn--form" type="submit">Search</button>
            </form>

            <div class="course-section__locations-items locations-items">
                <?php
                    while ($query->have_posts()): $query->the_post();
                        get_template_part('template-parts/items/location-item');
                    endwhile;
                ?>
            </div>
            <?php if($maxPage > 1): ?>
                <a href="#" id="load-more-location"  class="btn course-section__load-more">
                    load more
                </a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</section>