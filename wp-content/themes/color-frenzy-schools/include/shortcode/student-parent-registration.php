<?php
/*
 * Student Parent Registration
 * @ver:1.0
 */

function student_parent_registration($atts)
{
    $atts = shortcode_atts(array(), $atts, 'school_fundrising');
    $schoolID=base64_decode(@$_GET['school-id']);
    //echo $schoolID;
    $fundraisingPageDetail = get_school_webpage($schoolID);
    if (!is_user_logged_in()):
        ?>

       <div class="school_login">
			<div class="container padding">
				<div class="">
					<div class="Student_register">
						<div class="School_name">
							<div style="text-align: center;">
								<h3 class="h2"><?php echo $fundraisingPageDetail['data'][0]['event_name'] ; ?></h3>
							</div>
							<div class="Name_content">
								<p><?php echo get_field('event_information',$schoolID);?></p>
							</div>
						</div>
					</div>
					<div class="Student_parent_register_form">
						<div class="register_child_form_about">
							<div class="step">
								<h3 class="h2">Register Your Children</h3>
								<p>Three easy steps, Step 1 Parent or Guardian Details, Step2 Children<span>Details, Step 3 Extras and Fundrasing Profile.</span></p>
							</div>
							<div class="register_child_form">
								<p>Step <span class="step_number">1</span> of 3</p>
								<nav class="navbar">
									<div class="collapse">
										<ul class="navbar-nav Prnt_chld_t-sh">
											<li class="nav-item active item-step1 step-item">
												<img src="<?php echo get_template_directory_uri().'/assets/images/family-member.png'; ?>" alt="family-member">
												<a class="nav-link" href="#">Parent</a>
											</li>
											<li class="nav-item item-step2  step-item">
												<img src="<?php echo get_template_directory_uri().'/assets/images/kids-couple-1.png'; ?>" alt="kids-couple">
												<a class="nav-link" href="#">Children</a>
											</li>
											<li class="nav-item item-step3 step-item">
												<img src="<?php echo get_template_directory_uri().'/assets/images/summery(1).png'; ?>" alt="t-shirt">
												<a class="nav-link" href="#">Summary</a>
											</li>
										</ul>
									</div>
								</nav>
                                <form class="form-post tabbing add_parent_student" id="addStudentForm" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="action" value="addStudentParentField" />
                                    <?php wp_nonce_field('addstudent_action', 'addstudent_action'); ?>
                                    <input type="hidden" name="contributor_id" value="<?php echo get_current_user_id(); ?>" />
                                    <?php echo get_template_part('include/studentParent/register/step','1'); ?>
                                    <?php echo get_template_part('include/studentParent/register/step','2'); ?>
                                    <?php echo get_template_part('include/studentParent/register/step','3'); ?>
                                </form>
                                <div class="linked_about">
									<div>
										<div class="term">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											<a href="#">Event Terms & Conditions</a>
										</div>
										<div class="about_link">
<!--											<div class="money-back">-->
<!--												<span>-->
<!--													<p>Money back<span>Guarantee</span></p>-->
<!--													<img src="--><?php //echo get_template_directory_uri().'/assets/images/shield-1.png'; ?><!--" alt="money back">-->
<!--												</span>-->
<!--											</div>-->
											<div class="money-back2">
												<span>
                                                    <a href="<?php echo home_url(); ?>/faqs"><img style="margin: 0 auto;" src="<?php echo get_template_directory_uri().'/assets/images/faqs.png'; ?>" alt="Email">
													<p style="font-weight: 600;"> FAQ's Page</p></a>
												</span>
											</div>
<!--											<div class="money-back">-->
<!--												<span>-->
<!--													<p>Refund<span>Policy</span></p>-->
<!--													<img src="--><?php //echo get_template_directory_uri().'/assets/images/policy-1.png'; ?><!--" alt="policy">-->
<!--												</span>-->
<!--											</div>-->
										</div>
									</div>
								</div>
						   </div>
						</div>
					 </div>
				</div>
			</div>
		</div>

        <!-- Nickname -->
        <div id="myModal" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <p>Use if you require a Nickname for media concerns or easier reference.</p>
            </div>
        </div>
         <!-- SiZE CHART -->
        <div id="sizechart" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <div class="modal-body">
                    <h3 style="text-align: center;">Size chart</h3>
                    <img style="margin: 0 auto;" src="<?php echo home_url(); ?>/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-13T093322.594.png">
                </div>
            </div>
        </div>
        <!-- TSHirt -->
        <div id="tshirt" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <div class="modal-body">
                    <img style="margin: 0 auto;" src="<?php echo home_url(); ?>/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-12T172412.753.png">
                </div>
            </div>
        </div>
        <!-- TSHirt -->
        <div id="hatModal" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <div class="modal-body">
                    <img style="margin: 0 auto;" src="<?php echo home_url(); ?>/wp-content/uploads/sites/4/2021/07/hat-cf.png">
                </div>
            </div>
        </div>
    <?php else: ?>
        <?php wp_safe_redirect(home_url()); ?>
    <?php endif; ?> 
    <script>
    	 $(".btnAddSchoolNext").click( function() {
		   $(window).scrollTop(1050);
		 });
    </script>
    <?php
}
add_shortcode('student_registration', 'student_parent_registration');
?>