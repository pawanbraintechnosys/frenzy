<?php
$items = 'text-img-items__items';
$tipsItems = 'text-img-items__tips-items';
$btn = get_field('text-img-items__btn');
?>
<section class="text-img-items ">
    <div class="container">
        <?php while(have_rows($items)): the_row();
        $itemReverse = get_sub_field('reverse-cond');
        $itemTitle = get_sub_field('title');
        $itemText = get_sub_field('text');
        $itemImage = get_sub_field('image');
        ?>
        <div class="text-img-items__item text-img-item <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?>">
            <div class="text-img-item__img">
                <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
            </div>
            <div class="text-img-item__text">
                <h2>
                    <?= $itemTitle; ?>
                </h2>
                <?= $itemText; ?>
            </div>
        </div>
        <?php endwhile; ?>
        <div class="text-items text-items--colors-head slider-section__text-items">
            <?php while(have_rows($tipsItems)): the_row();
            $innerItemTitle = get_sub_field('title');
            $innerItemText = get_sub_field('text');
            ?>
            <div class="text-items__item text-item">
                <h3>
                    <?= $innerItemTitle; ?>
                </h3>
                <?= $innerItemText; ?>
            </div>
            <?php endwhile; ?>
        </div>
        <div class="text-img-items__btn">
            <?php if($btn): ?>
                <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn btn--transparent">
                    <?= $btn['title']; ?>
                </a>
            <?php endif; ?>
        </div>

    </div>
</section>
