<?php

/*

 * Student Parent Login

 * @package ColorFenzy

 * @ver 1.0

 */



add_action('wp_ajax_nopriv_studen_parent_login', 'studen_parent_login');

add_action('wp_ajax_studen_parent_login', 'studen_parent_login');

function studen_parent_login()

{

    global $wpdb;

    $html = '';

    $errors = new WP_Error();

    $response = array();

    $flag = false;

    $cred = array(

        'user_login' => $_POST['user_email'],

        'user_password' => $_POST['user_pass'],

        'remember' => true

    );

    $user = wp_signon($cred, false);

    if (is_wp_error($user)) {

        $errors->add("register_issues", __('Invalid details', 'fanclub'));

    } else {

        $user_id = $user->data->ID;

//        $school_id = get_user_meta($user_id, 'school_id', true);

//

//        $schoolExists=$wpdb->get_results("SELECT id from wp8t_school_details where school_id=$school_id",ARRAY_A);

//        if(!empty($schoolExists)){

//            $url = home_url("/school-dashboard/?id=") . base64_encode($school_id);

//        }else{

//            $url = home_url("/create-website-page/?id=") . base64_encode($school_id);

//        }



        $user_meta = get_userdata($user_id);

        $user_roles = $user_meta->roles;

        if (in_array('student_role', $user_roles)) {

            $flag = true;

            $html .= "Logged in successful.";

            $response['url'] = home_url() . '/parent-dashboard/';

        } else {

            $errors->add("register_issues", __('You are not authorized to login here.', 'fanclub'));

            wp_logout();

            $flag = false;

        }

    }

    if ($flag) {

        $response['status'] = "success";

    } else {

        $response['status'] = "failed";

    }

    $error_messages = $errors->get_error_messages();

    if (count($error_messages) > 0) {

        foreach ($error_messages as $k => $message) {

            $html .= $message;

        }

    }

    $response['message'] = $html;

    wp_send_json($response);

    die;

}



add_action('wp_ajax_nopriv_addStudentParentField', 'addStudentParentFieldRegistration');

add_action('wp_ajax_addStudentParentField', 'addStudentParentFieldRegistration');

function addStudentParentFieldRegistration()

{

    global $wpdb;

    $html = '';

    $errors = new WP_Error();

    $response = array();

    $flag = false;

    require_once 'stripe/init.php';

    $stripeSecret=STRIPE_SECRET;

    // See your keys here: https://dashboard.stripe.com/account/apikeys

    $stripe = new \Stripe\StripeClient($stripeSecret);



    /*

     * Create Stripe Payment

     */

    $token = $_POST['stripeToken'];

    $amount = round(@$_POST['total_sub_total'], 0);

    $amount=$amount*100;

    try{

        if(!email_exists($_POST['user_email'])) {

            $charge = $stripe->charges->create([

                'amount' => "$amount",

                'currency' => 'aud',

                'description' => ($_POST['event-name']) ? $_POST['event-name'] : 'No desc',

                'source' => $token,

            ]);

            if($charge->status=="succeeded"){

                if (isset($_POST)) {

                    $parentDetail = array(

                        'user_login' => @$_POST['user_email'],

                        'user_email' => @$_POST['user_email'],

                        'user_pass' =>  @$_POST['user_password'],

                    );

                    $user_id = wp_create_user( @$_POST['user_email'], @$_POST['user_password'], @$_POST['user_email']);

                    $user = new WP_User($user_id);

                    $user->set_role('student_role');

                    if($user_id){

                        wp_update_user( array ('ID' => $user_id, 'display_name' => @$_POST['parent-name']));

                        $parentData=array(

                            'phone'=>@$_POST['parent-phone'],

                            'event-name'=>@$_POST['event-name'],

                            'first_name'=>@$_POST['parent-firstName'],

                            'last_name'=>@$_POST['parent-lastName'],

                        );

                        foreach($parentData as $key=>$val){

                            update_user_meta($user_id,$key,$val);

                        }

                        /* Insert Table Data */

                        $parent_id = $user_id;

                        /* Insert Into Child Table */

                        $childDetail=[];
                        $fundriaisingDetails=[];
                        for($i=1;$i<=ALLOWED_CHILD;$i++){
                            if(isset($_POST['FirstName-'.$i]) && !empty($_POST['FirstName-'.$i])){
                                $childDetail[]=array(
                                    'parent_id'=>$parent_id,
                                    'school_id'=>($_POST['school_id'])?$_POST['school_id']:'0',
                                    'firstname'=>$_POST['FirstName-'.$i],
                                    'lastname'=>($_POST['lastName-'.$i])?$_POST['lastName-'.$i]:' ',
                                    'class'=>($_POST['childClass-'.$i])?$_POST['childClass-'.$i]:' ',
                                    'childClasss'=>($_POST['childsecondClasss-'.$i])?$_POST['childsecondClasss-'.$i]:' ',
                                    'nickname'=>($_POST['nickName-'.$i])?$_POST['nickName-'.$i]:' ',
                                    'cloth_number'=>($_POST['cloth-'.$i])?$_POST['cloth-'.$i]:' ',
                                    'cloth_size'=>($_POST['childSize-'.$i])?$_POST['childSize-'.$i]:' ',
                                    'hat_number'=>($_POST['hat-'.$i])?$_POST['hat-'.$i]:' ',
                                    'hat_size'=>($_POST['hatSize-'.$i])?$_POST['hatSize-'.$i]:' ',
                                    'event_name'=>($_POST['event-name'])?$_POST['event-name']:' ',
                                    'status'=>'1',
                                    'total_sub_total'=>$_POST['total_sub_total'],
                                    'created_date'=>current_time('mysql'),
                                    'modified_date'=>current_time('mysql'),
                                );
                                $fundriaisingDetails[]=array(
                                    'user_id'=>$parent_id,
                                    'title'=>($_POST['fundraising_title-'.$i])?$_POST['fundraising_title-'.$i]:'df',
                                    'description'=>'I want to help raise money for our school for new equipment and to help create a better future for our students.',
                                    'start-date'=>current_time('mysql'),
                                    'end-date'=>current_time('mysql'),
                                    'min-amount'=>'100',
                                    'max-amount'=>'100',
                                    'goal-amount'=>'500',
                                    'status'=>'1',
                                    'accepted_at'=>current_time('mysql'),
                                );

                            }



                        }

                        if(!empty($childDetail)){

                            foreach ($childDetail as $key=>$val){

                                $wpdb->insert('wp8t_child_details',$val);

                                $child_id=$wpdb->insert_id;

                            }

                        }

                        if(!empty($fundriaisingDetails)){

                            foreach ($fundriaisingDetails as $key=>$fundriaisingDetails){

                                $wpdb->insert('fundraise_posted_campiagn',$fundriaisingDetails);

                            }

                        }

                        /* Store Transaction */

                           $transaction=array(

                               'user_id'=>$parent_id,

                               'payment_id'=>$charge->id,

                               'amount'=>($charge->amount)/100,

                               'type'=>'STRIPE',

                               'status'=>$charge->status,

                               'created_at'=>current_time('mysql'),

                               'updated_at'=>current_time('mysql'),

                           );

                        save_transaction($transaction);

                        /*End Transaction */



                        /* Send welcome*/

                        ob_start();

                         send_welcome_email_parent($childDetail,$_POST['user_email']);

                        ob_end_clean();

                        /* * End Send welcome*/

                        /* Send Invoice */

                        ob_start();

                        send_invoice_parent($user_id,$charge,$_POST['user_email']);

                        ob_end_clean();

                        /* * End Invoice*/



                        $html .="Registration completed Successfully.";

                        $response['url'] = home_url().'/school-login';

                        $flag = true;

                    }

                    $flag=true;

                }

            }else{

                $flag=false;

                $html .="Unable to proceed the payment.";

            }

        }else {

            $flag=false;

            $html .="Email Already Exists";

        }



    }catch (Exception $e){

        $flag=false;

        $html .=$e->getMessage();

    }

    if ($flag) {

        $response['status'] = "success";

    } else {

        $response['status'] = "failed";

    }

    $error_messages = $errors->get_error_messages();

    if (count($error_messages) > 0) {

        foreach ($error_messages as $k => $message) {

            $html .= $message;

        }

    }

    $response['message'] = $html;

    wp_send_json($response);

    die;



}

function send_welcome_email_parent($childDetail,$to){

    get_template_part('include/email-template/welcome', null, $childDetail);



    $output = ob_get_contents();

    $emailData = array(

        'to' =>$to,

        'message' => $output,

        'subject' => 'Welcome to colourfrenzy.',

    );

    $mail = send_mail($emailData);

}

function send_invoice_parent($user_id,$charge,$to){

    $invoice=[];

    $invoice=array(

      'invoice'=>$charge,

      'user_id'=> $user_id

    );

    get_template_part('include/email-template/invoice', null, $invoice);

    $output = ob_get_contents();

    $emailData = array(

        'to' =>$to,

        'message' => $output,

        'subject' => 'Welcome to colourfrenzy.',

    );

    $mail = send_mail($emailData);

}

function save_transaction($data){

    global $wpdb;

    $table_name=TABLE_PREFIX_MAIN."payment_transaction";

    $wpdb->insert($table_name,$data);

    return array(

        'query'=>$wpdb->last_query,

        'data'=>$data

    );

}



/* Donate Studen Fundraising page */



add_action('wp_ajax_nopriv_donate_payment_student', 'donate_payment_for_student');

add_action('wp_ajax_donate_payment_student', 'donate_payment_for_student');

function donate_payment_for_student()

{

    

    $html = '';

    $errors = new WP_Error();

    $response = array();

    $flag = false;

    global $wpdb;

    $donateTransaction=array(

        'fundpage_id'=>($_POST['fundpage_id'])?$_POST['fundpage_id']:' ',

        'user_id'=>($_POST['user_id'])?$_POST['user_id']:' ',

        'child_id'=>($_POST['child_id'])?$_POST['child_id']:' ',

        'shipping_firstname'=>($_POST['first_name'])?$_POST['first_name']:' ',

        'shipping_lastname'=>($_POST['last_name'])?$_POST['last_name']:' ',

        'shipping_email'=>($_POST['email'])?$_POST['email']:' ',

        'amount'=>($_POST['paypal']['purchase_units'][0]['amount']['value'])?$_POST['paypal']['purchase_units'][0]['amount']['value']:' ',

        'status'=>($_POST['paypal']['status'])?$_POST['paypal']['status']:' ',

        'payer_id'=>($_POST['paypal']['payer']['payer_id'])?$_POST['paypal']['payer']['payer_id']:' ',

        'invoice_id'=>($_POST['paypal']['purchase_units'][0]['invoice_id'])?$_POST['paypal']['purchase_units'][0]['invoice_id']:' ',

        'merchant_id'=>($_POST['paypal']['purchase_units'][0]['payee']['merchant_id'])?$_POST['paypal']['purchase_units'][0]['payee']['merchant_id']:' ',

        'mode'=>'PAYPAL',

        'type'=>'STUDENT',

        'created_date'=>current_time('mysql'),

        'updated_date'=>current_time('mysql'),

    );

    /* Insert Into Table */

    $wpdb->insert('fundraise_donation',

        $donateTransaction

    );

    $id=$wpdb->insert_id;

    if($id){

        $flag=true;

        //send_donation_email();

    }else{

        $flag=false;

    }

    if ($flag) {

        $html .= "Donation successful";

        $response['status'] = "success";

        $response['url'] = home_url().'/thank-you/?id='.$donateTransaction['payer_id'];

    } else {

        $response['status'] = "failed";

        $html .= "Unable to make the donation.";

    }

    $error_messages = $errors->get_error_messages();

    if (count($error_messages) > 0) {

        foreach ($error_messages as $k => $message) {

            $html .= $message;

        }

    }

    $response['message'] = $html;

    wp_send_json($response);

    die;

}

add_action('wp_ajax_nopriv_donate_student_payment', 'donate_student_payment_callback');

add_action('wp_ajax_donate_student_payment', 'donate_student_payment_callback');

function donate_student_payment_callback()

{



    $html = '';

    $errors = new WP_Error();

    $response = array();

    $flag = false;

    global $wpdb;

    $donateTransaction=array(

        'fundpage_id'=>($_POST['fundpage_id'])?$_POST['fundpage_id']:' ',

        'user_id'=>($_POST['user_id'])?$_POST['user_id']:' ',

        'child_id'=>($_POST['child_id'])?$_POST['child_id']:' ',

        'shipping_firstname'=>($_POST['first_name'])?$_POST['first_name']:' ',

        'shipping_lastname'=>($_POST['last_name'])?$_POST['last_name']:' ',

        'shipping_email'=>($_POST['email'])?$_POST['email']:' ',

        'amount'=>($_POST['paypal']['purchase_units'][0]['amount']['value'])?$_POST['paypal']['purchase_units'][0]['amount']['value']:' ',

        'status'=>($_POST['paypal']['status'])?$_POST['paypal']['status']:' ',

        'payer_id'=>($_POST['paypal']['payer']['payer_id'])?$_POST['paypal']['payer']['payer_id']:' ',

        'invoice_id'=>($_POST['paypal']['purchase_units'][0]['invoice_id'])?$_POST['paypal']['purchase_units'][0]['invoice_id']:' ',

        'merchant_id'=>($_POST['paypal']['purchase_units'][0]['payee']['merchant_id'])?$_POST['paypal']['purchase_units'][0]['payee']['merchant_id']:' ',

        'mode'=>'PAYPAL',

        'type'=>'STUDENT',

        'created_date'=>current_time('mysql'),

        'updated_date'=>current_time('mysql'),

    );

    /* Insert Into Table */

    $wpdb->insert('fundraise_donation',

        $donateTransaction

    );

    $id=$wpdb->insert_id;

    if($id){

        $flag=true;

        //send_donation_email();

    }else{

        $flag=false;

    }

    if ($flag) {

        $html .= "Donation successful";

        $response['status'] = "success";

        $response['url'] = home_url().'/thank-you/?id='.$donateTransaction['payer_id'];

    } else {

        $response['status'] = "failed";

        $html .= "Unable to make the donation.";

    }

    $error_messages = $errors->get_error_messages();

    if (count($error_messages) > 0) {

        foreach ($error_messages as $k => $message) {

            $html .= $message;

        }

    }

    $response['message'] = $html;

    wp_send_json($response);

    die;

}



/** Student Change Request to admin */

add_action('wp_ajax_nopriv_student_change_request', 'student_change_request');

add_action('wp_ajax_student_change_request', 'student_change_request');

function student_change_request()

{

    $nonce=@$_POST['nounce'];

    $user_id=base64_decode(@$_POST['parent_id']);

    $message=@$_POST['message'];

    $user=get_user_data($user_id);

    $data=array(

        'user'=>$user,

        'message'=>$message

    );

    $html = '';

    $errors = new WP_Error();

    $response = array();

    $flag = false;

    if ( ! wp_verify_nonce( $nonce, 'change-profile-request' ) ) {

        $response['message'] = "Please refresh to send the change request.";

        wp_send_json($response);

        die;

    }

    ob_start();

    get_template_part('include/email-template/change-request', null, $data);

    $output = ob_get_contents();

    $emailData = array(

        'to' =>DEV_EMAIL,

//        'to' =>get_option('admin_email'),

        'message' => $output,

        'subject' => $user['user_email'].' has sent the profile change Request.',

    );

    ob_clean();

    $mail = send_mail($emailData);

    if($mail){

        $flag=true;

    }else{

        $flag=false;

    }

    if ($flag) {

        $html .= "Change Request sent to admin Successfully.";

        $response['status'] = "success";

    } else {

        $response['status'] = "failed";

        $html .= "Unable to make the donation.";

    }

    $error_messages = $errors->get_error_messages();

    if (count($error_messages) > 0) {

        foreach ($error_messages as $k => $message) {

            $html .= $message;

        }

    }

    $response['message'] = $html;

    wp_send_json($response);

    die;

}





?>