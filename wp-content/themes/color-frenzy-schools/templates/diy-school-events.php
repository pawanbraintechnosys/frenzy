<?php

//Template Name: DIY School Events

get_header();

get_template_part('template-parts/hero-section', null, array(
    'section-classes' => 'hero-section--small hero-section--content-center'
));

get_template_part('template-parts/three-col-imgs');

get_template_part('template-parts/form-section');

get_footer();

