import Swiper, {Navigation} from 'swiper';

Swiper.use([Navigation]);

export function initOneRowSlider() {
    const oneCol = new Swiper('.one-row-slider__slider-container', {
        slidesPerView:4,
        spaceBetween: 19,
        navigation: {
            nextEl: '.one-row-slider__slider-arrow-next',
            prevEl: '.one-row-slider__slider-arrow-prev',
        },

        breakpoints: {
            320: {
                slidesPerView:"auto",
            },
            576: {
                slidesPerView:2,
            },
            767: {
                slidesPerView:3,
            },
            1024: {

            },
            1280: {

            }
        }
    });
}