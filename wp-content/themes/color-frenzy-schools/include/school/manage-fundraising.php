<?php
   $schoolData=$args;
   $school_id=$args['data']['school_id'];
   //echo base64_encode($school_id);

?>
<div id="myModal" class="modal fade" role="dialog">
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <?php  $childDetail=getChildDetailFromChildID(base64_decode($_GET['child-id']))['data']; echo $childDetail;
      ?>
    <div class="school_login school-login-form" style="Padding: 40px 0;background: #1b7eaa;">
        <div class="container" style="padding-right: 0rem;padding-left: 0rem;">
            <h2 style="text-align: center;">Manage <?=  $childDetail['firstname'].' '.@$childDetail['lastname']  ?> Fundpage</h2>
            <div class="row myform">
                <div class="col-9 col-md-12 col-lg-9" style="max-width: 100%;flex: 0 0 100%;">
                    <div class="card">
                        <div class="card-body">
                            <p class=""></p>
                            <form class="form-post" id="create-fundraiser" method="POST" action="">
                                <input type="hidden" name="action" value="create_fundraising_page">
                                <input type="hidden" name="fundpage_id" value="<?= $childDetail['id'] ?>">
                                <input type="hidden" name="child_id" value="<?= @$_GET['child-id'] ?>">
                                <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'update-fundraising-page' ); ?>">
                                <div class="form-group">
                                    <label class="control-label" for="email">Title</label>
                                    <input id="title" Placeholder="Fundraising Page title" name="title" type="text" value="<?= $childDetail['title'] ?>" class="form-control " required="" autocomplete="off" autofocus="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="email">Description</label>
                                    <?php
                                    $content   = $childDetail['description'];
                                    $editor_id = 'description';
                                    wp_editor( $content, $editor_id );
                                    ?>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Start Date</label>
                                            <input id="event_start_date datepicker" Placeholder="Fundraising Start Date" value="<?= $childDetail['start-date'] ?>" name="start-date" type="text" class="form-control datepicker" required="" autocomplete="off" autofocus="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">End Date</label>
                                            <input id="event_end_date" Placeholder="Fundraising End Date" name="end-date" value="<?= $childDetail['end-date'] ?>" type="text" class="form-control " required="" autocomplete="off" autofocus="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Minimum Amount</label>
                                            <input id="min-amount" Placeholder="Enter Minimum Amount" name="min-amount" type="number" class="form-control " value="<?= $childDetail['min-amount'] ?>" required="" autocomplete="off" autofocus="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Maximun Amount</label>
                                            <input id="max-amount" Placeholder="Enter Maximum Amount" name="max-amount" type="number" class="form-control " value="<?= $childDetail['max-amount'] ?>" required="" autocomplete="off" autofocus="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Fundraising Goal</label>
                                            <input id="goal-amount" Placeholder="Enter Goal Amount" name="goal-amount" type="number" class="form-control " value="<?= $childDetail['goal-amount'] ?>" required="" autocomplete="off" autofocus="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="predefined-amount">Predefined Pledge Amount</label>
                                            <input id="predefined-amount" Placeholder="Enter Predefined Amount" name="predefined-amount" type="text" value="<?= $childDetail['predefined-amount'] ?>" class="form-control " required="" autocomplete="off" autofocus="">
                                            <p style="font-size: 9px;line-height: 11px;font-style: italic;">Predefined amount allow you to place the amount in donate box by click, price should separated by comma (,), example: 10,20,30,40</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-lg btn-block w-100" value="Submit"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<div class="manage_fundraising">
   <div class="manage_head">
      <div class="head">
         <p>Manage Fundraising</p>
      </div>
      <div class="filter">
         <div class="man-search"><input type="search" id="myInput" class="form-control" placeholder="To search, type and press enter" aria-label="Search"> 
		 <img src="<?php echo get_template_directory_uri().'/assets/images/search.png'; ?>">            
		 </div>
      </div>
   </div>
   <div class="manag_fund_data">
      <table class="table">
         <thead>
            <tr>
               <th scope="col">Title</th>
               <th scope="col">First Name</th>
               <th scope="col">Last Name</th>
               <th scope="col">Class</th>
               <th scope="col">Start date</th>
               <th scope="col">Close date</th>
               <th scope="col">Goal</th>
               <th scope="col">Raised</th>
               <th scope="col" class="last_act_but">Action</th>
            </tr>
         </thead>
         <tbody id="myTable">
		    <?php 
            //$funddatas = getFundraisedData($school_id); 
				$num_per_page = 10;
            if(isset($_GET['pages'])){
               $page = $_GET['pages'] ;
               //echo $page;
            }else {
               $page = 1;
            }
            $start_form = ($page-1)*$num_per_page;
            //echo $start_form;
				$funddatas = $wpdb->get_results("SELECT * FROM wp8t_child_details WHERE school_id= $school_id LIMIT $start_form,$num_per_page",ARRAY_A); 
           
           
			   foreach($funddatas as $key=>$funddata){ 
   				global $wpdb;
   				$fundtitle_firstname = $funddata['firstname'];
   				$fundtitle_lastname = substr($funddata['lastname'],0,1);
   				$fundtitle_class = $funddata['class'];
   				$fundtitle=  $fundtitle_firstname.' '.$fundtitle_lastname.' '.$fundtitle_class;
               
   				$campiagns = $wpdb->get_results("SELECT * FROM fundraise_posted_campiagn WHERE title = '$fundtitle'",ARRAY_A);
					foreach($campiagns as $key=>$campiagn ){
			 ?>
            <tr class="alldata">
               <td class="date-other"><?php echo $campiagn['title']; ?></td>
               <td><?php echo $funddata['firstname']; ?></td>
               <td><?php echo $funddata['lastname']; ?></td>
               <td><?php echo $funddata['class']; ?></td>
               <td class="date-other"><?php echo $campiagn['start-date']; ?></td>
               <td class="date-other"><?php echo $campiagn['end-date']; ?></td>
               <td class="date-other">$<?php echo $campiagn['goal-amount']; ?></td>
               <td class="date-other">
                  <?php
   			         $totalRaisedAmount = $wpdb->get_results("SELECT SUM(amount) as total_raise FROM fundraise_donation WHERE fundpage_id ={$campiagn['id']}",ARRAY_A)[0];
   			         if(isset($totalRaisedAmount['total_raise']) && !empty($totalRaisedAmount['total_raise'])){
                        echo DEFAULT_CURRENCY.number_format($totalRaisedAmount['total_raise'],2);
                     }else{
                        echo DEFAULT_CURRENCY."00.00";
                     }
   			      ?>
               </td>
   			   <td class='edited'>
                  <button class="las_act_but" data-target="#myModal" id="edit_button" >Edit</button>
                  <!--a id="edit_button" href="<?php //echo home_url().'/manage-student-fundpage/?compaign-id='.base64_encode($campiagn['id']); ?>" class="las_act_but" >Edit</a-->

               </td>
			   </tr>
			<?php 
               }
            } 
         ?>
         </tbody>
      </table>
      <?php 
         $fundreord = $wpdb->get_results("SELECT COUNT(school_id) as rec_count FROM wp8t_child_details WHERE school_id= $school_id",ARRAY_A); 
               $rec_count = $fundreord[0] ['rec_count']; 
               if($rec_count>10){
      ?>
      <div class='paginat' style='background: #f83d96; color: #fff; margin-top: 20px;margin-right:17px;padding: 3px 18px;font-size: 20px; border-radius: 2px; float: right;'>
         <?php
               //$fundreord = $wpdb->get_results("SELECT COUNT(school_id) as rec_count FROM wp8t_child_details WHERE school_id= $school_id",ARRAY_A); 
               //$rec_count = $fundreord[0] ['rec_count'];
               $total_pages = ceil($rec_count/$num_per_page);
               //echo $rec_count; 
               //echo "<a href='".home_url()."/school-dashboard/?id=".base64_encode($school_id)."&pages=1'>".$i."</a>";
               for($i=1;$i<=$total_pages;$i++){
                  if($i== $page){
                  echo "<a class='active' href='".home_url()."/school-dashboard/?id=".base64_encode($school_id)."&pages=".$i."'>".$i."</a>";
               }else{
                  echo "<a href='".home_url()."/school-dashboard/?id=".base64_encode($school_id)."&pages=".$i."'>".$i."</a>";
                  //echo "<a href='".home_url()."/school-dashboard/?id=".base64_encode($school_id)."&page=".$i."'>".$i."</a>";
               }
            }
               //echo "<a href='".home_url()."/school-dashboard/?id=".base64_encode($school_id)."&pages=".$total_pages."'>".$i."</a>";
         ?>
      </div>
   <?php } ?>
   </div>
</div>
<script>
   $(document).ready(function(){
   	$("#myInput").on("keyup", function() {
       var value = $(this).val().toLowerCase();    
   		$("#myTable tr").filter(function() {
            //alert('gtrgrt');
   			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)    
   		});  
   	});
   });



/*var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
   $('#myBtn').val($(this).data('<?php echo $campiagn['id']; ?>'));
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}*/

$("#edit_button").click(function(){
  var Id = $(this).attr('<?php echo $campiagn['id']?>');
  
  alert(Id);
});

</script>