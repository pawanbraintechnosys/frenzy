<?php
$title = get_field('three-col-items__title');
$description = get_field('three-col-items__description');
$itemsRow = 'three-col-items__items';
$subTitle = get_field('three-col-items__subtitle');
$secondTitle = get_field('three-col-items__second-title');
$pageImage = get_field('three-col-items__image');
$pageLink = get_field('three-col-items__link');
?>
<section class="three-col-items">
    <div class="container">
        <div class="title-three-column">
            <h2>
                <?= $title; ?>
            </h2>
            <?= $description? $description : ''; ?>
        </div>

        <div class="three-col-items__inner">
            <?php if(have_rows($itemsRow)): ?>
            <div class=" ticket-items">
                <?php while(have_rows($itemsRow)): the_row();
                $itemTitle = get_sub_field('item_title');
                $itemText = get_sub_field('item_text');
                $itemLink = get_sub_field('item_link');
                ?>
                <div class="ticket-items__item ticket-item circle-mark-list">
                    <div class="ticket-item__bg">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/ticket-items/bg.png" alt="img">
                    </div>
                    <h3>
                        <?= $itemTitle; ?>
                    </h3>
                    <div class="ticket-item__text">
                        <?= $itemText; ?>
                    </div>
                    <a href="<?= $itemLink['url']; ?>" class="btn" >
                        <?= $itemLink['title']; ?>
                    </a>
                </div>
                <?php endwhile;?>
            </div>
            <?php endif; ?>
            <p class="text-center">
                <?= $subTitle; ?>
            </p>
        </div>
        <h2>
            <?= $secondTitle; ?>
        </h2>
        <div class="three-col-items__img">
            <?= wp_get_attachment_image($pageImage['id'], 'full'); ?>
            <a href="<?= $pageLink['url']; ?>" class="btn" >
                <?= $pageLink['title']; ?>
            </a>
        </div>
    </div>
</section>
