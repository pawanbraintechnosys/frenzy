jQuery(function($){
    $('#load-more-location').on('click', function(e){
        e.preventDefault();

        let data = {
            'action': 'location_loadmore',
            'query': location_params.posts,
            'page': location_params.current_page
        };

        $.ajax({
            url : location_params.ajaxurl,
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {

            },
            success : function( data ){
                if( data ) {
                    location_params.current_page++
                    $('.course-section__locations-items').append(data)
                    if(location_params.max_page == location_params.current_page){
                        $('#load-more-location').addClass('hidden')
                    }
                }
            }
        });
    });

    $('#search-location').on('submit', function (e) {
        e.preventDefault()

        let data = {
            'action': 'location_search',
            'query': location_params.posts,
            'search' : $(this).find('input').val()
        };

        $.ajax({
            url : location_params.ajaxurl,
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {

            },
            success : function( data ){
                $('#load-more-location').addClass('hidden')
                if( data ) {
                    location_params.current_page = 1;
                    $('.course-section__locations-items').html(data)
                }
                else{
                    $('.course-section__locations-items').html('<h2 style="text-align: center;">Nothing found</h2>')
                }
            }
        });
    })
});
