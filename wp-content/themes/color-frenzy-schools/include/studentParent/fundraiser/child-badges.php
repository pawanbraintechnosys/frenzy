<?php

/*

 * Student Badges

 * @Package ColourFrenzy

 * @Version 1.0.0

 */
$ID = get_the_ID();
$child_id=base64_encode($_GET['child_id']);
$childDetail=$args;
$totalFundRaised=@$childDetail['data']['total_fund_raised']['total_donation'];
//pr($childDetail);
//$IDd = base64_encode($childDetail['data']['child_id']); echo $IDd;
$child_school_name = $childDetail['data']['child_school_name'];
//echo $child_school_name;
    $args=array(
    'post_type'=>'badges',
    'order_by'=>'ID',
    'order'=>'ASC',
    'posts_per_page'=>($args['data']['number_of_badge'])?$args['data']['number_of_badge']:10,
    'post_status'=>'publish'
);

$badges=new WP_Query($args);
if($badges->have_posts()):
?>
<div class="">
    <div class="row overflow">
        <?php
        $badge_found=false;
        while ($badges->have_posts()):$badges->the_post();
          $badgePrice=get_the_excerpt(get_the_ID());
          //if($totalFundRaised >= $badgePrice):
              $badge_found=true;
        ?>
        
        <div class="col-lg-4 col-md-4 cemter">
            <?php $badge_url = get_the_post_thumbnail_url(get_the_ID(),array(100,100)); ?>
            <img <?php if($totalFundRaised <= $badgePrice){ ?>style="opacity: 0.2;"<?php }?> src="<?php echo $badge_url; ?>">
            <p><?php the_title(); ?></p>
        </div>
          <?php //endif; ?>
        <?php endwhile; ?>

        <?php if(!$badge_found): ?>

            <div class="not_found">

                <h4 style="padding-left: 22px;">No Badges Earned.</h4>

            </div>

        <?php endif; ?>

        <?php else: ?>

            <div class="not_found">

                <h4 style="padding-left: 22px;">No Badges Earned.</h4>

            </div>

        <?php endif; ?>

    </div>



    <div class="priz">

        <div class="row">

            <div class="col-lg-8" <?php if($ID==7618){ ?> style="display: none;" <?php } ?>><h3>Fundraising Profile</h3></div>

            <div class="col-lg-4 car" <?php if($ID==7618){ ?> style="display: none;" <?php } ?>>
                <a href="<?php echo home_url().'/student-fundraising-page/?'.$childDetail['data']['child_school_name'].'&child-id='.base64_encode($childDetail['data']['child_id']); ?>" class="btn btn-warning">Page</a>

            </div>

        </div>

    </div>

</div>

