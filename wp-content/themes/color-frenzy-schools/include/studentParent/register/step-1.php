<div class="Parent steps-form mt-4 frm-step frm-step1" id="Parent frmSchoolStep1">
    <div class="form-group group first">
        <input type="hidden" name="school_id" value="<?php echo base64_decode(@$_GET['school-id']); ?>">
        <div class="detail">
            <label for="exampleInputEmail1">Parent Name <small style="padding-left: 3px;"> (Required)</small></label>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <input type="text" class="form-control" id="ParentFirstName" name="parent-firstName" required aria-describedby="emailHelp">
                <label for="exampleInputEmail1">First</label>
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" id="ParentLastName" name="parent-lastName" aria-describedby="emailHelp">
                <label for="exampleInputEmail1">Last</label>
            </div>
        </div>
    </div>
    <div class="form-group group">
        <label for="exampleInputEmail1">Email <small style="padding-left: 3px;"> (Required)</small></label>
        <input type="email" class="form-control" id="ParentEmail" name="user_email"  aria-describedby="emailHelp" required>
    </div>
    <div class="form-group group">
        <label for="parentPhone">Phone Number  <small style="padding-left: 3px;"> (Required)</small></label>
        <input type="number" class="form-control" id="parentPhone" name="phone" required="">
    </div>
    <div class="form-group group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="password" name="user_password" required>
    </div>
    <div class="form-group group">
        <label for="exampleInputPassword1">Confirm Password</label>
        <input type="password" class="form-control" id="confirm_pass" name="confm_user_pass" required>
    </div>
    <div class="next">
        <button class="btn_border btn btn-primary float-right btnAddSchoolNext"  data-current="step1" data-next="step2"><?php _e("Next", "colorfenzy"); ?></button>
    </div>
</div>
