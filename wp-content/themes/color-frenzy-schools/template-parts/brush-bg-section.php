<?php
$brushBgTitle = get_field('brush-background__title');
$brushBgItems = 'brush-background__items';
$brushBgDescTitle = get_field('brush-background__description-title');
$brushBgDescBtn = get_field('brush-background__description-btn');
$brushBgDescBg = get_field('brush-background__description-bg');
?>
<?php if(have_rows($brushBgItems)): ?>
    <section class="brush-bg-section brush-bg-section--white-head brush-bg-section--pink-bg">
        <div class="container">
            <h2>
                <?= $brushBgTitle; ?>
            </h2>
            <?php while(have_rows($brushBgItems)): the_row();
                $brushBgItemReverse = get_sub_field('reverse_items');
                $brushBgItemText = get_sub_field('text');
                $brushBgItemImg = get_sub_field('image');
                ?>
                <div class="text-img-item text-img-item--not-full-width <?= $brushBgItemReverse ? 'text-img-item--reverse' : ''; ?> ">
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($brushBgItemImg['id'], 'full'); ?>
                    </div>
                    <div class="text-img-item__text">
                        <?= $brushBgItemText; ?>
                    </div>
                </div>
            <?php endwhile; ?>
            <div class="brush-bg-section__description">
                <?php if($brushBgDescTitle): ?>
                    <div class="brush-bg-section__message">
                        <?= wp_get_attachment_image($brushBgDescBg['id'], 'full'); ?>
                        <h2><?= $brushBgDescTitle; ?></h2>
                    </div>
                <?php endif; ?>
                
                <?php if($brushBgDescBtn): ?>
                    <a href="<?= $brushBgDescBtn['url']; ?>" target="<?= $brushBgDescBtn['target'] ?>" class="btn">
                        <?= $brushBgDescBtn['title']; ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>