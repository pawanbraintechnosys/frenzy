<?php
$title = get_field('text-img-progress__title');
$img = get_field('text-img-progress__image');
$content = get_field('text-img-progress__content');
$currentValue = get_field('text-img-progress__progress-bar');
$endValue = get_field('text-img-progress__end-value');
$titleSearch = get_field('text-img-progress__title-search');
$button = get_field('text-img-progress__button');
?>
<section class="text-img-items text-img-items--violet ">
    <div class="container">
        <div class="text-img-items__wrapper">
            <h2><?= $title; ?></h2>
            <div class="text-img-items__item text-img-item text-img-item--yellow-header text-img-item--brush-bg-violet text-img-item--progress-bar">
                <div class="text-img-item__img">
                    <?= wp_get_attachment_image($img['id'], 'full'); ?>
                </div>
                <div class="text-img-item__text">
                    <?= $content; ?>
                    <div class="text-img-item__progress-bar-wr progress-bar-wr ">
                        <div class="progress-bar-wr__text">
                            <div class="progress-bar-wr__value">
                                $<?= $currentValue; ?>
                            </div>
                            <div class="progress-bar-wr__limit-value">
                                <?= $endValue; ?>
                            </div>
                        </div>
                        <progress max="100000" value="<?= $currentValue; ?>">
                            <span id="value"></span>
                        </progress>
                    </div>
                </div>
            </div>
            <div class="text-img-items__description">
                <h2><?= $titleSearch; ?></h2>

                <?php if($button): ?>
                    <a href="<?= $button['url']; ?>" class="btn" >
                        <?= $button['title']; ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>