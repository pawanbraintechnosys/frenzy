<?php
if( ! function_exists( 'custom_comments' ) ):
    function custom_comments($comment, $args, $depth) {
    $commentAuthor = get_comment_author();
    $commentDate = get_comment_date();
    $commentText = get_comment_text();
    ?>
    <li id="li-comment-<?php comment_ID() ?>">
        <div class="comment">
            <span class="comment-author">
                <?= $commentAuthor; ?>
            </span>
            <span class="comment-date">
                <?= $commentDate; ?>
            </span>
            <p>
                <?= ($comment->comment_approved == '0') ? 'Your comment is awaiting moderation.' : $commentText; ?>
            </p>
            <a href="#"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></a>
        </div>

        <?php
    }
endif;
