<?php
$title = get_field('video-section__title');
$videoPreview = get_field('video-section__video-preview');
$videoLink = get_field('video-section__video-link');
?>
<section class="video-section">
    <div class="container">
        <h2>
            <?= $title; ?>
        </h2>
        <div class="video-section__item <?= $args['item-classes']; ?>">
            <div class="text-video-item__wr">
                <a href="#" data-video-src="<?= $videoLink; ?>" class="text-video-item__img">
                    <?= wp_get_attachment_image($videoPreview['id'], 'full') ?>
                    <button class="play-video">
                        <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                    </button>
                </a>
            </div>
        </div>
    </div>
</section>
