<?php
$items = 'text-image-items__items';
$btnTitle = get_field('text-image-items__button-title');
$btn = get_field('text-image-items__button');
?>
<?php if(have_rows($items)): ?>
    <section class="text-img-items <?= $args['section-classes']; ?>">
        <div class="container">
            <?php while(have_rows($items)): the_row();
                $itemReverse = get_sub_field('reverse');
                $itemImage = get_sub_field('image');
                $itemText = get_sub_field('text');
                $itemBtn = get_sub_field('button');
                ?>
                <div class="text-img-items__item text-img-item <?= $args['bg-title']? 'text-img-item--head-bg-green' : ''; ?> <?= $args['item-classes']; ?> <?= $itemReverse ? ' text-img-item--reverse ' : ''; ?>">
                    <div class="text-img-item__img">
                        <?= wp_get_attachment_image($itemImage['id'], 'full') ?>
                    </div>
                    <div class="text-img-item__text">
                        <?= $itemText; ?>
                        <?php if($itemBtn): ?>
                            <a href="<?= $itemBtn['url'] ?>"  target="<?= $itemBtn['target'] ?>" class="btn">
                                <?= $itemBtn['title'] ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php if($btnTitle || $btn): ?>
                <div class="text-img-items__description-bg">
                    <img src="<?= get_template_directory_uri(); ?>/assets/img/content-img/brush-violet-bg.png" alt="image">
                    <?php if($btnTitle): ?>
                        <h3>
                            <?= $btnTitle; ?>
                        </h3>
                    <?php endif; ?>

                    <?php if($btn): ?>
                        <a href="<?= $btn['url']; ?>"  target="<?= $btn['target']; ?>" class="btn">
                            <?= $btn['title']; ?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>
