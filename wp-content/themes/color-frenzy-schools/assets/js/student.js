$(document).ready(function () {
    /**
     * Student Fundraising Payment
     *
     * @package colorFenzy
     * @version 1.0.0
     */
    $(".donate_amount_student").click(function(e){
        $(".donate_amount_student").removeClass('active');
        let donate_price=$(this).data('donate');
        $(this).addClass('active');
        $(".donate_student_other_amount").val(donate_price);
        $(".dnt-student-amnt").val(donate_price);
        $(".final-payment-student").val("DONATE $"+donate_price);
    });
    $(".donate_student_other_amount").on('change',function(e){
        let other_amount=$(this).val();
        if ($(".donate_amount_student").hasClass("active")) {
            $(".donate_amount_student").removeClass("active")
        }
        $(".dnt-student-amnt").val(other_amount);
        $(".final-payment-student").val("DONATE $"+other_amount);
    });

    $(".btnDonateStudentNext").click(function(e){
        e.preventDefault();
        var stdform = $("#donate-student-fund");
        stdform.validate({
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass("has-error");
            },
            rules: {
                donate_price:{
                    required: true,
                },
                first_name: {
                    required: true,
                },
                email: {
                    required: true,
                },
                card_name: {
                    required: true,
                },

            },
            messages: {
                username: {
                    required: "Username required",
                },
            },
            submitHandler: function(form) {
                console.log('submitted');
                $(".ajaxLoading").removeClass("hide");
                $.ajax({
                    type: 'POST',
                    url: COLORFENZY_OPTION.ajaxURL,
                    data: $('#donate-student-fund').serialize(),
                    success: function (data) {
                        $(".ajaxLoading").addClass("hide");
                        console.log(data);
                        if (data.status == 'success') {
                        } else {

                        }
                    },
                });
            }
        });
        if (stdform.valid() === true){
            if ($('#donate-student-step-1').is(":visible")){
                current_fs = $('#donate-student-step-1');
                next_fs = $('#donate-student-step-2');
                /* step Number */
                current_step = $('#student-main-step-1').removeClass('active');
                next_step = $('#student-main-step-2').addClass('active');

            }else if($('#donate-student-step-2').is(":visible")){
                current_fs = $('#donate-student-step-2');
                next_fs = $('#donate-student-step-3');
                /* step Number */
                current_step = $('#student-main-step-2').removeClass('active');
                next_step = $('#student-main-step-3').addClass('active');

            }
            next_fs.show();
            current_fs.hide();
        }
    });

    $(document).on('click','#btnstddonatePrev',function(){
        if($('#donate-student-step-2').is(":visible")){
            current_fs = $('#donate-student-step-2');
            next_fs = $('#donate-student-step-1');
            /* step Number */
            current_step = $('#student-main-step-2').removeClass('active');
            next_step = $('#student-main-step-1').addClass('active');

        }else if ($('#donate-student-step-3').is(":visible")){
            current_fs = $('#donate-student-step-3');
            next_fs = $('#donate-student-step-2');
            current_step = $('#student-main-step-3').removeClass('active');
            next_step = $('#student-main-step-2').addClass('active');

        }
        next_fs.show();
        current_fs.hide();

    });
});
/*
* Change Request Modal
* @Package Colourfrenzy
* @Version 1.0.0
*/
$(document).on('click','#change_request',function(e){
    $("#change-request-modal").show();
});
$(document).ready(function (e){
    $("#change-request-modal .close").click(function(){
        $("#change-request-modal").hide();
        $('#student-profile_change')[0].reset();
        $("#message-error").text("");
    })
    /* Send change request to admin */
    
    $("#student-profile_change").on('submit',function(e){
        e.preventDefault();
        const message=$("#message").val();
        let form_valid=false;
        if(message==''){
            $("#message-error").text("Please enter the message");
        }else{
            $("#message-error").text("");
            form_valid=true;
        }
        if(form_valid){
            $(".ajaxLoading").removeClass("hide");
            $.ajax({
                type: 'POST',
                url: COLORFENZY_OPTION.ajaxURL,
                data: $('#student-profile_change').serialize(),
                success: function (data) {
                  $(".ajaxLoading").addClass("hide");
                  if(data.status=='success'){
                      toastr.success(data.message);
                      $("#change-request-modal").hide();
                      $('#student-profile_change')[0].reset();
                  }else{
                      toastr.error(data.message);
                  }
                },
            });
        }

    });
});
