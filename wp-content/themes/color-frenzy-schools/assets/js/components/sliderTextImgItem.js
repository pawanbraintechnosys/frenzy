import Swiper, {Navigation} from 'swiper';

Swiper.use([Navigation]);

export function initTextImgSlider() {
    const oneCol = new Swiper('.text-img-item__slider-container', {



        navigation: {
            nextEl: '.text-img-item__slider-arrow-next',
            prevEl: '.text-img-item__slider-arrow-prev',
        },

        breakpoints: {
            320: {
                slidesPerView:"auto",
                initialSlide: 0,
            },
            576: {
                slidesPerView:3,
                spaceBetween: 29,
                initialSlide: 1,
                centeredSlides:true,
            },

        }
    });
}