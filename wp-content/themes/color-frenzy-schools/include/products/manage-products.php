<?php
/*
 * Product page
 * @ver:1.0
 */
function school_products_callback($atts){
  
  
         ?>
        <div class="main-dashboard school_dashboard school_products">
            <div class="product_profile">
                <div class="container">
                    <div class="row school_dash">
						<div class="col-lg-6">
							<div class="product_about">
								<div class="product_img">
									<img src="<?php echo get_template_directory_uri().'/assets/img/guardian.png'; ?>" class="logo_product">
								</div>
                            <div style="position: relative;top: 32%;margin: 0px 100px;">
                            	<h1 style="text-align: center;">FUNDRAISER!</h1>
                                <span class="fund_left">Progress
								<?php echo DEFAULT_CURRENCY.number_format(get_all_fund_donation($schoolID)['total_fund_raised']['total_donation']); ?>
								</span>
                                <span class="fund_right">
								<?php 
									/*$totalGoal = get_school_webpage($schoolID)['data'];
									foreach($totalGoal as $key=>$Toatal_Goals){
										echo DEFAULT_CURRENCY.number_format($Toatal_Goals ['fundrasing_goal']);
									}*/
								?>
								Goal</span>
                                <div class="school-light-grey">
								<?php /*$A = $Toatal_Goals ['fundrasing_goal'];
									  $B = get_all_fund_donation($schoolID)['total_fund_raised']['total_donation'];
									  $per = round(($B/$A) * 100);*/
								?>
                                    <div class="outdder">
									<div style="height:25px;width:<?php //echo $per?>70%;border-right:solid 1px #000;background-color:#f83d96;">
									</div>
									</div>
                                </div>
							</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="product_support_cont">
								<h3 class="heading">Thank you for supporting</h3>
								<h5>Student Profile Title</h5>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							</div>
						</div>
                        
                    </div>
                </div>
            </div>
			<div class="product_profile product_profile_about">
                <div class="container">
                    <div class="row school_dash">
						<div class="col-lg-6">
							<div class="product_support_cont">
								<h3 class="heading">Text about the product</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							</div>
						</div>
                        <div class="col-lg-6">
							<div class="product_about">
								<div class="product_img">
									<img src="<?php echo get_template_directory_uri().'/assets/img/about_product.png'; ?>" class="logo_product">
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
			<div class="add_to_cart_main">
			<?php
	            $args = array(
	                'post_type'      => 'productitem',
	                'posts_per_page' => 10,
	            );
	            $loop = new WP_Query( $args );
	            while ( $loop->have_posts() ) : $loop->the_post();
                global $productitem;
            ?>
				<div class="add_to_cart">
					<div class="container" style="margin-top: 20px;margin-bottom:30px">
						<div class="row">
							<div class="col-lg-12">
								<div class="row school_dash">
									<div class="col-lg-6">
										<div class="product_about">
											<div class="product_img">
												<h3 class="heading"><?php the_field('product_title'); ?></h3>
												<img src="<?php the_field('product_image_1'); ?>" class="logo_product">
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="product_support_cont">
											<h5><?php the_field('product_sub_title'); ?></h5>
											<p><?php the_field('product_description_1'); ?></p>
											<p><?php the_field('product_description_2'); ?></p>
										</div>
										<div class="up_down_product">
											<p><?php //the_field('product_cost'); ?></p>
											<div class="up-down">
												<p>
													<table>
											    <tr class="cart_item">
											        <td class="cart_price" style="display: flex; margin-right: 30px;">
											        	<p>$</p>
											            <p class="actual_price"><?php the_field('product_cost'); ?></p>
											        </td>
											        <td class="cart_quantity">
											            <input type='button' value='-' class='quantityminus' field='quantity' />
											            <input type='text' style="width: 12%;text-align:center;" name='quantity' value='0' class='quantity' />
											            <input type='button' value='+' class='quantityplus' field='quantity' />
											        </td>
											    </tr>
											    
											</table>

													<!--img src="<?php //echo home_url().'/wp-content/uploads/sites/4/2021/12/minus.png';  ?>" id="minus_1" width="40" height="20" class="minus" />
													<input id="clothqty-1" type="text" value="0" class="qty" readonly name="cloth-1" />
													<img id="add_1" src="<?php //echo home_url().'/wp-content/uploads/sites/4/2021/12/plus.png';  ?>" width="40" height="20" class="add" /-->
												</p>
												<a href="#down" class="btn">Add to Cart</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
            endwhile;
            ?>
			<div class="thank_support product_profile_about">
				<div class="support_content">
					<div class="card_detail">
						<div class="support_cont">
							<h3>Thank you for your support</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
							, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
							, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
							, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						</div>
						<form class="form-post tabbing add_parent_student" id="addStudentForm" enctype="multipart/form-data" method="post" novalidate="novalidate">
							<input type="hidden" name="action" value="addStudentParentField">
							<input type="hidden" id="addstudent_action" name="addstudent_action" value="27612f504c"><input type="hidden" name="_wp_http_referer" value="/schools/student-parent-registration/?school-id=Nzg1Mg==">                                    <input type="hidden" name="contributor_id" value="0">
							<div class="Parent steps-form mt-4 frm-step frm-step1" id="Parent frmSchoolStep1" style="">
								<div class="form-group group first">
									<div class="detail">
										
									
									</div>
									<div class="row">
										<div class="col-lg-6">
											<label for="exampleInputEmail1">First Name <small style="padding-left: 3px;"></small></label>
											<input type="text" class="form-control" id="ParentFirstName" name="parent-firstName" required="" aria-describedby="emailHelp">
										</div>

										<div class="col-lg-6">
											<label for="exampleInputEmail1">Last Name <small style="padding-left: 3px;"></small></label>
											<input type="text" class="form-control" id="ParentLastName" name="parent-lastName" aria-describedby="emailHelp">
											
										</div>
									</div>
								</div>
								<div class="form-group group">
									<label for="exampleInputEmail1">Address</label>
									<input type="email" class="form-control" id="ParentEmail" name="user_email" aria-describedby="emailHelp" required="">
								</div>
								
								<div class="form-group group first">
									<input type="hidden" name="school_id" value="7852">
									<div class="detail">
										
									</div>
									<div class="row">
										<div class="col-lg-6">
											<label for="exampleInputEmail1">State</label>
											<input type="text" class="form-control" id="ParentFirstName" name="parent-firstName" required="" aria-describedby="emailHelp">
										</div>
										<div class="col-lg-6">
											<label for="exampleInputEmail1">Postcode</label>
											<input type="text" class="form-control" id="ParentLastName" name="parent-lastName" aria-describedby="emailHelp">
										</div>
									</div>
								</div>
								<div class="form-group group">
									<label for="parentPhone">Phone Number  <small style="padding-left: 3px;"></small></label>
									<input type="text" class="form-control" id="parentPhone" name="phone" required="">
								</div>
								<div class="form-group group">
									<label for="exampleInputEmail1">Email <small style="padding-left: 3px;"></small></label>
									<input type="email" class="form-control" id="ParentEmail" name="user_email" aria-describedby="emailHelp" required="">
								</div>
							</div>
						</form>
						<div class="payment_detail" id="down" >
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<div class="form-check">
										<label class="form-check-label check-label" for="flexCheckChecked">Terms (Required)</label>
										<div class="terms-cond">
											<input class="form-check-input" type="checkbox" value="1" name="term-check" id="flexCheckChecked" required="">
											<p>I accept color Fenzy T&amp;C</p>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="row">
										<div class="col-lg-6 col-md-6">
											<div class="form-check" style="padding-top: 25px;">
												<label class="form-check-label check-label payment" for="flexCheckChecked">Payment Method</label>
												<span class="crd" style="display:inline-flex;margin-top: 10px;">
													<input class="form-check-input" type="checkbox" value="1" name="term-check" id="flexCheckChecked" required="">
													  <p>Credit Card </p>
												</span>
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="form-check">
												<div class="total_pric"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="credit-card-detail">
								<div class="form-check" style="padding-top: 25px;">
									<label class="form-check-label" for="flexCheckChecked">Card Details</label>
									<div class="vertical" style="    margin-top: 8px;">
										<input type="text" class="form-control" maxlength="20" id="card_number" name="card_number" aria-describedby="emailHelp" required="">
										<div class="card_cvv">
											<input type="text" class="form-control" placeholder="MM" maxlength="2" id="expiry_month" name="expiry_month">
											<input type="text" class="form-control" placeholder="YYYY" maxlength="4" id="expiry_year" name="expiry_year">
											<input type="text" class="form-control" placeholder="CVV" maxlength="3" id="cvv" name="cvv">
										</div>
									</div>
								</div>
							</div>
							<div class="cardholder-detail">
								<div class="form-check" style="margin-top: 15px;">
									<label class="form-check-label">Cardholder Name</label>
									<input type="text" id="name_on_card" class="form-control" name="name_on_card">
								</div>
							</div>
							<div class="next">
								<button class="btn_border btn btn-primary float-right btnAddSchoolNext" data-current="step1" data-next="step2">Purchase</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
}
add_shortcode('School_products', 'school_products_callback');
?>