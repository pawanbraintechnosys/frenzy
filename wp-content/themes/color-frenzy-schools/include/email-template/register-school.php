<?php

/*

 * Email Template

 * Desc : Send email to school on registeration

 * @ver 1.0

 */

?>

<!DOCTYPE html>

<html>

<head>

    <title></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <style type="text/css">

        @media screen {

            @font-face {

                font-family: 'Lato';

                font-style: normal;

                font-weight: 400;

                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');

            }



            @font-face {

                font-family: 'Lato';

                font-style: normal;

                font-weight: 700;

                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');

            }



            @font-face {

                font-family: 'Lato';

                font-style: italic;

                font-weight: 400;

                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');

            }



            @font-face {

                font-family: 'Lato';

                font-style: italic;

                font-weight: 700;

                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');

            }

        }



        /* CLIENT-SPECIFIC STYLES */

        body,

        table,

        td,

        a {

            -webkit-text-size-adjust: 100%;

            -ms-text-size-adjust: 100%;

        }



        table,

        td {

            mso-table-lspace: 0pt;

            mso-table-rspace: 0pt;

        }



        img {

            -ms-interpolation-mode: bicubic;

        }



        /* RESET STYLES */

        img {

            border: 0;

            height: auto;

            line-height: 100%;

            outline: none;

            text-decoration: none;

        }



        table {

            border-collapse: collapse !important;

        }



        body {

            height: 100% !important;

            margin: 0 !important;

            padding: 0 !important;

            width: 100% !important;

            font-family: "Have Heart Two";

        }



        table td p{}



        /* iOS BLUE LINKS */

        a[x-apple-data-detectors] {

            color: inherit !important;

            text-decoration: none !important;

            font-size: inherit !important;

            font-family: inherit !important;

            font-weight: inherit !important;

            line-height: inherit !important;

        }



        /* MOBILE STYLES */

        @media screen and (max-width:600px) {

            h1 {

                font-size: 32px !important;

                line-height: 32px !important;

            }

        }



        /* ANDROID CENTER FIX */

        div[style*="margin: 16px 0;"] {

            margin: 0 !important;

        }

    </style>

</head>



<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->

<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;"> We're thrilled to have you here! Get ready to dive into your new account. </div>

<div style="background: #fafafa;padding: 30px 33px;max-width: 600px;align-items: center;display: flex;width: 100%;margin: 0 auto;}">

    <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <!-- LOGO -->

        <tr>

            <td align="center">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" style="max-width: 720px;">

                    <tr>

                        <td align="center" valign="top" ><img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/12/email-image.png" style="width:100%;" alt="color"></td>

                    </tr>

                </table>

            </td>

        </tr>

        <tr>

            <td align="center" style="" >

                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 720px;">

                    <tr>

                        <td bgcolor="#ffffff" align="center" valign="top" style="padding-bottom:12px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">

                            <p style="font-size: 37px; font-weight: 600;line-height: 42px;letter-spacing: 1px; margin:0px;padding: 10px 0;"><strong>Welcome <?php echo ($args['campaign_name']?$args['campaign_name']:$args['display_name']);  ?></strong></p>

                            <p style="font-size: 26px;font-weight: 400;line-height: 26px;letter-spacing: 1px;background: #1e8ec3;margin:0; padding:25px 5px;color:#fff;"><strong>DIY Fundraising is coming to your school!</strong></p>

                        </td>

                    </tr>

                </table>

            </td>

        </tr>

        <tr>

            <td bgcolor="#f4f4f4" align="center" style="">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <p style="margin: 0;">Start your Event now with Fundraise for Schools.</p>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 22px 15px 0 15px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <h4 style="font-weight:normal;margin:0;">Next Step:</h4>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px 30px 15px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <p style="margin: 0;">Click on the link below to create your Fundraising Website in 1 easy step and finish signing up for the event.</p>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 22px 15px 0 15px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <h4 style="font-weight:normal;margin:0;">Email : <?php echo $args['user_email']; ?> </h4>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 22px 15px 0 15px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <h4 style="font-weight:normal;margin:0;">Password : <?php echo $args['user_pass']; ?> </h4>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 22px 15px 0 15px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <p style="margin: 0;">Please create your fundraising page. You will need the following:</p>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px 30px 15px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <p style="margin: 0;">• Your school Logo in 200px x 200px size on transparent background.</p>
                            <p style="margin: 0;">• A hero image for the school main page 300px x 300px</p>
                            <p style="margin: 0;">• Short write up about the school and what you are fundraising for (up to 100 words).</p>
                            <p style="margin: 0;">• Campaign Fundraising Goal in dollars, Example goal is $15,000 written as 15000.</p>
                            <p style="margin: 0;">• Schools Class list or lists for the school (Can create two lists if needed, example 1 for yr level and one for tutor. If only need 1 leave Class 2 blank).</p>
                            <p style="margin: 0;">• RTO Number if needed and your Crios Code if needed. If not needed leave blank.</p>


                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="left" style="padding: 0px 15px 40px 15px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

                            <p style="margin: 0;">Once you create your fundraising website, login to our Schools portal to view and manage all your schools fundraising needs in one easy place.</p>

                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="center" valign="top" style="padding-bottom:20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">

                            <p style="font-size: 26px;font-weight: 400;line-height: 26px;letter-spacing: 1px;background: #1e8ec3;margin:0; padding:30px 5px;color:#fff;"><strong>Happy Fundraising!</strong></p>

                        </td>



                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 0px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">

                           <a  href="https://colourfrenzy.onlineprojectprogress.com/schools/school-login" style="background-color: #F32085;border-radius: 40px;border: 1px solid #fff;font-size: 25px;text-transform: uppercase;color: #fff;white-space: nowrap;text-decoration: none;padding: 14px 30px;"  >Create Website</a>
                        </td>

                    </tr>

                    <tr>

                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 0px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">

                            <p style="background-color: #F32085;border-radius: 40px;border: 1px solid #fff;font-size: 25px;text-transform: uppercase;color: #fff;white-space: nowrap;text-decoration: none;padding: 14px 30px;">Create Website : <?php echo $args['url']; ?></p>
                        </td>

                    </tr>

                </table>

            </td>

        </tr>

    </table>

</div>

</body>

</html>