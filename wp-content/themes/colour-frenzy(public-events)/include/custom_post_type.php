<?php
add_action( 'init', 'create_custom_post_type' );
function create_custom_post_type(){
    register_post_type( 'events', [
        'label'               => 'Events',
        'labels'              => array(
            'name'          => 'Events',
            'singular_name' => 'Events',
            'menu_name'     => 'Events',
            'all_items'     => 'All Events',
            'add_new'       => 'Add Event',
            'add_new_item'  => 'Add new Event',
            'edit'          => 'Edit',
            'edit_item'     => 'Edit Event',
            'new_item'      => 'New Event',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => false,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
      //'rewrite'             => array( 'slug'=>'/', 'with_front'=>false ),
//        'has_archive'         => true,
        'query_var'           => true,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
//        'taxonomies'          => array( 'jobsCat' ),
        'menu_icon'           => 'dashicons-palmtree'
    ] );
}