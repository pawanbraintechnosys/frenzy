<?php
$items = 'text-video-items__items';
?>
<section class="text-video-items <?= $args['section-classes'] ?>">
    <div class="text-video-items__container container">
        <div class="text-video-items__wrapper">
            <?php while(have_rows($items)): the_row();
            $itemReverse = get_sub_field('reverse_items');
            $itemTitle = get_sub_field('title');
            $itemText = get_sub_field('text');
            $itemVideoPreview = get_sub_field('video_preview');
            $itemBrushBg = get_sub_field('brush_background');
            $itemVideoLink = get_sub_field('video_link');
            ?>
            <div class="text-video-item <?= $args['items-classes'] ?>
                <?= $itemReverse ? ' text-video-item--reverse ' : ''; ?>
                <?= $itemBrushBg ? ' text-video-item--brush ' : ''; ?>
                ">
                <div class="text-video-item__wr">
                    <a href="#" data-video-src="<?= $itemVideoLink; ?>"
                       class="text-video-item__img">
                        <?= wp_get_attachment_image($itemVideoPreview['id'], 'full') ?>
                        <button class="play-video">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon/ic-video.svg" alt="image">
                        </button>
                    </a>
                    <div class="text-video-item__bg">
                        <?= wp_get_attachment_image($itemBrushBg['id'], 'full') ?>
                    </div>
                </div>
                <div class="text-video-item__text">
                    <h2>
                        <?= $itemTitle; ?>
                    </h2>
                    <?= $itemText; ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
