<?php
/*
 * School Fundraising Detail page
 * @ver:1.0
 * $Package Colourfenzy
 */
function school_fundraising_detail_callback($atts){   
 $schoolID=get_user_meta(get_current_user_id(),'school_id',true); 
    $id = base64_decode($_GET['id']);
    echo $id;
    if (isset($id)):        
	 $school_fund_page = new Parent_model();        
	 $table = "wp8t_school_details";        
	 $where = " WHERE school_id=" . $id;        
	 $school_fund_page = $school_fund_page->get($table, $where);		
      if (!empty($school_fund_page['data'])): 
        $child = new Parent_model();            
        $tabl="wp8t_child_details";
        $wher=" WHERE school_id=".$id;
        $child=$child->get($tabl,$wher);
        if(!empty($child['data'])){
            $child=$child['data'][0];
            $user=get_userdata($child['parent_id']);
            /* Fund Detail */
            $child_fundpage=get_child_fundraising_page($child['parent_id'],$child['firstname']);
            $donation= get_all_fund_donation(@$child_fundpage['data']['id']);
            $fundpagee_id = $donation['latest_donation'][0]['fundpage_id'] ;
        }
    endif;
        if (is_user_logged_in()):
?>
            <div class="main-dashboard">
                <section class="funddash form-section for-mobile" style="padding: 3% 0;">
                    <div class="playground for-mobile">
                        <div class="child_register max_width_set">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="playground_content" style="background: #04a3d2;height: 100%;    border-radius: 15px;">
                                        <div class="user-n-ab">
                                            <?php  
                                                $logo = get_school_webpage($id)['data']; 
                                                foreach($logo as $key=>$logos){
                                                }
                                            ?>
                                            <div class="user-img" style="display: flex;">
                                                <img style="width:40%;" src="<?php echo get_field('school_logo',$id);?>">
                                                <div class="name_abcd">
                                                    <h3 style="margin-bottom:0px;font-size:20px;">Welcome To</h3>
                                                    <div>
                                                        <h3 style="margin-bottom: 0px;"><?php echo $logos['event_name'];?></h3>
                                                        <h3 style="font-size:20px;">FUNDRAISER!</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <p style="padding-top: 10px;"><?php echo get_field('event_information',$id);?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="playground_image">
                                        <!--img src="<?php //echo get_the_post_thumbnail_url($id);?>"-->
                                        <img src="<?php echo $logos['hero_image']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="child_register max_width_set" style="margin-top: 15px;border-radius: 15px;">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="user-detail donte" style="background: #7a498b;">
                                    <div class="goal_summary goal_summary_help_step">
										<div class="first_icon">
											<img style="width: 100%;" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/imageedit_7_3907638477-8.png" />
										</div>
										<div class="fundraise_graph">
											<h3 style="margin-bottom:0px;">HELP US FUNDRAISER</h3>
											<a style="margin:0 auto;" href="https://colourfrenzy.onlineprojectprogress.com/schools/find-your-school/" class="btn btn--form donate_1">Donate</a>
										</div>
										<div class="fundrise_lv">
											<img src="<?php echo home_url(); ?>/wp-content/uploads/sites/4/2021/06/header-logo.svg"/ >
										</div>
									</div>
                                    <div class="goal_summary" style="padding: 0px 15px;">
                                        <div class="fundraise_graph">
                                                <?php
                                                    $totalGoal = get_school_webpage($id)['data'];
                                                    foreach($totalGoal as $key=>$Toatal_Goals){
                                                      $A = $Toatal_Goals ['fundrasing_goal'];
                                                      $B = get_all_fund_donation($fundpagee_id)['total_fund_raised']['total_donation'];
                                                      $per = round(($B/$A) * 100);
                                                      
                                                ?>
                                                <div class="fund">
                                                <span class="fund_left lft">raised <?php echo DEFAULT_CURRENCY.number_format(get_all_fund_donation($fundpagee_id)['total_fund_raised']['total_donation']); ?></span>
                                                <span class="fund_right cent"></span> <span class="fund_right rght">goal 
                                                     <?php 
                                                        echo DEFAULT_CURRENCY.number_format($Toatal_Goals ['fundrasing_goal']);
                                                     }
                                                    ?>
                                                </span>
                                            </div>
                                              <div class="school-light-grey">
                                               <?php  if($per>0){ ?>
                                                <div class="outdder">
                                                    <div style="height:25px;max-width:<?php echo $per?>%;border-right:solid 1px #000;background-color:#f83d96;">
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <div class="outdder">
                                                    <div style="height:25px;max-width:0%;border-right:solid 1px #000;background-color:#f83d96;">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            </div>
                                            
                                          
                                            
                                        </div>
                                    </div>
                                    <div class="fundrase_profile" style="padding:10px;margin:0 15px;border-radius:15px;">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="classes">
												<?php 
                                                    $schoolID = get_user_meta(get_current_user_id(), 'school_id', true);
												    $schoolFundEndDate=get_field('incentive_end_date',$schoolID);
												    $currentDate=date('F j, Y'); 
												    $newcurrentDate = date("d-m-Y", strtotime($currentDate));
                                                    $date1 = strtotime($newcurrentDate);
                                                    $newendDate = date("d-m-Y", strtotime($schoolFundEndDate)); 
                                                    $date2 = strtotime($newendDate);
												    if($date2>=$date1):	
												?>
                                                    <h3>Fundrasing closes</h3>
                                                    <div class="d_h_m_s">
                                                        <div class="d"><span id="day" class="day"></span><p>Days</p></div>
                                                        <div class="d"><span id="hour" class="hour"></span><p>Hours</p></div>
                                                        <div class="d"><span id="minute" class="minute"></span><p>Minute</p></div>
                                                        <div class="d"><span id="second" class="second"></span><p>Second</p></div>
                                                    </div>
                                                <?php else: ?>
                                                    <h3>Fundrasing Closed</h3>
                                                    <h3><?php echo $newendDate; ?></h3>
                                                <?php endif; ?>
                                                </div>
												</div> 
												<div class="col-lg-6">
                                                    <div class="profile">
												        <h3>Share Profile</h3>  
    												    <div class="f_en_cop">   
    												        <?php echo social_share(); ?> 
    												    </div>                   
												    </div>            
												</div>            
										</div>            
									</div>
                                </div>                           
							</div>
							<div class="col-lg-4">
								<div class="help_how_to_fundraise">
									<div class="steps_of_hhtf">
										<div class="step_wise-detail">
											<h4>Step 1</h4>
											<img src="<?php echo get_template_directory_uri().'/assets/images/step_1_hhtf.png'; ?>">
										</div>
										<p>Search for the fundraising profile you wish to donate to or ask them for their unique link.</p>
									</div>
									<div class="steps_of_hhtf">
										<div class="step_wise-detail">
											<h4>Step 2</h4>
											<img src="<?php echo get_template_directory_uri().'/assets/images/step_2_hhtf.png'; ?>">
										</div>
										<p>Search for the fundraising profile you wish to donate to or ask them for their unique link.</p>
									</div>
									<div class="steps_of_hhtf">
										<div class="step_wise-detail">
											<h4>Step 3</h4>
											<img src="<?php echo get_template_directory_uri().'/assets/images/step_3_hhtf.png'; ?>">
										</div>
										<p>Search for the fundraising profile you wish to donate to or ask them for their unique link.</p>
									</div>
								</div>
							</div>
								<!--div class="col-lg-4">            
								<?php  		/*get_template_part('include/school/donate', null, array( 
													'data' => array(
                                                'school_id' => $school_fund_page['school_id'],
                                                'user_id' =>get_current_user_id(),
                                        ))
                                );*/
                                ?>
                                <div class="Badges_earnt mt-2">
                                    <div>
                                        <h3>Badges Earnt</h3>
                                    </div>
                                    <div class="">
                                        <div class="row overflow">
                                            <div class="col-lg-4 col-md-4 cemter">
                                                <p>$500 Goal</p>
                                                <img src="<?php //echo get_template_directory_uri() . '/assets/images/badge.png'; ?>">
                                                <p>Bronze 25%</p>
                                            </div>
                                            <div class="col-lg-4 col-md-4 cemter">
                                                <p>$500 Goal</p>
                                                <img src="<?php //echo get_template_directory_uri() . '/assets/images/badge.png'; ?>">
                                                <p>Silver 50%</p>
                                            </div>
                                            <div class="col-lg-4 col-md-4 cemter">
                                                <p>$500 Goal</p>
                                                <img src="<?php //echo get_template_directory_uri() . '/assets/images/badge.png'; ?>">
                                                <p>Gold 100%</p>
                                            </div>
                                        </div>
                                        <div class="row overflow bronz">
                                            <div class="col-lg-4 col-md-4 cemter">
                                                <p>$500 Goal</p>
                                                <img src="<?php //echo get_template_directory_uri() . '/assets/images/TrophyBadge-(1).png'; ?>">
                                                <p>$500 Goal</p>
                                            </div>
                                            <div class="col-lg-4 col-md-4 cemter">
                                                <p>$500 Goal</p>
                                                <img src="<?php //echo get_template_directory_uri() . '/assets/images/RoyalBadge-(1).png'; ?>">
                                                <p>$500 Goal</p>
                                            </div>
                                            <div class="col-lg-4 col-md-4 cemter">
                                                <p>$500 Goal</p>
                                                <img src="<?php //echo get_template_directory_uri() . '/assets/images/DiamondBadge-(1).png'; ?>">
                                                <p>$500 Goal</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div-->
                        </div>
                        <section class="search-section" style="background-color: #8DB833;border-radius:15px;margin-top:15px;padding: 3% 0;">
						    <div class="container">
						        <div class="search-section__title" style="margin: 0 auto 0.0rem auto;">
						            <h2>FIND A PROFILE AND Donate</h2>
						            <p><?php echo get_field('how_to_donate_editable_text'); ?></p>
						        </div>
						        <div class="state_image"  style="margin: 0 auto;text-align: center;"  >
						            <style>text {fill: #fff;}</style>
						        </div>
						            <h3 style="text-align: center;margin-bottom: 0px;font-size: 17px;">Search For Profile First Name and class</h3>
                                    <?php $search = $_POST['search'];
                                        $table_name ="SELECT *  FROM wp8t_child_details where firstname like '%$search%'";
                                     ?>
						        <form action="" method="post" class="search-section__single-input-form single-input-form">
						            <input type="text" id="search" name="search" placeholder="">
						            <button class="btn btn--form" type="submit">Search</button>
						        </form>
                                
						    </div>
						</section>
                        <section class="">
                            <div class="latest_leader donate_board">
                                <div class="container p0-0">
                                    <div class="row" style="margin-bottom: -30px;">
                                        <div class="col-lg-6">
                                            <?php if(!empty($donation['latest_donation'])): ?>
                                            <h3 class="heading">RECENT SCHOOL DONATIONS</h3>
                                            <?php
                                                
                                                    foreach ($donation['latest_donation'] as $key=>$top_donation):
                                                        if($key==5){
                                                            break;
                                                        }    
                                            ?>
                                            <div class="">
                                                <div class="card">
                                                    <div class="card-title">
                                                        <p><?php echo DEFAULT_CURRENCY.number_format($top_donation['amount']) ; ?></p>
                                                    </div>
                                                    <div class="line"></div>
                                                    <div class="card-description">
                                                        <p><?php echo $top_donation['shipping_firstname'].' '.$top_donation['shipping_lastname'];  ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                            <h3 class="heading">RECENT SCHOOL DONATIONS</h3>
                                        <div class="">
                                            <div class="card">
                                                <div class="Blankcard-description">
                                                   <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="card">
                                                <div class="Blankcard-description">
                                                   <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="card">
                                                <div class="Blankcard-description">
                                                   <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="card">
                                                <div class="Blankcard-description">
                                                   <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="card">
                                                <div class="Blankcard-description">
                                                   <p style="font-size: 20px;padding: 7px;">Awaiting Donation - Donate Now to Help</p>
                                                </div>
                                            </div>
                                        </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php 
                                                $leader_bord = $donation['top_donation'];
                                                if(!empty($leader_bord)){
                                             ?>
                                            <h3 class="heading">SCHOOL LEADER BOARD</h3>
                                             <?php      
                                                foreach($leader_bord as $key=>$leader_bords){
                                                   //pr($leader_bords);
                                                   // if($leader_bords['type'] == 'SCHOOL'){
                                                    if($key==5){
                                                        break;
                                                    }
                                                    
                                            ?>
                                            <div class="leader">
                                                <div class="card">
                                                    <div class="card-title"><img
                                                        src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/imageedit_7_3907638477-8.png">
                                                    </div>
                                                    <div class="card-description progress-report">
                                                        <div class="name">
                                                            <p><?php echo $leader_bords['shipping_firstname'].' '.$leader_bords['shipping_lastname'];  ?></p>
                                                            <div class="school-light-grey">
                                                                <div class="school-red" style="max-width:<?php echo (round($leader_bords['amount'])*100)/(round($leader_bords['amount'])); ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>
                                                        <?php 
                                                            if(!empty($leader_bords['amount'])){
                                                                echo DEFAULT_CURRENCY.number_format($leader_bords['amount']);
                                                            }else{
                                                                echo DEFAULT_CURRENCY."0.00";
                                                            }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                         <?php }}else{ ?>
                                            <h3 class="heading">SCHOOL LEADER BOARD</h3>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="leader">
                    <div class="card" style="padding-top: 42px;">
                        <div class="card-description progress-report">
                            <div class="name">
                                <p></p>
                                <div class="school-light-grey">
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                                            <?php   }  ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                          </div>
                </section>
				<section class="search-section d-none" style="background-color: #8DB833;">
				    <div class="container">
				        <div class="search-section__title">
				            <h2>How To Donate</h2>
				              <p><?php //echo get_field('how_to_donate_editable_text'); ?></p>
				        </div>
				        <div class="state_image"  style="margin: 0 auto;text-align: center;"  >
				            <style>text {fill: #fff;}</style>
				        </div>
				            <h3>Search For Profile First Name and class</h3>

				        <form id="search-form" class="search-section__single-input-form single-input-form">
				            <input id="autoComplete" placeholder="">
				            <button class="btn btn--form" type="submit">Search</button>
				        </form>
				        <div class="div" id="autoCompleteList"></div>
				        <div class="search-section__logo-wr hidden" id="school-info">
				            <div class="search-section__logo" id="school-logo">
				                <img id="school_logo" src="" alt="image">
				            </div>
				        </div>
				      
				    </div>

				</section>
				<section class="one-row-slider one-row-slider--yellow-brush d-none">
					<div class="container">
						<h2>Some of our great prizes you can win for fundraising</h2>
						<div class="one-row-slider__slider">
							<button class="slider-arrow slider-arrow--pink slider-arrow--prev one-row-slider__slider-arrow one-row-slider__slider-arrow-prev swiper-button-disabled" disabled="">

							</button>
							<div class="one-row-slider__slider-container swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
								<div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
									<div class="one-row-slider__item one-row-slider-item swiper-slide swiper-slide-active" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616.png" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616.png" class="attachment-full size-full lazy loaded" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px" sizes="(max-width: 500px) 100vw, 500px" srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190451.616-150x150.png 150w" data-was-processed="true">                        
										</div>
										<h3>Ride on ATV 12v</h3>
									</div>
									<div class="one-row-slider__item one-row-slider-item swiper-slide swiper-slide-next" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="289" height="289" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/06/img-2.png" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/06/img-2.png" class="attachment-full size-full lazy loaded" alt="image" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/06/img-2.png 289w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/06/img-2-150x150.png 150w" data-sizes="(max-width: 289px) 100vw, 289px" sizes="(max-width: 289px) 100vw, 289px" srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/06/img-2.png 289w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/06/img-2-150x150.png 150w" data-was-processed="true">                        
										</div>
										<h3>Instax Camera</h3>
									</div>
									<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159.png" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159.png" class="attachment-full size-full lazy loaded" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px" sizes="(max-width: 500px) 100vw, 500px" srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185820.159-150x150.png 150w" data-was-processed="true">                        
										</div>
										<h3>Smart Watch</h3>
									</div>
										<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png" class="attachment-full size-full lazy loaded" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px" sizes="(max-width: 500px) 100vw, 500px" srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190520.725-150x150.png 150w" data-was-processed="true">                        
										</div>
										<h3>NRL Football</h3>
									</div>
									<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20500%20500'%3E%3C/svg%3E" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190419.294.png" class="attachment-full size-full lazy" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190419.294.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190419.294-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190419.294-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px">                        
										</div>
										<h3>Bubbadoo Large Doll House</h3>
									</div>
									<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20500%20500'%3E%3C/svg%3E" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668.png" class="attachment-full size-full lazy" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190549.668-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px">                        </div>
										<h3>
											Rainbow Salon                        </h3>
									</div>
														<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20500%20500'%3E%3C/svg%3E" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190347.489.png" class="attachment-full size-full lazy" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190347.489.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190347.489-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190347.489-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px">                        </div>
										<h3>
											Call me Chole Doll                        </h3>
									</div>
														<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20500%20500'%3E%3C/svg%3E" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484.png" class="attachment-full size-full lazy" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T190238.484-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px">                        </div>
										<h3>
											Electric Scooter                        </h3>
									</div>
														<div class="one-row-slider__item one-row-slider-item swiper-slide" style="width: 309.75px; margin-right: 19px;">
										<div class="one-row-slider-item__img">
											<img width="500" height="500" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20500%20500'%3E%3C/svg%3E" data-src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185725.219.png" class="attachment-full size-full lazy" alt="" loading="lazy" data-srcset="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185725.219.png 500w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185725.219-300x300.png 300w, https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-04T185725.219-150x150.png 150w" data-sizes="(max-width: 500px) 100vw, 500px">                        </div>
										<h3>
											Go Pro                        </h3>
									</div>
								</div>
							</div>
							<button class="slider-arrow slider-arrow--pink slider-arrow--next one-row-slider__slider-arrow one-row-slider__slider-arrow-next">

							</button>
						</div>
					</div>
				</section>
				<section class="search-section" style="background-color: #8DB833;">
					<div class="latest_leader">
						<div class="container">
							<div class="row">
								<div class="col-lg-6">
									<img src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2022/03/img-2-1.png"
                                    >
								</div>
								<div class="col-lg-6">
									<div class="make_a_differ_content" style="margin-left: 30px;">
										<h3 class="heading">COLOR FRENZY SCHOOL FUNDRAISER</h3>
										<p>Search For Profile First Name and class</p>
										<p>A Colour Frenzy school fundraiser is an event that students love and look forward to. It encourages both physical activity while building school community and camaraderie amongst students and staff.</p>
										<p>A Colour Frenzy school fundraiser is an event that students love and look forward to. It encourages both physical activity while building school community and camaraderie amongst students and staff.</p>
										<h4>Donate and make a difference</h4>
										<div class="button_make_a_diffre">
											<a href="https://colourfrenzy.onlineprojectprogress.com/schools/find-your-school/" class="btn btn--form donate_1">Donate</a>
											<a href="https://colourfrenzy.onlineprojectprogress.com/schools/find-your-school/" class="btn btn--form">Find a profile</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
                <?php
                 	if (!empty($school_fund_page['data'])){
                        $schoolID = get_user_meta(get_current_user_id(), 'school_id', true);					
                        $schoolFundEndDate=get_field('incentive_end_date',$schoolID);					
                        $res = date('M d, Y  h:i:s', strtotime($schoolFundEndDate));					
                        //echo $schoolID;					
                        //echo $schoolFundEndDate;					
                        //echo $res;			?>                
                        <script>
                    function childDetailTimer() {
                    var x = setInterval(function () {
                    var countDownDate = new Date("<?php echo $res; ?>").getTime();                          
                    // var countDownDate = new Date("Jan 5, 2022 15:37:25").getTime();                            
                    var now = new Date().getTime();                            
                    var distance = countDownDate - now;                            
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));                            
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));                           
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));                            
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);                            
                    document.querySelector("#day").innerHTML = days;							
                    document.getElementById("day").innerHTML = days;                            
                    document.querySelector("#hour").innerHTML = hours;                           
                    document.querySelector("#minute").innerHTML = minutes;                            
                    document.querySelector("#second").innerHTML = seconds;                           
                    if (distance < 0) {							
                    document.querySelector("#day").innerHTML = '00';                            
                    document.querySelector("#hour").innerHTML = '00';                            
                    document.querySelector("#minute").innerHTML = '00';                            
                    document.querySelector("#second").innerHTML = '00';                           
                    clearInterval(x);                            }                        
                     }, 1000);                    
                 }                    
                 childDetailTimer();    


                 </script>				
             <?php }?>
            </div>
				<?php else: ?>
            <?php echo get_template_part('template-parts/unauthorized'); ?>
        <?php endif; ?>
    <?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}

add_shortcode('school_fundraising_detail', 'school_fundraising_detail_callback');
?>


