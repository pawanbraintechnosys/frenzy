<?php
$title = get_field('three-col-imgs__title');
$text = get_field('three-col-imgs__text');
$items = 'three-col-imgs__items';
?>
<section class="three-col-imgs <?= $args['section-classes']; ?>">
    <div class="container">
        <div class="three-col-imgs__head">
            <h2>
                <?= $title; ?>
            </h2>
            <?= $text; ?>
        </div>
        <div class="three-col-imgs__inner">
            <?php while(have_rows($items)): the_row();
            $itemImg = get_sub_field('image');
            ?>
            <div class="three-col-imgs__img">
                <?= wp_get_attachment_image($itemImg['id'], 'full') ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
