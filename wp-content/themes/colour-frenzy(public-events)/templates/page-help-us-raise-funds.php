<?php
//Template Name: Help Us Raise Funds
get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => 'hero-section--small hero-section--content-center'));

get_template_part('template-parts/text-img-with-progress-bar');

get_template_part('template-parts/text-img-items-section', null, array('bg-title' => true));

get_template_part('template-parts/step-with-description');

get_template_part('template-parts/video-section-with-two-btns');

get_template_part('template-parts/text-img-items-with-subscribe', null, array('bg-title' => true));

get_template_part('template-parts/logo-section');

get_footer();
?>