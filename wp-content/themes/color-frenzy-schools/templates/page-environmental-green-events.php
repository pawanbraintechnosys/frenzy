<?php

/* Template Name: Environmental green events page */


get_header();

get_template_part('template-parts/hero-section', null, array('hero-classes' => ' hero-section--small hero-section--content-center '));

get_template_part('template-parts/text-image-section', null,
    array(
        'text-classes' => ' check-mark-list ',
    )
);

get_footer();

