<?php
/*
Edit Fundpage
@Package Package colourfrenzy
@version 1.0.0
*/
?>
<?php

function manage_school_student_fundraising_callback(){
        $child_id=@base64_decode($_GET['compaign-id']);
        if (is_user_logged_in() && !empty($child_id)):
            $childDetails=GetchildAndFundriaseDetails($child_id);
        if (!empty($childDetails)) {
            $childDetail = $childDetails;
        }
?>
        <div class="school_login school-login-form" style="Padding: 40px 0;background: #1b7eaa;">
            <div class="container">
                <h2 style="text-align: center;">Manage <?=  $childDetail['firstname'].' '.@$childDetail['lastname']  ?> Fundpage</h2>
                <div class="row myform">
                    <div class="col-9 col-md-12 col-lg-9">
                        <div class="card">
                            <div class="card-body">
                                <p class=""></p>
                                <form class="form-post" id="create-fundraiser" method="POST" action="">
                                    <input type="hidden" name="action" value="create_fundraising_page">
                                    <input type="hidden" name="fundpage_id" value="<?= $childDetail['id'] ?>">
                                    <input type="hidden" name="child_id" value="<?= @$_GET['compaign-id'] ?>">
                                    <input type="hidden" name="nounce" value="<?php echo wp_create_nonce( 'update-fundraising-page' ); ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Title</label>
                                        <input id="title" Placeholder="Fundraising Page title" name="title" type="text" value="<?= $childDetail['title'] ?>" class="form-control " required="" autocomplete="off" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="email">Description</label>
                                        <?php
                                        $content   = $childDetail['description'];
                                        $editor_id = 'description';
                                        wp_editor( $content, $editor_id );
                                        ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="email">Start Date</label>
                                                <input id="event_start_date datepicker" Placeholder="Fundraising Start Date" value="<?= $childDetail['start-date'] ?>" name="start-date" type="text" class="form-control datepicker" required="" autocomplete="off" autofocus="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="email">End Date</label>
                                                <input id="event_end_date" Placeholder="Fundraising End Date" name="end-date" value="<?= $childDetail['end-date'] ?>" type="text" class="form-control " required="" autocomplete="off" autofocus="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="email">Minimum Amount</label>
                                                <input id="min-amount" Placeholder="Enter Minimum Amount" name="min-amount" type="number" class="form-control " value="<?= $childDetail['min-amount'] ?>" required="" autocomplete="off" autofocus="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="email">Maximun Amount</label>
                                                <input id="max-amount" Placeholder="Enter Maximum Amount" name="max-amount" type="number" class="form-control " value="<?= $childDetail['max-amount'] ?>" required="" autocomplete="off" autofocus="">
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-6">

                                            <div class="form-group">

                                                <label class="control-label" for="email">Fundraising Goal</label>

                                                <input id="goal-amount" Placeholder="Enter Goal Amount" name="goal-amount" type="number" class="form-control " value="<?= $childDetail['goal-amount'] ?>" required="" autocomplete="off" autofocus="">

                                            </div>

                                        </div>

                                        <div class="col-lg-6">

                                            <div class="form-group">

                                                <label class="control-label" for="predefined-amount">Predefined Pledge Amount</label>

                                                <input id="predefined-amount" Placeholder="Enter Predefined Amount" name="predefined-amount" type="text" value="<?= $childDetail['predefined-amount'] ?>" class="form-control " required="" autocomplete="off" autofocus="">

                                                <p style="font-size: 9px;line-height: 11px;font-style: italic;">Predefined amount allow you to place the amount in donate box by click, price should separated by comma (,), example: 10,20,30,40</p>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <input type="submit" class="btn btn-lg btn-block w-100" value="Submit"></input>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
<?php else: ?>
        <?php echo get_template_part('template-parts/unauthorized'); ?>
    <?php endif; ?>
    <?php
}
add_shortcode('manage_school_student_fundraising', 'manage_school_student_fundraising_callback');
?>

