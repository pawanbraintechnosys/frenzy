<?php
/*
 * Fundraiser Ajax
 * Package ColorFenzy
 * Version @1.0.0
 */
add_action('wp_ajax_nopriv_create_fundraising_page', 'create_fundraising_page_call');
add_action('wp_ajax_create_fundraising_page', 'create_fundraising_page_call');
function create_fundraising_page_call()
{
    $html = '';
    $errors = new WP_Error();
    $response = array();
    $flag = false;
    global $wpdb;
    $user_id=get_current_user_id();
    $fundpage_id=@$_POST['fundpage_id'];
    $child_id=base64_decode(@$_POST['child_id']);
    $fundRaisingData=array(
        'user_id'=>$user_id,
        'child_id'=>($child_id)?$child_id:' ',
        'display_name'=>(@$_POST['display_name'])?$_POST['display_name']:' ',
        'title'=>@$_POST['title'],
        'description'=>@$_POST['description'],
        'start-date'=>@$_POST['start-date'],
        'end-date'=>@$_POST['end-date'],
        'min-amount'=>@$_POST['min-amount'],
        'max-amount'=>@$_POST['max-amount'],
        'goal-amount'=>@$_POST['goal-amount'],
        'predefined-amount'=>(@$_POST['predefined-amount'])?$_POST['predefined-amount']:' ',
        'status'=>'1',
        'accepted_at'=>current_time('mysql'),
        'modified_at'=>current_time('mysql'),
    );
    if(isset($_POST)){
        if(isset($fundpage_id)){
            /* Update Fundpage */
            $where=array('id'=>$fundpage_id);
            $wpdb->update('fundraise_posted_campiagn',
                $fundRaisingData,
                $where
            );
            $flag=true;
            $html .="Fund page Updated Successfully.";
        }else{
            $flag=true;
            $wpdb->insert('fundraise_posted_campiagn',
                $fundRaisingData
            );
            $html .="Fund page created Successfully.";
        }
        if($flag){
            $response['url'] = home_url().'/student-fundraising-page/?dtab=edit-fundpage&child-id='.base64_encode($child_id);
        }else{
            $html .="Unable to create fund Page.";
        }
    }
    if ($flag) {
        $response['status'] = "success";
    } else {
        $response['status'] = "failed";
    }
    $error_messages = $errors->get_error_messages();
    if (count($error_messages) > 0) {
        foreach ($error_messages as $k => $message) {
            $html .= $message;
        }
    }
    $response['message'] = $html;
    wp_send_json($response);
    die;
}