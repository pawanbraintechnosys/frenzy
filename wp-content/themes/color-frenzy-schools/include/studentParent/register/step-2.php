<?php
$schoolID = base64_decode(@$_GET['school-id']);
//echo $schoolID;
 $fundraisingPageDetail = get_school_webpage($schoolID);
        if (!empty($fundraisingPageDetail['data'])) {
            $fundraisingPageDetail = $fundraisingPageDetail['data'][0];
        }
        $clasOne = $fundraisingPageDetail['class_detail'];
        $clasTwo = $fundraisingPageDetail['class_detail_2']; 
        $allowedSchoolClass_one = explode(',', $clasOne) ;
        $allowedSchoolClass_Two = explode(',', $clasTwo) ;
        $Class_details_One = str_replace(',', '', $allowedSchoolClass_one);
        $Class_details_Two = str_replace(',', '', $allowedSchoolClass_Two);
        //pr($Class_details_Two);
?>
<div class="Children steps-form d-none frm-step frm-step2" id="children frmSignupStep2">
    <div class="child_1">
        <input type="hidden" id="child-register-price-1" value="<?php echo get_post_meta(base64_decode(@$_GET['school-id']),'child_entry_fee',true); ?>">
        <div class="form-group group first">
            <div class="detail">
                <label for="exampleInputEmail1">Child 1 <small>(Required)</small></label>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <input type="text" class="netEmp form-control first_name"  id="FirstName-1" name="FirstName-1" aria-describedby="emailHelp" required >
                    <label for="exampleInputEmail1">First</label>
                </div>
                <div class="col-lg-6">
                    <input type="text" class="form-control" id="lastName-1" name="lastName-1" aria-describedby="emailHelp">
                    <label for="exampleInputEmail1">Last</label>
                </div>
                <div class="col-lg-6">
                    <div class="form-group group">
                        <label for="exampleInputEmail1">Child 1 Class 1</label>
                        <div class="for-mobile">
                            <select id="childClass-1" required Placeholder="Junior or Senior" name="childClass-1"
                                    class="form-control netEmp" >
                                <option value="">Choose</option>
                                <?php
                                    foreach($Class_details_One as $key=>$val){
                                ?>
                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php //$childClass=child_class_detail();
                    //if(!empty($childClass)){
                 ?>
                <div class="col-lg-6">
                    <div class="form-group group">
                        <label for="exampleInputEmail1">Child 1 Class 2</label>
                        <div class="for-mobile">
                            <select id="childsecondClasss-1" Placeholder="Junior or Senior" name="childsecondClasss-1"
                                    class="form-control">
                                <option value="">Choose</option>
                                <?php
                                foreach($Class_details_Two as $key=>$val){
                                ?>
                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                               <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> <?php //} ?>
            </div>
        </div>
        <div class="form-group group">
            <div class="row">
                <div class="col-lg-6 check-options">
                    <label for="exampleInputEmail1" style="display: inline-flex;">Nickname Required?</label>
                    <div class="size-ch" style="background: #fff;"><a id="myBtn" href="javascript:void(0);">i</a></div>
                    <div class="form-check checkbox-flex nickname-check-1">
                        <input class="form-check-input nickname_checkbox-1" name="nickname_checkbox-1" type="radio" value="0" id="flexCheckDefault nickname_checkbox" checked>
                        <label class="form-check-label check-label" for="flexCheckDefault">
                            No
                        </label>
                        <input class="form-check-input nickname_checkbox-1" type="radio" value="1" name="nickname_checkbox-1" id="flexCheckChecked nickname_checkbox" >
                        <label class="form-check-label check-label" for="flexCheckChecked">
                            Yes
                        </label>
                    </div>
                </div>
                <div class="col-lg-6 nickname_field-1" style="display: none;">
                    <label for="exampleInputEmail1">Profile Nickname Only use when required for Media Release (Required)</label>
                    <input type="text" class="form-control nick" id="nickName-1" name="nickName-1" aria-describedby="emailHelp">
                </div>
            </div>
        </div>
        <div class="liner"></div>
        <div class="child_merchandise_1">
            <div class="form-group group">
                <h3 class="heading">Optional Extras to wear at your school's Colour Frenzy</h3>
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <div class="sizing">
                            <div class="count">
                                <p>ColorFrenzy TShirt <span data-type="CLOTH" data-price="0" data-total_price="0" class="d-block price">$0</span></p>
                                <p style="padding: 7px 0px;font-size: 10px;text-align: start;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
                                <div class="size-chart">
                                    <p>Size chart (Required)</p>
                                    <div class="size-ch"><a id="sizechartBtn" href="javascript:void(0);" required>i</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 increase">
                        <div class="shirt">
                            <img class="wooco-img" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/Untitled-design-2021-07-12T172412.753.png">																
                        </div>
                        <div class="size-ch popup"><a id="TshirtBtn" href="javascript:void(0);">i</a>
                        </div>
                        <div class="up-down">
                            <p>
                                <img src="<?php echo home_url().'/wp-content/uploads/sites/4/2021/12/minus.png';  ?>" id="minus1" width="40" height="20" class="minus" />
                                <input id="clothqty-1" type="text" value="0" min="0" max="1" class="qty" readonly name="cloth-1" />
                                <img id="add1" src="<?php echo home_url().'/wp-content/uploads/sites/4/2021/12/plus.png';  ?>" width="40" height="20" class="add" />
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label for="exampleInputEmail1">Child  1</label>
                        <div class="">
                            <select id="childSize-1" Placeholder="Junior or Senior" name="childSize-1"
                                    class="form-control">
                                    <option value="">Choose</option>
                                    <?php
                                        $args = array(  
                                                'post_type' => 'tshirt-size',
                                                'post_status' => 'publish',
                                                'orderby' => 'date',
                                                'order' => 'ASC', 
                                        );
                                        $loop = new WP_Query( $args ); 
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                    ?>
                                <option value="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></option>
                            <?php endwhile; ?></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group group">
                <div class="for-back-color">
                    <div class="row align-items-center">
                        <div class="col-lg-4">
                            <div class="sizing">
                                <div class="count">
                                    <p>Bucket Mat <span data-type="HAT" data-price="0" data-total_price="0" class="d-block price">$0</span></p>
                                    <p style="font-size: 10px;text-align: start;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 increase">
                            <div class="hat">
                                <img class="wooco-img" src="https://colourfrenzy.onlineprojectprogress.com/schools/wp-content/uploads/sites/4/2021/07/hat-cf.png">																
                            </div>
<!--							<div class="popup">-->
									<div class="size-ch popup"><a id="hatBtn" href="javascript:void(0);">i</a>
									</div>
<!--                        	</div>-->
                            <div class="up-down">
                                <p>
                                    <img src="<?php echo home_url().'/wp-content/uploads/sites/4/2021/12/minus.png';  ?>" id="minus1" width="40" height="20" class="minus" />
                                    <input id="hatqty-1" type="text" value="0" min="0" max="1" class="qty" readonly name="hat-1"/>
                                    <img id="add1" src="<?php echo home_url().'/wp-content/uploads/sites/4/2021/12/plus.png';  ?>" width="40" height="20" class="add" />
                                </p>
<!--                                <a href="#" class="decrease_num"><label for="exampleInputEmail1" class="arrow_descrease">-</label></a>-->
<!--                                <input readonly class="quantity" id="id_form-0-quantity hat-1" min="0" name="hat-1" value="1">-->
<!--                                <a href="#" class="increase_num"><label for="exampleInputEmail1" class="arrow_increase">+</label></a>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-none">
                    <div class="col-lg-6">
                        <label for="exampleInputEmail1">Child  1</label>
                        <div class="">
                            <select id="hatSize-1" Placeholder="Junior or Senior" name="hatSize-1"
                                    class="form-control">
                                <option>Choose</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="liner"></div>
    </div>
        <div class="total">
        <input type="hidden" name="total_price" id="total_price" value="85" />
        <label for="exampleInputPassword1" id="total_price_detail">Total : $00.00</label>
    </div>
    <div class="next margi">
        <a class="btn_border float-left btnAddSchoolPrev btn btn-primary" href="javascript:;" data-current="step2" data-prev="step1"><?php _e("back", "colorfenzy"); ?></a>
        <a class="btn_border float-left add_more btn btn-primary" id="button_disable" href="javascript:;" ><?php _e("Add Another Child", "colorfenzy"); ?></a>
        <button data-current="step2" data-next="step3" class="btn_border  btn btn-primary float-right sr-next-btn btnAddSchoolNext">Next</button>
    </div>
</div>
