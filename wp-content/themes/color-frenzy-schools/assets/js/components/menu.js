export function initMenu() {
  const $header = document.getElementById('header')
  const toggleElements = [
    '.header__btn',
    '.header__fader',
    '.header__burger'
  ]

  toggleElements.forEach(selector => {
    document.querySelectorAll(selector).forEach(el => {
      if (!el) return
      el.addEventListener('click', () => {
        $header.classList.toggle('header--open-menu')
      })
    })
  })
}

