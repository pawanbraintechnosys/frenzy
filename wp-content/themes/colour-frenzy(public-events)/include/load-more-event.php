<?php
function ajax_event_scripts() {
        $wp_query = new WP_QUERY(
            array(
                'post_type' => 'events',
                'posts_per_page' => 4,
                'post_status' => 'publish'
            )
        );

        wp_register_script( 'ajax-event', get_template_directory_uri() . '/js/load-more-event-ajax.js', array('jquery') );

        wp_localize_script( 'ajax-event', 'event_params', array(
            'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
            'posts' => json_encode( $wp_query->query_vars ),
            'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages,
        ) );
        wp_enqueue_script( 'ajax-event' );
}

add_action( 'wp_enqueue_scripts', 'ajax_event_scripts' );

//Event Load More
function event_loadmore(){
    $params = json_decode( stripslashes( $_POST['query'] ), true );
    $params['paged'] = $_POST['page'] + 1;
    query_posts($params);


    while(have_posts()): the_post();
        get_template_part('template-parts/items/event-location-item');
    endwhile;

    die;
}
add_action('wp_ajax_event_loadmore', 'event_loadmore');
add_action('wp_ajax_nopriv_event_loadmore', 'event_loadmore');